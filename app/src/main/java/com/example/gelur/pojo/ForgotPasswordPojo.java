package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ForgotPasswordPojo implements Serializable {
    @SerializedName("email")
    String email;

    @SerializedName("forgot_password_code")
    String forgot_password_code;

    public ForgotPasswordPojo() {

    }

    public ForgotPasswordPojo(String email, String forgot_password_code) {
        this.email = email;
        this.forgot_password_code = forgot_password_code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForgot_password_code() {
        return forgot_password_code;
    }

    public void setForgot_password_code(String forgot_password_code) {
        this.forgot_password_code = forgot_password_code;
    }
}
