package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class CollegePojo {
    @SerializedName("email")
    String email;

    @SerializedName("college_name")
    String college_name;

    @SerializedName("faculty_major")
    String faculty_major;

    @SerializedName("graduated")
    String graduated;

    @SerializedName("since")
    String since;

    @SerializedName("description")
    String description;

    public CollegePojo() {

    }

    public CollegePojo(String email, String college_name, String graduated, String description) {
        this.email = email;
        this.college_name = college_name;
        this.graduated = graduated;
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCollege_name() {
        return college_name;
    }

    public void setCollege_name(String college_name) {
        this.college_name = college_name;
    }

    public String getFaculty_major() {
        return faculty_major;
    }

    public void setFaculty_major(String faculty_major) {
        this.faculty_major = faculty_major;
    }

    public String getGraduated() {
        return graduated;
    }

    public void setGraduated(String graduated) {
        this.graduated = graduated;
    }

    public String getSince() {
        return since;
    }

    public void setSince(String since) {
        this.since = since;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
