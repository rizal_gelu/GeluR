package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class PhonePojo {

    @SerializedName("Id") String id;

    @SerializedName("email")
    String email;

    @SerializedName("phone")
    String phone;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_updated")
    long date_updated;

    public PhonePojo() {

    }

    public PhonePojo(String email, String phone, long date_created, long date_updated) {
        this.email = email;
        this.phone = phone;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
