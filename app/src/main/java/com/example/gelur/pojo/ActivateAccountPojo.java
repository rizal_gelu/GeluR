package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class ActivateAccountPojo {
    @SerializedName("email") String email;

    @SerializedName("activation_code") String activation_code;


    public ActivateAccountPojo() {

    }

    public ActivateAccountPojo(String email, String activation_code) {
        this.email = email;
        this.activation_code = activation_code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActivation_code() {
        return activation_code;
    }

    public void setActivation_code(String activation_code) {
        this.activation_code = activation_code;
    }
}
