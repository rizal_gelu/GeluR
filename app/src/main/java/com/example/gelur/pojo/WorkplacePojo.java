package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class WorkplacePojo {

    @SerializedName("email")
    String email;

    @SerializedName("job_title")
    String job_title;

    @SerializedName("name_company")
    String name_company;

    @SerializedName("location")
    String location;

    @SerializedName("description")
    String description;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_updated")
    long date_updated;

    public WorkplacePojo() {

    }

    public WorkplacePojo(String email, String name_company, String job_title, String location, long date_created, long date_updated) {
        this.email = email;
        this.name_company = name_company;
        this.job_title = job_title;
        this.location = location;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getName_company() {
        return name_company;
    }

    public void setName_company(String name_company) {
        this.name_company = name_company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
