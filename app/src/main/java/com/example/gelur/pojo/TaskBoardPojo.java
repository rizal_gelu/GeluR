package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TaskBoardPojo {

    @SerializedName("Id") String id;

    @SerializedName("email") String email;

    @SerializedName("title_task") String title_task;

    @SerializedName("company_id") String company_id;

    @SerializedName("description_task") String description_task;

    @SerializedName("deadline") String deadline;

    @SerializedName("create_by") String create_by;

    @SerializedName("date_created") long date_created;

    @SerializedName("date_updated") long date_updated;
//
//    @SerializedName("data")
//    private List<TimelinePojo> timeline = new ArrayList<>();

    public TaskBoardPojo() {

    }

    public TaskBoardPojo(String id, String email, String title_task, String company_id, String description_task, String deadline, String create_by, long date_created, long date_updated) {
        this.id = id;
        this.email = email;
        this.title_task = title_task;
        this.company_id = company_id;
        this.description_task = description_task;
        this.deadline = deadline;
        this.create_by = create_by;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle_task() {
        return title_task;
    }

    public void setTitle_task(String title_task) {
        this.title_task = title_task;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getDescription_task() {
        return description_task;
    }

    public void setDescription_task(String description_task) {
        this.description_task = description_task;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
