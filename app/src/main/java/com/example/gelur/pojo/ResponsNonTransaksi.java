package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ResponsNonTransaksi {

    @SerializedName("resp") String resp;

    @SerializedName("status_code") String status_code;

    @SerializedName("msg") String msg;

    public ResponsNonTransaksi(String resp, String status_code, String msg) {
        this.resp = resp;
        this.status_code = status_code;
//        this.msg = msg;
    }

    public String getResp() {
        return resp;
    }

    public void setResp(String resp) {
        this.resp = resp;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

//    @SerializedName("msg")
//    private List<ResponseNonTransaksiMsgPojo> msg_user = new ArrayList<>();
//
//    public List<ResponseNonTransaksiMsgPojo> getMsg_user() {
//        return msg_user;
//    }
//
//    public void setMsg_user(List<ResponseNonTransaksiMsgPojo> msg_user) {
//        this.msg_user = msg_user;
//    }

//    @SerializedName("timeline_user")
//    private List<TimelinePojo> timeline_user = new ArrayList<>();
}
