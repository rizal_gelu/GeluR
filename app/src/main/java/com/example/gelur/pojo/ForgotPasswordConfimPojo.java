package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class ForgotPasswordConfimPojo {

    @SerializedName("email")
    String email;

    @SerializedName("forgot_password_code")
    String forgot_password_code;

    @SerializedName("new_password")
    String new_password;

    public ForgotPasswordConfimPojo() {

    }

    public ForgotPasswordConfimPojo(String email, String forgot_password_code, String new_password) {
        this.email = email;
        this.forgot_password_code = forgot_password_code;
        this.new_password = new_password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getForgot_password_code() {
        return forgot_password_code;
    }

    public void setForgot_password_code(String forgot_password_code) {
        this.forgot_password_code = forgot_password_code;
    }

    public String getNew_password() {
        return new_password;
    }

    public void setNew_password(String new_password) {
        this.new_password = new_password;
    }
}
