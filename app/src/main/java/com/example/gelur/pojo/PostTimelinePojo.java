package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PostTimelinePojo implements Serializable {
    @SerializedName("email")
    String email;

    @SerializedName("description")
    String description;

    @SerializedName("file")
    String file;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_update")
    long date_updated;

    public PostTimelinePojo() {

    }

    public PostTimelinePojo(String email, String description, String file, long date_created, long date_updated) {
        this.email = email;
        this.description = description;
        this.file = file;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
