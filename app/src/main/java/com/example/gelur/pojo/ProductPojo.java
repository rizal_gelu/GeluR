package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class ProductPojo {

    @SerializedName("id")
    String id;

    @SerializedName("name_product")
    String name_product;

    @SerializedName("description_product")
    String description_product;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_updated")
    long date_updated;

    public ProductPojo() {

    }

    public ProductPojo(String id, String name_product, String description_product, long date_created, long date_updated) {
        this.id = id;
        this.name_product = name_product;
        this.description_product = description_product;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName_product() {
        return name_product;
    }

    public void setName_product(String name_product) {
        this.name_product = name_product;
    }

    public String getDescription_product() {
        return description_product;
    }

    public void setDescription_product(String description_product) {
        this.description_product = description_product;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
