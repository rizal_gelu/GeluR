package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UsersPojo implements Serializable {

    @SerializedName("email")
    String email;

    @SerializedName("password")
    String password;

    @SerializedName("first_name")
    String first_name;

    @SerializedName("last_name")
    String last_name;

    @SerializedName("avatar")
    String avatar;

    @SerializedName("status")
    int status;

    public UsersPojo() {

    }

    public UsersPojo(String email, String password, String first_name, String last_name, String avatar, int status) {
        this.email      = email;
        this.password   = password;
        this.first_name = first_name;
        this.last_name  = last_name;
        this.avatar     = avatar;
        this.status     = status;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
