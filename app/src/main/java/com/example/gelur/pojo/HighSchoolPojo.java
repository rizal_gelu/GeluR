package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class HighSchoolPojo implements Serializable {
    @SerializedName("email") String email;

    @SerializedName("high_school_name") String high_school_name;

    @SerializedName("year_graduate") String year_graduate;

    @SerializedName("graduated") int graduated;

    @SerializedName("description") String description;

    public HighSchoolPojo() {

    }

    public HighSchoolPojo(String email, String high_school_name, String year_graduate, int graduated, String description) {
        this.email = email;
        this.high_school_name = high_school_name;
        this.year_graduate = year_graduate;
        this.graduated = graduated;
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHigh_school_name() {
        return high_school_name;
    }

    public void setHigh_school_name(String high_school_name) {
        this.high_school_name = high_school_name;
    }

    public String getYear_graduate() {
        return year_graduate;
    }

    public void setYear_graduate(String year_graduate) {
        this.year_graduate = year_graduate;
    }

    public int getGraduated() {
        return graduated;
    }

    public void setGraduated(int graduated) {
        this.graduated = graduated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
