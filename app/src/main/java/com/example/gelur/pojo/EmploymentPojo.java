package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EmploymentPojo implements Serializable {
    @SerializedName("email") String email;

    @SerializedName("company") String company;

    @SerializedName("position") String position;

    @SerializedName("city") String city;

    @SerializedName("project_name") String project_name;

    @SerializedName("description") String description;

    @SerializedName("date_start") String date_start;

    @SerializedName("date_end") String date_end;

    public EmploymentPojo() {

    }

    public EmploymentPojo(String email, String company, String position, String city, String project_name, String description, String date_start, String date_end) {
        this.email = email;
        this.company = company;
        this.position = position;
        this.city = city;
        this.project_name = project_name;
        this.description = description;
        this.date_start = date_start;
        this.date_end = date_end;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate_start() {
        return date_start;
    }

    public void setDate_start(String date_start) {
        this.date_start = date_start;
    }

    public String getDate_end() {
        return date_end;
    }

    public void setDate_end(String date_end) {
        this.date_end = date_end;
    }
}
