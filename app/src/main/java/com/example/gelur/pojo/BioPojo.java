package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class BioPojo {
    @SerializedName("id")
    String id;

    @SerializedName("email")
    String email;

    @SerializedName("bio")
    String bio;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_updated")
    long date_updated;

    public BioPojo() {

    }

    public BioPojo(String id, String email, String bio, long date_created, long date_updated) {
        this.id = id;
        this.email = email;
        this.bio = bio;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
