package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class RequestNonTransaksi {

    @SerializedName("req") String req;

    @SerializedName("kodereseller") String kodereseller;

    @SerializedName("perintah") String perintah;

    @SerializedName("time") String time;

    @SerializedName("pin") String pin;

    @SerializedName("password") String password;

    public RequestNonTransaksi(String req, String kodereseller, String perintah, String time, String pin, String password) {
        this.req = req;
        this.kodereseller = kodereseller;
        this.perintah = perintah;
        this.time = time;
        this.pin = pin;
        this.password = password;
    }

    public String getReq() {
        return req;
    }

    public void setReq(String req) {
        this.req = req;
    }

    public String getKodereseller() {
        return kodereseller;
    }

    public void setKodereseller(String kodereseller) {
        this.kodereseller = kodereseller;
    }

    public String getPerintah() {
        return perintah;
    }

    public void setPerintah(String perintah) {
        this.perintah = perintah;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
