package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FormaterResponse implements Serializable {
    public FormaterResponse(){

    }

    @SerializedName("code")
    private Long mCode;

    public Long getmCode() {
        return mCode;
    }

    public void setmCode(Long mCode) {
        this.mCode = mCode;
    }

    @SerializedName("message")
    private String mMessage;

    public String getmMessage() {
        return mMessage;
    }

    public void setmMessage(String mMessage) {
        this.mMessage = mMessage;
    }

    @SerializedName("timeline_user")
    private List<TimelinePojo> timeline_user = new ArrayList<>();

    public List<TimelinePojo> getTimeline_user() {
        return timeline_user;
    }

    public void setTimeline_user(List<TimelinePojo> timeline_user) {
        this.timeline_user = timeline_user;
    }

    @SerializedName("data_timeline")
    private List<TimelinePojo> timeline = new ArrayList<>();

    public List<TimelinePojo> getTimeline() {
        return timeline;
    }

    public void setTimeline(List<TimelinePojo> timeline) {
        this.timeline = timeline;
    }

    @SerializedName("data_taskboard")
    private List<TaskBoardPojo> taskboard = new ArrayList<>();

    public List<TaskBoardPojo> getTaskboard() {
        return taskboard;
    }

    public void setTaskboard(List<TaskBoardPojo> taskboard) {
        this.taskboard = taskboard;
    }

    @SerializedName("data_education_high_school")
    private List<HighSchoolPojo> data_education_high_school = new ArrayList<>();

    public List<HighSchoolPojo> getData_education_high_school() {
        return data_education_high_school;
    }

    public void setData_education_high_school(List<HighSchoolPojo> data_education_high_school) {
        this.data_education_high_school = data_education_high_school;
    }

    @SerializedName("data_employment_Pojo_user")
    private List<EmploymentPojo> data_employment_Pojo_user = new ArrayList<>();

    public List<EmploymentPojo> getData_employment_Pojo_user() {
        return data_employment_Pojo_user;
    }

    public void setData_employment_Pojo_user(List<EmploymentPojo> data_employment_Pojo_user) {
        this.data_employment_Pojo_user = data_employment_Pojo_user;
    }

    @SerializedName("data_profile")
    private List<UsersPojo> data_profile = new ArrayList<>();

    public List<UsersPojo> getData_profile() {
        return data_profile;
    }

    public void setData_profile(List<UsersPojo> data_profile) {
        this.data_profile = data_profile;
    }

    @SerializedName("data_bio")
    private List<BioPojo> data_bio = new ArrayList<>();

    public List<BioPojo> getData_bio() {
        return data_bio;
    }

    public void setData_bio(List<BioPojo> data_bio) {
        this.data_bio = data_bio;
    }
}
