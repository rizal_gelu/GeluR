package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class RequestTransaksiPulsa {

    @SerializedName("req") String req;

    @SerializedName("kodereseller") String kodereseller;

    @SerializedName("produk") String produk;

    @SerializedName("msisdn") String msisdn;

    @SerializedName("qty") String qty;

    @SerializedName("counter") String counter;

    @SerializedName("reffid") String reffid;

    @SerializedName("time") String time;

    @SerializedName("pin") String pin;

    @SerializedName("password") String password;

    public RequestTransaksiPulsa(String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        this.req = req;
        this.kodereseller = kodereseller;
        this.produk = produk;
        this.msisdn = msisdn;
        this.qty = qty;
        this.counter = counter;
        this.reffid = reffid;
        this.time = time;
        this.pin = pin;
        this.password = password;
    }

    public String getReq() {
        return req;
    }

    public void setReq(String req) {
        this.req = req;
    }

    public String getKodereseller() {
        return kodereseller;
    }

    public void setKodereseller(String kodereseller) {
        this.kodereseller = kodereseller;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getReffid() {
        return reffid;
    }

    public void setReffid(String reffid) {
        this.reffid = reffid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
