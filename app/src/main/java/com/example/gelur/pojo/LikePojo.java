package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class LikePojo {

    @SerializedName("Id") String id;

    @SerializedName("id_timeline") String id_timeline;

    @SerializedName("email")
    String email;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_updated")
    long date_updated;

    public LikePojo() {

    }

    public LikePojo(String email, String id_timeline, long date_created, long date_updated) {
        this.email = email;
        this.id_timeline = id_timeline;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId_timeline() {
        return id_timeline;
    }

    public void setId_timeline(String id_timeline) {
        this.id_timeline = id_timeline;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
