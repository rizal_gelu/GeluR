package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class FormatResponseNonTransaksi {
    @SerializedName("resp") String resp;

    @SerializedName("status_code") String status_code;

//    @SerializedName("msg") String msg;


    public String getResp() {
        return resp;
    }

    public void setResp(String resp) {
        this.resp = resp;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    @SerializedName("msg")
    private List<FormatResponseNonTransaksi> msg_user = new ArrayList<FormatResponseNonTransaksi>();

    public List<FormatResponseNonTransaksi> getMsg_user() {
        return msg_user;
    }

    public void setMsg_user(List<FormatResponseNonTransaksi> msg_user) {
        this.msg_user = msg_user;
    }

    //
//    public List<ResponseNonTransaksiMsgPojo> getMsg_user() {
//        return msg_user;
//    }
//
//    public void setMsg_user(List<ResponseNonTransaksiMsgPojo> msg_user) {
//        this.msg_user = msg_user;
//    }

//    @SerializedName("timeline_user")
//    private List<TimelinePojo> timeline_user = new ArrayList<>();
}
