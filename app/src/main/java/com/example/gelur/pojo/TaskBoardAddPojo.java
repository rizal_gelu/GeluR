package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class TaskBoardAddPojo {
    @SerializedName("Id") String id;

    @SerializedName("title_list_task") String title_list_task;

    @SerializedName("company_id") String company_id;

    @SerializedName("description_list_task") String description_list_task;

    @SerializedName("priority") String priority;

    @SerializedName("due_date") String due_date;

    @SerializedName("create_by") String create_by;

    @SerializedName("task_id") String task_id;

    @SerializedName("date_created") long date_created;

    @SerializedName("date_updated") long date_updated;

    public TaskBoardAddPojo() {

    }

    public TaskBoardAddPojo(String id, String title_list_task, String title_task, String company_id, String description_list_task, String priority, String due_date, String create_by, long date_created, long date_updated, String task_id) {
        this.id = id;
        this.title_list_task = title_list_task;
        this.company_id = company_id;
        this.description_list_task = description_list_task;
        this.priority = priority;
        this.due_date = due_date;
        this.create_by = create_by;
        this.date_created = date_created;
        this.date_updated = date_updated;
        this.task_id = task_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle_list_task() {
        return title_list_task;
    }

    public void setTitle_list_task(String title_list_task) {
        this.title_list_task = title_list_task;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getDescription_list_task() {
        return description_list_task;
    }

    public void setDescription_list_task(String description_list_task) {
        this.description_list_task = description_list_task;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getCreate_by() {
        return create_by;
    }

    public void setCreate_by(String create_by) {
        this.create_by = create_by;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
