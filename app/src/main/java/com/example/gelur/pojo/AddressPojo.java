package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class AddressPojo {
    @SerializedName("id") String id;

    @SerializedName("email")
    String email;

    @SerializedName("name_city")
    String name_city;

    @SerializedName("type_city")
    String type_city;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_updated")
    long date_updated;

    public AddressPojo() {

    }

    public AddressPojo(String id, String email, String name_city, String type_city, long date_created, long date_updated) {
        this.id = id;
        this.email = email;
        this.name_city = name_city;
        this.type_city = type_city;
        this.date_created = date_created;
        this.date_updated = date_updated;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName_city() {
        return name_city;
    }

    public void setName_city(String name_city) {
        this.name_city = name_city;
    }

    public String getType_city() {
        return type_city;
    }

    public void setType_city(String type_city) {
        this.type_city = type_city;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }
}
