package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TimelinePersonalUser implements Serializable {
    @SerializedName("avatar")
    private String avatar;

    @SerializedName("date_created")
    private long mDateCreated;

    @SerializedName("date_updated")
    private long mDateUpdated;

    @SerializedName("description")
    private String mDescription;

    @SerializedName("email")
    private String mEmail;

    @SerializedName("file")
    private String mFile;

    @SerializedName("id")
    private String mId;

    @SerializedName("first_name")
    private String mFirst_name;

    @SerializedName("last_name")
    private String mLast_name;

    public TimelinePersonalUser() {

    }

    public TimelinePersonalUser(String avatar, long mDateCreated, long mDateUpdated, String mDescription, String mEmail, String mFile, String mId, String mFirst_name, String mLast_name) {
        this.avatar         = avatar;
        this.mDateCreated   = mDateCreated;
        this.mDateUpdated   = mDateUpdated;
        this.mDescription   = mDescription;
        this.mEmail         = mEmail;
        this.mFile          = mFile;
        this.mId            = mId;
        this.mFirst_name    = mFirst_name;
        this.mLast_name     = mLast_name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getmDateCreated() {
        return mDateCreated;
    }

    public void setmDateCreated(long mDateCreated) {
        this.mDateCreated = mDateCreated;
    }

    public long getmDateUpdated() {
        return mDateUpdated;
    }

    public void setmDateUpdated(long mDateUpdated) {
        this.mDateUpdated = mDateUpdated;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmFile() {
        return mFile;
    }

    public void setmFile(String mFile) {
        this.mFile = mFile;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmFirst_name() {
        return mFirst_name;
    }

    public void setmFirst_name(String mFirst_name) {
        this.mFirst_name = mFirst_name;
    }

    public String getmLast_name() {
        return mLast_name;
    }

    public void setmLast_name(String mLast_name) {
        this.mLast_name = mLast_name;
    }
}
