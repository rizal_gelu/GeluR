package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TimelinePojo implements Serializable {

    @SerializedName("id")
    String id;

    @SerializedName("avatar")
    String avatar;

    @SerializedName("date_created")
    long date_created;

    @SerializedName("date_updated")
    long date_updated;

    @SerializedName("description")
    String description;

    @SerializedName("email")
    String email;

    @SerializedName("file")
    String file;

    @SerializedName("first_name")
    String first_name;

    @SerializedName("last_name")
    String last_name;

    @SerializedName("count_like")
    int count_like;

    public TimelinePojo() {

    }

    public TimelinePojo(String avatar, Long date_created, Long date_updated, String Description, String email, String file, String first_name, String last_name, String id, int count_like) {
        this.avatar = avatar;
        this.date_created = date_created;
        this.date_updated = date_updated;
        this.description = Description;
        this.email = email;
        this.file = file;
        this.first_name = first_name;
        this.last_name = last_name;
        this.id = id;
        this.count_like = count_like;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public long getDate_created() {
        return date_created;
    }

    public void setDate_created(long date_created) {
        this.date_created = date_created;
    }

    public long getDate_updated() {
        return date_updated;
    }

    public void setDate_updated(long date_updated) {
        this.date_updated = date_updated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCount_like() {
        return count_like;
    }

    public void setCount_like(int count_like) {
        this.count_like = count_like;
    }
}
