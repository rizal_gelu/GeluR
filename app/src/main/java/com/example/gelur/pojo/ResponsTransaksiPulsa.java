package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class ResponsTransaksiPulsa {

    @SerializedName("resp") String resp;

    @SerializedName("status_code") String status_code;

    @SerializedName("status") String status;

    @SerializedName("trxid") String trxid;

    @SerializedName("msisdn") String msisdn;

    @SerializedName("produk") String produk;

    @SerializedName("harga") String harga;

    @SerializedName("reffid") String reffid;

    @SerializedName("counter") String counter;

    @SerializedName("qty") String qty;

    @SerializedName("sn") String sn;

    @SerializedName("saldo_awal") String saldo_awal;

    @SerializedName("saldo") String saldo;

    @SerializedName("bon") String bon;

    @SerializedName("bonus") String bonus;

    @SerializedName("poin") String poin;

    @SerializedName("msg") String msg;

    public ResponsTransaksiPulsa(String resp, String status_code, String status, String trxid, String msisdn, String produk, String harga, String reffid,
                                 String counter, String qty, String sn, String saldo_awal, String saldo, String bon, String bonus, String poin, String msg) {
        this.resp = resp;
        this.status_code = status_code;
        this.status = status;
        this.trxid = trxid;
        this.msisdn = msisdn;
        this.produk = produk;
        this.harga = harga;
        this.reffid = reffid;
        this.counter = counter;
        this.qty = qty;
        this.sn = sn;
        this.saldo_awal = saldo_awal;
        this.saldo = saldo;
        this.bon = bon;
        this.bonus = bonus;
        this.poin = poin;
        this.msg = msg;
    }

    public String getResp() {
        return resp;
    }

    public void setResp(String resp) {
        this.resp = resp;
    }

    public String getStatus_code() {
        return status_code;
    }

    public void setStatus_code(String status_code) {
        this.status_code = status_code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTrxid() {
        return trxid;
    }

    public void setTrxid(String trxid) {
        this.trxid = trxid;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getProduk() {
        return produk;
    }

    public void setProduk(String produk) {
        this.produk = produk;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getReffid() {
        return reffid;
    }

    public void setReffid(String reffid) {
        this.reffid = reffid;
    }

    public String getCounter() {
        return counter;
    }

    public void setCounter(String counter) {
        this.counter = counter;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getSaldo_awal() {
        return saldo_awal;
    }

    public void setSaldo_awal(String saldo_awal) {
        this.saldo_awal = saldo_awal;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public String getBon() {
        return bon;
    }

    public void setBon(String bon) {
        this.bon = bon;
    }

    public String getBonus() {
        return bonus;
    }

    public void setBonus(String bonus) {
        this.bonus = bonus;
    }

    public String getPoin() {
        return poin;
    }

    public void setPoin(String poin) {
        this.poin = poin;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
