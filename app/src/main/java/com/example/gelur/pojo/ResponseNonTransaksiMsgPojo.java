package com.example.gelur.pojo;

import com.google.gson.annotations.SerializedName;

public class ResponseNonTransaksiMsgPojo {
    @SerializedName("Produk") String Produk;

    @SerializedName("Tujuan") String Tujuan;

    @SerializedName("Status") String Status;

    public ResponseNonTransaksiMsgPojo() {

    }

    public ResponseNonTransaksiMsgPojo(String Produk, String Tujuan, String Status) {
        this.Produk = Produk;
        this.Tujuan = Tujuan;
        this.Status = Status;
    }

    public String getProduk() {
        return Produk;
    }

    public void setProduk(String produk) {
        Produk = produk;
    }

    public String getTujuan() {
        return Tujuan;
    }

    public void setTujuan(String tujuan) {
        Tujuan = tujuan;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
