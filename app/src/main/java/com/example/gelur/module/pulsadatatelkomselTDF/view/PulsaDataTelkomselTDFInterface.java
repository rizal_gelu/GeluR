package com.example.gelur.module.pulsadatatelkomselTDF.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTelkomselTDFInterface {
    void TransferPulsaDataTelkomselTDFSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTelkomselTDFGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaDataTelkomselTDF(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
