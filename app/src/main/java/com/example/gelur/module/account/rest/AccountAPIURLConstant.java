package com.example.gelur.module.account.rest;

import com.example.gelur.util.AppsConstant;

public class AccountAPIURLConstant {
    // User EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/users/"; // User UriSegment 2
    public static final String GET_ACCOUNT_DETAIL = BASE_API_USER + "get_profile/"; // User UriSegment 3

    // Timeline EndPoint
    public static final String BASE_API_TIMELINES = AppsConstant.BASE_API_URL + "v1/timelines/"; // Timeline UriSegment 2
    public static final String GET_FEED_USER_PERSONAL = BASE_API_TIMELINES + "get_timeline_by_email/"; // Timeline UriSegment 3
}
