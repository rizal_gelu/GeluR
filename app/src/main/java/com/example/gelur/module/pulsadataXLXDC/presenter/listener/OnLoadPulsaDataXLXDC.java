package com.example.gelur.module.pulsadataXLXDC.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataXLXDC {
    void RequestTransferPulsaDataXLXDCSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataXLXDCGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataXLXDC(String data);
    void ResponseListPricePulsaDataXLXDCSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataXLXDCGagal(String m);
}
