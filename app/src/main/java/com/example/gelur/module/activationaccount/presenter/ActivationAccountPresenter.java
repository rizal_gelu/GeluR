package com.example.gelur.module.activationaccount.presenter;

import android.content.Context;

import com.example.gelur.module.activationaccount.model.ActivationAccountModel;
import com.example.gelur.module.activationaccount.presenter.listener.ActivationAccountFinishedListener;
import com.example.gelur.module.activationaccount.view.ActivationAccount;
import com.example.gelur.module.activationaccount.view.ActivationAccountInterface;
import com.example.gelur.pojo.ActivateAccountPojo;

import java.lang.ref.WeakReference;

public class ActivationAccountPresenter implements  ActivationAccountPresenterInterface, ActivationAccountFinishedListener {

    private WeakReference<ActivationAccountInterface> view;
    private ActivationAccountModel model;

    public ActivationAccountPresenter(ActivationAccountInterface view) {
        this.view = new WeakReference<ActivationAccountInterface>(view);
        this.model = new ActivationAccountModel();
    }

    @Override
    public void onActiveAccountSuccess(ActivateAccountPojo activateAccountPojo) {
        if (view.get() != null) view.get().onActivateSuccess(activateAccountPojo);
    }

    @Override
    public void onActiveAccountFail(String message) {

    }

    @Override
    public void submitActivateCode(Context context, String email, String active_code) {
        model.activate_account(context, email, active_code, this);
    }
}
