package com.example.gelur.module.plnpascabayar.presenter.listener;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadLaporanPLNPascaBayar {
    void onLoadBayarPLNPascaBayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void onLoadBayarPLNPascaBayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa);
}
