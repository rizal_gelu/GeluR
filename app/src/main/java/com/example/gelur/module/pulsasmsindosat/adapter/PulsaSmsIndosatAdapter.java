package com.example.gelur.module.pulsasmsindosat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsasmsindosat.presenter.listener.OnLoadPulsaSmsIndosat;

import java.util.List;

public class PulsaSmsIndosatAdapter extends RecyclerView.Adapter<PulsaSmsIndosatAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaSmsIndosat listener;
    List<String> stringList;

    public PulsaSmsIndosatAdapter(Activity activity, List<String> datum, OnLoadPulsaSmsIndosat listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaSmsIndosatAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_sms_indosat, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaSmsIndosatAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_sms_indosat.setText(data);

        holder.ll_kumpulan_option_pulsa_sms_indosat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaSmsIndosat(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_sms_indosat;
        public final TextView pulsa_sms_indosat;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_sms_indosat = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_sms_indosat);
            pulsa_sms_indosat = (TextView) view.findViewById(R.id.pulsa_sms_indosat);
        }
    }
}
