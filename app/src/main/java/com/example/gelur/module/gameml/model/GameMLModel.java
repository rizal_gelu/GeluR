package com.example.gelur.module.gameml.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.ecommerceovo.rest.EcommerceOvoRestClient;
import com.example.gelur.module.gameff.presenter.listener.OnLoadGameFF;
import com.example.gelur.module.gameff.rest.GameFFRestClient;
import com.example.gelur.module.gameml.presenter.listener.OnLoadGameML;
import com.example.gelur.module.gameml.rest.GameMLRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GameMLModel implements GameMLModelInterface{
    @Override
    public void RequestTransferDiamondML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGameML listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = GameMLRestClient.get().req_transfer_diamond_ml(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferDiamondMLSukses(response.body());
                } else {
                    listener.RequestTransferDiamondMLGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPriceGameML(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadGameML listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = GameMLRestClient.get().getPriceListPriceGameML(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPriceGameMLSukses(response.body());
                } else {
                    listener.ResponseListPriceGameMLGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPriceGameMLGagal(t.getMessage());
            }
        });
    }
}
