package com.example.gelur.module.taskboardcompany.view;

import com.example.gelur.pojo.TaskBoardPojo;

import java.util.List;

public interface TaskBoardInterface {
    void onLoadTaskBoardSuccess(List<TaskBoardPojo> taskBoardPojo);
    void onLoadTaskBoardFailed(String Message);
    void onClickTaskItem();
    void goToAddJob();
}
