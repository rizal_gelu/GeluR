package com.example.gelur.module.editprofile.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import com.bumptech.glide.Glide;
import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.bio.view.Bio;
import com.example.gelur.module.editprofile.presenter.EditProfilePresenter;
//import com.example.gelur.module.linkuser.view.LinkUser;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.sociallink.view.SocialLink;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.youraboutinfo.view.YourAboutInfo;
import com.example.gelur.pojo.AddressPojo;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.parceler.Parcels;

import java.util.HashMap;

public class EditProfile extends AppCompatActivity implements EditProfileInterface {

    @BindView(R.id.activity_edit_profile_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_edit_profile_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_edit_profile)
    View viewContent;

    @BindView(R.id.layout_loading_account) View viewLoading;

    @BindView(R.id.edit_your_about_info)
    AppCompatButton btnYourInfo;

    @BindView(R.id.profile_pict_circle_image)
    CircleImageView profile_pict_circle_image;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    EditProfilePresenter presenter;
    UsersPojo user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_profile);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EditProfile.this, PostTimeline.class));
            }
        });

        /*
        Get email from session
         */
        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserDetailsFromSession();
        String email = userDetails.get(AppsConstant.KEY_ID_USER);

//        presenter.onLoadBio(email);

        ButterKnife.bind(this);
        presenter = new EditProfilePresenter(this);

        showContent(true);
    }

    private void populateUI(UsersPojo userjo) {
        this.user = userjo;
        if (null != userjo) {
            Glide.with(this)
                    .load(AppsConstant.BASE_IMAGE_PROFILE_URL + userjo.getAvatar())
                    .into(profile_pict_circle_image);
        }
        showContent(true);
    }

    public void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToProfile() {
        startActivity(new Intent(EditProfile.this, Account.class));
    }

    private void goToNotification() {
        startActivity(new Intent(EditProfile.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(EditProfile.this, Search.class));
    }

    private void goToHome() {
        startActivity(new Intent(EditProfile.this, Timeline.class));
    }

    @OnClick(R.id.bio_edit)
    public void goEditBio() {
        startActivity(new Intent(EditProfile.this, Bio.class));
    }


    @OnClick(R.id.edit_your_about_info)
    public void editYourAboutInfo(View v) {
        presenter.onEditYourAboutInfo();
    }

    @Override
    public void goEditYourAboutInfo() {
        startActivity(new Intent(EditProfile.this, YourAboutInfo.class));
    }


    @Override
    public void goEditMoreInfo() {

    }

    @Override
    public void goEditProfilePict() {

    }

    @OnClick(R.id.edit_links)
    public void goEditLinks() {
        startActivity(new Intent(EditProfile.this, SocialLink.class));
    }
}
