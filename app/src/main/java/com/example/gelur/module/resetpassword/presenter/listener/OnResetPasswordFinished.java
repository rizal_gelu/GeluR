package com.example.gelur.module.resetpassword.presenter.listener;

import com.example.gelur.pojo.ResetPasswordPojo;

public interface OnResetPasswordFinished {
    void onResetPasswordSuccess(ResetPasswordPojo resetPasswordPojo);
    void onResetPasswordFail(String message);
}
