package com.example.gelur.module.pulsadataSMDEVO.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataSmartfrenSMDEVO {
    void RequestTransferPulsaDataSmartFrenSMDEVOSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataSmartFrenSMDEVOGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataSmartFrenSMDEVO(String data);
    void ResponseListPricePulsaDataSmartFrenSMDEVOSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataSmartFrenSMDEVOGagal(String m);
}
