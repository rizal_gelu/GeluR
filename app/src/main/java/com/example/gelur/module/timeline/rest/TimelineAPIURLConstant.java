package com.example.gelur.module.timeline.rest;

import com.example.gelur.util.AppsConstant;

public class TimelineAPIURLConstant {
    public static final String BASE_API_URL = AppsConstant.BASE_API_URL + "v1/timelines/";
    public static final String GET_TIMELINES_URL = BASE_API_URL + "get_all/"; // get all timeline
    public static final String POST_LIKE_TL = BASE_API_URL + "post_like_tl/"; // post liked
    public static final String UNLIKE_TL = BASE_API_URL + "unlike_tl/"; // Unlike post
}
