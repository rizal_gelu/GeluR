package com.example.gelur.module.listgame.view;

public interface ListGameInterface {

    void onClickMobileLegend();
    void onClickPubgMobile();
    void onClickFreeFire();

}
