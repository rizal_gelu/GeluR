package com.example.gelur.module.plnpascabayar.model;

import android.content.Context;

import com.example.gelur.module.listbpjsppob.presenter.listener.OnLoadBPJS;
import com.example.gelur.module.plnpascabayar.presenter.listener.OnLoadPlnPascaBayar;

public interface PlnPascaBayarModelInterface {
    void RequestCheckPLNPascaBayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPlnPascaBayar listener);
}
