package com.example.gelur.module.pulsatelponindosat.presenter;

import android.content.Context;

import com.example.gelur.module.pulsatelponindosat.model.PulsaTelponIndosatModel;
import com.example.gelur.module.pulsatelponindosat.presenter.listener.OnLoadPulsaTelponIndosat;
import com.example.gelur.module.pulsatelponindosat.view.PulsaTelponIndosatInterface;
import com.example.gelur.module.pulsatelpontelkomsel.model.PulsaTelponTelkomselModel;
import com.example.gelur.module.pulsatelpontelkomsel.view.PulsaTelponTelkomselInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaTelponIndosatPresenter implements PulsaTelponIndosatPresenterInterface, OnLoadPulsaTelponIndosat {

    private WeakReference<PulsaTelponIndosatInterface> view;
    private PulsaTelponIndosatModel model;

    public PulsaTelponIndosatPresenter(PulsaTelponIndosatInterface view) {
        this.view = new WeakReference<PulsaTelponIndosatInterface>(view);
        this.model = new PulsaTelponIndosatModel();
    }

    @Override
    public void onRequesttransferpulsatelponIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaTelponIndosat(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricepulsaTelponIndosat(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaTelponIndosat(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaTelponIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaTelponIndosatSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaTelponIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaTelponIndosatGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaTelponIndosat(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaTelponIndosatSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaTelponIndosat(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaTelponIndosatagal(String m) {

    }
}
