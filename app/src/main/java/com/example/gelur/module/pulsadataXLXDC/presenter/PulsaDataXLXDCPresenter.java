package com.example.gelur.module.pulsadataXLXDC.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataXLXDC.model.PulsaDataXLXDCModel;
import com.example.gelur.module.pulsadataXLXDC.presenter.listener.OnLoadPulsaDataXLXDC;
import com.example.gelur.module.pulsadataXLXDC.view.PulsaDataXLXDCInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataXLXDCPresenter implements PulsaDataXLXDCPresenterInterface, OnLoadPulsaDataXLXDC {

    private WeakReference<PulsaDataXLXDCInterface> view;
    private PulsaDataXLXDCModel model;

    public PulsaDataXLXDCPresenter(PulsaDataXLXDCInterface view) {
        this.view = new WeakReference<PulsaDataXLXDCInterface>(view);
        this.model = new PulsaDataXLXDCModel();
    }

    @Override
    public void onRequesttransferpulsaDataXLXDC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataXLXDC(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataXLXDC(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataXLXDC(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataXLXDCSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXDCSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataXLXDCGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXDCGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataXLXDC(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataXLXDCSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaXLXDCSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataXLXDCGagal(String m) {

    }
}
