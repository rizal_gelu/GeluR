package com.example.gelur.module.pulsasmsindosat.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsasmsindosat.presenter.listener.OnLoadPulsaSmsIndosat;
import com.example.gelur.module.pulsasmsindosat.rest.PulsaSmsIndosatRestClient;
import com.example.gelur.module.pulsasmstelkomsel.rest.PulsaSmsTelkomselRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaSmsIndosatModel implements PulsaSmsIndosatModelInterface{
    @Override
    public void RequestTransferPulsaSmsIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaSmsIndosat listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaSmsIndosatRestClient.get().req_transfer_sms_indosat(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaSmsIndosatSukses(response.body());
                } else {
                    listener.RequestTransferPulsaSmsIndosatGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaSmsIndosat(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaSmsIndosat listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = PulsaSmsTelkomselRestClient.get().getPriceListPulsaSmsTelkomsel(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaSmsIndosatSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaSmsIndosatGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaSmsIndosatGagal(t.getMessage());
            }
        });
    }
}
