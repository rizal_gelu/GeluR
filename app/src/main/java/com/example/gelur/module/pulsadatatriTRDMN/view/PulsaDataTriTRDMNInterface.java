package com.example.gelur.module.pulsadatatriTRDMN.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDMNInterface {

    void TransferPulsaDataTriTRDMNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDMNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDMNSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDMNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
