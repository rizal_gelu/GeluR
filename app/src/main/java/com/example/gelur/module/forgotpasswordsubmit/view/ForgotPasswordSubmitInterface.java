package com.example.gelur.module.forgotpasswordsubmit.view;

import com.example.gelur.pojo.ForgotPasswordConfimPojo;
import com.example.gelur.pojo.ForgotPasswordPojo;

public interface ForgotPasswordSubmitInterface {
    void onSubmitForgotPasswordCodeSuccess(ForgotPasswordConfimPojo forgotPasswordConfimPojo);
    void onSubmitForgotPasswordCodeFail(String message);
}
