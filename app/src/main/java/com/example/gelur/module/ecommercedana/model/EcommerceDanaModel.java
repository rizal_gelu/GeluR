package com.example.gelur.module.ecommercedana.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.ecommercedana.presenter.listener.OnLoadDanaEcommerce;
import com.example.gelur.module.ecommercedana.rest.EcommerceDanaRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EcommerceDanaModel implements EcommerceDanaModelInterface{
    @Override
    public void RequestTransferSaldoDana(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadDanaEcommerce listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = EcommerceDanaRestClient.get().req_transfer_dana_ecommerce(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferSaldoDanaSukses(response.body());
                } else {
                    listener.RequestTransferSaldoDanaGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPriceDana(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadDanaEcommerce listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = EcommerceDanaRestClient.get().getListDana(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPriceDanaSukses(response.body());
                } else {
                    listener.ResponseListPriceDanaGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPriceDanaGagal(t.getMessage());
            }
        });
    }
}
