package com.example.gelur.module.pulsadataXLPXD.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataXLPXDInterface {
    void TransferPulsaDataXLPXDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataXLPXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaXLPXDSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaXLPXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
