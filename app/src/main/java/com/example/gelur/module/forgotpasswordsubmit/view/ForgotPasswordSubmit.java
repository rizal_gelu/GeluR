package com.example.gelur.module.forgotpasswordsubmit.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.activationaccount.presenter.ActivationAccountPresenter;
import com.example.gelur.module.activationaccount.view.ActivationAccount;
import com.example.gelur.module.forgotpasswordsubmit.presenter.ForgotPasswordSubmitPresenter;
import com.example.gelur.module.login.view.Login;
import com.example.gelur.module.resetpassword.view.ResetPassword;
import com.example.gelur.pojo.ForgotPasswordConfimPojo;
import com.example.gelur.pojo.ForgotPasswordPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

public class ForgotPasswordSubmit extends AppCompatActivity implements ForgotPasswordSubmitInterface {
    @BindView(R.id.activity_activation_forgot_password_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_forgot_password_submit_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_login_layout_content)
    View layoutContent;

    @BindView(R.id.activity_login_layout_loading) View layoutLoading;

    @BindView(R.id.tv_forgot_password_code)
    TextView tvForgotPasswordCode;

    @BindView(R.id.et_forgot_password_code)
    EditText etForgotPasswordCode;

    @BindView(R.id.btn_confirm)
    Button btnConfirm;

    ForgotPasswordSubmitPresenter presenter;
    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password_submit);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.forgot_password_code);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new ForgotPasswordSubmitPresenter(this);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            layoutContent.setVisibility(View.VISIBLE);
            layoutLoading.setVisibility(View.GONE);
        } else {
            layoutContent.setVisibility(View.GONE);
            layoutLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_confirm)
    public void onClickConfirmCode() {
        loadingDialog.dismiss();

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserDetailsFromSession();

        String email = userDetails.get(AppsConstant.KEY_ID_USER);
        String forgotPassword_code = etForgotPasswordCode.getText().toString();

        presenter.onForgotPasswordCofirmSubmited(this, email, forgotPassword_code);

        // Create Forgot Password Session
        String prefName = AppsConstant.KEY_NAME;
        AppsUtil sessionManagement = new AppsUtil(this);
        sessionManagement.createForgotPasswordSession(prefName, email, forgotPassword_code);
    }


    @Override
    public void onSubmitForgotPasswordCodeSuccess(ForgotPasswordConfimPojo forgotPasswordConfimPojo) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, "Your account active now. Please log in first", Snackbar.LENGTH_LONG).show();
        finish();
        Intent intent = new Intent(ForgotPasswordSubmit.this, ResetPassword.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onSubmitForgotPasswordCodeFail(String message) {

    }
}
