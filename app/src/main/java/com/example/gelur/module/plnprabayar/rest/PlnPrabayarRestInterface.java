package com.example.gelur.module.plnprabayar.rest;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PlnPrabayarRestInterface {
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsTransaksiPulsa> req_transfer_token_listrik(@Body RequestTransaksiPulsa requestTransaksiPulsa);

    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")Call<ResponsNonTransaksi> getPriceListPlnPrabayar(@Body RequestNonTransaksi requestNonTransaksi);
}
