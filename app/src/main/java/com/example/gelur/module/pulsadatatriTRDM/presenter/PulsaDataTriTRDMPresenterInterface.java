package com.example.gelur.module.pulsadatatriTRDM.presenter;

import android.content.Context;

public interface PulsaDataTriTRDMPresenterInterface {
    void onRequesttransferpulsaDataTriTRDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRDM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
