package com.example.gelur.module.forgotpassword.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.example.gelur.R;

import com.example.gelur.module.activationaccount.view.ActivationAccount;
import com.example.gelur.module.forgotpassword.presenter.ForgotPasswordPresenter;
import com.example.gelur.module.forgotpasswordsubmit.view.ForgotPasswordSubmit;
import com.example.gelur.module.login.view.Login;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPassword extends AppCompatActivity implements ForgotPasswordInterface{
    @BindView(R.id.activity_forgot_password_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_forgot_password_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_forgot_password_etEmail)
    EditText etEmail;

    @BindView(R.id.activity_forgot_password_btnSubmit)
    AppCompatButton btSubmit;

    ForgotPasswordPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forgot_password);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        presenter = new ForgotPasswordPresenter(this);
    }

    @Override
    public void onForgotPasswordResult(String message) {
        Snackbar.make(coordinatorLayout, "Please check your email", Snackbar.LENGTH_LONG).show();
        finish();
        Intent intent = new Intent(ForgotPassword.this, ForgotPasswordSubmit.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @OnClick(R.id.activity_forgot_password_btnSubmit)
    public void onForgotPasswordClick(View v) {
        String email            = etEmail.getText().toString();

        if (email.equals("") || !AppsUtil.isValidEmail(email)) {
            Snackbar.make(coordinatorLayout, "Email is not valid", Snackbar.LENGTH_SHORT).show();
        } else {
            presenter.submitForgotPassword(email);

            // create session for get email
            String name = AppsConstant.KEY_NAME;
            AppsUtil sessionManagement = new AppsUtil(this);
            String forgot_password_code = "";
            sessionManagement.createForgotPasswordSession(name, email, forgot_password_code);
        }
    }
}
