package com.example.gelur.module.address.rest;

import com.example.gelur.module.bio.rest.BioAPIURLConstant;
import com.example.gelur.pojo.AddressPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AddressRestInterface {
    @FormUrlEncoded
    @POST(AddressAPIURLConstant.POST_ADDRESS_USER)
    Call<AddressPojo> postAddress(@Field("email") String email, @Field("name_city") String name_city, @Field("type_city") String type_city);

}
