package com.example.gelur.module.ecommercegojekpenumpang.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface EcommerceGojekPenumpangInterface {
    void TransferSaldoGojekPenumpangSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferSaldoGojekPenumpangGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceGojekPenumpang(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
