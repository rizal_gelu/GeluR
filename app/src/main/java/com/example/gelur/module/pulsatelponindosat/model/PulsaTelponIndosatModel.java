package com.example.gelur.module.pulsatelponindosat.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsasmstelkomsel.rest.PulsaSmsTelkomselRestClient;
import com.example.gelur.module.pulsatelponindosat.presenter.listener.OnLoadPulsaTelponIndosat;
import com.example.gelur.module.pulsatelponindosat.rest.PulsaTelponIndosatRestClient;
import com.example.gelur.module.pulsatelpontelkomsel.rest.PulsaTelponTelkomselRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaTelponIndosatModel implements PulsaTelponIndosatModelInterface{
    @Override
    public void RequestTransferPulsaTelponIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponIndosat listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaTelponIndosatRestClient.get().req_transfer_pulsa_telpon_indosat(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaTelponIndosatSukses(response.body());
                } else {
                    listener.RequestTransferPulsaTelponIndosatGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaTelponIndosat(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponIndosat listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = PulsaTelponIndosatRestClient.get().getPriceListPulsaTelponIndosat(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaTelponIndosatSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaTelponIndosatagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaTelponIndosatagal(t.getMessage());
            }
        });
    }
}
