package com.example.gelur.module.gameml.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface GameMLInterface {
    void TransferDiamondMLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferDiamondMLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceGameML(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
