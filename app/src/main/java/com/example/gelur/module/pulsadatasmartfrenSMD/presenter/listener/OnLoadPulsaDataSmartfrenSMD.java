package com.example.gelur.module.pulsadatasmartfrenSMD.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataSmartfrenSMD {
    void RequestTransferPulsaDataSmartFrenSMDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataSmartFrenSMDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataSmartFrenSMD(String data);
    void ResponseListPricePulsaDataSmartFrenSMDSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataSmartFrenSMDGagal(String m);
}
