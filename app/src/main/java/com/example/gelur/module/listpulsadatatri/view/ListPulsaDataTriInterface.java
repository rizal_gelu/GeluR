package com.example.gelur.module.listpulsadatatri.view;

public interface ListPulsaDataTriInterface {
    void onClickTRD();
    void onClickTRDDP();
    void onClickTRDM();
    void onClickTRDPY();
    void onClickTRDMINI();
    void onClickTRDGM();
    void onClickTRDUL();
    void onClickTRDMN();
    void onClickTRDAON();
}
