package com.example.gelur.module.pulsadatatriTRDMINI.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDMINI.presenter.listener.OnLoadPulsaDataTriTRDMINI;

public interface PulsaDataTriTRDMINIModelInterface {
    void RequestTransferPulsaDataTriTRDMINI(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDMINI listener);
    void RequestListPricePulsaDataTriTRDMINI(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDMINI listener);
}
