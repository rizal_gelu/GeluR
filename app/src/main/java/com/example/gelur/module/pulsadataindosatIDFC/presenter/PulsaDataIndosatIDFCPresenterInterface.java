package com.example.gelur.module.pulsadataindosatIDFC.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDFCPresenterInterface {
    void onRequesttransferpulsadataIndosatIDFC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDFC(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
