package com.example.gelur.module.bio.presenter.listener;

import com.example.gelur.pojo.BioPojo;

public interface OnSaveBioFinishedListener {
    void onSaveBioSuccess(BioPojo bioPojo);
    void onSaveBioFailed(String message);
}
