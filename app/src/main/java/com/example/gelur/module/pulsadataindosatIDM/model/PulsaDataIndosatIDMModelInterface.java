package com.example.gelur.module.pulsadataindosatIDM.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDM.presenter.listener.OnLoadPulsaDataIndosatIDM;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;

public interface PulsaDataIndosatIDMModelInterface {
    void RequestTransferDataIndosatIDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDM listener);
    void RequestListPricePulsaDataIndosatIDM(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDM listener);
}
