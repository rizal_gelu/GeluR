package com.example.gelur.module.ppobindihome.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.module.ppobindihome.presenter.PpobIndihomePresenter;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class PpobIndihome extends AppCompatActivity implements PpobIndihomeInterface {

    @BindView(R.id.coordinator_ppob_indihome)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_indihome)
    Toolbar toolbar;

    @BindView(R.id.content_ppob_indihome)
    View viewContent;

    @BindView(R.id.layout_loading_indihome) View viewLoading;

    @BindView(R.id.Et_nomor_indihome)
    EditText Et_nomor_indihome;

    @BindView(R.id.qty_indihome) EditText qty_indihome;

    @BindView(R.id.submit_request_check_indihome)
    AppCompatButton submit_request_check_indihome;

    AlertDialog loadingDialog;
    AlertDialog.Builder dialogBuilder;

    PpobIndihomePresenter presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ppob_indihome);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - Check Tagihan Indihome");

        presenter = new PpobIndihomePresenter(this);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.submit_request_check_indihome)
    public void checkTagihanIndihome() {
        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        System.out.println("Random value in int from "+min+" to "+max+ ":");
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String req = "topup";
        String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
        String qty = qty_indihome.getText().toString();
        String refid = String.valueOf(random_int);
        String get_no_hp = Et_nomor_indihome.getText().toString();
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
        String nominal_cek = "CEKINHOME";
        String counter = qty_indihome.getText().toString();

        presenter.onRequestCheckTagihanIndihome(this, req, kode_reseller, nominal_cek, get_no_hp, counter, qty, refid, time, pin, password);
    }

    @Override
    public void OnResponseTagihanIndihomeSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();
        String get_status_code = responsTransaksiPulsa.getStatus_code();

        Intent intent = new Intent(PpobIndihome.this, LaporanCheckIndihome.class);
        intent.putExtra("status_code", get_status_code);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void OnResponseTagihanIndihomeGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();
        String get_status_code = responsTransaksiPulsa.getStatus_code();

        Intent intent = new Intent(PpobIndihome.this, LaporanCheckIndihome.class);
        intent.putExtra("status_code", get_status_code);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }
}
