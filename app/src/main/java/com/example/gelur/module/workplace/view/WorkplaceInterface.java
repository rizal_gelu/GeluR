package com.example.gelur.module.workplace.view;

public interface WorkplaceInterface {
    void onSuccesPostWorkplace();
    void onFailedPostWorkplace();
}
