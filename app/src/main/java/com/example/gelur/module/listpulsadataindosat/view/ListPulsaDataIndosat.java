package com.example.gelur.module.listpulsadataindosat.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.pulsadataindosatIDF.view.PulsaDataIndosatIDF;
import com.example.gelur.module.pulsadataindosatIDFC.view.PulsaDataIndosatIDFC;
import com.example.gelur.module.pulsadataindosatIDFN.view.PulsaDataIndosatIDFN;
import com.example.gelur.module.pulsadataindosatIDM.view.PulsaDataIndosatIDM;
import com.example.gelur.module.pulsadataindosatIDP.view.PulsaDataIndosatIDP;
import com.example.gelur.module.pulsadataindosatIDT.view.PulsaDataIndosatIDT;
import com.example.gelur.module.pulsadataindosatIDU.view.PulsaDataIndosatIDU;
import com.example.gelur.module.pulsadataindosatIDUA.view.PulsaDataIndosatIDUA;
import com.example.gelur.module.pulsadataindosatIDY.view.PulsaDataIndosatIDY;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListPulsaDataIndosat extends AppCompatActivity implements ListPulsaDataIndosatInterface{

    @BindView(R.id.activity_list_pulsa_data_indosat)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_list_pulsa_data_indosat_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_data_indosat) View viewContent;

    @BindView(R.id.layout_loading_list_pulsa_data_indosat) View viewLoading;

    @BindView(R.id.kumpulan_option_list_product_data_indosat)
    LinearLayout kumpulan_option_list_product_data_indosat;

    @BindView(R.id.ll_pulsa_data_IDY) LinearLayout ll_pulsa_data_IDY;

    @BindView(R.id.ll_pulsa_data_IDF) LinearLayout ll_pulsa_data_IDF;

    @BindView(R.id.ll_pulsa_data_IDFN) LinearLayout ll_pulsa_data_IDFN;

    @BindView(R.id.ll_pulsa_data_IDFC) LinearLayout ll_pulsa_data_IDFC;

    @BindView(R.id.ll_pulsa_data_IDM) LinearLayout ll_pulsa_data_IDM;

    @BindView(R.id.ll_pulsa_data_IDP) LinearLayout ll_pulsa_data_IDP;

    @BindView(R.id.ll_pulsa_data_IDT) LinearLayout ll_pulsa_data_IDT;

    @BindView(R.id.ll_pulsa_data_IDU) LinearLayout ll_pulsa_data_IDU;

    @BindView(R.id.ll_pulsa_data_IDUA) LinearLayout ll_pulsa_data_IDUA;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_pulsa_data_indosat);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Pulsa Data Indosat");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListPulsaDataIndosat.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListPulsaDataIndosat.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListPulsaDataIndosat.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDY)
    public void onClickIDY() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDY.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDF)
    public void onClickIDF() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDF.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDFN)
    public void onClickIDFN() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDFN.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDFC)
    public void onClickIDFC() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDFC.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDM)
    public void onClickIDM() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDM.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDP)
    public void onClickIDP() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDP.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDT)
    public void onClickIDT() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDT.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDU)
    public void onClickIDU() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDU.class));
    }

    @OnClick(R.id.ll_pulsa_data_IDUA)
    public void onClickIDUA() {
        startActivity(new Intent(ListPulsaDataIndosat.this, PulsaDataIndosatIDUA.class));
    }
}
