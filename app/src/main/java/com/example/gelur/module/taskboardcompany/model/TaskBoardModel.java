package com.example.gelur.module.taskboardcompany.model;

import android.content.Context;

import com.example.gelur.module.taskboardcompany.presenter.listener.OnLoadTaskFinished;
import com.example.gelur.module.taskboardcompany.rest.TaskBoardRestClient;
import com.example.gelur.module.timeline.rest.TimelineRestClient;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.TaskBoardPojo;
import com.example.gelur.pojo.TimelinePojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskBoardModel implements TaskBoardModelInterface {
    @Override
    public void loadDataTaskBoard(Context context, String company_id, OnLoadTaskFinished listener) {
        Call<FormaterResponse> taskboardResponse = TaskBoardRestClient.get().get_taskBoard(company_id);

        taskboardResponse.enqueue(new Callback<FormaterResponse>() {
            @Override
            public void onResponse(Call<FormaterResponse> call, Response<FormaterResponse> response) {
                FormaterResponse resource = response.body();
                List<TaskBoardPojo> taskBoardPojoList = resource.getTaskboard();
                listener.onLoadTaskBoardSuccess(taskBoardPojoList);
            }

            @Override
            public void onFailure(Call<FormaterResponse> call, Throwable t) {
                listener.onLoadTaskBoardFailed("Error Response");
            }
        });
    }
}
