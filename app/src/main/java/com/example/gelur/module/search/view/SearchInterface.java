package com.example.gelur.module.search.view;

public interface SearchInterface {
    void onSuccessSearch();
    void onFailSearch();
}
