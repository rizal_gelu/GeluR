package com.example.gelur.module.workplace.presenter.listener;

import com.example.gelur.pojo.WorkplacePojo;

public interface OnSaveWorkplaceFinishedListener {
    void onSaveWorkplaceSuccess(WorkplacePojo workplacePojo);
    void onSaveWorkplaceFailed(String message);
}
