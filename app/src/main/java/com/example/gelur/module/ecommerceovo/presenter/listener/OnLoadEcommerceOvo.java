package com.example.gelur.module.ecommerceovo.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadEcommerceOvo {
    void RequestTransferSaldoOvoSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferSaldoOvoGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickOvo(String data);
    void ResponseListPriceOvoSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceOvoGagal(String m);
}
