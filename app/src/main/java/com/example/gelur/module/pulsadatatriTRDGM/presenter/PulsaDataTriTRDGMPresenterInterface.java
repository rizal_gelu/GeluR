package com.example.gelur.module.pulsadatatriTRDGM.presenter;

import android.content.Context;

public interface PulsaDataTriTRDGMPresenterInterface {
    void onRequesttransferpulsaDataTriTRDGM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRDGM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
