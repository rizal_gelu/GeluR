package com.example.gelur.module.pulsadataindosatIDUA.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDUA.model.PulsaDataIndosatIDUAModel;
import com.example.gelur.module.pulsadataindosatIDUA.presenter.listener.OnLoadPulsaDataIndosatIDUA;
import com.example.gelur.module.pulsadataindosatIDUA.view.PulsaDataIndosatIDUAInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDUAPresenter implements PulsaDataIndosatIDUAPresenterInterface, OnLoadPulsaDataIndosatIDUA {

    private WeakReference<PulsaDataIndosatIDUAInterface> view;
    private PulsaDataIndosatIDUAModel model;

    public PulsaDataIndosatIDUAPresenter(PulsaDataIndosatIDUAInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDUAInterface>(view);
        this.model = new PulsaDataIndosatIDUAModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDUASukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDUASukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDUAGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDUAGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDUA(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDUASukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDUASukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDUAGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDUA(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDUA(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDUA(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDUA(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
