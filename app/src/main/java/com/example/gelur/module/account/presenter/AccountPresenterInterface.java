package com.example.gelur.module.account.presenter;

import android.content.Context;

public interface AccountPresenterInterface {
    void loadProfileDetail(Context context, String email);
    void loadFeedTimeline(Context context, String email);
}
