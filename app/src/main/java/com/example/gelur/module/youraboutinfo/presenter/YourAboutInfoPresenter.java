package com.example.gelur.module.youraboutinfo.presenter;

import com.example.gelur.module.youraboutinfo.model.YourAboutInfoModel;
import com.example.gelur.module.youraboutinfo.presenter.listener.OnLoadYourAboutInfoListener;
import com.example.gelur.module.youraboutinfo.view.YourAboutInfoInterface;
import com.example.gelur.pojo.WorkplacePojo;

import java.lang.ref.WeakReference;

public class YourAboutInfoPresenter implements YourAboutInfoPresenterInterface, OnLoadYourAboutInfoListener {

    private WeakReference<YourAboutInfoInterface> view;
    private YourAboutInfoModel model;

    public YourAboutInfoPresenter(YourAboutInfoInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new YourAboutInfoModel();
    }

    @Override
    public void addWorkplace() {
        if(null != view.get()) view.get().goToAddWorkplace();
    }

    @Override
    public void editWorkplace() {

    }

    @Override
    public void addCollege() {
        if(null != view.get()) view.get().goToAddCollege();
    }

    @Override
    public void editCollege() {

    }

    @Override
    public void addHighSchool() {
        if(null != view.get()) view.get().goToAddHighSchool();
    }

    @Override
    public void editHighSchool() {

    }

    @Override
    public void addCity() {
        if(null != view.get()) view.get().goToAddCollege();
    }

    @Override
    public void editCity() {

    }

    @Override
    public void editContactInfo() {
        if(null != view.get()) view.get().goToEditContactInfo();
    }

    @Override
    public void onLoadSuccessWorkplace(WorkplacePojo workplacePojo) {

    }

    @Override
    public void onLoadFailedWorkplace(String message) {

    }
}
