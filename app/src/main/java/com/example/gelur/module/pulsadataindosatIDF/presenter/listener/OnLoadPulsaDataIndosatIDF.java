package com.example.gelur.module.pulsadataindosatIDF.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataIndosatIDF {
    void RequestTransferPulsaDataIndosatIDFSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataIndosatIDFGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataIndosatIDF(String data);
    void ResponseListPricePulsaDataIndosatIDFSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataIndosatIDFGagal(String m);
}
