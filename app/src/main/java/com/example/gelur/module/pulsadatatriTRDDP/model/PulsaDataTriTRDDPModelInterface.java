package com.example.gelur.module.pulsadatatriTRDDP.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRD.presenter.listener.OnLoadPulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;

public interface PulsaDataTriTRDDPModelInterface {
    void RequestTransferPulsaDataTriTRDDP(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDDP listener);
    void RequestListPricePulsaDataTriTRDDP(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDDP listener);
}
