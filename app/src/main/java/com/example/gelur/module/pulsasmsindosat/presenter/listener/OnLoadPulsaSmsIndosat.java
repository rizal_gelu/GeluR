package com.example.gelur.module.pulsasmsindosat.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaSmsIndosat {
    void RequestTransferPulsaSmsIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaSmsIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaSmsIndosat(String data);
    void ResponseListPricePulsaSmsIndosatSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaSmsIndosatGagal(String m);
}
