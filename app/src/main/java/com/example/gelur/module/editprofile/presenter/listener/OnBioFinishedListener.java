package com.example.gelur.module.editprofile.presenter.listener;

import com.example.gelur.pojo.BioPojo;

public interface OnBioFinishedListener {
    void onPostBioSuccess(BioPojo bioPojo);
    void onPostBioFail(String message);
}
