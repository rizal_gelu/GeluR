package com.example.gelur.module.login.presenter;

import android.content.Context;

import com.example.gelur.module.login.presenter.listener.OnLoginFinishedListener;
import com.example.gelur.module.login.view.LoginInterface;
import com.example.gelur.module.login.model.LoginModel;
import com.example.gelur.pojo.UsersPojo;

import java.lang.ref.WeakReference;
import java.util.List;

public class LoginPresenter implements LoginPresenterInterface, OnLoginFinishedListener{

    private WeakReference<LoginInterface> view;
    private LoginModel model;

    public LoginPresenter(LoginInterface view) {
        this.view = new WeakReference<LoginInterface>(view);
        this.model = new LoginModel();
    }

    @Override
    public void onLoginClicked(Context context, String email, String password) {
        model.checkUser(context, email, password, this);
    }

    @Override
    public void onSignUpClicked() {
        if(null != view.get()) view.get().goToSignUpScreen();
    }

    @Override
    public void onForgotPasswordClicked() {
        if (null != view.get()) view.get().goToForgotPasswordScreen();
    }

    @Override
    public void onLoginSuccess(List<UsersPojo> user) {
        if (view.get() != null) view.get().onLoginSuccess(user);
    }

    @Override
    public void onLoginFailed(String message) {
        if (view.get() != null) view.get().onLoginFailed(message);
    }
}
