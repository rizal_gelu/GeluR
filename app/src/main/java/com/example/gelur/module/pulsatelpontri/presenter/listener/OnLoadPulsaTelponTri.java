package com.example.gelur.module.pulsatelpontri.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaTelponTri {
    void RequestTransferPulsatelponTriSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsatrlponTriGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaTelponTri(String data);
    void ResponseListPricePulsaTelponTriSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaTelponTriGagal(String m);
}
