package com.example.gelur.module.phone.view;

public interface PhoneInterface {
    void onSavePhoneSuccess();
    void onSavePhoneFail(String message);
}
