package com.example.gelur.module.posttimeline.presenter.listener;

import com.example.gelur.pojo.PostTimelinePojo;

public interface OnPostTimelineFinishedListener {
    void onPostTimelineSuccess(PostTimelinePojo postTimelinePojo);
    void onPostTimelineFailed(String message);
}
