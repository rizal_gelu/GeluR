package com.example.gelur.module.gameml.presenter;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.model.EcommerceOvoModel;
import com.example.gelur.module.ecommerceovo.view.EcommerceOvoInterface;
import com.example.gelur.module.gameml.model.GameMLModel;
import com.example.gelur.module.gameml.presenter.listener.OnLoadGameML;
import com.example.gelur.module.gameml.view.GameMLInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class GameMLPresenter implements GameMLPresenterInterface, OnLoadGameML {

    private WeakReference<GameMLInterface> view;
    private GameMLModel model;

    public GameMLPresenter(GameMLInterface view) {
        this.view = new WeakReference<GameMLInterface>(view);
        this.model = new GameMLModel();
    }

    @Override
    public void onRequesttransferDiamondML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDiamondML(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceGameMl(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceGameML(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferDiamondMLSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferDiamondMLSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferDiamondMLGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferDiamondMLGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickGameML(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceGameMLSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceGameML(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceGameMLGagal(String m) {

    }
}
