package com.example.gelur.module.pulsadatatriTRDAON.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRD.model.PulsaDataTriTRDModel;
import com.example.gelur.module.pulsadatatriTRD.presenter.PulsaDataTriTRDPresenterInterface;
import com.example.gelur.module.pulsadatatriTRD.presenter.listener.OnLoadPulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRD.view.PulsaDataTriTRDInterface;
import com.example.gelur.module.pulsadatatriTRDAON.model.PulsaDataTriTRDAONModel;
import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDAON.view.PulsaDataTriTRDAONInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDAONPresenter implements PulsaDataTriTRDAONPresenterInterface, OnLoadPulsaDataTriTRDAON {

    private WeakReference<PulsaDataTriTRDAONInterface> view;
    private PulsaDataTriTRDAONModel model;

    public PulsaDataTriTRDAONPresenter(PulsaDataTriTRDAONInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDAONInterface>(view);
        this.model = new PulsaDataTriTRDAONModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDAON(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDAON(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDAON(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDAON(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDAONSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDAONSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDAONGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDAONGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDAON(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDAONSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDAONSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDAONGagal(String m) {

    }
}
