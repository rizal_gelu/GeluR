package com.example.gelur.module.pulsatelponxl.model;

import android.content.Context;

import com.example.gelur.module.pulsaregulerxl.presenter.listener.OnLoadPulsaRegulerXL;
import com.example.gelur.module.pulsatelpontri.presenter.listener.OnLoadPulsaTelponTri;
import com.example.gelur.module.pulsatelponxl.presenter.listener.OnLoadPulsaTelponXL;

public interface PulsaTelponXLModelInterface {
    void RequestTransferPulsaTelponXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponXL listener);
    void RequestListPricePulsaTelponXL(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponXL listener);
}
