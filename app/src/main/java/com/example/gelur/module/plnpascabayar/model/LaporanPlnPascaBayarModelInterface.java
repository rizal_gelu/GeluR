package com.example.gelur.module.plnpascabayar.model;

import android.content.Context;

import com.example.gelur.module.plnpascabayar.presenter.listener.OnLoadLaporanPLNPascaBayar;

public interface LaporanPlnPascaBayarModelInterface {
    void RequestBayarPLNPascaBayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadLaporanPLNPascaBayar listener);

}
