package com.example.gelur.module.pulsatelpontelkomsel.presenter;

import android.content.Context;

public interface PulsaTelponTelkomselPresenterInterface {
    void onRequesttransferpulsatelponTelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaTelponTelkomsel(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);

}
