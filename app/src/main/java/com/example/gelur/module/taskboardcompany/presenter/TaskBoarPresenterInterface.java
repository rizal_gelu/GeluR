package com.example.gelur.module.taskboardcompany.presenter;

import android.content.Context;

public interface TaskBoarPresenterInterface {
    void onLoadTaskBoard(Context mContext, String compnay_id);
    void onClickBtnAddJob();
}
