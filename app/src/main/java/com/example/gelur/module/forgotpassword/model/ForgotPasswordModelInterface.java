package com.example.gelur.module.forgotpassword.model;

import com.example.gelur.module.forgotpassword.presenter.listener.OnForgotPasswordFinishedListener;

public interface ForgotPasswordModelInterface {
    void forgotPasswordRequest(String email, OnForgotPasswordFinishedListener listener);
}
