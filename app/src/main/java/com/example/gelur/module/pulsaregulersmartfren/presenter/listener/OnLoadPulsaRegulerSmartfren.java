package com.example.gelur.module.pulsaregulersmartfren.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaRegulerSmartfren {
    void RequestTransferPulsaRegulerSmartfrenSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaRegulerSmartfrenGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaRegulerSmartfren(String data);
    void ResponseListPricePulsaRegulerSmartfrenSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaRegulerSmartfrenGagal(String m);
}
