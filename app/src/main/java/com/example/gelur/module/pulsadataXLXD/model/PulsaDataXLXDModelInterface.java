package com.example.gelur.module.pulsadataXLXD.model;

import android.content.Context;

import com.example.gelur.module.pulsadataXLPXD.presenter.listener.OnLoadPulsaDataXLPXD;
import com.example.gelur.module.pulsadataXLXD.presenter.listener.OnLoadPulsaDataXLXD;

public interface PulsaDataXLXDModelInterface {
    void RequestTransferPulsaDataXLXD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataXLXD listener);
    void RequestListPricePulsaDataXLXD(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataXLXD listener);

}
