package com.example.gelur.module.loginpulsa.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.login.presenter.LoginPresenter;
import com.example.gelur.module.login.view.LoginInterface;
import com.example.gelur.module.loginpulsa.presenter.LoginPulsaPresenter;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.snackbar.Snackbar;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginPulsa extends AppCompatActivity implements LoginPulsaInterface {

    @BindView(R.id.activity_login_pulsa_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_login_pulsa_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.kode_reseller)
    EditText etKodereseller;

    @BindView(R.id.password) EditText etPassword;

    @BindView(R.id.pin_1) EditText etPin;

    @BindView(R.id.activity_login_layout_content)
    View layoutContent;

    @BindView(R.id.activity_login_layout_loading) View layoutLoading;

    LoginPulsaPresenter presenter;
    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login_pulsa);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.title_activity_login);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new LoginPulsaPresenter(this);

        showContent(true);
    }

    @OnClick(R.id.activity_login_btn_login_pulsa)
    public void login(View v) {
        loadingDialog.show();

        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        System.out.println("Random value in int from "+min+" to "+max+ ":");
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        String time = param_split;
        String perintah = "sal";
        String kodereseler = etKodereseller.getText().toString();
        String password = etPassword.getText().toString();
        String pin = etPin.getText().toString();
        String req = "cmd";

        if(kodereseler.isEmpty()) {
            Snackbar.make(coordinatorLayout, "Email is not valid", Snackbar.LENGTH_SHORT).show();
        } else if (password.isEmpty() || password.toString().length() < 6) {
            Snackbar.make(coordinatorLayout, "Password length must be least six (6) characters", Snackbar.LENGTH_SHORT).show();
        } else {
            presenter.LoginPulsaSubmit(this, req, kodereseler, perintah, time, pin, password);
            String name = AppsConstant.KEY_NAME;
            AppsUtil sessionManagement = new AppsUtil(this);
            sessionManagement.createLoginPulsaSession(kodereseler, pin, password);
        }
    }

    @Override
    public void onLoginFailed(String message) {

    }

    @Override
    public void onLoginSuccess(ResponsNonTransaksi responsNonTransaksi) {
        startActivity(new Intent(LoginPulsa.this, TransferPulsa.class));
    }

    private void showContent(boolean yes) {
        if (yes) {
            layoutContent.setVisibility(View.VISIBLE);
            layoutLoading.setVisibility(View.GONE);
        } else {
            layoutContent.setVisibility(View.GONE);
            layoutLoading.setVisibility(View.VISIBLE);
        }
    }
}
