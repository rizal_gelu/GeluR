package com.example.gelur.module.plnpascabayar.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.module.laporantransaksi.view.LaporanTransaksi;
import com.example.gelur.module.listbpjsppob.view.BPJS;
import com.example.gelur.module.plnpascabayar.presenter.PlnPascaBayarPresenter;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class PlnPascaBayar extends AppCompatActivity implements PlnPascaBayarInterface {

    @BindView(R.id.coordinator_pln_pasca_bayar)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_pln_pasca_bayar) Toolbar toolbar;

    @BindView(R.id.content_pln_pasca_bayar)
    View viewContent;

    @BindView(R.id.layout_loading_pln_pasca_bayar) View viewLoading;

    @BindView(R.id.Et_nomor_pln)
    EditText Et_nomor_pln;

    @BindView(R.id.qty_pln) EditText qty_pln;

    @BindView(R.id.submit_request_check_pln)
    AppCompatButton submit_request_check_pln;

    AlertDialog loadingDialog;
    AlertDialog.Builder dialogBuilder;

    PlnPascaBayarPresenter presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pln_pasca_bayar);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - PPOB PLN");

        submit_request_check_pln.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkTagihanPLNPascaBayar();
            }
        });
        presenter = new PlnPascaBayarPresenter(this);
        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    public void checkTagihanPLNPascaBayar() {
        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String req = "topup";
        String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
        String qty = qty_pln.getText().toString();
        String refid = String.valueOf(random_int);
        String get_no_hp = Et_nomor_pln.getText().toString();
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
        String nominal_cek = "CEKPLN";
        String counter = qty_pln.getText().toString();

        presenter.onRequestCheckPLNPascaBayar(this, req, kode_reseller, nominal_cek, get_no_hp, counter, qty, refid, time, pin, password);
    }

    @Override
    public void responCheckTagihanPLNPascaBayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();
        String get_msisdn = responsTransaksiPulsa.getMsisdn();

        Intent intent = new Intent(PlnPascaBayar.this, LaporanPLNPascaBayar.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);
        intent.putExtra("msisdn", get_msisdn);

        startActivity(intent);
    }

    @Override
    public void responCheckTagihanPLNPascaBayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();
        String get_msisdn = responsTransaksiPulsa.getMsisdn();

        Intent intent = new Intent(PlnPascaBayar.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);
        intent.putExtra("msisdn", get_msisdn);

        startActivity(intent);
    }


}
