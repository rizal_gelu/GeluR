package com.example.gelur.module.workplace.models;

import android.content.Context;

import com.example.gelur.module.bio.rest.BioRestClient;
import com.example.gelur.module.workplace.presenter.listener.OnSaveWorkplaceFinishedListener;
import com.example.gelur.module.workplace.rest.WorkplaceRestClient;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.WorkplacePojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WorkplaceModel implements WorkplaceModelInterface {
    @Override
    public void clickSaveWorkplace(Context context, String email, String name_company, String job_title, String location, OnSaveWorkplaceFinishedListener listener) {
        Call<WorkplacePojo> workplace = WorkplaceRestClient.get().postWorkplace(email, name_company, job_title, location);
        workplace.enqueue(new Callback<WorkplacePojo>() {
            @Override
            public void onResponse(Call<WorkplacePojo> call, Response<WorkplacePojo> response) {
                if (response.isSuccessful()) {
                    listener.onSaveWorkplaceSuccess(response.body());
                } else {
                    listener.onSaveWorkplaceFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<WorkplacePojo> call, Throwable t) {
                listener.onSaveWorkplaceFailed(t.getMessage());
            }
        });
    }
}
