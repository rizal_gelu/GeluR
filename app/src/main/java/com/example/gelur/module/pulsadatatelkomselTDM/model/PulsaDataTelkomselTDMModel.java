package com.example.gelur.module.pulsadatatelkomselTDM.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatelkomselTDF.model.PulsaDataTelkomselTDFModelInterface;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;
import com.example.gelur.module.pulsadatatelkomselTDF.rest.PulsaDataTelkomselTDFRestClient;
import com.example.gelur.module.pulsadatatelkomselTDM.presenter.listener.OnLoadPulsaDataTelkomselTDM;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataTelkomselTDMModel implements PulsaDataTelkomselTDMModelInterface {
    @Override
    public void RequestTransferPulsaDataTelkomselTDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTelkomselTDM listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataTelkomselTDFRestClient.get().req_transfer_pulsa_data_telkomsel_tdf(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataTelkomselTDMSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataTelkomselTDMGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTelkomselTDM(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTelkomselTDM listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTelkomselTDFRestClient.get().getPriceListPulsaDataTelkomselTDF(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataTelkomselTDMSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataTelkomselTDMGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataTelkomselTDMGagal(t.getMessage());
            }
        });
    }
}
