package com.example.gelur.module.pulsadatatelkomselTDB.presenter;

import android.content.Context;

public interface PulsaDataTelkomselTDBPresenterInterface {
    void onRequesttransferPulsaDataTelkomselTDB(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTelkomselTDB(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
