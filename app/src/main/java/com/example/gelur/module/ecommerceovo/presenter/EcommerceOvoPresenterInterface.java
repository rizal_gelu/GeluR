package com.example.gelur.module.ecommerceovo.presenter;

import android.content.Context;

public interface EcommerceOvoPresenterInterface {
    void onRequesttransfersaldoOvo(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPriceOvo(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
