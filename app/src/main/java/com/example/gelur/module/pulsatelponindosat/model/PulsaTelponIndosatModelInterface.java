package com.example.gelur.module.pulsatelponindosat.model;

import android.content.Context;

import com.example.gelur.module.pulsasmstelkomsel.presenter.listener.OnLoadPulsaSmsTelkomsel;
import com.example.gelur.module.pulsatelponindosat.presenter.listener.OnLoadPulsaTelponIndosat;
import com.example.gelur.module.pulsatelpontelkomsel.presenter.listener.OnLoadPulsaTelponTelkomsel;

public interface PulsaTelponIndosatModelInterface {
    void RequestTransferPulsaTelponIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponIndosat listener);
    void RequestListPricePulsaTelponIndosat(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponIndosat listener);
}
