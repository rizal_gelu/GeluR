package com.example.gelur.module.taskboardadd.rest;

import com.example.gelur.util.AppsConstant;

public class TaskBoardAddAPIURLConstant {
    public static final String BASE_API = AppsConstant.BASE_API_URL + "v1/company/";
    public static final String POST_TASK_BOARD_ADD = BASE_API + "post_task_job/";
}
