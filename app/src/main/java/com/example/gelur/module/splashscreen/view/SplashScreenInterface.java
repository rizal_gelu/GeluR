package com.example.gelur.module.splashscreen.view;

public interface SplashScreenInterface {

    void goToTimeline();
    void goToLogin();
    void isLoggedIn();
}
