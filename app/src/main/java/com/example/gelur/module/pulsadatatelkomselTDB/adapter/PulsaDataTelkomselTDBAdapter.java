package com.example.gelur.module.pulsadatatelkomselTDB.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatelkomselTDB.presenter.listener.OnLoadPulsaDataTelkomselTDB;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;

import java.util.List;

public class PulsaDataTelkomselTDBAdapter extends RecyclerView.Adapter<PulsaDataTelkomselTDBAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataTelkomselTDB listener;
    List<String> stringList;

    public PulsaDataTelkomselTDBAdapter(Activity activity, List<String> datum, OnLoadPulsaDataTelkomselTDB listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_telkomsel_tdb, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_telkomsel_tdb.setText(data);

        holder.ll_kumpulan_option_pulsa_data_telkomsel_tdb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataTelkomselTDB(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_telkomsel_tdb;
        public final TextView pulsa_data_telkomsel_tdb;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_telkomsel_tdb = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_telkomsel_tdb);
            pulsa_data_telkomsel_tdb = (TextView) view.findViewById(R.id.pulsa_data_telkomsel_tdb);
        }
    }
}
