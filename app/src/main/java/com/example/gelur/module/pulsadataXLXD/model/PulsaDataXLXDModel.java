package com.example.gelur.module.pulsadataXLXD.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataXLPXD.presenter.listener.OnLoadPulsaDataXLPXD;
import com.example.gelur.module.pulsadataXLPXD.rest.PulsaDataXLPXDRestClient;
import com.example.gelur.module.pulsadataXLXD.presenter.listener.OnLoadPulsaDataXLXD;
import com.example.gelur.module.pulsadataXLXD.rest.PulsaDataXLXDRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataXLXDModel implements PulsaDataXLXDModelInterface {
    @Override
    public void RequestTransferPulsaDataXLXD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataXLXD listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataXLXDRestClient.get().req_transfer_pulsa_data_xl_xd(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataXLXDSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataXLXDGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataXLXD(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataXLXD listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataXLXDRestClient.get().getPriceListPulsaDataXLXD(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataXLXDSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataXLXDGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataXLXDGagal(t.getMessage());
            }
        });
    }
}
