package com.example.gelur.module.pulsatelponxl.presenter;

import android.content.Context;

import com.example.gelur.module.pulsatelponxl.model.PulsaTelponXLModel;
import com.example.gelur.module.pulsatelponxl.presenter.listener.OnLoadPulsaTelponXL;
import com.example.gelur.module.pulsatelponxl.view.PulsaTelponXLInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaTelponXLPresenter implements PulsaTelponXLPresenterInterface, OnLoadPulsaTelponXL {
    private WeakReference<PulsaTelponXLInterface> view;
    private PulsaTelponXLModel model;

    public PulsaTelponXLPresenter(PulsaTelponXLInterface view) {
        this.view = new WeakReference<PulsaTelponXLInterface>(view);
        this.model = new PulsaTelponXLModel();
    }

    @Override
    public void onRequesttransferpulsaTelponXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaTelponXL(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaTelponXL(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaTelponXL(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaTelponXLSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaTelponXLSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaTelponXLGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaTelponXLGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaTelponXL(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaTelponXLSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaTelponXL(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaTelponXLGagal(String m) {

    }
}
