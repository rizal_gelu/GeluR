package com.example.gelur.module.youraboutinfo.presenter;

public interface YourAboutInfoPresenterInterface {
    void addWorkplace();
    void editWorkplace();
    void addCollege();
    void editCollege();
    void addHighSchool();
    void editHighSchool();
    void addCity();
    void editCity();
    void editContactInfo();
}
