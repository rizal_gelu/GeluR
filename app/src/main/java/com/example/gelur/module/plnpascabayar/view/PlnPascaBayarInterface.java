package com.example.gelur.module.plnpascabayar.view;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PlnPascaBayarInterface {
    void checkTagihanPLNPascaBayar();

    void responCheckTagihanPLNPascaBayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void responCheckTagihanPLNPascaBayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

}
