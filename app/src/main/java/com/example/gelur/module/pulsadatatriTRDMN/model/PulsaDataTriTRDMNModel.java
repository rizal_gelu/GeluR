package com.example.gelur.module.pulsadatatriTRDMN.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDAON.rest.PulsaDataTriTRDAONRestClient;
import com.example.gelur.module.pulsadatatriTRDM.rest.PulsaDataTriTRDMRestClient;
import com.example.gelur.module.pulsadatatriTRDMN.presenter.listener.OnLoadPulsaDataTriTRDMN;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataTriTRDMNModel implements PulsaDataTriTRDMNModelInterface {
    @Override
    public void RequestTransferPulsaDataTriTRDMN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDMN listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataTriTRDMRestClient.get().req_transfer_pulsa_data_tri_trdm(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataTriTRDMNSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataTriTRDMNGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTriTRDMN(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDMN listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTriTRDMRestClient.get().getPriceListPulsaDataTriTRDM(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataTriTRDMNSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataTriTRDMNGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataTriTRDMNGagal(t.getMessage());
            }
        });
    }
}
