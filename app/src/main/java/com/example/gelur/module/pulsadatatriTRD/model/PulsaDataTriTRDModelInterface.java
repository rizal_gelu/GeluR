package com.example.gelur.module.pulsadatatriTRD.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDF.presenter.listener.OnLoadPulsaDataIndosatIDF;
import com.example.gelur.module.pulsadatatriTRD.presenter.listener.OnLoadPulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;

public interface PulsaDataTriTRDModelInterface {
    void RequestTransferPulsaDataTriTRD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRD listener);
    void RequestListPricePulsaDataTriTRD(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRD listener);
}
