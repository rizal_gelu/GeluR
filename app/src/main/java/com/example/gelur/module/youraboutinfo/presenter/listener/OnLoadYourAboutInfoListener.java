package com.example.gelur.module.youraboutinfo.presenter.listener;

import com.example.gelur.pojo.WorkplacePojo;

public interface OnLoadYourAboutInfoListener {
    void onLoadSuccessWorkplace(WorkplacePojo workplacePojo);
    void onLoadFailedWorkplace(String message);


}
