package com.example.gelur.module.listbpjsppob.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.ecommerceovo.presenter.EcommerceOvoPresenter;
import com.example.gelur.module.ecommerceovo.view.EcommerceOvo;
import com.example.gelur.module.laporantransaksi.view.LaporanTransaksi;
import com.example.gelur.module.listbpjsppob.presenter.BPJSPresenter;
import com.example.gelur.module.pulsaregulertelkomsel.view.PulsaRegulerTelkomsel;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class BPJS extends AppCompatActivity implements BPJSInterface {

    @BindView(R.id.coordinator_list_bpjs)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_bpjs)
    Toolbar toolbar;

    @BindView(R.id.content_bpjs)
    View viewContent;

    @BindView(R.id.layout_loading_bpjs) View viewLoading;

    @BindView(R.id.Et_nomor_bpjs)
    EditText Et_nomor_bpjs;

    @BindView(R.id.qty_bpjs) EditText qty_bpjs;

    @BindView(R.id.submit_request_check_bpjs) AppCompatButton submit_request_check_bpjs;

    TextView status_sukses, sn_transaksi_sukses, harga_product_sukses, saldo_akhir_sukses;

    AlertDialog loadingDialog;

    AlertDialog.Builder dialogBuilder;

    BPJSPresenter presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ppob_bpjs);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD");

        presenter = new BPJSPresenter(this);
        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.submit_request_check_bpjs)
    public void checkTagihan() {
        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        System.out.println("Random value in int from "+min+" to "+max+ ":");
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String req = "topup";
        String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
        String qty = qty_bpjs.getText().toString();
        String refid = String.valueOf(random_int);
        String get_no_hp = Et_nomor_bpjs.getText().toString();
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
        String nominal_cek = "CEKBPJS";
        String counter = qty_bpjs.getText().toString();

        presenter.onRequestCheckBPJS(this, req, kode_reseller, nominal_cek, get_no_hp, counter, qty, refid, time, pin, password);
    }

    @Override
    public void responCheckTagihanSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popUpDialog = getLayoutInflater().inflate(R.layout.popup_check_tagihan_bpjs, null);

        status_sukses = (TextView) popUpDialog.findViewById(R.id.status_sukses);
        sn_transaksi_sukses = (TextView) popUpDialog.findViewById(R.id.sn_transaksi_sukses);
        harga_product_sukses = (TextView) popUpDialog.findViewById(R.id.harga_product_sukses);

        status_sukses.setText(responsTransaksiPulsa.getStatus());
        sn_transaksi_sukses.setText(responsTransaksiPulsa.getSn());
        harga_product_sukses.setText(responsTransaksiPulsa.getHarga());

        dialogBuilder.setView(popUpDialog).show();
        loadingDialog = dialogBuilder.create();

        bayarBpjs(responsTransaksiPulsa.getMsisdn());
    }

    @Override
    public void responCheckTagihanGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(BPJS.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void bayarBpjs(String msisdn) {
        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        System.out.println("Random value in int from "+min+" to "+max+ ":");
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(BPJS.this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String req = "topup";
        String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
        String qty = qty_bpjs.getText().toString();
        String refid = String.valueOf(random_int);
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
        String nominal = "BYRBPJS";
        String counter = qty_bpjs.getText().toString();

        presenter.onRequestBayarBPJS(BPJS.this, req, kode_reseller, nominal, msisdn,counter, qty, refid, time, pin, password);
    }

    @Override
    public void responBayarBpjsSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(BPJS.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void responBayarBpjsGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(BPJS.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }
}
