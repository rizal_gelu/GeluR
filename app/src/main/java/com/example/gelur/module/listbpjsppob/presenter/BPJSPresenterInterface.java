package com.example.gelur.module.listbpjsppob.presenter;

import android.content.Context;

public interface BPJSPresenterInterface {
    void onRequestCheckBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequestBayarBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
}
