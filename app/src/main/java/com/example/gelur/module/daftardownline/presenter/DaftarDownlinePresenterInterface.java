package com.example.gelur.module.daftardownline.presenter;

import android.content.Context;

public interface DaftarDownlinePresenterInterface {
    void onRegsitrasiMember(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
