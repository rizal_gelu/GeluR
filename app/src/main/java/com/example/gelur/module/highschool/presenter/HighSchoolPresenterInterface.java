package com.example.gelur.module.highschool.presenter;

import android.content.Context;

public interface HighSchoolPresenterInterface {
    void onSaveHighSchoolClicked(Context context, String email, String high_school_name, String year_graduate, String graduated, String description);
    void onCancelHighSchoolClicked();
}
