package com.example.gelur.module.pulsadataXLXD.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataXLXD {
    void RequestTransferPulsaDataXLXDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataXLXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataXLXD(String data);
    void ResponseListPricePulsaDataXLXDSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataXLXDGagal(String m);
}
