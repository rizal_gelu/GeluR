package com.example.gelur.module.bio.presenter;

import android.content.Context;

import com.example.gelur.pojo.BioPojo;

public interface BioPresenterInterface {
    void onLoadBio(Context context, String email);
    void onSaveClicked(Context context, String email, String bio);
    void onCancelClicked();
}
