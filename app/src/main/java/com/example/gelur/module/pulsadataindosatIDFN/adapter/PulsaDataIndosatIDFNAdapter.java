package com.example.gelur.module.pulsadataindosatIDFN.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDFC.presenter.listener.OnLoadPulsaDataIndosatIDFC;
import com.example.gelur.module.pulsadataindosatIDFN.presenter.listener.OnLoadPulsaDataIndosatIDFN;

import java.util.List;

public class PulsaDataIndosatIDFNAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDFNAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDFN listener;
    List<String> stringList;

    public PulsaDataIndosatIDFNAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDFN listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idfn, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idfn.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idfn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDFN(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idfn;
        public final TextView pulsa_data_indosat_idfn;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idfn = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idfn);
            pulsa_data_indosat_idfn = (TextView) view.findViewById(R.id.pulsa_data_indosat_idfn);
        }
    }
}
