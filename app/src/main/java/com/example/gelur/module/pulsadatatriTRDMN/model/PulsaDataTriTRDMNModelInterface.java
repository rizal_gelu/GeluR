package com.example.gelur.module.pulsadatatriTRDMN.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDMN.presenter.listener.OnLoadPulsaDataTriTRDMN;

public interface PulsaDataTriTRDMNModelInterface {
    void RequestTransferPulsaDataTriTRDMN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDMN listener);
    void RequestListPricePulsaDataTriTRDMN(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDMN listener);
}
