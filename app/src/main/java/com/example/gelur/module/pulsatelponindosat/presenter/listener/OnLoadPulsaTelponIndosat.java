package com.example.gelur.module.pulsatelponindosat.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaTelponIndosat {
    void RequestTransferPulsaTelponIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaTelponIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaTelponIndosat(String data);
    void ResponseListPricePulsaTelponIndosatSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaTelponIndosatagal(String m);
}
