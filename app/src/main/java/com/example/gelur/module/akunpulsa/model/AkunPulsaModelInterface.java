package com.example.gelur.module.akunpulsa.model;

import android.content.Context;

import com.example.gelur.module.akunpulsa.presenter.listener.OnLoadAkunPulsa;
import com.example.gelur.module.transferpulsa.presenter.listener.OnLoadCekSaldoFinished;

public interface AkunPulsaModelInterface {
    void getInformationSaldo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadAkunPulsa listener);
}
