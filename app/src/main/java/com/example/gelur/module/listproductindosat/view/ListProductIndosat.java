package com.example.gelur.module.listproductindosat.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listpulsadataindosat.view.ListPulsaDataIndosat;
import com.example.gelur.module.pulsadataindosatIDY.view.PulsaDataIndosatIDY;
import com.example.gelur.module.pulsaregulerindosat.view.PulsaRegulerIndosat;
import com.example.gelur.module.pulsasmsindosat.view.PulsaSmsIndosat;
import com.example.gelur.module.pulsatelponindosat.view.PulsaTelponIndosat;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ListProductIndosat extends AppCompatActivity implements ListProductIndosatInterface {

    @BindView(R.id.coordinator_list_product_indosat)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_list_product_indosat)
    Toolbar toolbar;

    @BindView(R.id.content_list_content_indosat)
    View viewContent;

    @BindView(R.id.layout_loading_list_product_indosat) View viewLoading;

    @BindView(R.id.kumpulan_option_list_product_indosat)
    LinearLayout kumpulan_option_list_product_indosat;

    @BindView(R.id.ll_pulsa_reguller_indosat) LinearLayout ll_pulsa_reguller_indosat;

    @BindView(R.id.ll_paket_telpon_indosat) LinearLayout ll_paket_telpon_indosat;

    @BindView(R.id.paket_telpon_indosat)
    TextView paket_telpon_indosat;

    @BindView(R.id.ll_paket_data_indosat) LinearLayout ll_paket_data_indosat;

    @BindView(R.id.paket_data_indosat) TextView paket_data_indosat;

    @BindView(R.id.ll_paket_sms_indosat) LinearLayout ll_paket_sms_indosat;

    @BindView(R.id.paket_sms_indosat) TextView paket_sms_indosat;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_product_indosat);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Produk Indosat");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListProductIndosat.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListProductIndosat.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListProductIndosat.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_pulsa_reguller_indosat)
    public void onClickPulsaRegullerIndosat() {
        startActivity(new Intent(ListProductIndosat.this, PulsaRegulerIndosat.class));
    }

    @OnClick(R.id.ll_paket_data_indosat)
    public void onClickPaketDataIndosat() {
        startActivity(new Intent(ListProductIndosat.this, ListPulsaDataIndosat.class));
    }

    @OnClick(R.id.paket_telpon_indosat)
    public void onClickPaketTelponIndosat() {
        startActivity(new Intent(ListProductIndosat.this, PulsaTelponIndosat.class));
    }

    @OnClick(R.id.ll_paket_sms_indosat)
    public void onclckPaketSmsIndosat() {
        startActivity(new Intent(ListProductIndosat.this, PulsaSmsIndosat.class));
    }
}
