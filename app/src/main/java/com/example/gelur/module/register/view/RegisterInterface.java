package com.example.gelur.module.register.view;

import com.example.gelur.pojo.UsersPojo;

public interface RegisterInterface {
    void onRegisterSuccess(UsersPojo usersPojo);
    void onRegisterFailed(String message);
}
