package com.example.gelur.module.pulsadatatriTRD.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDInterface {

    void TransferPulsaDataTriTRDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
