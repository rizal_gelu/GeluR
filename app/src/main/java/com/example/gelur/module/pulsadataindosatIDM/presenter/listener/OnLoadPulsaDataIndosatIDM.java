package com.example.gelur.module.pulsadataindosatIDM.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataIndosatIDM {
    void RequestTransferPulsaDataIndosatIDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataIndosatIDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataIndosatIDM(String data);
    void ResponseListPricePulsaDataIndosatIDMSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataIndosatIDMGagal(String m);
}
