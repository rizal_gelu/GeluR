package com.example.gelur.module.pulsadataindosatIDFN.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDFN.presenter.listener.OnLoadPulsaDataIndosatIDFN;
import com.example.gelur.module.pulsadataindosatIDY.model.PulsaDataIndosatIDYModelInterface;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;
import com.example.gelur.module.pulsadataindosatIDY.rest.PulsaDataIndosatIDYRestClient;
import com.example.gelur.module.pulsadatatelkomselTDF.rest.PulsaDataTelkomselTDFRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataIndosatIDFNModel implements PulsaDataIndosatIDFNModelInterface {
    @Override
    public void RequestTransferDataIndosatIDFN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDFN listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataIndosatIDYRestClient.get().req_transfer_pulsa_data_indosat(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataIndosatIDFNSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataIndosatIDFNGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataIndosatIDFN(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDFN listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTelkomselTDFRestClient.get().getPriceListPulsaDataTelkomselTDF(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataIndosatIDFNSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataIndosatIDFNGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataIndosatIDFNGagal(t.getMessage());
            }
        });
    }
}
