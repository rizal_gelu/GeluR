package com.example.gelur.module.listecommercegojek.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.ecommercegojekdriver.view.EcommerceGojekDriver;
import com.example.gelur.module.ecommercegojekpenumpang.view.EcommerceGojekPenumpang;
import com.example.gelur.util.AppsUtil;

public class ListEcommerceGojek extends AppCompatActivity implements ListEcommerceGojekInterface {
    @BindView(R.id.coordinator_list_gojek_ecommerce)
    CoordinatorLayout coordinator_list_gojek_ecommerce;

    @BindView(R.id.activity_toolbar_gojek_ecommerce)
    Toolbar toolbar;

    @BindView(R.id.content_list_ecommerce_gojek)
    View viewContent;

    @BindView(R.id.layout_loading_list_ecommerce_gojek) View viewLoading;

    @BindView(R.id.kumpulan_option_list_gojek)
    LinearLayout kumpulan_option_list_gojek;

    @BindView(R.id.ll_gojek_driver) LinearLayout ll_gojek_driver;
    @BindView(R.id.ll_gojek_penumpang) LinearLayout ll_gojek_penumpang;

    @BindView(R.id.gojek_driver)
    TextView gojek_driver;
    @BindView(R.id.gojek_penumpang)
    TextView gojek_penumpang;
    AlertDialog loadingDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_gojek);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - Ecommerce");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.ll_gojek_driver)
    public void onClickGojekDriver() {
        startActivity(new Intent(ListEcommerceGojek.this, EcommerceGojekDriver.class));
    }

    @OnClick(R.id.ll_gojek_penumpang)
    public void onClickGojekPenumpang() {
        startActivity(new Intent(ListEcommerceGojek.this, EcommerceGojekPenumpang.class));
    }
}
