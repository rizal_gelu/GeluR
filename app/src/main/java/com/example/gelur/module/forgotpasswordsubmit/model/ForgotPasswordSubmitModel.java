package com.example.gelur.module.forgotpasswordsubmit.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.forgotpasswordsubmit.presenter.listener.OnForgotPasswordSubmitListener;
import com.example.gelur.module.forgotpasswordsubmit.rest.ForgotPasswordSubmitRestClient;
import com.example.gelur.pojo.ForgotPasswordConfimPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordSubmitModel implements ForgotPasswordSubmitModelInterface{
    @Override
    public void OnSubmitForgotPassword(final Context context, String email, String forgot_password_code, OnForgotPasswordSubmitListener listener) {
        Call<ForgotPasswordConfimPojo> active = ForgotPasswordSubmitRestClient.get().confirm_forgot_pwd(email, forgot_password_code);
        active.enqueue(new Callback<ForgotPasswordConfimPojo>() {
            @Override
            public void onResponse(Call<ForgotPasswordConfimPojo> call, Response<ForgotPasswordConfimPojo> response) {
                if (response.isSuccessful()) {
                    listener.onForgotPasswordConfirmRequestSuccess(response.body());
                } else {
                    listener.onForgotPasswordConfirmRequestFail(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordConfimPojo> call, Throwable t) {
                listener.onForgotPasswordConfirmRequestFail(t.getMessage());
            }
        });
    }
}
