package com.example.gelur.module.pulsadatatriTRD.presenter;

import android.content.Context;

public interface PulsaDataTriTRDPresenterInterface {
    void onRequesttransferpulsaDataTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRD(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
