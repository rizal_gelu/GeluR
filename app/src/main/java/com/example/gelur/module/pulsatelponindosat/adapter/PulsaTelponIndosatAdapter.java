package com.example.gelur.module.pulsatelponindosat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsatelponindosat.presenter.listener.OnLoadPulsaTelponIndosat;

import java.util.List;

public class PulsaTelponIndosatAdapter extends RecyclerView.Adapter<PulsaTelponIndosatAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaTelponIndosat listener;
    List<String> stringList;

    public PulsaTelponIndosatAdapter(Activity activity, List<String> datum, OnLoadPulsaTelponIndosat listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_telpon_indosat, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_telpon_indosat.setText(data);

        holder.ll_kumpulan_option_pulsa_telpon_indosat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaTelponIndosat(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_telpon_indosat;
        public final TextView pulsa_telpon_indosat;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_telpon_indosat = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_telpon_indosat);
            pulsa_telpon_indosat = (TextView) view.findViewById(R.id.pulsa_telpon_indosat);
        }
    }
}
