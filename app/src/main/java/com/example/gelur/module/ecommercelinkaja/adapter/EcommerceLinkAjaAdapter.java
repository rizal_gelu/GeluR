package com.example.gelur.module.ecommercelinkaja.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.ecommercelinkaja.presenter.listener.OnLoadEcommerceLinkAja;

import java.util.List;

public class EcommerceLinkAjaAdapter extends RecyclerView.Adapter<EcommerceLinkAjaAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadEcommerceLinkAja listener;
    List<String> stringList;

    public EcommerceLinkAjaAdapter(Activity activity, List<String> datum, OnLoadEcommerceLinkAja listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ecommerce_link_aja, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(EcommerceLinkAjaAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.link_aja.setText(data);

        holder.ll_kumpulan_option_link_aja.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickLinkAja(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_link_aja;
        public final TextView link_aja;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_link_aja = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_link_aja);
            link_aja = (TextView) view.findViewById(R.id.link_aja);
        }
    }
}
