package com.example.gelur.module.login.model;

import android.content.Context;

import com.example.gelur.module.forgotpassword.presenter.listener.OnForgotPasswordFinishedListener;
import com.example.gelur.module.login.presenter.listener.OnLoginFinishedListener;
import com.example.gelur.pojo.UsersPojo;

public interface LoginModelInterface {
    void checkUser(Context context, String email, String password, OnLoginFinishedListener listener);
}
