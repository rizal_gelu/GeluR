package com.example.gelur.module.highschool.rest;

import com.example.gelur.util.AppsConstant;

public class HighSchoolAPIURLConstant {
    // POST BIO EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/users/"; // User UriSegment 2
    public static final String POST_HIGH_SCHOOL = BASE_API_USER + "post_education_high_school/"; // User UriSegment 3

    // POST BIO EndPoint
    public static final String GET_HIGH_SCHOOL = BASE_API_USER + "get_education_high_school/"; // User UriSegment 3

}
