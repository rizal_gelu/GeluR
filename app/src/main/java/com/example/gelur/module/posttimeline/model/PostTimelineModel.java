package com.example.gelur.module.posttimeline.model;

import android.content.Context;

import com.example.gelur.module.posttimeline.presenter.listener.OnPostTimelineFinishedListener;
import com.example.gelur.module.posttimeline.rest.PostTimelineRestClient;
import com.example.gelur.pojo.PostTimelinePojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostTimelineModel implements  PostTimelineModelInterface {
    @Override
    public void submitPostTimeline(final Context context, String email, String description, String file_name, final OnPostTimelineFinishedListener listener) {
        Call<PostTimelinePojo> postFedd = PostTimelineRestClient.get().doPostFeed(email, description, file_name);
        postFedd.enqueue(new Callback<PostTimelinePojo>() {
            @Override
            public void onResponse(Call<PostTimelinePojo> call, Response<PostTimelinePojo> response) {
                if (response.isSuccessful()) {
                    listener.onPostTimelineSuccess(response.body());
                } else {
                    listener.onPostTimelineFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<PostTimelinePojo> call, Throwable t) {
                listener.onPostTimelineFailed(t.getMessage());
            }
        });
    }
//    public void submitPostTimeline(PostTimelinePojo postTimelinePojo, final OnPostTimelineFinishedListener listener) {
//        Call<PostTimelinePojo> postFedd = PostTimelineRestClient.get().doPostFeed(postTimelinePojo.getEmail(),
//                postTimelinePojo.getDescription(), postTimelinePojo.getFile());
//
//        postFedd.enqueue(new Callback<PostTimelinePojo>() {
//            @Override
//            public void onResponse(Call<PostTimelinePojo> call, Response<PostTimelinePojo> response) {
//                if (response.isSuccessful()) {
//                    listener.onPostTimelineSuccess(response.body());
//                } else {
//                    listener.onPostTimelineFailed(response.message());
//                }
//            }
//
//            @Override
//            public void onFailure(Call<PostTimelinePojo> call, Throwable t) {
//                listener.onPostTimelineFailed(t.getMessage());
//            }
//        });
//    }
}