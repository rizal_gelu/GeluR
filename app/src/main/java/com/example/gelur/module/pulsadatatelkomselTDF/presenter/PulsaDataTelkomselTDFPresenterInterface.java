package com.example.gelur.module.pulsadatatelkomselTDF.presenter;

import android.content.Context;

public interface PulsaDataTelkomselTDFPresenterInterface {
    void onRequesttransferPulsaDataTelkomselTDF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTelkomselTDF(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
