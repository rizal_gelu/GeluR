package com.example.gelur.module.resetpassword.model;

import android.content.Context;

import com.example.gelur.module.resetpassword.presenter.listener.OnResetPasswordFinished;

public interface ResetPasswordModelInterface {
    void OnSubmitResetPassword(Context context, String email, String forgot_password_code, String new_password, OnResetPasswordFinished listener);
}
