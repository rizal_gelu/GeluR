package com.example.gelur.module.pulsadataxlXML.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataxlXML.model.PulsaDataXLXMLModel;
import com.example.gelur.module.pulsadataxlXML.presenter.listener.OnLoadPulsaDataXLXML;
import com.example.gelur.module.pulsadataxlXML.view.PulsaDataXLXMLInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataXLXMLPresenter implements PulsaDataXLXMLPresenterInterface, OnLoadPulsaDataXLXML {

    private WeakReference<PulsaDataXLXMLInterface> view;
    private PulsaDataXLXMLModel model;

    public PulsaDataXLXMLPresenter(PulsaDataXLXMLInterface view) {
        this.view = new WeakReference<PulsaDataXLXMLInterface>(view);
        this.model = new PulsaDataXLXMLModel();
    }

    @Override
    public void onRequesttransferpulsaDataXLXML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataXLXML(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataXLXML(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataXLXML(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataXLXMLSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXMLSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataXLXMLGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXMLGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataXLXML(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataXLXMLSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaXLXMLSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataXLXMLGagal(String m) {

    }
}
