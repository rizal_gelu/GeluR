package com.example.gelur.module.pulsatelpontri.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaTelponTriInterface {
    void TransferPulsaTelponTriSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaTelponTriGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaTelponTri(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
