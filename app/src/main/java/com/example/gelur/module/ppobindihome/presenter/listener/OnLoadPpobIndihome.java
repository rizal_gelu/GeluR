package com.example.gelur.module.ppobindihome.presenter.listener;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPpobIndihome {
    void OnResponsesTagihanIndihomeSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void OnResponsesTagihanIndihomeGagal(ResponsTransaksiPulsa responsTransaksiPulsa);
}
