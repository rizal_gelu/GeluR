package com.example.gelur.module.pulsadatatriTRDMN.presenter;

import android.content.Context;

public interface PulsaDataTriTRDMNPresenterInterface {
    void onRequesttransferpulsaDataTriTRDMN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRDMN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
