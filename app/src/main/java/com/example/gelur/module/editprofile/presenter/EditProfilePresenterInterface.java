package com.example.gelur.module.editprofile.presenter;

import android.content.Context;

public interface EditProfilePresenterInterface {
    void onLoadBio(Context context, String email);
    void onClickEditProfile();
    void onEditYourAboutInfo();
    void onClickLinks();
}
