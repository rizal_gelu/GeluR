package com.example.gelur.module.college.rest;

import com.example.gelur.util.AppsConstant;

public class CollegeAPIURLConstant {
    // POST BIO EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/users/"; // User UriSegment 2
    public static final String POST_COLLEGE = BASE_API_USER + "post_education_college/"; // User UriSegment 3

    // POST BIO EndPoint
    public static final String GET_COLLEGE = BASE_API_USER + "get_education_college/"; // User UriSegment 3
}
