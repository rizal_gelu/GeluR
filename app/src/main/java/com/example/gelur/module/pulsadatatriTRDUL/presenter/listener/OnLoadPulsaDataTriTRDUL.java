package com.example.gelur.module.pulsadatatriTRDUL.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTriTRDUL {
    void RequestTransferPulsaDataTriTRDULSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTriTRDULGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTriTRDUL(String data);
    void ResponseListPricePulsaDataTriTRDULSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTriTRDULGagal(String m);
}
