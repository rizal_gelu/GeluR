package com.example.gelur.module.contactinfo.presenter;

import com.example.gelur.module.contactinfo.model.ContactInfoModel;
import com.example.gelur.module.contactinfo.view.ContactInfoInterface;

import java.lang.ref.WeakReference;

public class ContactInfoPresenter implements ContactInfoPresenterInterface{

    private WeakReference<ContactInfoInterface> view;
    private ContactInfoModel model;

    public ContactInfoPresenter(ContactInfoInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new ContactInfoModel();
    }


    @Override
    public void addPhone() {
        if(null != view.get()) view.get().GoToAddPhone();
    }

    @Override
    public void addAddress() {
        if(null != view.get()) view.get().GoToAddAddress();
    }

    @Override
    public void addSocialLink() {

    }

    @Override
    public void addWebsite() {

    }
}
