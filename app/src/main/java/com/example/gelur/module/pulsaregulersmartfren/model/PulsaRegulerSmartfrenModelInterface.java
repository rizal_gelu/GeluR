package com.example.gelur.module.pulsaregulersmartfren.model;

import android.content.Context;

import com.example.gelur.module.pulsaregulersmartfren.presenter.listener.OnLoadPulsaRegulerSmartfren;
import com.example.gelur.module.pulsaregulertri.presenter.listener.OnLoadPulsaRegulerTri;

public interface PulsaRegulerSmartfrenModelInterface {
    void RequestTransferPulsaRegulerSmartFren(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerSmartfren listener);
    void RequestListPricePulsaRegulerSmartfren(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerSmartfren listener);
}
