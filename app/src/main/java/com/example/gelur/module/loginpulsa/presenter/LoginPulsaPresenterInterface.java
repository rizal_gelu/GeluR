package com.example.gelur.module.loginpulsa.presenter;

import android.content.Context;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

public interface LoginPulsaPresenterInterface {
    void LoginPulsaSubmit(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);

}
