package com.example.gelur.module.plnprabayar.model;

import android.content.Context;

import com.example.gelur.module.plnprabayar.presenter.listener.OnLoadPlnPrabayar;
import com.example.gelur.module.pulsadataindosatIDF.presenter.listener.OnLoadPulsaDataIndosatIDF;

public interface PlnPrabayarModelInterface {
    void RequestPlnPrabayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPlnPrabayar listener);
    void RequestListPricePlnPrabayar(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPlnPrabayar listener);
}
