package com.example.gelur.module.resetpassword.view;

import com.example.gelur.pojo.ResetPasswordPojo;

public interface ResetPasswordInterface {
    void onResetPasswordSuccess(ResetPasswordPojo resetPasswordPojo);
    void onResetPasswordFail(String message);
}
