package com.example.gelur.module.pulsatelpontri.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsatelpontelkomsel.rest.PulsaTelponTelkomselRestClient;
import com.example.gelur.module.pulsatelpontri.presenter.listener.OnLoadPulsaTelponTri;
import com.example.gelur.module.pulsatelpontri.rest.PulsaTelponTriRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaTelponTriModel implements PulsaTelponTriModelInterface{

    @Override
    public void RequestTransferPulsaTelponTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponTri listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaTelponTriRestClient.get().req_transfer_pulsa_reguler_tri(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsatelponTriSukses(response.body());
                } else {
                    listener.RequestTransferPulsatrlponTriGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaTelponTri(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponTri listener) {
        Call<ResponsNonTransaksi> sal = PulsaTelponTriRestClient.get().getPricePulsaTelponTri(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaTelponTriSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaTelponTriGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaTelponTriGagal(t.getMessage());
            }
        });
    }
}
