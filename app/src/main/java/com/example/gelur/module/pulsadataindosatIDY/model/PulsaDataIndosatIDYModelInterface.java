package com.example.gelur.module.pulsadataindosatIDY.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;
import com.example.gelur.module.pulsadatatelkomselTDM.presenter.listener.OnLoadPulsaDataTelkomselTDM;

public interface PulsaDataIndosatIDYModelInterface {
    void RequestTransferDataIndosatIDY(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDY listener);
    void RequestListPricePulsaDataIndosatIDY(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDY listener);
}
