package com.example.gelur.module.loginpulsa.rest;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface LoginPulsaRestInterface {
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsNonTransaksi> getLogin(@Body RequestNonTransaksi requestNonTransaksi);
}
