package com.example.gelur.module.pulsatelpontelkomsel.presenter;

import android.content.Context;

import com.example.gelur.module.pulsaregulertelkomsel.model.PulsaRegulerTelkomselModel;
import com.example.gelur.module.pulsaregulertelkomsel.view.PulsaRegulerTelkomselInterface;
import com.example.gelur.module.pulsatelpontelkomsel.model.PulsaTelponTelkomselModel;
import com.example.gelur.module.pulsatelpontelkomsel.presenter.listener.OnLoadPulsaTelponTelkomsel;
import com.example.gelur.module.pulsatelpontelkomsel.view.PulsaTelponTelkomselInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaTelponTelkomselPresenter implements PulsaTelponTelkomselPresenterInterface, OnLoadPulsaTelponTelkomsel {

    private WeakReference<PulsaTelponTelkomselInterface> view;
    private PulsaTelponTelkomselModel model;

    public PulsaTelponTelkomselPresenter(PulsaTelponTelkomselInterface view) {
        this.view = new WeakReference<PulsaTelponTelkomselInterface>(view);
        this.model = new PulsaTelponTelkomselModel();
    }

    @Override
    public void onRequesttransferpulsatelponTelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaTelponTelkomsel(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaTelponTelkomsel(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTelkomselTDB(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaTelponTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerTelkomselSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaTelponTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerTelkomselGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaTelponTelkomsel(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaTelponTelkomselSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaTelponTelkomsel(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaTelponTelkomselGagal(String m) {

    }
}
