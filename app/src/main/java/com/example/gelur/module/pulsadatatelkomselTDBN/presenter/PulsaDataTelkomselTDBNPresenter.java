package com.example.gelur.module.pulsadatatelkomselTDBN.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDBN.model.PulsaDataTelkomselTDBNModel;
import com.example.gelur.module.pulsadatatelkomselTDBN.model.PulsaDataTelkomselTDBNModelInterface;
import com.example.gelur.module.pulsadatatelkomselTDBN.presenter.listener.OnLoadPulsaDataTelkomselTDBN;
import com.example.gelur.module.pulsadatatelkomselTDBN.view.PulsaDataTelkomselTDBNInterface;
import com.example.gelur.module.pulsadatatelkomselTDF.model.PulsaDataTelkomselTDFModel;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.PulsaDataTelkomselTDFPresenterInterface;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;
import com.example.gelur.module.pulsadatatelkomselTDF.view.PulsaDataTelkomselTDFInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTelkomselTDBNPresenter implements PulsaDataTelkomselTDBNPresenterInterface, OnLoadPulsaDataTelkomselTDBN {

    private WeakReference<PulsaDataTelkomselTDBNInterface> view;
    private PulsaDataTelkomselTDBNModel model;

    public PulsaDataTelkomselTDBNPresenter(PulsaDataTelkomselTDBNInterface view) {
        this.view = new WeakReference<PulsaDataTelkomselTDBNInterface>(view);
        this.model = new PulsaDataTelkomselTDBNModel();
    }

    @Override
    public void onRequesttransferPulsaDataTelkomselTDBN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTelkomselTDBN(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTelkomselTDBN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTelkomselTDBN(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDBNSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTelkomselTDBNSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDBNGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    @Override
    public void OnClickPulsaDataTelkomselTDBN(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDBNSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaDataTelkomselTDBN(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDBNGagal(String m) {

    }
}
