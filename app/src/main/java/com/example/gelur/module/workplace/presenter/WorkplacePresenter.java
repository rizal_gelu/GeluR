package com.example.gelur.module.workplace.presenter;

import android.content.Context;

import com.example.gelur.module.workplace.models.WorkplaceModel;
import com.example.gelur.module.workplace.presenter.listener.OnSaveWorkplaceFinishedListener;
import com.example.gelur.module.workplace.view.Workplace;
import com.example.gelur.module.youraboutinfo.presenter.listener.OnLoadYourAboutInfoListener;
import com.example.gelur.pojo.WorkplacePojo;

import java.lang.ref.WeakReference;

public class WorkplacePresenter implements WorkplacePresenterInterface, OnSaveWorkplaceFinishedListener {

    private WeakReference<Workplace> view;
    private WorkplaceModel model;

    public WorkplacePresenter(Workplace view) {
        this.view = new WeakReference<>(view);
        this.model = new WorkplaceModel();
    }

    @Override
    public void onSaveWorkplaceClicked(Context context, String email, String workplace, String job_title, String location_workplace) {
        model.clickSaveWorkplace(context, email, workplace, job_title, location_workplace, this);
    }

    @Override
    public void onCancelWorkplaceClicked() {

    }

    @Override
    public void onSaveWorkplaceSuccess(WorkplacePojo workplacePojo) {

    }

    @Override
    public void onSaveWorkplaceFailed(String message) {

    }
}
