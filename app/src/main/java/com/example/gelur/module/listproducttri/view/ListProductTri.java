package com.example.gelur.module.listproducttri.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listpulsadatatri.view.ListPulsaDataTri;
import com.example.gelur.module.pulsadatatriTRD.view.PulsaDataTriTRD;
import com.example.gelur.module.pulsaregulertri.view.PulsaRegulerTri;
import com.example.gelur.module.pulsatelpontri.view.PulsaTelponTri;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListProductTri extends AppCompatActivity implements ListProductTriInterface {

    @BindView(R.id.coordinator_list_product_tri)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_list_product_tri)
    Toolbar toolbar;

    @BindView(R.id.content_list_product_tri)
    View viewContent;

    @BindView(R.id.layout_loading_list_product_tri) View viewLoading;

    @BindView(R.id.ll_paket_telpon_tri)
    LinearLayout ll_paket_telpon_tri;

    @BindView(R.id.paket_telpon_tri)
    TextView paket_telpon_tri;

    @BindView(R.id.ll_paket_data_tri) LinearLayout ll_paket_data_tri;

    @BindView(R.id.paket_data_tri) TextView paket_data_tri;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_product_tri);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Produk Tri");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListProductTri.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListProductTri.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListProductTri.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_reguler_tri)
    public void onClickPulsaRegulerTri() {
        startActivity(new Intent(ListProductTri.this, PulsaRegulerTri.class));
    }

    @OnClick(R.id.ll_paket_data_tri)
    public void onClickPaketDataTri() {
        startActivity(new Intent(ListProductTri.this, ListPulsaDataTri.class));
    }

    @OnClick(R.id.ll_paket_telpon_tri)
    public void onClickPaketTelponTri() {
        startActivity(new Intent(ListProductTri.this, PulsaTelponTri.class));
    }
}
