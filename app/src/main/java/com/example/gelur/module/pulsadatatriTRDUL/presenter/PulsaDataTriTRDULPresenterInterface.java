package com.example.gelur.module.pulsadatatriTRDUL.presenter;

import android.content.Context;

public interface PulsaDataTriTRDULPresenterInterface {
    void onRequesttransferpulsaDataTriTRDUL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRDUL(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
