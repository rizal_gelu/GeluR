package com.example.gelur.module.pulsaregulersmartfren.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulerindosat.rest.PulsaRegulerIndosatRestClient;
import com.example.gelur.module.pulsaregulersmartfren.presenter.listener.OnLoadPulsaRegulerSmartfren;
import com.example.gelur.module.pulsaregulersmartfren.rest.PulsaRegulerSmartfrenRestClient;
import com.example.gelur.module.pulsaregulertri.rest.PulsaRegulerTriRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaRegulerSmartfrenModel implements PulsaRegulerSmartfrenModelInterface{
    @Override
    public void RequestTransferPulsaRegulerSmartFren(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerSmartfren listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaRegulerSmartfrenRestClient.get().req_transfer_pulsa_reguler_smartfren(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaRegulerSmartfrenSukses(response.body());
                } else {
                    listener.RequestTransferPulsaRegulerSmartfrenGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaRegulerSmartfren(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerSmartfren listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = PulsaRegulerSmartfrenRestClient.get().getPriceListPulsaRegulerSmartfren(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaRegulerSmartfrenSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaRegulerSmartfrenGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaRegulerSmartfrenGagal(t.getMessage());
            }
        });
    }
}
