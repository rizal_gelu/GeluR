package com.example.gelur.module.pulsadatasmartfrenSMD.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataSmartfrenSMDInterface {
    void TransferPulsaDataSmartfrenSMDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataSmartfrenSMDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataSmartfrenSMDSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataSmartfrenSMDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
