package com.example.gelur.module.pulsaregulerxl.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulerindosat.rest.PulsaRegulerIndosatRestClient;
import com.example.gelur.module.pulsaregulerxl.presenter.listener.OnLoadPulsaRegulerXL;
import com.example.gelur.module.pulsaregulerxl.rest.PulsaRegulerXLRestClient;
import com.example.gelur.module.pulsasmstelkomsel.rest.PulsaSmsTelkomselRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaRegulerXLModel implements PulsaRegulerXLModelInterface {
    @Override
    public void RequestTransferPulsaRegulerXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerXL listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaRegulerXLRestClient.get().req_transfer_pulsa_reguler_xl(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn,counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaRegulerXLSukses(response.body());
                } else {
                    listener.RequestTransferPulsaRegulerXLSukses(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaRegulerXl(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerXL listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = PulsaRegulerXLRestClient.get().getPriceListPulsaRegulerXL(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaRegulerXLSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaRegulerXlGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaRegulerXlGagal(t.getMessage());
            }
        });
    }
}
