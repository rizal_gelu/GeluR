package com.example.gelur.module.listecommerce.view;

public interface ListEcommerceInterface {

    void onClickDanaEcommerce();
    void onClickOvoEcommerce();
    void onClickLinkAjaEcommerce();
    void onClickShopePayEcommerce();
    void onClickGojekEcommerce();
}
