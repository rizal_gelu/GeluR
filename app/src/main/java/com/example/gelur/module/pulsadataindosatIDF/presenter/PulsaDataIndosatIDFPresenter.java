package com.example.gelur.module.pulsadataindosatIDF.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDF.model.PulsaDataIndosatIDFModel;
import com.example.gelur.module.pulsadataindosatIDF.presenter.listener.OnLoadPulsaDataIndosatIDF;
import com.example.gelur.module.pulsadataindosatIDF.view.PulsaDataIndosatIDFInterface;
import com.example.gelur.module.pulsadataindosatIDY.model.PulsaDataIndosatIDYIDYModel;
import com.example.gelur.module.pulsadataindosatIDY.view.PulsaDataIndosatIDYInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDFPresenter implements PulsaDataIndosatIDFPresenterInterface, OnLoadPulsaDataIndosatIDF {

    private WeakReference<PulsaDataIndosatIDFInterface> view;
    private PulsaDataIndosatIDFModel model;

    public PulsaDataIndosatIDFPresenter(PulsaDataIndosatIDFInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDFInterface>(view);
        this.model = new PulsaDataIndosatIDFModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDFSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDFSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDFGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDFGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDF(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDFSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDFSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDFGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDF(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDF(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDF(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
