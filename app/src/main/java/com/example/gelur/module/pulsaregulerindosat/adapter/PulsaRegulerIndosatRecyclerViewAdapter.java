package com.example.gelur.module.pulsaregulerindosat.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulerindosat.presenter.listener.OnLoadPulsaRegulerIndosat;
import com.example.gelur.module.timeline.adapter.TimelineRecyclerViewAdapter;
import com.example.gelur.pojo.FormatResponseNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.pojo.ResponseNonTransaksiMsgPojo;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PulsaRegulerIndosatRecyclerViewAdapter extends RecyclerView.Adapter<PulsaRegulerIndosatRecyclerViewAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaRegulerIndosat listener;
    List<String> stringList;

    public PulsaRegulerIndosatRecyclerViewAdapter(Activity activity, List<String> datum, OnLoadPulsaRegulerIndosat listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_reguler_indosat, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
//        holder.mItem = Collections.singletonList(stringList.get(position));
//
//        holder.reguler_indosat.setText((CharSequence) holder.mItem);

        String data = stringList.get(position);

        holder.reguler_indosat.setText(data);

        holder.ll_kumpulan_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClickListPulsa(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option;
        public final TextView reguler_indosat;
//        public List<String> mItem;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option);
            reguler_indosat = (TextView) view.findViewById(R.id.reguler_indosat);
        }
    }
}
