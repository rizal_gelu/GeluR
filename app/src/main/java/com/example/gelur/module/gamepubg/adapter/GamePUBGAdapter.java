package com.example.gelur.module.gamepubg.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.gameml.presenter.listener.OnLoadGameML;

import java.util.List;

public class GamePUBGAdapter extends RecyclerView.Adapter<GamePUBGAdapter.ViewHolderContent> {
    private Activity activity;
    private OnLoadGameML listener;
    List<String> stringList;

    public GamePUBGAdapter(Activity activity, List<String> datum, OnLoadGameML listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }
    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game_ml, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.game_ml.setText(data);

        holder.ll_kumpulan_option_game_ml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickGameML(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_game_ml;
        public final TextView game_ml;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_game_ml = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_game_ml);
            game_ml = (TextView) view.findViewById(R.id.game_ml);
        }
    }
}
