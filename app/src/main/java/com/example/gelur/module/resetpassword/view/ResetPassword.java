package com.example.gelur.module.resetpassword.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.activationaccount.presenter.ActivationAccountPresenter;
import com.example.gelur.module.activationaccount.view.ActivationAccount;
import com.example.gelur.module.login.view.Login;
import com.example.gelur.module.resetpassword.presenter.ResetPasswordPresenter;
import com.example.gelur.pojo.ResetPasswordPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ResetPassword extends AppCompatActivity implements ResetPasswordInterface {

    @BindView(R.id.activity_reset_password_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_reset_password_toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_new_password)
    TextView tvNewPassword;

    @BindView(R.id.et_new_password)
    EditText et_new_password;

    @BindView(R.id.et_confirm_password) EditText etConfirmPassword;

    @BindView(R.id.btn_reset_password)
    Button btnResetPassword;

    @BindView(R.id.activity_login_layout_content)
    View layoutContent;

    @BindView(R.id.activity_login_layout_loading) View layoutLoading;

    ResetPasswordPresenter presenter;
    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_reset_password);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.reset_password);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new ResetPasswordPresenter(this);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            layoutContent.setVisibility(View.VISIBLE);
            layoutLoading.setVisibility(View.GONE);
        } else {
            layoutContent.setVisibility(View.GONE);
            layoutLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_reset_password)
    public void onResetPassword() {
        loadingDialog.dismiss();

        // Generate email from session
        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserDetailsFromSession();
        String email = userDetails.get(AppsConstant.KEY_ID_USER); // email from session

        String new_password = et_new_password.getText().toString(); // get text from inputing user
        String repeatPassword = etConfirmPassword.getText().toString();

        if (new_password.equals("") || new_password.length() < 6) {
            et_new_password.setError("This field is required and the minimum length is 6");
            return;
        } else if (!repeatPassword.equals(new_password)) {
            etConfirmPassword.setError("Password not match");
            return;
        }

        // get forgot password code
        AppsUtil session_forgot_password = new AppsUtil(this);
        HashMap<String, String> ForgotPasswordUserDetails = session_forgot_password.getForgotPasswordCodeFormSession();
        String forgotPassword_code = ForgotPasswordUserDetails.get(AppsConstant.FORGOT_PASSWORD_CODE);

        presenter.submitResetPassword(this, email, forgotPassword_code, new_password);

        sessionUser.logoutUserSession();
    }


    @Override
    public void onResetPasswordSuccess(ResetPasswordPojo resetPasswordPojo) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, "Your account active now. Please log in first", Snackbar.LENGTH_LONG).show();
        finish();
        Intent intent = new Intent(ResetPassword.this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onResetPasswordFail(String message) {

    }
}
