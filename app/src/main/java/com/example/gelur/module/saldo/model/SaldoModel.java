package com.example.gelur.module.saldo.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.saldo.presenter.listener.OnCekSaldoFinished;
import com.example.gelur.module.transferpulsa.rest.TransferPulsaRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SaldoModel implements SaldoModelInterface{
    @Override
    public void InformationSaldo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnCekSaldoFinished listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = TransferPulsaRestClient.get().getSaldo(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.SaldoSuksesCek(response.body());
                } else {
                    listener.SaldoGagalCek(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.SaldoGagalCek(t.getMessage());
            }
        });
    }
}
