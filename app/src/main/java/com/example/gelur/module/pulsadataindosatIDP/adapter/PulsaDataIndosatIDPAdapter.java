package com.example.gelur.module.pulsadataindosatIDP.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDM.presenter.listener.OnLoadPulsaDataIndosatIDM;
import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;

import java.util.List;

public class PulsaDataIndosatIDPAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDPAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDP listener;
    List<String> stringList;

    public PulsaDataIndosatIDPAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDP listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idp, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idp.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDP(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idp;
        public final TextView pulsa_data_indosat_idp;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idp = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idp);
            pulsa_data_indosat_idp = (TextView) view.findViewById(R.id.pulsa_data_indosat_idp);
        }
    }
}
