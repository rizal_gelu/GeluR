package com.example.gelur.module.pulsadataindosatIDM.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDM.model.PulsaDataIndosatIDMModel;
import com.example.gelur.module.pulsadataindosatIDM.presenter.listener.OnLoadPulsaDataIndosatIDM;
import com.example.gelur.module.pulsadataindosatIDM.view.PulsaDataIndosatIDMInterface;
import com.example.gelur.module.pulsadataindosatIDY.model.PulsaDataIndosatIDYIDYModel;
import com.example.gelur.module.pulsadataindosatIDY.presenter.PulsaDataIndosatIDYPresenterInterface;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;
import com.example.gelur.module.pulsadataindosatIDY.view.PulsaDataIndosatIDYInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDMPresenter implements PulsaDataIndosatIDMPresenterInterface, OnLoadPulsaDataIndosatIDM {

    private WeakReference<PulsaDataIndosatIDMInterface> view;
    private PulsaDataIndosatIDMModel model;

    public PulsaDataIndosatIDMPresenter(PulsaDataIndosatIDMInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDMInterface>(view);
        this.model = new PulsaDataIndosatIDMModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDMSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDMGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDM(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDMSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDMSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDMGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDM(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDM(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
