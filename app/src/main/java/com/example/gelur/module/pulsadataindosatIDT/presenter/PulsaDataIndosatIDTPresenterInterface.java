package com.example.gelur.module.pulsadataindosatIDT.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDTPresenterInterface {
    void onRequesttransferpulsadataIndosatIDT(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDT(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
