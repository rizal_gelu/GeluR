package com.example.gelur.module.gamepubg.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadGamePUBG {
    void RequestTransferDiamondPUBGSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferDiamondPUBGGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickGamePUBG(String data);
    void ResponseListPriceGamePUBGSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceGamePUBGGagal(String m);
}
