package com.example.gelur.module.taskboardcompany.presenter;

import android.content.Context;

import com.example.gelur.module.taskboardcompany.model.TaskBoardModel;
import com.example.gelur.module.taskboardcompany.presenter.listener.OnLoadTaskFinished;
import com.example.gelur.module.taskboardcompany.view.TaskBoardInterface;
import com.example.gelur.pojo.TaskBoardPojo;

import java.lang.ref.WeakReference;
import java.util.List;

public class TaskBoardPresenter implements TaskBoarPresenterInterface, OnLoadTaskFinished {

    private WeakReference<TaskBoardInterface> view;
    private TaskBoardModel model;

    public TaskBoardPresenter(TaskBoardInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new TaskBoardModel();
    }

    public void onLoadTaskBoard(Context context, String company_id) {
        model.loadDataTaskBoard(context, company_id, this);
    }

    @Override
    public void onClickBtnAddJob() {
        if(null != view.get()) view.get().goToAddJob();
    }

    @Override
    public void onLoadTaskBoardSuccess(List<TaskBoardPojo> taskBoardPojo) {
        if (view.get() != null) view.get().onLoadTaskBoardSuccess(taskBoardPojo);
    }

    @Override
    public void onLoadTaskBoardFailed(String message) {

    }
}
