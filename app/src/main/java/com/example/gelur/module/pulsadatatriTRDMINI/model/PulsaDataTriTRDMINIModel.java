package com.example.gelur.module.pulsadatatriTRDMINI.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRD.rest.PulsaDataTriTRDRestClient;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDDP.rest.PulsaDataTriTRDDPRestClient;
import com.example.gelur.module.pulsadatatriTRDMINI.presenter.listener.OnLoadPulsaDataTriTRDMINI;
import com.example.gelur.module.pulsadatatriTRDMINI.rest.PulsaDataTriTRDMINIRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataTriTRDMINIModel implements PulsaDataTriTRDMINIModelInterface {
    @Override
    public void RequestTransferPulsaDataTriTRDMINI(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDMINI listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataTriTRDMINIRestClient.get().req_transfer_pulsa_data_tri_trdmini(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataTriTRDMINISukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataTriTRDMINIGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTriTRDMINI(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDMINI listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTriTRDMINIRestClient.get().getPriceListPulsaDataTriTRDMINI(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataTriTRDMINISukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataTriTRDMINIGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataTriTRDMINIGagal(t.getMessage());
            }
        });
    }
}
