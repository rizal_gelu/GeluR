package com.example.gelur.module.pulsadatasmartfrenSMD.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;
import com.example.gelur.module.pulsadatasmartfrenSMD.presenter.listener.OnLoadPulsaDataSmartfrenSMD;

public interface PulsaDataSmartfrenSMDModelInterface {
    void RequestTransferDataSmartfren(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMD listener);
    void RequestListPricePulsaDataSmartfrenSMD(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMD listener);
}
