package com.example.gelur.module.address.model;

import android.content.Context;

import com.example.gelur.module.address.presenter.listener.OnSaveAddressUserFinishedListener;

public interface AddressModelInterface {
    void clickAddressSave(Context context, String email, String name_city, String type_city, OnSaveAddressUserFinishedListener listener);
}
