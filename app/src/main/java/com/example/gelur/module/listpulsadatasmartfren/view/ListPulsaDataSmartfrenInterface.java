package com.example.gelur.module.listpulsadatasmartfren.view;

public interface ListPulsaDataSmartfrenInterface {
    void onClickSMD();
    void onClickSMDEVO();
    void onClickUNLK();
}
