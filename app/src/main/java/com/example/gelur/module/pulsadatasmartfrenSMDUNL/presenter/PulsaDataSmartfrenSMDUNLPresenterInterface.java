package com.example.gelur.module.pulsadatasmartfrenSMDUNL.presenter;

import android.content.Context;

public interface PulsaDataSmartfrenSMDUNLPresenterInterface {
    void onRequesttransferpulsadataSmartfrenSMDUNL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataSmartfrenSMDUNL(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
