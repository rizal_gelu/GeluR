package com.example.gelur.module.pulsadatatriTRDUL.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDPY.model.PulsaDataTriTRDPYModel;
import com.example.gelur.module.pulsadatatriTRDPY.view.PulsaDataTriTRDPYInterface;
import com.example.gelur.module.pulsadatatriTRDUL.model.PulsaDataTriTRDULModel;
import com.example.gelur.module.pulsadatatriTRDUL.presenter.listener.OnLoadPulsaDataTriTRDUL;
import com.example.gelur.module.pulsadatatriTRDUL.view.PulsaDataTriTRDULInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDULPresenter implements PulsaDataTriTRDULPresenterInterface, OnLoadPulsaDataTriTRDUL {

    private WeakReference<PulsaDataTriTRDULInterface> view;
    private PulsaDataTriTRDULModel model;

    public PulsaDataTriTRDULPresenter(PulsaDataTriTRDULInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDULInterface>(view);
        this.model = new PulsaDataTriTRDULModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDUL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDUL(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDUL(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDUL(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDULSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDULSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDULGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDULGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDUL(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDULSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDULSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDULGagal(String m) {

    }
}
