package com.example.gelur.module.taskboardadd.presenter;

import android.content.Context;

public interface TaskBoardAddPresenterInterface {
    void saveTaskJobList(Context context, String title_list_task, String company_id, String description_task, String priority, String due_date, String create_by, String task_id);
//    void
}
