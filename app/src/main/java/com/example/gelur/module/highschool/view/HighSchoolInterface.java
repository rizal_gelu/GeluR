package com.example.gelur.module.highschool.view;

public interface HighSchoolInterface {
    void onSuccessPostHighSchool();
    void onFailedPostHighSchool(String message);
}
