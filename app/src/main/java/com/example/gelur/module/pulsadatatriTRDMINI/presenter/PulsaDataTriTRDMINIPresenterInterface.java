package com.example.gelur.module.pulsadatatriTRDMINI.presenter;

import android.content.Context;

public interface PulsaDataTriTRDMINIPresenterInterface {
    void onRequesttransferpulsaDataTriTRDMINI(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRDMINI(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
