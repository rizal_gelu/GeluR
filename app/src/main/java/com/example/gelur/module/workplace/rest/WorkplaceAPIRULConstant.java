package com.example.gelur.module.workplace.rest;

import com.example.gelur.util.AppsConstant;

public class WorkplaceAPIRULConstant {
    // POST BIO EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/users/"; // User UriSegment 2
    public static final String POST_WORKPLACE = BASE_API_USER + "post_workplace/"; // User UriSegment 3

    // POST BIO EndPoint
    public static final String GET_WORKPLACE = BASE_API_USER + "get_workplace/"; // User UriSegment 3
}
