package com.example.gelur.module.bio.presenter;

import android.content.Context;

import com.example.gelur.module.bio.model.BioModel;
import com.example.gelur.module.bio.presenter.listener.OnLoadBioFinished;
import com.example.gelur.module.bio.presenter.listener.OnSaveBioFinishedListener;
import com.example.gelur.module.bio.view.BioInterface;
import com.example.gelur.pojo.BioPojo;

import java.lang.ref.WeakReference;
import java.util.List;

public class BioPresenter implements BioPresenterInterface, OnLoadBioFinished, OnSaveBioFinishedListener {

    private WeakReference<BioInterface> view;
    private BioModel model;

    public BioPresenter(BioInterface view) {
        this.view = new WeakReference<BioInterface>(view);
        this.model = new BioModel();
    }

    @Override
    public void onLoadBio(Context context, String email) {
        model.loadBio(context, email, this);
    }

    @Override
    public void onSaveClicked(Context context, String email, String bio) {
        model.clickSave(context, email, bio, this);
    }

    @Override
    public void onCancelClicked() {

    }

    @Override
    public void onSaveBioSuccess(BioPojo bioPojo) {

    }

    @Override
    public void onSaveBioFailed(String message) {

    }

    @Override
    public void onLoadBioDataSuccess(BioPojo bioUser) {

    }

    @Override
    public void onLoadBioeDataFailed(String message) {

    }
}
