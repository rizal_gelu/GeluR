package com.example.gelur.module.forgotpassword.rest;

import com.example.gelur.module.login.rest.LoginAPIURLConstant;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordRestClient {
    private static ForgotPasswordRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    private ForgotPasswordRestClient() {
    }

    public static ForgotPasswordRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(LoginAPIURLConstant.BASE_API_LOGIN_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(ForgotPasswordRestInterface.class);
    }
}
