package com.example.gelur.module.pulsadataindosatIDU.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDU.model.PulsaDataIndosatIDUModel;
import com.example.gelur.module.pulsadataindosatIDU.presenter.listener.OnLoadPulsaDataIndosatIDU;
import com.example.gelur.module.pulsadataindosatIDU.view.PulsaDataIndosatIDUInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDUPresenter implements PulsaDataIndosatIDUPresenterInterface, OnLoadPulsaDataIndosatIDU {

    private WeakReference<PulsaDataIndosatIDUInterface> view;
    private PulsaDataIndosatIDUModel model;

    public PulsaDataIndosatIDUPresenter(PulsaDataIndosatIDUInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDUInterface>(view);
        this.model = new PulsaDataIndosatIDUModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDUSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDUSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDUGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDUGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDU(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDUSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDUSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDUGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDU(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDU(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDU(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDU(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
