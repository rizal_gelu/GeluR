package com.example.gelur.module.splashscreen.presenter;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;

import com.example.gelur.module.splashscreen.view.SplashScreen;
import com.example.gelur.module.splashscreen.view.SplashScreenInterface;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.util.AppsUtil;

public class SplashScreenPresenter implements SplashScreenPresenterInterface {

    private final static int MSG_CONTINUE = 1234;

    private final static long DELAY = 3000;
    private CustomHandler customHandler;

    private SplashScreenInterface view;

    public SplashScreenPresenter(SplashScreenInterface view) {
        this.view = view;
    }

    @Override
    public void doSplash() {
        customHandler = new CustomHandler();
        customHandler.sendEmptyMessageDelayed(MSG_CONTINUE, DELAY);
    }

    @Override
    public void destroySplash() {
        customHandler.removeMessages(MSG_CONTINUE);
    }

    class CustomHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case MSG_CONTINUE:
                    _continue();
                    break;
            }
        }

        private void _continue() {
//            view.checkLoggedIn();
            view.isLoggedIn();
        }

    }
}
