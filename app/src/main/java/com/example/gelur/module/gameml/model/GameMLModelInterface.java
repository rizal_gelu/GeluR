package com.example.gelur.module.gameml.model;

import android.content.Context;

import com.example.gelur.module.ecommerceshopee.presenter.listener.OnLoadShopeePay;
import com.example.gelur.module.gameff.presenter.listener.OnLoadGameFF;
import com.example.gelur.module.gameml.presenter.listener.OnLoadGameML;

public interface GameMLModelInterface {
    void RequestTransferDiamondML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGameML listener);
    void RequestListPriceGameML(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadGameML listener);
}
