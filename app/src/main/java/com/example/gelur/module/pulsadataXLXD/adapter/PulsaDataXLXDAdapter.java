package com.example.gelur.module.pulsadataXLXD.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataXLPXD.presenter.listener.OnLoadPulsaDataXLPXD;
import com.example.gelur.module.pulsadataXLXD.presenter.listener.OnLoadPulsaDataXLXD;

import java.util.List;

public class PulsaDataXLXDAdapter extends RecyclerView.Adapter<PulsaDataXLXDAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataXLXD listener;
    List<String> stringList;

    public PulsaDataXLXDAdapter(Activity activity, List<String> datum, OnLoadPulsaDataXLXD listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_xl_xd, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_xl_xd.setText(data);

        holder.ll_kumpulan_option_pulsa_data_xl_xd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataXLXD(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_xl_xd;
        public final TextView pulsa_data_xl_xd;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_xl_xd = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_xl_xd);
            pulsa_data_xl_xd = (TextView) view.findViewById(R.id.pulsa_data_xl_xd);
        }
    }
}
