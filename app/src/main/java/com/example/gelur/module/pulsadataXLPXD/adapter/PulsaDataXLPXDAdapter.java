package com.example.gelur.module.pulsadataXLPXD.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataXLPXD.presenter.listener.OnLoadPulsaDataXLPXD;
import com.example.gelur.module.pulsadataxlXML.presenter.listener.OnLoadPulsaDataXLXML;

import java.util.List;

public class PulsaDataXLPXDAdapter extends RecyclerView.Adapter<PulsaDataXLPXDAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataXLPXD listener;
    List<String> stringList;

    public PulsaDataXLPXDAdapter(Activity activity, List<String> datum, OnLoadPulsaDataXLPXD listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_xl_pxd, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_xl_pxd.setText(data);

        holder.ll_kumpulan_option_pulsa_data_xl_pxd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataXLPXD(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_xl_pxd;
        public final TextView pulsa_data_xl_pxd;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_xl_pxd = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_xl_pxd);
            pulsa_data_xl_pxd = (TextView) view.findViewById(R.id.pulsa_data_xl_pxd);
        }
    }
}
