package com.example.gelur.module.workplace.presenter;

import android.content.Context;

public interface WorkplacePresenterInterface {
    void onSaveWorkplaceClicked(Context context, String email, String workplace, String job_title, String location_workplace);
    void onCancelWorkplaceClicked();
}
