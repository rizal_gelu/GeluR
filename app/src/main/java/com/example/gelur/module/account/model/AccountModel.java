package com.example.gelur.module.account.model;

import android.content.Context;

import com.example.gelur.module.account.presenter.listener.OnLoadAccountFinished;
import com.example.gelur.module.account.rest.AccountRestClient;
import com.example.gelur.pojo.TimelinePersonalUser;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.pojo.UsersPojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountModel implements AccountModelInterface {
    @Override
    public void getFeedAccount(Context mContext, String email, OnLoadAccountFinished listener) {
        Call<FormaterResponse> feedResponse = AccountRestClient.get().getFeedAccount(email);

        feedResponse.enqueue(new Callback<FormaterResponse>() {
            @Override
            public void onResponse(Call<FormaterResponse> call, Response<FormaterResponse> response) {
                FormaterResponse resource = response.body();
                List<TimelinePojo> feedList = resource.getTimeline_user();
                listener.onLoadFeedTimelineSuccess(feedList);
            }

            @Override
            public void onFailure(Call<FormaterResponse> call, Throwable t) {
                listener.onLoadFeedTimelineFailed("error");
            }
        });
    }


    @Override
    public void getProfileAccount(Context context, String email, OnLoadAccountFinished listener) {
        Call<FormaterResponse> userProfile = AccountRestClient.get().getAccountDetail(email);
        userProfile.enqueue(new Callback<FormaterResponse>() {
            @Override
            public void onResponse(Call<FormaterResponse> call, Response<FormaterResponse> response) {
                if (response.isSuccessful()) {
                    FormaterResponse resource = response.body();
                    listener.onLoadProfileDetail(resource.getData_profile());
                } else {
                    listener.onLoadFeedTimelineFailed("error njing");
                }
            }

            @Override
            public void onFailure(Call<FormaterResponse> call, Throwable t) {
                listener.onLoadFeedTimelineFailed(t.getMessage());
            }
        });
    }

}
