package com.example.gelur.module.bio.presenter.listener;

import com.example.gelur.module.bio.view.Bio;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.TimelinePojo;

import java.util.List;

public interface OnLoadBioFinished {
    void onLoadBioDataSuccess(BioPojo bioUser);
    void onLoadBioeDataFailed(String message);
}
