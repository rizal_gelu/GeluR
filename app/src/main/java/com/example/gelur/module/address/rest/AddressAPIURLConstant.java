package com.example.gelur.module.address.rest;

import com.example.gelur.util.AppsConstant;

public class AddressAPIURLConstant {
    // POST BIO EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/users/"; // User UriSegment 2
    public static final String POST_ADDRESS_USER = BASE_API_USER + "post_address/"; // User UriSegment 3

    // POST BIO EndPoint
    public static final String GET_ADDRESS_USER = BASE_API_USER + "get_address/"; // User UriSegment 3
}
