package com.example.gelur.module.topup.model;

import android.content.Context;

import com.example.gelur.module.topup.presenter.listener.OnRequestTopfinished;

public interface TopUpModelInterface {
    void RequestTopUp(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnRequestTopfinished listener);
}
