package com.example.gelur.module.pulsasmstelkomsel.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsasmstelkomsel.presenter.listener.OnLoadPulsaSmsTelkomsel;

import java.util.List;

public class PulsaSmsTelkomselAdapter extends RecyclerView.Adapter<PulsaSmsTelkomselAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaSmsTelkomsel listener;
    List<String> stringList;

    public PulsaSmsTelkomselAdapter(Activity activity, List<String> datum, OnLoadPulsaSmsTelkomsel listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaSmsTelkomselAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_sms_telkomsel, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaSmsTelkomselAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_sms_telkomsel.setText(data);

        holder.ll_kumpulan_option_pulsa_sms_telkomsel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaSmsTelkomsel(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_sms_telkomsel;
        public final TextView pulsa_sms_telkomsel;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_sms_telkomsel = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_sms_telkomsel);
            pulsa_sms_telkomsel = (TextView) view.findViewById(R.id.pulsa_sms_telkomsel);
        }
    }
}
