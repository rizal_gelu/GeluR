package com.example.gelur.module.pulsatelpontelkomsel.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaTelponTelkomsel {
    void RequestTransferPulsaTelponTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaTelponTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaTelponTelkomsel(String data);
    void ResponseListPricePulsaTelponTelkomselSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaTelponTelkomselGagal(String m);
}
