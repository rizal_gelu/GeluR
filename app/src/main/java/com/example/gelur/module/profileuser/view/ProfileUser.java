package com.example.gelur.module.profileuser.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.gelur.R;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ProfileUser extends AppCompatActivity implements ProfileUserInterface {

    @BindView(R.id.activity_profile_user_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_profile_user_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_profile_user)
    View viewContent;

    @BindView(R.id.layout_loading_account) View viewLoading;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_profile_user);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProfileUser.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);
//        setSupportActionBar(toolbar);

        showContent(true);

    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToSearch() {
    }

    private void goToNotification() {
    }

    private void goToHome() {
    }

    @Override
    public void followClickBtn() {

    }

    @Override
    public void seeAllFriendBtn() {

    }

    @Override
    public void seePhotosBtn() {

    }

    @Override
    public void seeAgenda() {

    }

    @Override
    public void moreInformationUser() {

    }
}
