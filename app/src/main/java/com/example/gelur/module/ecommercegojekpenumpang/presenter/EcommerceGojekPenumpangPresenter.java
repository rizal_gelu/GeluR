package com.example.gelur.module.ecommercegojekpenumpang.presenter;

import android.content.Context;

import com.example.gelur.module.ecommercegojekdriver.model.EcommerceGojekDriverModel;
import com.example.gelur.module.ecommercegojekdriver.view.EcommerceGojekDriverInterface;
import com.example.gelur.module.ecommercegojekpenumpang.model.EcommerceGojekPenumpangModel;
import com.example.gelur.module.ecommercegojekpenumpang.presenter.listener.OnLoadGojekPenumpang;
import com.example.gelur.module.ecommercegojekpenumpang.view.EcommerceGojekPenumpangInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class EcommerceGojekPenumpangPresenter implements EcommerceGojekPenumpangPresenterInterface, OnLoadGojekPenumpang {

    private WeakReference<EcommerceGojekPenumpangInterface> view;
    private EcommerceGojekPenumpangModel model;

    public EcommerceGojekPenumpangPresenter(EcommerceGojekPenumpangInterface view) {
        this.view = new WeakReference<EcommerceGojekPenumpangInterface>(view);
        this.model = new EcommerceGojekPenumpangModel();
    }

    @Override
    public void onRequesttransfersaldoGojekPenumpang(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferSaldoGojekPenumpang(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceGojekPenumpang(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceGojekPenumpang(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferSaldoGojekPenumpangSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoGojekPenumpangSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferSaldoGojekPenumpangGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoGojekPenumpangGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickGojekPenumpang(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceGojekDriverSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceGojekPenumpang(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceGojekDriverGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
