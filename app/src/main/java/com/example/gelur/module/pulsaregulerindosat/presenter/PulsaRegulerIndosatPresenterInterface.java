package com.example.gelur.module.pulsaregulerindosat.presenter;

import android.content.Context;

public interface PulsaRegulerIndosatPresenterInterface {
    void onRequesttransferpulsareguler(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPulsaIndosat(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);

    void createNewPopUpDialog(String nominal);
}
