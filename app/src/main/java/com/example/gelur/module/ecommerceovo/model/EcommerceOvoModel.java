package com.example.gelur.module.ecommerceovo.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.ecommercedana.rest.EcommerceDanaRestClient;
import com.example.gelur.module.ecommercelinkaja.presenter.listener.OnLoadEcommerceLinkAja;
import com.example.gelur.module.ecommercelinkaja.rest.EcommerceLinkAjaRestClient;
import com.example.gelur.module.ecommerceovo.presenter.listener.OnLoadEcommerceOvo;
import com.example.gelur.module.ecommerceovo.rest.EcommerceOvoRestClient;
import com.example.gelur.module.ecommercedana.model.EcommerceDanaModelInterface;
import com.example.gelur.module.ecommercedana.presenter.listener.OnLoadDanaEcommerce;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EcommerceOvoModel implements EcommerceOvoModelInterface {
    @Override
    public void RequestTransferSaldoOvo(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadEcommerceOvo listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = EcommerceOvoRestClient.get().req_transfer_ovo_ecommerce(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferSaldoOvoSukses(response.body());
                } else {
                    listener.RequestTransferSaldoOvoGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPriceOvo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadEcommerceOvo listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = EcommerceOvoRestClient.get().getPriceListOvo(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPriceOvoSukses(response.body());
                } else {
                    listener.ResponseListPriceOvoGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPriceOvoGagal(t.getMessage());
            }
        });
    }
}
