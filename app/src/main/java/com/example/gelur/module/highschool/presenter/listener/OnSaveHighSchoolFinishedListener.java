package com.example.gelur.module.highschool.presenter.listener;


import com.example.gelur.pojo.HighSchoolPojo;

public interface OnSaveHighSchoolFinishedListener {
    void onSaveHighSchoolSuccess(HighSchoolPojo highSchoolPojo);
    void onSaveHighSchoolFailed(String message);
}
