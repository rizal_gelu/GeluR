package com.example.gelur.module.gameff.presenter;

import android.content.Context;

public interface GameFFPresenterInterface {
    void onRequesttransferDiamondFF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPriceGameFF(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
