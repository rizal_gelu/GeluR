package com.example.gelur.module.pulsadatatriTRDAON.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRD.presenter.listener.OnLoadPulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;

import java.util.List;

public class PulsaDataTriTRDAONAdapter extends RecyclerView.Adapter<PulsaDataTriTRDAONAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataTriTRDAON listener;
    List<String> stringList;

    public PulsaDataTriTRDAONAdapter(Activity activity, List<String> datum, OnLoadPulsaDataTriTRDAON listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_tri_trdaon, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_tri_trdaon.setText(data);

        holder.ll_kumpulan_option_pulsa_data_tri_trdaon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataTriTRDAON(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_tri_trdaon;
        public final TextView pulsa_data_tri_trdaon;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_tri_trdaon = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_tri_trdaon);
            pulsa_data_tri_trdaon = (TextView) view.findViewById(R.id.pulsa_data_tri_trdaon);
        }
    }
}
