package com.example.gelur.module.gameff.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.ecommerceshopee.rest.EcommerceShopeeRestClient;
import com.example.gelur.module.gameff.presenter.listener.OnLoadGameFF;
import com.example.gelur.module.gameff.rest.GameFFRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GameFFModel implements GameFFModelInterface {
    @Override
    public void RequestTransferDiamondFF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGameFF listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = GameFFRestClient.get().req_transfer_diamond_ff(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferDiamondFFSukses(response.body());
                } else {
                    listener.RequestTransferDiamondFFGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPriceGameFF(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadGameFF listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = GameFFRestClient.get().getPriceListPriceGameFF(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPriceGameFFSukses(response.body());
                } else {
                    listener.ResponseListPriceGameFFGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPriceGameFFGagal(t.getMessage());
            }
        });
    }
}
