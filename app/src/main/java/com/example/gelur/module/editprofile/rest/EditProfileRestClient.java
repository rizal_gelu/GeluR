package com.example.gelur.module.editprofile.rest;

import com.example.gelur.module.activationaccount.rest.ActivationAccountAPIURLConstant;
import com.example.gelur.module.activationaccount.rest.ActivationAccountRestInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EditProfileRestClient {
    private static EditProfileRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    private EditProfileRestClient() {

    }

    public static EditProfileRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(logging);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(ActivationAccountAPIURLConstant.BASE_API_ACTIVATE_ACCOUNT_URL)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(EditProfileRestInterface.class);
    }
}
