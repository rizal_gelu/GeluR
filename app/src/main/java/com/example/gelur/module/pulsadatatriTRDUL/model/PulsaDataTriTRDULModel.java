package com.example.gelur.module.pulsadatatriTRDUL.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDPY.presenter.listener.OnLoadPulsaDataTriTRDPY;
import com.example.gelur.module.pulsadatatriTRDPY.rest.PulsaDataTriTRDPYRestClient;
import com.example.gelur.module.pulsadatatriTRDUL.presenter.listener.OnLoadPulsaDataTriTRDUL;
import com.example.gelur.module.pulsadatatriTRDUL.rest.PulsaDataTriTRDULRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataTriTRDULModel implements PulsaDataTriTRDULModelInterface {
    @Override
    public void RequestTransferPulsaDataTriTRDUL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDUL listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataTriTRDULRestClient.get().req_transfer_pulsa_data_tri_trdul(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataTriTRDULSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataTriTRDULGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTriTRDUL(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDUL listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTriTRDULRestClient.get().getPriceListPulsaDataTriTRDUL(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataTriTRDULSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataTriTRDULGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataTriTRDULGagal(t.getMessage());
            }
        });
    }
}
