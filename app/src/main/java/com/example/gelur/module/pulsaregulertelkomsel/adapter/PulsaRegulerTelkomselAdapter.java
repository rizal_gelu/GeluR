package com.example.gelur.module.pulsaregulertelkomsel.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.listener.OnLoadPulsaRegulerTelkomsel;

import java.util.List;

public class PulsaRegulerTelkomselAdapter extends RecyclerView.Adapter<PulsaRegulerTelkomselAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaRegulerTelkomsel listener;
    List<String> stringList;

    public PulsaRegulerTelkomselAdapter(Activity activity, List<String> datum, OnLoadPulsaRegulerTelkomsel listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaRegulerTelkomselAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_reguler_telkomsel, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaRegulerTelkomselAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_reguler_telkomsel.setText(data);

        holder.ll_kumpulan_pulsa_reguler_telkomsel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaRegulerTelkomsel(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_pulsa_reguler_telkomsel;
        public final TextView pulsa_reguler_telkomsel;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_pulsa_reguler_telkomsel = (LinearLayout) view.findViewById(R.id.ll_kumpulan_pulsa_reguler_telkomsel);
            pulsa_reguler_telkomsel = (TextView) view.findViewById(R.id.pulsa_reguler_telkomsel);
        }
    }
}
