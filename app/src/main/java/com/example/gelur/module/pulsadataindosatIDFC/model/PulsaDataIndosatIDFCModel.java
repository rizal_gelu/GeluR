package com.example.gelur.module.pulsadataindosatIDFC.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDFC.presenter.listener.OnLoadPulsaDataIndosatIDFC;
import com.example.gelur.module.pulsadataindosatIDFC.rest.PulsaDataIndosatIDFCRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataIndosatIDFCModel implements PulsaDataIndosatIDFCModelInterface {
    @Override
    public void RequestTransferDataIndosatIDFC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDFC listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataIndosatIDFCRestClient.get().req_transfer_pulsa_data_indosat(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataIndosatIDFCSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataIndosatIDFCGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataIndosatIDFC(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDFC listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataIndosatIDFCRestClient.get().getPriceListPulsaDataIndosatIDY(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataIndosatIDFCSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataIndosatIDFCGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataIndosatIDFCGagal(t.getMessage());
            }
        });
    }
}
