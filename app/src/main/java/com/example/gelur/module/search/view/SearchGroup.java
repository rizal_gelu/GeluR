package com.example.gelur.module.search.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class SearchGroup extends Fragment {

    public static SearchGroup newInstance(int page, String title) {
        SearchGroup searchGroup = new SearchGroup();
        Bundle args = new Bundle();
        args.putInt("1", page);
        args.putString("Search Person", title);
        searchGroup.setArguments(args);
        return searchGroup;
    }
}
