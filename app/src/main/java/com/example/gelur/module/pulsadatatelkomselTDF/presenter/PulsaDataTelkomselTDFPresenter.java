package com.example.gelur.module.pulsadatatelkomselTDF.presenter;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.model.EcommerceOvoModel;
import com.example.gelur.module.ecommerceovo.view.EcommerceOvoInterface;
import com.example.gelur.module.pulsadatatelkomselTDF.model.PulsaDataTelkomselTDFModel;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;
import com.example.gelur.module.pulsadatatelkomselTDF.view.PulsaDataTelkomselTDFInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTelkomselTDFPresenter implements PulsaDataTelkomselTDFPresenterInterface, OnLoadPulsaDataTelkomselTDF {

    private WeakReference<PulsaDataTelkomselTDFInterface> view;
    private PulsaDataTelkomselTDFModel model;

    public PulsaDataTelkomselTDFPresenter(PulsaDataTelkomselTDFInterface view) {
        this.view = new WeakReference<PulsaDataTelkomselTDFInterface>(view);
        this.model = new PulsaDataTelkomselTDFModel();
    }

    @Override
    public void onRequesttransferPulsaDataTelkomselTDF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTelkomselTDF(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTelkomselTDF(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTelkomselTDF(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDFSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTelkomselTDFSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDFGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    @Override
    public void OnClickPulsaDataTelkomselTDF(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDFSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaDataTelkomselTDF(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDFGagal(String m) {

    }
}
