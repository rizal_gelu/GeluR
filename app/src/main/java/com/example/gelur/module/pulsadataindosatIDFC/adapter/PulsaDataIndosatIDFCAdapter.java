package com.example.gelur.module.pulsadataindosatIDFC.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDFC.presenter.listener.OnLoadPulsaDataIndosatIDFC;

import java.util.List;

public class PulsaDataIndosatIDFCAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDFCAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDFC listener;
    List<String> stringList;

    public PulsaDataIndosatIDFCAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDFC listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idfc, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idfc.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDFC(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idfc;
        public final TextView pulsa_data_indosat_idfc;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idfc = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idfc);
            pulsa_data_indosat_idfc = (TextView) view.findViewById(R.id.pulsa_data_indosat_idfc);
        }
    }
}
