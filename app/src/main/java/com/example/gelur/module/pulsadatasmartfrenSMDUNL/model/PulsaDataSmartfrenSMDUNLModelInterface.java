package com.example.gelur.module.pulsadatasmartfrenSMDUNL.model;

import android.content.Context;

import com.example.gelur.module.pulsadataSMDEVO.presenter.listener.OnLoadPulsaDataSmartfrenSMDEVO;
import com.example.gelur.module.pulsadatasmartfrenSMDUNL.presenter.listener.OnLoadPulsaDataSmartfrenSMDUNL;

public interface PulsaDataSmartfrenSMDUNLModelInterface {
    void RequestTransferDataSmartfrenSMDUNL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMDUNL listener);
    void RequestListPricePulsaDataSmartfrenSMDUNL(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMDUNL listener);
}
