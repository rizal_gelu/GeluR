package com.example.gelur.module.product.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.bio.view.Bio;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.pojo.ProductPojo;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.gelur.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Product extends AppCompatActivity implements ProductInterface {

    @BindView(R.id.activity_product_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_product_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_product_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_product);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Product.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
    }

    private void goToNotification() {
        startActivity(new Intent(Product.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(Product.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(Product.this, Account.class));
    }

    private void goToHome() {
        startActivity(new Intent(Product.this, Timeline.class));
    }

    @Override
    public void onLoadProductListSuccess(ProductPojo productPojo) {

    }

    @Override
    public void onLoadProductListFail(String Message) {

    }

    @Override
    public void onClickProductList() {

    }
}
