package com.example.gelur.module.daftardownline.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.daftardownline.presenter.DaftarDownlinePresenter;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;


import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DaftarDownline extends AppCompatActivity implements DaftarDownlineInterface{
    @BindView(R.id.coordinator_daftar_downline)
    CoordinatorLayout coordinator_daftar_downline;

    @BindView(R.id.activity_toolbar_daftar_downline)
    Toolbar toolbar;

    @BindView(R.id.content_daftar_downline)
    View viewContent;

    @BindView(R.id.layout_loading_daftar_downline) View viewLoading;

    @BindView(R.id.nama_member) EditText nama_member;

    @BindView(R.id.alamat) EditText alamat;

    @BindView(R.id.phone) EditText phone;

    @BindView(R.id.daftarkan_downline) AppCompatButton daftarkan_downline;

//    @BindView(R.id.back_to_home) AppCompatButton back_to_home;

    BottomNavigationView bottomNavigationView;

    AlertDialog loadingDialog;
    DaftarDownlinePresenter presenter;

    AlertDialog.Builder dialogBuilder;
    AppCompatButton backhome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registrasi_downline);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                    case R.id.home:
//                        goToNotification();
                        break;
                }
                return true;
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Registrasi Downline");

        presenter = new DaftarDownlinePresenter(this);

        showContent(true);
    }

    private void goToAkun() {
        startActivity(new Intent(DaftarDownline.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.daftarkan_downline)
    public void daftarkan() {
        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        String nama = nama_member.getText().toString();
        String alamat_member = alamat.getText().toString();
        String phone_member = phone.getText().toString();
        String perintah = "reg."+nama+"."+alamat_member+"."+phone_member+"."+"200"+"#"+pin;
        presenter.onRegsitrasiMember(this, req, kodereseller, perintah, time, pin, password);
    }

    @Override
    public void RegistrasiDownlineSukses(ResponsNonTransaksi responsNonTransaksi) {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popUpDialog = getLayoutInflater().inflate(R.layout.pop_up_laporan_registrasi_downline, null);
        backhome = (AppCompatButton) popUpDialog.findViewById(R.id.back_to_home);


        dialogBuilder.setView(popUpDialog);
        loadingDialog = dialogBuilder.create();
        loadingDialog.show();

        backhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DaftarDownline.this, TransferPulsa.class));
            }
        });
    }

    @Override
    public void RegistrasiDownlineGagal(ResponsNonTransaksi responsNonTransaksi) {

    }
}
