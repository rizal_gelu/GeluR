package com.example.gelur.module.pulsadataindosatIDM.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDMPresenterInterface {
    void onRequesttransferpulsadataIndosatIDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
