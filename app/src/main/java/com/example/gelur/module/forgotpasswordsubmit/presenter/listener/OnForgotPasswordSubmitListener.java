package com.example.gelur.module.forgotpasswordsubmit.presenter.listener;

import com.example.gelur.pojo.ForgotPasswordConfimPojo;

public interface OnForgotPasswordSubmitListener {
    void onForgotPasswordConfirmRequestSuccess(ForgotPasswordConfimPojo forgotPasswordConfimPojo);
    void onForgotPasswordConfirmRequestFail(String message);
}
