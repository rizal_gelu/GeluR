package com.example.gelur.module.pulsaregulertri.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaRegulerTri {
    void RequestTransferPulsaRegulerTriSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaRegulerTriGagal(ResponsTransaksiPulsa responsTransaksiPulsa);


    void OnClickPulsaRegulerTri(String data);
    void ResponseListPricePulsaRegulerTriSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaRegulerTriGagal(String m);
}
