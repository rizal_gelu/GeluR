package com.example.gelur.module.transferpulsa.view;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface TransferPulsaInterface {
    void getSaldoSukses(ResponsNonTransaksi responsNonTransaksi);
    void getSaldoGagal(String message);

    void goToPln();
    void goToEcommerce();
    void goToGame();
    void goToPPOB();

    void onUserLogout();
}
