package com.example.gelur.module.college.rest;

import com.example.gelur.module.account.rest.AccountAPIURLConstant;
import com.example.gelur.module.bio.rest.BioRestInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CollegeRestClient {
    private static CollegeRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    public static CollegeRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(AccountAPIURLConstant.GET_FEED_USER_PERSONAL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(CollegeRestInterface.class);
    }
}
