package com.example.gelur.module.pulsadatatelkomselTDB.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatelkomselTDB.presenter.listener.OnLoadPulsaDataTelkomselTDB;
import com.example.gelur.module.pulsadatatelkomselTDB.rest.PulsaDataTelkomselTDBRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataTelkomselTDBModel implements PulsaDataTelkomselTDBModelInterface {
    @Override
    public void RequestTransferPulsaDataTelkomselTDB(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTelkomselTDB listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataTelkomselTDBRestClient.get().req_transfer_pulsa_data_telkomsel_tdb(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataTelkomselTDBSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataTelkomselTDBGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTelkomselTDB(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTelkomselTDB listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTelkomselTDBRestClient.get().getPriceListPulsaDataTelkomselTDB(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataTelkomselTDBSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataTelkomselTDBGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataTelkomselTDBGagal(t.getMessage());
            }
        });
    }
}
