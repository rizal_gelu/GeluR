package com.example.gelur.module.topup.view;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

public interface TopUpInterface {
    void SuksesRequestTopUp(ResponsNonTransaksi responsNonTransaksi);
    void GagalRequestTopUp(String message);
}
