package com.example.gelur.module.pulsadatatelkomselTDM.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDF.model.PulsaDataTelkomselTDFModel;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.PulsaDataTelkomselTDFPresenterInterface;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;
import com.example.gelur.module.pulsadatatelkomselTDF.view.PulsaDataTelkomselTDFInterface;
import com.example.gelur.module.pulsadatatelkomselTDM.model.PulsaDataTelkomselTDMModel;
import com.example.gelur.module.pulsadatatelkomselTDM.presenter.listener.OnLoadPulsaDataTelkomselTDM;
import com.example.gelur.module.pulsadatatelkomselTDM.view.PulsaDataTelkomselTDMInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTelkomselTDMPresenter implements PulsaDataTelkomselTDMPresenterInterface, OnLoadPulsaDataTelkomselTDM {

    private WeakReference<PulsaDataTelkomselTDMInterface> view;
    private PulsaDataTelkomselTDMModel model;

    public PulsaDataTelkomselTDMPresenter(PulsaDataTelkomselTDMInterface view) {
        this.view = new WeakReference<PulsaDataTelkomselTDMInterface>(view);
        this.model = new PulsaDataTelkomselTDMModel();
    }

    @Override
    public void onRequesttransferPulsaDataTelkomselTDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTelkomselTDM(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTelkomselTDM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTelkomselTDM(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTelkomselTDMSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    @Override
    public void OnClickPulsaDataTelkomselTDM(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDMSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaDataTelkomselTDM(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDMGagal(String m) {

    }
}
