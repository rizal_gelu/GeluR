package com.example.gelur.module.saldo.presenter;

public interface SaldoPresenterInterface {
    void TopUp();
    void TransactionHistory();
    void TransferSaldo();
}
