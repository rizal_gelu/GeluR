package com.example.gelur.module.pulsadataXLXDCV.presenter;

import android.content.Context;

public interface PulsaDataXLXDCVPresenterInterface {
    void onRequesttransferpulsaDataXLXDCV(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataXLXDCV(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
