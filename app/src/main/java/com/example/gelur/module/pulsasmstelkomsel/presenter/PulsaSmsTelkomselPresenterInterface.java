package com.example.gelur.module.pulsasmstelkomsel.presenter;

import android.content.Context;

public interface PulsaSmsTelkomselPresenterInterface {
    void onRequesttransferpulsasmstelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricepulsasmstelkomsel(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
