package com.example.gelur.module.pulsasmstelkomsel.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.ecommerceovo.rest.EcommerceOvoRestClient;
import com.example.gelur.module.pulsaregulertelkomsel.rest.PulsaRegulerTelkomselRestClient;
import com.example.gelur.module.pulsasmstelkomsel.presenter.listener.OnLoadPulsaSmsTelkomsel;
import com.example.gelur.module.pulsasmstelkomsel.rest.PulsaSmsTelkomselRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaSmsTelkomselModel implements PulsaSmsTelkomselModelInterface {

    @Override
    public void RequestTransferPulsaSmsTelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaSmsTelkomsel listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaSmsTelkomselRestClient.get().req_transfer_sms_telkomsel(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn,counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaSmsTelkomselSukses(response.body());
                } else {
                    listener.RequestTransferPulsaSmsTelkomselGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaSmsTelkomsel(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaSmsTelkomsel listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = PulsaSmsTelkomselRestClient.get().getPriceListPulsaSmsTelkomsel(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaSmsTelkomselSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaSmsTelkomselGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaSmsTelkomselGagal(t.getMessage());
            }
        });
    }
}
