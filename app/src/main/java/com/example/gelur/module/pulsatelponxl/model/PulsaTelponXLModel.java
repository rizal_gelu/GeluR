package com.example.gelur.module.pulsatelponxl.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulerxl.rest.PulsaRegulerXLRestClient;
import com.example.gelur.module.pulsatelpontri.rest.PulsaTelponTriRestClient;
import com.example.gelur.module.pulsatelponxl.presenter.listener.OnLoadPulsaTelponXL;
import com.example.gelur.module.pulsatelponxl.rest.PulsaTelponXLRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaTelponXLModel implements PulsaTelponXLModelInterface{
    @Override
    public void RequestTransferPulsaTelponXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponXL listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaTelponXLRestClient.get().req_transfer_pulsa_telpon_xl(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaTelponXLSukses(response.body());
                } else {
                    listener.RequestTransferPulsaTelponXLGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaTelponXL(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponXL listener) {
        Call<ResponsNonTransaksi> sal = PulsaTelponXLRestClient.get().getPricePulsaTelponXL(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaTelponXLSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaTelponXLGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaTelponXLGagal(t.getMessage());
            }
        });
    }
}
