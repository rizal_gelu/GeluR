package com.example.gelur.module.plnprabayar.presenter;

import android.content.Context;

import com.example.gelur.module.plnprabayar.model.PlnPrabayarModel;
import com.example.gelur.module.plnprabayar.presenter.listener.OnLoadPlnPrabayar;
import com.example.gelur.module.plnprabayar.view.PlnPrabayarInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PlnPrabayarPresenter implements PlnPrabayarPresenterInterface, OnLoadPlnPrabayar {

    private WeakReference<PlnPrabayarInterface> view;
    private PlnPrabayarModel model;

    public PlnPrabayarPresenter(PlnPrabayarInterface view) {
        this.view = new WeakReference<PlnPrabayarInterface>(view);
        this.model = new PlnPrabayarModel();
    }

    @Override
    public void onRequestPlnPrabayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestPlnPrabayar(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPlnPrabayar(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePlnPrabayar(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPlnPrabayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPlnPrabayarSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPlnPrabayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPlnPrabayarGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPlnPrabayar(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePlnPrabayarSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePlnPrabayarSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePlnPrabayarGagal(String m) {

    }
}
