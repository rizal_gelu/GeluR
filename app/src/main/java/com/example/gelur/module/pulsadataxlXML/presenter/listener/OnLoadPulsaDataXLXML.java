package com.example.gelur.module.pulsadataxlXML.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataXLXML {
    void RequestTransferPulsaDataXLXMLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataXLXMLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataXLXML(String data);
    void ResponseListPricePulsaDataXLXMLSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataXLXMLGagal(String m);
}
