package com.example.gelur.module.listproducttelkomsel.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listpulsadatatelkomsel.view.ListPulsaDataTelkomsel;
import com.example.gelur.module.pulsaregulertelkomsel.view.PulsaRegulerTelkomsel;
import com.example.gelur.module.pulsasmstelkomsel.view.PulsaSmsTelkomsel;
import com.example.gelur.module.pulsatelpontelkomsel.view.PulsaTelponTelkomsel;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class ListProductTelkomsel extends AppCompatActivity implements ListProductTelkomselInterface {
    @BindView(R.id.coordinator_list_product_telkomsel)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_list_product_telkomsel)
    Toolbar toolbar;

    @BindView(R.id.content_reguler_telkomsel)
    View viewContent;

    @BindView(R.id.layout_loading_list_product_telkomsel) View viewLoading;

    @BindView(R.id.ll_paket_telpon)
    LinearLayout ll_paket_telpon;

    @BindView(R.id.paket_telpon)
    TextView paket_telpon;

    @BindView(R.id.ll_pulsa_reguller) LinearLayout ll_pulsa_reguller;

    @BindView(R.id.ll_paket_data) LinearLayout ll_paket_data;

    @BindView(R.id.paket_data) TextView paket_data;

    @BindView(R.id.ll_paket_sms) LinearLayout ll_paket_sms;

    @BindView(R.id.paket_sms) TextView paket_sms;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_product_telkomsel);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListProductTelkomsel.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListProductTelkomsel.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListProductTelkomsel.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_pulsa_reguller)
    public void onPulsaReguller() {
        startActivity(new Intent(ListProductTelkomsel.this, PulsaRegulerTelkomsel.class));
    }

    @OnClick(R.id.ll_paket_data)
    public void onClickPaketData() {
        startActivity(new Intent(ListProductTelkomsel.this, ListPulsaDataTelkomsel.class));
    }

    @OnClick(R.id.ll_paket_telpon)
    public void onClickPaketTelpon() {
        startActivity(new Intent(ListProductTelkomsel.this, PulsaTelponTelkomsel.class));
    }

    @OnClick(R.id.ll_paket_sms)
    public void onclckPaketSms() {
        startActivity(new Intent(ListProductTelkomsel.this, PulsaSmsTelkomsel.class));
    }
}
