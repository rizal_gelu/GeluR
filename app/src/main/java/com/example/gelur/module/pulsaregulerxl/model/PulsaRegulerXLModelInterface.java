package com.example.gelur.module.pulsaregulerxl.model;

import android.content.Context;

import com.example.gelur.module.pulsaregulerxl.presenter.listener.OnLoadPulsaRegulerXL;
import com.example.gelur.module.pulsasmsindosat.presenter.listener.OnLoadPulsaSmsIndosat;

public interface PulsaRegulerXLModelInterface {
    void RequestTransferPulsaRegulerXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerXL listener);
    void RequestListPricePulsaRegulerXl(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerXL listener);
}
