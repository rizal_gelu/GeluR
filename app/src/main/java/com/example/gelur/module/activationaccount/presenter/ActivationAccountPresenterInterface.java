package com.example.gelur.module.activationaccount.presenter;

import android.content.Context;

public interface ActivationAccountPresenterInterface {
    void submitActivateCode(Context context, String email, String activate_code);
}
