package com.example.gelur.module.akunpulsa.presenter;

import android.content.Context;

import com.example.gelur.module.akunpulsa.model.AkunPulsaModel;
import com.example.gelur.module.akunpulsa.presenter.listener.OnLoadAkunPulsa;
import com.example.gelur.module.akunpulsa.view.AkunPulsaInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;

import java.lang.ref.WeakReference;

public class AkunPulsaPresenter implements AkunPulsaPresenterInterface, OnLoadAkunPulsa {

    private WeakReference<AkunPulsaInterface> view;
    private AkunPulsaModel model;

    public AkunPulsaPresenter(AkunPulsaInterface view) {
        this.view = new WeakReference<AkunPulsaInterface>(view);
        this.model = new AkunPulsaModel();
    }

    @Override
    public void cekSaldoSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().getSaldoOnAkunPulsaSukses(responsNonTransaksi);
    }

    @Override
    public void cekSaldoGagal(String message) {

    }

    @Override
    public void onGetInformationAkun(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.getInformationSaldo(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
