package com.example.gelur.module.pulsadataindosatIDUA.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDU.presenter.listener.OnLoadPulsaDataIndosatIDU;
import com.example.gelur.module.pulsadataindosatIDUA.presenter.listener.OnLoadPulsaDataIndosatIDUA;

public interface PulsaDataIndosatIDUAModelInterface {
    void RequestTransferDataIndosatIDUA(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDUA listener);
    void RequestListPricePulsaDataIndosatIDUA(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDUA listener);
}
