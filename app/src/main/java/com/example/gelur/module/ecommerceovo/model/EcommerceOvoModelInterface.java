package com.example.gelur.module.ecommerceovo.model;

import android.content.Context;

import com.example.gelur.module.ecommercedana.presenter.listener.OnLoadDanaEcommerce;
import com.example.gelur.module.ecommercelinkaja.presenter.listener.OnLoadEcommerceLinkAja;
import com.example.gelur.module.ecommerceovo.presenter.listener.OnLoadEcommerceOvo;

public interface EcommerceOvoModelInterface {
    void RequestTransferSaldoOvo(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadEcommerceOvo listener);
    void RequestListPriceOvo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadEcommerceOvo listener);
}
