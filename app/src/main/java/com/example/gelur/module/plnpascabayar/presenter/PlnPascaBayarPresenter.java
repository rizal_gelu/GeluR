package com.example.gelur.module.plnpascabayar.presenter;

import android.content.Context;

import com.example.gelur.module.listbpjsppob.model.BPJSModel;
import com.example.gelur.module.listbpjsppob.view.BPJSInterface;
import com.example.gelur.module.plnpascabayar.model.PlnPascaBayarModel;
import com.example.gelur.module.plnpascabayar.presenter.listener.OnLoadPlnPascaBayar;
import com.example.gelur.module.plnpascabayar.view.PlnPascaBayarInterface;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PlnPascaBayarPresenter implements PlnPascaBayarPresenterInterface, OnLoadPlnPascaBayar {

    private WeakReference<PlnPascaBayarInterface> view;
    private PlnPascaBayarModel model;

    public PlnPascaBayarPresenter(PlnPascaBayarInterface view) {
        this.view = new WeakReference<PlnPascaBayarInterface>(view);
        this.model = new PlnPascaBayarModel();
    }

    @Override
    public void onRequestCheckPLNPascaBayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestCheckPLNPascaBayar(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onLoadCheckPLNPascaBayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().responCheckTagihanPLNPascaBayarSukses(responsTransaksiPulsa);
    }

    @Override
    public void onLoadCheckPLNPascaBayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().responCheckTagihanPLNPascaBayarGagal(responsTransaksiPulsa);
    }
}
