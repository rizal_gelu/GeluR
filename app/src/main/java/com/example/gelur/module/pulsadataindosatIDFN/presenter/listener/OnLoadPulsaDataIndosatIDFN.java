package com.example.gelur.module.pulsadataindosatIDFN.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataIndosatIDFN {
    void RequestTransferPulsaDataIndosatIDFNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataIndosatIDFNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataIndosatIDFN(String data);
    void ResponseListPricePulsaDataIndosatIDFNSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataIndosatIDFNGagal(String m);
}
