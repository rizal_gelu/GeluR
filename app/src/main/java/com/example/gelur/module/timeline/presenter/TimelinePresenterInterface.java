package com.example.gelur.module.timeline.presenter;

import android.content.Context;

import com.example.gelur.pojo.TimelinePojo;

public interface TimelinePresenterInterface {
    void loadTimeline(Context mContext);
//    void onClickLike(TimelinePojo timelinePojo);
    void onClickLikeProses(String id_timeline, String email);
    void userLogout(Context mContext);
}
