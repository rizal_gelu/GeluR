package com.example.gelur.module.pulsadatatriTRDAON.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRD.presenter.listener.OnLoadPulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;

public interface PulsaDataTriTRDAONModelInterface {
    void RequestTransferPulsaDataTriTRDAON(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDAON listener);
    void RequestListPricePulsaDataTriTRDAON(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDAON listener);
}
