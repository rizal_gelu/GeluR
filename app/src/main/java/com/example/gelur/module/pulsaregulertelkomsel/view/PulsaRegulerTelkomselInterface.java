package com.example.gelur.module.pulsaregulertelkomsel.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaRegulerTelkomselInterface {

    void TransferPulsaRegulerTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaRegulerTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaRegulerTelkomsel(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
