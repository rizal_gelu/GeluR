package com.example.gelur.module.pulsasmstelkomsel.model;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.presenter.listener.OnLoadEcommerceOvo;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.listener.OnLoadPulsaRegulerTelkomsel;
import com.example.gelur.module.pulsasmstelkomsel.presenter.listener.OnLoadPulsaSmsTelkomsel;

public interface PulsaSmsTelkomselModelInterface {
    void RequestTransferPulsaSmsTelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaSmsTelkomsel listener);
    void RequestListPricePulsaSmsTelkomsel(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaSmsTelkomsel listener);
}
