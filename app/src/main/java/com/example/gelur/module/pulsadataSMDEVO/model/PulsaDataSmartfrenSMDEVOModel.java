package com.example.gelur.module.pulsadataSMDEVO.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataSMDEVO.presenter.listener.OnLoadPulsaDataSmartfrenSMDEVO;
import com.example.gelur.module.pulsadataSMDEVO.rest.PulsaDataSmartfrenSMDEVORestClient;
import com.example.gelur.module.pulsadatasmartfrenSMD.presenter.listener.OnLoadPulsaDataSmartfrenSMD;
import com.example.gelur.module.pulsadatasmartfrenSMD.rest.PulsaDataSmartfrenSMDRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataSmartfrenSMDEVOModel implements PulsaDataSmartfrenSMDEVOModelInterface {
    @Override
    public void RequestTransferDataSmartfrenSMDEVO(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMDEVO listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataSmartfrenSMDEVORestClient.get().req_transfer_pulsa_data_smartfren_smdevo(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataSmartFrenSMDEVOSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataSmartFrenSMDEVOGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataSmartfrenSMDEVO(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMDEVO listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataSmartfrenSMDRestClient.get().getPriceListPulsaDataSmartfrenSMD(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataSmartFrenSMDEVOSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataSmartFrenSMDEVOGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataSmartFrenSMDEVOGagal(t.getMessage());
            }
        });
    }
}
