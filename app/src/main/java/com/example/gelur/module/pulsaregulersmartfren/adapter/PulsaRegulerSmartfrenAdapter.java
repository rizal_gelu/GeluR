package com.example.gelur.module.pulsaregulersmartfren.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulersmartfren.presenter.listener.OnLoadPulsaRegulerSmartfren;

import java.util.List;

public class PulsaRegulerSmartfrenAdapter extends RecyclerView.Adapter<PulsaRegulerSmartfrenAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaRegulerSmartfren listener;
    List<String> stringList;

    public PulsaRegulerSmartfrenAdapter(Activity activity, List<String> datum, OnLoadPulsaRegulerSmartfren listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaRegulerSmartfrenAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_reguler_smartfren, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaRegulerSmartfrenAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.reguler_smartfen.setText(data);

        holder.ll_kumpulan_option_pulsa_reguler_smartfren.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaRegulerSmartfren(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_reguler_smartfren;
        public final TextView reguler_smartfen;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_reguler_smartfren = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_reguler_smartfren);
            reguler_smartfen = (TextView) view.findViewById(R.id.reguler_smartfen);
        }
    }
}
