package com.example.gelur.module.activationaccount.view;

import com.example.gelur.pojo.ActivateAccountPojo;

public interface ActivationAccountInterface {
    void onActivateSuccess(ActivateAccountPojo activateAccountPojo);
    void onActivateFail(String message);
}
