package com.example.gelur.module.pulsasmstelkomsel.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaSmsTelkomselInterface {

    void TransferPulsaSmsTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaSmsTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaSmsTelkomsel(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
