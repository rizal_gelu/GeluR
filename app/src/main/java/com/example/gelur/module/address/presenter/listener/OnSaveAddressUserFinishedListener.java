package com.example.gelur.module.address.presenter.listener;

import com.example.gelur.pojo.AddressPojo;

public interface OnSaveAddressUserFinishedListener {
    void onSaveAddressSuccess(AddressPojo addressPojo);
    void onSaveAddressFailed(String message);
}
