package com.example.gelur.module.pulsatelponindosat.presenter;

import android.content.Context;

public interface PulsaTelponIndosatPresenterInterface {
    void onRequesttransferpulsatelponIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricepulsaTelponIndosat(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
