package com.example.gelur.module.pulsadataindosatIDF.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDF.presenter.listener.OnLoadPulsaDataIndosatIDF;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;

public interface PulsaDataIndosatIDFModelInterface {
    void RequestTransferDataIndosatIDF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDF listener);
    void RequestListPricePulsaDataIndosatIDF(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDF listener);
}
