package com.example.gelur.module.pulsaregulersmartfren.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaRegulerSmartfrenInterface {
    void TransferPulsaRegulerSmartfrenSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaRegulerSmartfrenGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaRegulerSmartfren(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
