package com.example.gelur.module.editprofile.view;

public interface EditProfileInterface {
    void goEditBio();
    void goEditYourAboutInfo();
    void goEditMoreInfo();
    void goEditProfilePict();
    void goEditLinks();
}
