package com.example.gelur.module.pulsadatatriTRDMINI.rest;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PulsaDataTriTRDMINIRestInterface {
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsTransaksiPulsa> req_transfer_pulsa_data_tri_trdmini(@Body RequestTransaksiPulsa requestTransaksiPulsa);

    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")Call<ResponsNonTransaksi> getPriceListPulsaDataTriTRDMINI(@Body RequestNonTransaksi requestNonTransaksi);
}
