package com.example.gelur.module.akunpulsa.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface OnLoadAkunPulsa {
    void cekSaldoSukses(ResponsNonTransaksi responsNonTransaksi);
    void cekSaldoGagal(String message);

//    void
}
