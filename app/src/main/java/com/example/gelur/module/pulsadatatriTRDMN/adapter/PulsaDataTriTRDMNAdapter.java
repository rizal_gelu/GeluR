package com.example.gelur.module.pulsadatatriTRDMN.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDMN.presenter.listener.OnLoadPulsaDataTriTRDMN;

import java.util.List;

public class PulsaDataTriTRDMNAdapter extends RecyclerView.Adapter<PulsaDataTriTRDMNAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataTriTRDMN listener;
    List<String> stringList;

    public PulsaDataTriTRDMNAdapter(Activity activity, List<String> datum, OnLoadPulsaDataTriTRDMN listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_tri_trdmn, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_tri_trdmn.setText(data);

        holder.ll_kumpulan_option_pulsa_data_tri_trdmn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataTriTRDMN(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_tri_trdmn;
        public final TextView pulsa_data_tri_trdmn;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_tri_trdmn = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_tri_trdmn);
            pulsa_data_tri_trdmn = (TextView) view.findViewById(R.id.pulsa_data_tri_trdmn);
        }
    }
}
