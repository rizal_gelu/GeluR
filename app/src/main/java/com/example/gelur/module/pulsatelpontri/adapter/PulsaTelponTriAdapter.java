package com.example.gelur.module.pulsatelpontri.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsatelpontri.presenter.listener.OnLoadPulsaTelponTri;

import java.util.List;

public class PulsaTelponTriAdapter extends RecyclerView.Adapter<PulsaTelponTriAdapter.ViewHolderContent> {
    private Activity activity;
    private OnLoadPulsaTelponTri listener;
    List<String> stringList;

    public PulsaTelponTriAdapter(Activity activity, List<String> datum, OnLoadPulsaTelponTri listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PulsaTelponTriAdapter.ViewHolderContent onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_telpon_tri, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PulsaTelponTriAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_telpon_tri.setText(data);

        holder.ll_kumpulan_option_pulsa_telpon_tri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaTelponTri(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_telpon_tri;
        public final TextView pulsa_telpon_tri;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_telpon_tri = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_telpon_tri);
            pulsa_telpon_tri = (TextView) view.findViewById(R.id.pulsa_telpon_tri);
        }
    }
}
