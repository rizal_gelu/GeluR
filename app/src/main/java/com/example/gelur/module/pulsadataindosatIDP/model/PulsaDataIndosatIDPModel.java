package com.example.gelur.module.pulsadataindosatIDP.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDM.presenter.listener.OnLoadPulsaDataIndosatIDM;
import com.example.gelur.module.pulsadataindosatIDM.rest.PulsaDataIndosatIDMRestClient;
import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataIndosatIDPModel implements PulsaDataIndosatIDPModelInterface {
    @Override
    public void RequestTransferDataIndosatIDP(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDP listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataIndosatIDMRestClient.get().req_transfer_pulsa_data_indosat_idm(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataIndosatIDPSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataIndosatIDPGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataIndosatIDP(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDP listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataIndosatIDMRestClient.get().getPriceListPulsaDataIndosatIDM(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataIndosatIDPSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataIndosatIDPGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataIndosatIDPGagal(t.getMessage());
            }
        });
    }
}
