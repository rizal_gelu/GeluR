package com.example.gelur.module.ppobindihome.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.gelur.R;
import com.example.gelur.util.AppsUtil;

public class LaporanCheckIndihome extends AppCompatActivity {

    @BindView(R.id.coordinator_laporan_pln_pasca_bayar)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_laporan_indihome)
    Toolbar toolbar;

    @BindView(R.id.content_laporan_indihome)
    View viewContent;

    @BindView(R.id.layout_loading_laporan_indihome) View viewLoading;

    @BindView(R.id.judul_laporan_bisa_berubah) TextView judul_laporan_bisa_berubah;

    @BindView(R.id.status_indihome_sukses)
    TextView status_indihome_sukses;

    @BindView(R.id.sn_indihome_transaksi_sukses) TextView sn_indihome_transaksi_sukses;

    @BindView(R.id.harga_product_indihome_sukses) TextView harga_product_indihome_sukses;

    @BindView(R.id.saldo_akhir_indihome_sukses) TextView saldo_akhir_indihome_sukses;

    @BindView(R.id.qty_indihome_byr)
    EditText qty_indihome_byr;

    @BindView(R.id.submit_bayar_indihome)
    AppCompatButton submit_bayar_indihome;

    @BindView(R.id.cancel_pembayaran) AppCompatButton cancel_pembayaran;

    AlertDialog loadingDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_laporan_indihome);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - Laporan Check Indihome");

        Intent intent = getIntent();

        String sn = intent.getStringExtra("sn");
        String status = intent.getStringExtra("status");
        String status_code = intent.getStringExtra("status_code");
        if (sn != null && status.equalsIgnoreCase("sukses") && status_code.equals("0")) {
            laporanSukses();
        } else {
            laporanGagal();
        }

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    public void laporanSukses() {
        Intent intent = getIntent();

        String status = intent.getStringExtra("status");
        String product = intent.getStringExtra("product");
        String harga = intent.getStringExtra("harga");
        String refid = intent.getStringExtra("refid");
        String sn = intent.getStringExtra("sn");
        String saldo_awal = intent.getStringExtra("saldo_awal");
        String saldo = intent.getStringExtra("saldo");
        String msisdn = intent.getStringExtra("msisdn");

        status_indihome_sukses.setText(status);
        sn_indihome_transaksi_sukses.setText(sn);
        harga_product_indihome_sukses.setText(harga);

        judul_laporan_bisa_berubah.setVisibility(View.VISIBLE);
        cancel_pembayaran.setVisibility(View.VISIBLE);
        submit_bayar_indihome.setVisibility(View.VISIBLE);
    }

    public void laporanGagal() {
        Intent intent = getIntent();
        String status = intent.getStringExtra("status");

        status_indihome_sukses.setText("Status : " + status);
        judul_laporan_bisa_berubah.setVisibility(View.GONE);
        qty_indihome_byr.setVisibility(View.GONE);
        cancel_pembayaran.setVisibility(View.GONE);
        submit_bayar_indihome.setVisibility(View.GONE);
    }
}
