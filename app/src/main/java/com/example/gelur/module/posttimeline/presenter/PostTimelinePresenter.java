package com.example.gelur.module.posttimeline.presenter;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.gelur.module.posttimeline.model.PostTimelineModel;
import com.example.gelur.module.posttimeline.presenter.listener.OnPostTimelineFinishedListener;
import com.example.gelur.module.posttimeline.view.PostTimelineInterface;
import com.example.gelur.pojo.PostTimelinePojo;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.R;

import java.lang.ref.WeakReference;

public class PostTimelinePresenter implements PostTimelinePresenterInterface, OnPostTimelineFinishedListener {

    private WeakReference<PostTimelineInterface> view;
    private PostTimelineModel model;

    public PostTimelinePresenter(PostTimelineInterface view) {
        this.view = new WeakReference<PostTimelineInterface>(view);
        this.model = new PostTimelineModel();
    }

    @Override
    public void onPostTimelineSuccess(PostTimelinePojo postTimelinePojo) {
        if (null != view.get()) view.get().onSuccesPost(postTimelinePojo);
    }

    @Override
    public void onPostTimelineFailed(String message) {
        if (null != view.get()) view.get().onFailedPost(message);
    }

    @Override
    public void submitPostTimeline(Context context, String email, String description, String file_name) {
        model.submitPostTimeline(context, email, description, file_name, this);
    }

    private UsersPojo getLoggedInUser(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(context.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        UsersPojo usersPojo = new UsersPojo();
        usersPojo.setEmail(preferences.getString(AppsConstant.KEY_ID_USER, ""));
        usersPojo.setFirst_name(preferences.getString(AppsConstant.KEY_FIRST_NAME, ""));
        usersPojo.setLast_name(preferences.getString(AppsConstant.KEY_LAST_NAME, ""));
        return usersPojo;
    }
}
