package com.example.gelur.module.pulsadatatriTRDMN.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDAON.model.PulsaDataTriTRDAONModel;
import com.example.gelur.module.pulsadatatriTRDAON.view.PulsaDataTriTRDAONInterface;
import com.example.gelur.module.pulsadatatriTRDM.view.PulsaDataTriTRDMInterface;
import com.example.gelur.module.pulsadatatriTRDMN.model.PulsaDataTriTRDMNModel;
import com.example.gelur.module.pulsadatatriTRDMN.presenter.listener.OnLoadPulsaDataTriTRDMN;
import com.example.gelur.module.pulsadatatriTRDMN.view.PulsaDataTriTRDMNInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDMNPresenter implements PulsaDataTriTRDMNPresenterInterface, OnLoadPulsaDataTriTRDMN {

    private WeakReference<PulsaDataTriTRDMNInterface> view;
    private PulsaDataTriTRDMNModel model;

    public PulsaDataTriTRDMNPresenter(PulsaDataTriTRDMNInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDMNInterface>(view);
        this.model = new PulsaDataTriTRDMNModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDMN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDMN(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDMN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDMN(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDMNSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDMNSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDMNGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDMNGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDMN(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDMNSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDMNSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDMNGagal(String m) {

    }
}
