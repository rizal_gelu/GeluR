package com.example.gelur.module.pulsaregulertelkomsel.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaRegulerTelkomsel {
    void RequestTransferPulsaRegulerTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaRegulerTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaRegulerTelkomsel(String data);
    void ResponseListPricePulsaRegulerTelkomselSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaRegulerTelkomselGagal(String m);
}
