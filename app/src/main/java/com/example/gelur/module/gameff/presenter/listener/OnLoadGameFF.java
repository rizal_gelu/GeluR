package com.example.gelur.module.gameff.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadGameFF {
    void RequestTransferDiamondFFSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferDiamondFFGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickGameFF(String data);
    void ResponseListPriceGameFFSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceGameFFGagal(String m);
}
