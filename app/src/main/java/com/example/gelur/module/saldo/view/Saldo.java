package com.example.gelur.module.saldo.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.pulsatelpontelkomsel.view.PulsaTelponTelkomsel;
import com.example.gelur.module.saldo.presenter.SaldoPresenter;
import com.example.gelur.module.topup.view.TopUp;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Saldo extends AppCompatActivity implements SaldoInterface {

    @BindView(R.id.coordinator_saldo)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_saldo_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_saldo)
    View viewContent;

    @BindView(R.id.layout_loading_saldo) View viewLoading;

    @BindView(R.id.showupSaldo)
    LinearLayout ll_showupsaldo;

    @BindView(R.id.tv_saldo_anda)
    TextView tv_saldo_anda;

    @BindView(R.id.tv_saldo_nominal) TextView tv_saldo_nominal;

    @BindView(R.id.kumpulan_option) LinearLayout ll_kumpulan_option;

    @BindView(R.id.ll_topup) LinearLayout ll_topup;

    @BindView(R.id.tv_top_up) TextView tv_top_up;

    @BindView(R.id.ll_transaction_history) LinearLayout ll_transaction_history;

    @BindView(R.id.tv_transaction_history) TextView tv_transaction_history;

    @BindView(R.id.ll_transfer_saldo) LinearLayout ll_transfer_saldo;

    @BindView(R.id.tv_transfer_saldo) TextView tv_transfer_saldo;

    AlertDialog loadingDialog;
    SaldoPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_saldo);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new SaldoPresenter(this);

        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        System.out.println("Random value in int from "+min+" to "+max+ ":");
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(Saldo.this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        //hardcode
        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String perintah = "sal";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        presenter.getInformasiSaldo(this, req, kodereseller, perintah, time, pin, password);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void SaldoSukses(ResponsNonTransaksi responsNonTransaksi) {
        String msg = responsNonTransaksi.getMsg();
        String[] kata = msg.split("\\r\\n");

        String get_saldo = kata[1];
        String[] saldo = get_saldo.split("\\s+");

        viewLoading.setVisibility(View.VISIBLE);

        tv_saldo_nominal.setText(saldo[1]);
        showContent(true);
    }

    @Override
    public void SaldoGagal(String message) {

    }

    @OnClick(R.id.ll_topup)
    public void TopUp() {
        startActivity(new Intent(Saldo.this, TopUp.class));
    }
}
