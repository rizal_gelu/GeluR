package com.example.gelur.module.posttimeline.presenter;

import android.content.Context;

import com.example.gelur.module.posttimeline.presenter.listener.OnPostTimelineFinishedListener;
import com.example.gelur.pojo.PostTimelinePojo;

public interface PostTimelinePresenterInterface {
    void submitPostTimeline(Context context, String email, String description, String file_name);
}
