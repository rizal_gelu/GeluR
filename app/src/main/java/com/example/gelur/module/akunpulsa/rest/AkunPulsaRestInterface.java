package com.example.gelur.module.akunpulsa.rest;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AkunPulsaRestInterface {
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsNonTransaksi> getSaldoAkun(@Body RequestNonTransaksi requestNonTransaksi);
}
