package com.example.gelur.module.company.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.college.view.College;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.taskboardcompany.view.TaskBoard;
import com.example.gelur.module.timeline.view.Timeline;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Company extends AppCompatActivity implements CompanyInterface {

    @BindView(R.id.activity_company_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_company_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_company_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    BottomNavigationView bottomNavigationView;

    FloatingActionButton floatingActionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Company.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHome() {
        startActivity(new Intent(Company.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(Company.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(Company.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(Company.this, Account.class));
    }

    @Override
    public void onClickAttendance() {

    }

    @OnClick(R.id.linear_bungkus_task_board)
    public void onClickTaskBoard() {
        startActivity(new Intent(Company.this, TaskBoard.class));
    }
}
