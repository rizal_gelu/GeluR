package com.example.gelur.module.historytransaksi.presenter;

import android.content.Context;

public interface HistoryTransaksiPresenterInterface {
    void findHistoryTransaksi(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
