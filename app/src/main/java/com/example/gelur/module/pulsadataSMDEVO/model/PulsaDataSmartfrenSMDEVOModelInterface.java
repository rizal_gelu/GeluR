package com.example.gelur.module.pulsadataSMDEVO.model;

import android.content.Context;

import com.example.gelur.module.pulsadataSMDEVO.presenter.listener.OnLoadPulsaDataSmartfrenSMDEVO;
import com.example.gelur.module.pulsadatasmartfrenSMD.presenter.listener.OnLoadPulsaDataSmartfrenSMD;

public interface PulsaDataSmartfrenSMDEVOModelInterface {
    void RequestTransferDataSmartfrenSMDEVO(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMDEVO listener);
    void RequestListPricePulsaDataSmartfrenSMDEVO(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataSmartfrenSMDEVO listener);
}
