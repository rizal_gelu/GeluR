package com.example.gelur.module.pulsaregulerindosat.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulerindosat.presenter.listener.OnLoadPulsaRegulerIndosat;
import com.example.gelur.module.pulsaregulerindosat.rest.PulsaRegulerIndosatRestClient;
import com.example.gelur.pojo.FormatResponseNonTransaksi;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaRegulerIndosatModel implements PulsaRegulerIndosatModelInterface{
    @Override
    public void RequestTransferPulsaIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerIndosat listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaRegulerIndosatRestClient.get().req_transfer_pulsa_indosat(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaRegulerIndosatSukses(response.body());
                } else {
                    listener.RequestTransferPulsaRegulerIndosatGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void CheckListPulsa(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerIndosat listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = PulsaRegulerIndosatRestClient.get().getListPulsaIndosat(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.RequestListPulsaRegulerIndosatSukses(response.body());
                } else {
                    listener.RequestListPulsaRegulerIndosatGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.RequestListPulsaRegulerIndosatGagal(t.getMessage());
            }
        });
    }
}
