package com.example.gelur.module.transferpulsa.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.transferpulsa.presenter.listener.OnLoadCekSaldoFinished;
import com.example.gelur.module.transferpulsa.rest.TransferPulsaRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransferPulsaModel implements TransferPulsaModelInterface{
    @Override
    public void getInformationSaldo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadCekSaldoFinished listener) {

        retrofit2.Call<ResponsNonTransaksi> sal = TransferPulsaRestClient.get().getSaldo(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.cekSaldoSukses(response.body());
                } else {
                    listener.cekSaldoGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.cekSaldoGagal(t.getMessage());
            }
        });
    }
}
