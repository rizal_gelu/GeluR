package com.example.gelur.module.address.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.gelur.R;
import com.example.gelur.module.address.presenter.AddressPresenter;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Address extends AppCompatActivity implements AddressInterface {

    @BindView(R.id.activity_address_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_address_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_address_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    BottomNavigationView bottomNavigationView;

    FloatingActionButton floatingActionButton;

    AddressPresenter presenter;

    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_address);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return false;
            }
        });

        ButterKnife.bind(this);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new AddressPresenter();

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSaveBioSuccess() {

    }

    @Override
    public void onSaveBioFail(String message) {

    }
}
