package com.example.gelur.module.forgotpassword.rest;

import com.example.gelur.util.AppsConstant;

public class ForgotPasswordAPIRULConstant {
    public static final String BASE_API_FORGOT_PASSWORD_URL = AppsConstant.BASE_API_URL + "v1/users/";
    public static final String FORGOT_PASSWORD_URL = BASE_API_FORGOT_PASSWORD_URL + "forgot_password/";
}
