package com.example.gelur.module.ecommercelinkaja.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface EcommerceLinkAjaInterface {

    void TransferSaldoLinkAjaSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferSaldoLinkAjaGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceLinkAja(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
