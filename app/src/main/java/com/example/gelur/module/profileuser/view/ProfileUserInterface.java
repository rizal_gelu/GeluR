package com.example.gelur.module.profileuser.view;

public interface ProfileUserInterface {
    void followClickBtn();
    void seeAllFriendBtn();
    void seePhotosBtn();
    void seeAgenda();
    void moreInformationUser();
}
