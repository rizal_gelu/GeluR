package com.example.gelur.module.pulsadataXLXDC.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataXLXDCInterface {
    void TransferPulsaDataXLXDCSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataXLXDCGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaXLXDCSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaXLXDCGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
