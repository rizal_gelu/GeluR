package com.example.gelur.module.pulsadataxlXML.presenter;

import android.content.Context;

public interface PulsaDataXLXMLPresenterInterface {
    void onRequesttransferpulsaDataXLXML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataXLXML(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
