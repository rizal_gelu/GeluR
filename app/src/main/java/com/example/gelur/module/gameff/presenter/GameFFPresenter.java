package com.example.gelur.module.gameff.presenter;

import android.content.Context;

import com.example.gelur.module.gameff.model.GameFFModel;
import com.example.gelur.module.gameff.model.GameFFModelInterface;
import com.example.gelur.module.gameff.presenter.listener.OnLoadGameFF;
import com.example.gelur.module.gameff.view.GameFFInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class GameFFPresenter implements GameFFPresenterInterface, OnLoadGameFF {

    private WeakReference<GameFFInterface> view;
    private GameFFModel model;

    public GameFFPresenter(GameFFInterface view) {
        this.view = new WeakReference<GameFFInterface>(view);
        this.model = new GameFFModel();
    }

    @Override
    public void RequestTransferDiamondFFSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferDiamondFFSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferDiamondFFGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferDiamondFFGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickGameFF(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceGameFFSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceGameFF(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceGameFFGagal(String m) {

    }

    @Override
    public void onRequesttransferDiamondFF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDiamondFF(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceGameFF(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceGameFF(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
