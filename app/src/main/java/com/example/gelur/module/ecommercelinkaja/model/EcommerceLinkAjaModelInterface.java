package com.example.gelur.module.ecommercelinkaja.model;

import android.content.Context;

import com.example.gelur.module.ecommercegojekpenumpang.presenter.listener.OnLoadGojekPenumpang;
import com.example.gelur.module.ecommercelinkaja.presenter.listener.OnLoadEcommerceLinkAja;

public interface EcommerceLinkAjaModelInterface {
    void RequestTransferSaldoLinkAja(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadEcommerceLinkAja listener);
    void RequestListPriceLinkAja(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadEcommerceLinkAja listener);
}
