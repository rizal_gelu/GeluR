package com.example.gelur.module.plnprabayar.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PlnPrabayarInterface {
    void TransferPlnPrabayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPlnPrabayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePlnPrabayarSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePlnPrabayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
