package com.example.gelur.module.pulsaregulerxl.presenter;

import android.content.Context;

import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PulsaRegulerXLPresenterInterface {
    void onRequesttransferpulsaRegulerXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricepulsaregulerXl(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
