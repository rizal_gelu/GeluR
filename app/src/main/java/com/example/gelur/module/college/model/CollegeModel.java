package com.example.gelur.module.college.model;

import android.content.Context;

import com.example.gelur.module.bio.rest.BioRestClient;
import com.example.gelur.module.college.presenter.listener.OnSaveCollegeFinishedListener;
import com.example.gelur.module.college.rest.CollegeRestClient;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.CollegePojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollegeModel implements CollegeModelInterface{
    @Override
    public void clickSaveCollege(Context context, String email, String college_name, String faculty_major, int graduated, String years_graduate, String description, OnSaveCollegeFinishedListener listener) {
        Call<CollegePojo> college_data = CollegeRestClient.get().postCollege(email, college_name, faculty_major, graduated, years_graduate, description);
        college_data.enqueue(new Callback<CollegePojo>() {
            @Override
            public void onResponse(Call<CollegePojo> call, Response<CollegePojo> response) {
                if (response.isSuccessful()) {
                    listener.onSaveCollegeSuccess(response.body());
                } else {
                    listener.onSaveCollegeFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<CollegePojo> call, Throwable t) {
                listener.onSaveCollegeFailed(t.getMessage());
            }
        });
    }
}
