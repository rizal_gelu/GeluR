package com.example.gelur.module.listproductxl.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listpulsadataXL.view.ListPulsaDataXL;
import com.example.gelur.module.pulsadataxlXML.view.PulsaDataXLXML;
import com.example.gelur.module.pulsaregulerxl.view.PulsaRegulerXL;
import com.example.gelur.module.pulsatelponxl.view.PulsaTelponXL;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ListProductXl extends AppCompatActivity implements ListProductXlInterface {
    @BindView(R.id.coordinator_list_product_xl)
    CoordinatorLayout coordinator_list_product_xl;

    @BindView(R.id.activity_toolbar_list_product_xl)
    Toolbar toolbar;

    @BindView(R.id.content_list_content_xl)
    View viewContent;

    @BindView(R.id.layout_loading_list_product_xl) View viewLoading;

    @BindView(R.id.kumpulan_option_list_product_xl)
    LinearLayout kumpulan_option_list_product_xl;

    @BindView(R.id.ll_paket_telpon_xl) LinearLayout ll_paket_telpon_xl;

    @BindView(R.id.paket_telpon_xl)
    TextView paket_telpon_xl;

    @BindView(R.id.ll_paket_data_xl) LinearLayout ll_paket_data_xl;

    @BindView(R.id.paket_data_xl) TextView paket_data_xl;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_product_xl);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Produk XL");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListProductXl.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListProductXl.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListProductXl.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_pulsa_reguller_xl)
    public void onClickPulsaRegullerXl() {
        startActivity(new Intent(ListProductXl.this, PulsaRegulerXL.class));
    }

    @OnClick(R.id.ll_paket_data_xl)
    public void onClickPaketDataXl() {
        startActivity(new Intent(ListProductXl.this, ListPulsaDataXL.class));
    }

    @OnClick(R.id.ll_paket_telpon_xl)
    public void onClickPaketTelponXl() {
        startActivity(new Intent(ListProductXl.this, PulsaTelponXL.class));
    }
}
