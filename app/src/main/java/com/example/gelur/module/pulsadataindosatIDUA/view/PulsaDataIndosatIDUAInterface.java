package com.example.gelur.module.pulsadataindosatIDUA.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataIndosatIDUAInterface {
    void TransferPulsaDataIndosatIDUASukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataIndosatIDUAGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataIndosatIDUASukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataIndosatIDUAGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
