package com.example.gelur.module.transferpulsa.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface OnLoadCekSaldoFinished {
    void cekSaldoSukses(ResponsNonTransaksi responsNonTransaksi);
    void cekSaldoGagal(String message);
}
