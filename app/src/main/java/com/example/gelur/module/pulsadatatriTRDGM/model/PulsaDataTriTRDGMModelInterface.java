package com.example.gelur.module.pulsadatatriTRDGM.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDGM.presenter.listener.OnLoadPulsaDataTriTRDGM;

public interface PulsaDataTriTRDGMModelInterface {
    void RequestTransferPulsaDataTriTRDGM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDGM listener);
    void RequestListPricePulsaDataTriTRDGM(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDGM listener);
}
