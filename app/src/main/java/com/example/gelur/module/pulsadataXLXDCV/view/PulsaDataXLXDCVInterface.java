package com.example.gelur.module.pulsadataXLXDCV.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataXLXDCVInterface {
    void TransferPulsaDataXLXDCVSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataXLXDCVGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaXLXDCVSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaXLXDCVGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
