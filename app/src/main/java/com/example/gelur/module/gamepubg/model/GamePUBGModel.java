package com.example.gelur.module.gamepubg.model;

import android.content.Context;

import com.example.gelur.module.gameml.rest.GameMLRestClient;
import com.example.gelur.module.gamepubg.presenter.listener.OnLoadGamePUBG;
import com.example.gelur.module.gamepubg.rest.GamePUBGRestClient;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GamePUBGModel implements GamePUBGModelInterface {
    @Override
    public void RequestTransferDiamondML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGamePUBG listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = GamePUBGRestClient.get().req_transfer_diamond_pubg(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn,counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferDiamondPUBGSukses(response.body());
                } else {
                    listener.RequestTransferDiamondPUBGGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }
}
