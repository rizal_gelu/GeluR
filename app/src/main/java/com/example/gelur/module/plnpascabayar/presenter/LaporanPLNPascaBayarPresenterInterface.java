package com.example.gelur.module.plnpascabayar.presenter;

import android.content.Context;

public interface LaporanPLNPascaBayarPresenterInterface {
    void onRequestBayarPLNPascaBayar(Context context, String req, String kodereseller, String produk, String msisdn, String qty, String reffid, String time, String pin, String password);
}
