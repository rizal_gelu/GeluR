package com.example.gelur.module.pulsaregulerindosat.model;

import android.content.Context;

import com.example.gelur.module.pulsaregulerindosat.presenter.listener.OnLoadPulsaRegulerIndosat;
import com.example.gelur.module.transferpulsa.presenter.listener.OnLoadCekSaldoFinished;

public interface PulsaRegulerIndosatModelInterface {
    void RequestTransferPulsaIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerIndosat listener);
    void CheckListPulsa(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerIndosat listener);
}
