package com.example.gelur.module.activationaccount.rest;

import com.example.gelur.util.AppsConstant;

public class ActivationAccountAPIURLConstant {
    public static final String BASE_API_ACTIVATE_ACCOUNT_URL = AppsConstant.BASE_API_URL + "v1/users/";
    public static final String ACTIVATE_ACCOUNT = BASE_API_ACTIVATE_ACCOUNT_URL + "activation_user/";
}
