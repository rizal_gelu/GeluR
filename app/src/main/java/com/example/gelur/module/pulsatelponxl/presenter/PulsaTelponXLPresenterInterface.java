package com.example.gelur.module.pulsatelponxl.presenter;

import android.content.Context;

public interface PulsaTelponXLPresenterInterface {
    void onRequesttransferpulsaTelponXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaTelponXL(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
