package com.example.gelur.module.ubahpin.view;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.daftardownline.presenter.DaftarDownlinePresenter;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UbahPin extends AppCompatActivity {

    @BindView(R.id.coordinator_ubah_pin)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_ubah_pin)
    Toolbar toolbar;

    @BindView(R.id.content_ubah_pin)
    View viewContent;

    @BindView(R.id.layout_loading_ubah_pin) View viewLoading;

    @BindView(R.id.pin_lama)
    EditText pin_lama;

    @BindView(R.id.pin_baru) EditText pin_baru;

    @BindView(R.id.pin_baru_ulang) EditText pin_baru_ulang;

    @BindView(R.id.reset_pin)
    AppCompatButton reset_pin;

    AlertDialog loadingDialog;
//    DaftarDownlinePresenter presenter;

    AlertDialog.Builder dialogBuilder;
    AppCompatButton backhome;

    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ubah_pin);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home:
                        goToHome();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Registrasi Downline");

//        presenter = new DaftarDownlinePresenter(this);

        showContent(true);
    }

    private void goToHome() {
    }

    private void goToHistoryTransaksi() {
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }
}
