package com.example.gelur.module.forgotpassword.presenter.listener;

public interface OnForgotPasswordFinishedListener {
    void onForgotPasswordRequestFinish(String message);
    void onForgotPasswordRequestFinishFail(String message);
}
