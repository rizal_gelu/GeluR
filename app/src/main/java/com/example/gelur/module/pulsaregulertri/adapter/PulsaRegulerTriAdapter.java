package com.example.gelur.module.pulsaregulertri.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulertri.presenter.listener.OnLoadPulsaRegulerTri;

import java.util.List;

public class PulsaRegulerTriAdapter extends RecyclerView.Adapter<PulsaRegulerTriAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaRegulerTri listener;
    List<String> stringList;

    public PulsaRegulerTriAdapter(Activity activity, List<String> datum, OnLoadPulsaRegulerTri listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaRegulerTriAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_reguler_tri, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaRegulerTriAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.reguler_tri.setText(data);

        holder.ll_kumpulan_option_pulsa_reguler_tri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaRegulerTri(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_reguler_tri;
        public final TextView reguler_tri;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_reguler_tri = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_reguler_tri);
            reguler_tri = (TextView) view.findViewById(R.id.reguler_tri);
        }
    }
}
