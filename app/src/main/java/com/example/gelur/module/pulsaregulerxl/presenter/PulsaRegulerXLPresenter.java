package com.example.gelur.module.pulsaregulerxl.presenter;

import android.content.Context;

import com.example.gelur.module.pulsaregulerindosat.model.PulsaRegulerIndosatModel;
import com.example.gelur.module.pulsaregulerindosat.view.PulsaRegulerIndosatInterface;
import com.example.gelur.module.pulsaregulerxl.model.PulsaRegulerXLModel;
import com.example.gelur.module.pulsaregulerxl.presenter.listener.OnLoadPulsaRegulerXL;
import com.example.gelur.module.pulsaregulerxl.view.PulsaRegulerXLInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaRegulerXLPresenter implements PulsaRegulerXLPresenterInterface, OnLoadPulsaRegulerXL {

    private WeakReference<PulsaRegulerXLInterface> view;
    private PulsaRegulerXLModel model;

    public PulsaRegulerXLPresenter(PulsaRegulerXLInterface view) {
        this.view = new WeakReference<PulsaRegulerXLInterface>(view);
        this.model = new PulsaRegulerXLModel();
    }

    @Override
    public void onRequesttransferpulsaRegulerXL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaRegulerXL(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricepulsaregulerXl(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaRegulerXl(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaRegulerXLSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerXLSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaRegulerXLGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerXLGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaRegulerXl(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaRegulerXLSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaRegulerXL(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaRegulerXlGagal(String m) {

    }
}
