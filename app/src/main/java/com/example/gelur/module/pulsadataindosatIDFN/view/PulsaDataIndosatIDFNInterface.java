package com.example.gelur.module.pulsadataindosatIDFN.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataIndosatIDFNInterface {
    void TransferPulsaDataIndosatIDFNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataIndosatIDFNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataIndosatIDFNSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataIndosatIDFNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
