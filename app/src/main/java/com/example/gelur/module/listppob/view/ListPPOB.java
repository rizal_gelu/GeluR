package com.example.gelur.module.listppob.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.listbpjsppob.view.BPJS;
import com.example.gelur.module.plnpascabayar.view.PlnPascaBayar;
import com.example.gelur.module.ppobindihome.view.PpobIndihome;
import com.example.gelur.util.AppsUtil;

public class ListPPOB extends AppCompatActivity implements ListPPOBInterface {

    @BindView(R.id.coordinator_list_gojek_ecommerce)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_list_ppob)
    Toolbar toolbar;

    @BindView(R.id.content_list_ppob)
    View viewContent;

    @BindView(R.id.layout_loading_ppob) View viewLoading;

    @BindView(R.id.kumpulan_option_list_ppob)
    LinearLayout kumpulan_option_list_ppob;

    @BindView(R.id.ll_PLN) LinearLayout ll_PLN;
    @BindView(R.id.ll_BPJS) LinearLayout ll_BPJS;

    @BindView(R.id.PLN)
    TextView pln;
    @BindView(R.id.BPJS)
    TextView BPJS;

    AlertDialog loadingDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_ppob);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - Ecommerce");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.ll_PLN)
    public void onClickPLN() {
        startActivity(new Intent(ListPPOB.this, PlnPascaBayar.class));
    }

    @OnClick(R.id.ll_BPJS)
    public void onClickBPJS() {
        startActivity(new Intent(ListPPOB.this, com.example.gelur.module.listbpjsppob.view.BPJS.class));
    }

    @OnClick(R.id.ll_indihome)
    public void onClickIndihome() {
        startActivity(new Intent(ListPPOB.this, PpobIndihome.class));
    }
}
