package com.example.gelur.module.plnpascabayar.presenter.listener;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPlnPascaBayar {
    void onLoadCheckPLNPascaBayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void onLoadCheckPLNPascaBayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa);
}
