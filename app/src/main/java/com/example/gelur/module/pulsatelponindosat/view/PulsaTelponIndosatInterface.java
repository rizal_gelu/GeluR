package com.example.gelur.module.pulsatelponindosat.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaTelponIndosatInterface {
    void TransferPulsaTelponIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaTelponIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaTelponIndosat(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
