package com.example.gelur.module.pulsasmstelkomsel.presenter;

import android.content.Context;

import com.example.gelur.module.pulsasmstelkomsel.model.PulsaSmsTelkomselModel;
import com.example.gelur.module.pulsasmstelkomsel.presenter.listener.OnLoadPulsaSmsTelkomsel;
import com.example.gelur.module.pulsasmstelkomsel.view.PulsaSmsTelkomselInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaSmsTelkomselPresenter implements PulsaSmsTelkomselPresenterInterface, OnLoadPulsaSmsTelkomsel {

    private WeakReference<PulsaSmsTelkomselInterface> view;
    private PulsaSmsTelkomselModel model;

    public PulsaSmsTelkomselPresenter(PulsaSmsTelkomselInterface view) {
        this.view = new WeakReference<PulsaSmsTelkomselInterface>(view);
        this.model = new PulsaSmsTelkomselModel();
    }

    @Override
    public void onRequesttransferpulsasmstelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaSmsTelkomsel(context, req, kodereseller, produk, msisdn,counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricepulsasmstelkomsel(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaSmsTelkomsel(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaSmsTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaSmsTelkomselSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaSmsTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaSmsTelkomselGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaSmsTelkomsel(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaSmsTelkomselSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaSmsTelkomsel(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaSmsTelkomselGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
