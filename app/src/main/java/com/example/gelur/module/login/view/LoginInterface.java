package com.example.gelur.module.login.view;

import com.example.gelur.pojo.UsersPojo;

import java.util.List;

public interface LoginInterface {
    void onLoginFailed(String message);
    void onLoginSuccess(List<UsersPojo> user);
    void goToSignUpScreen();
    void goToForgotPasswordScreen();
}
