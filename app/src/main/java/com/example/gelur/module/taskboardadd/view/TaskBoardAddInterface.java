package com.example.gelur.module.taskboardadd.view;

public interface TaskBoardAddInterface {
    void onSaveTask();
    void onCancelTask();
}
