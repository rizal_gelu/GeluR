package com.example.gelur.module.pulsaregulersmartfren.presenter;

import android.content.Context;

public interface PulsaRegulerSmartfrenPresenterInterface {
    void onRequesttransferpulsaregulerSmartfren(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaRegulerSmartfren(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
