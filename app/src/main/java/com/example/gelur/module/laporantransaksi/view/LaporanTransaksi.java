package com.example.gelur.module.laporantransaksi.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.module.saldo.presenter.SaldoPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.gelur.R;
import com.example.gelur.util.AppsUtil;

public class LaporanTransaksi extends AppCompatActivity implements LaporanTransaksiInterface {

    @BindView(R.id.coordinator_laporan_transaksi)
    CoordinatorLayout coordinator_laporan_transaksi;

    @BindView(R.id.activity_laporan_transaksi_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_laporan_transaksi)
    View viewContent;

    @BindView(R.id.layout_loading_laporan_transaksi) View viewLoading;

    @BindView(R.id.laporan_sukses) LinearLayout laporan_sukses;

    @BindView(R.id.transaksi_sukses) ImageView transaksi_sukses;

    @BindView(R.id.status_sukses) TextView status_sukses;

    @BindView(R.id.sn_transaksi_sukses) TextView sn_transaksi_sukses;

    @BindView(R.id.harga_product_sukses) TextView harga_product_sukses;

    @BindView(R.id.saldo_akhir_sukses) TextView saldo_akhir_sukses;


    @BindView(R.id.laporan_Gagal) LinearLayout laporan_Gagal;

    @BindView(R.id.transaksi_gagal) ImageView transaksi_gagal;

    @BindView(R.id.status_gagal) TextView status_gagal;

    @BindView(R.id.sn_transaksi_gagal) TextView sn_transaksi_gagal;

    @BindView(R.id.harga_product_gagal) TextView harga_product_gagal;

    @BindView(R.id.saldo_akhir_gagal) TextView saldo_akhir_gagal;

    AlertDialog loadingDialog;
    SaldoPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_laporan_transaksi);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - Laporan Transaksi");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        Intent intent = getIntent();

        String status = intent.getStringExtra("status");
        String product = intent.getStringExtra("product");
        String harga = intent.getStringExtra("harga");
        String refid = intent.getStringExtra("refid");
        String sn = intent.getStringExtra("sn");
        String saldo_awal = intent.getStringExtra("saldo_awal");
        String saldo = intent.getStringExtra("saldo");

        if ("sukses".equalsIgnoreCase(intent.getStringExtra("status"))) {
            transaksiSukses(status, product, harga, refid, sn, saldo_awal, saldo);
        } else {
            transaksiGagal(status, product, harga, refid, sn, saldo_awal, saldo);
        }

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void transaksiSukses(String status, String product, String harga, String refid, String sn, String saldo_awal, String saldo) {
        laporan_sukses.setVisibility(View.VISIBLE);
        laporan_Gagal.setVisibility(View.GONE);
//
        status_sukses.setText("Status Transaksi : " + status);
        sn_transaksi_sukses.setText("Sn Transaksi : " +sn);
        harga_product_sukses.setText(harga);
        saldo_akhir_sukses.setText("Saldo : " +saldo_awal + "-" + harga + "=" + saldo);
    }

    @Override
    public void transaksiGagal(String status, String product, String harga, String refid, String sn, String saldo_awal, String saldo) {
        laporan_Gagal.setVisibility(View.VISIBLE);
        laporan_sukses.setVisibility(View.GONE);

        status_gagal.setText("Status Transaksi : " + status + " Mohon di cek kembali nomor tujuan");
//        sn_transaksi_gagal.setText(sn);
//        harga_product_gagal.setText(harga);
//        saldo_akhir_gagal.setText(saldo_awal + "-" + harga + "=" + saldo);

    }
}
