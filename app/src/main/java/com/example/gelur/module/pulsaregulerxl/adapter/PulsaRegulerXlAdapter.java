package com.example.gelur.module.pulsaregulerxl.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulerxl.presenter.listener.OnLoadPulsaRegulerXL;

import java.util.List;

public class PulsaRegulerXlAdapter extends RecyclerView.Adapter<PulsaRegulerXlAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaRegulerXL listener;
    List<String> stringList;

    public PulsaRegulerXlAdapter(Activity activity, List<String> datum, OnLoadPulsaRegulerXL listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaRegulerXlAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_reguler_xl, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaRegulerXlAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.reguler_xl.setText(data);

        holder.ll_kumpulan_option_pulsa_reguler_xl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaRegulerXl(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_reguler_xl;
        public final TextView reguler_xl;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_reguler_xl = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_reguler_xl);
            reguler_xl = (TextView) view.findViewById(R.id.reguler_xl);
        }
    }
}
