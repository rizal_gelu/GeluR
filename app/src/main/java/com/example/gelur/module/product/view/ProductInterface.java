package com.example.gelur.module.product.view;

import com.example.gelur.pojo.ProductPojo;

public interface ProductInterface {
    void onLoadProductListSuccess(ProductPojo productPojo);
    void onLoadProductListFail(String Message);
    void onClickProductList();
}
