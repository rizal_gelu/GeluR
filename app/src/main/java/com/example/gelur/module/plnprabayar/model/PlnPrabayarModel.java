package com.example.gelur.module.plnprabayar.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.plnprabayar.presenter.listener.OnLoadPlnPrabayar;
import com.example.gelur.module.plnprabayar.rest.PlnPrabayarRestClient;
import com.example.gelur.module.pulsadataindosatIDF.rest.PulsaDataIndosatIDFRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlnPrabayarModel implements PlnPrabayarModelInterface {
    @Override
    public void RequestPlnPrabayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPlnPrabayar listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PlnPrabayarRestClient.get().req_transfer_token_listrik(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPlnPrabayarSukses(response.body());
                } else {
                    listener.RequestTransferPlnPrabayarGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePlnPrabayar(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPlnPrabayar listener) {
        Call<ResponsNonTransaksi> sal = PlnPrabayarRestClient.get().getPriceListPlnPrabayar(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePlnPrabayarSukses(response.body());
                } else {
                    listener.ResponseListPricePlnPrabayarGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePlnPrabayarGagal(t.getMessage());
            }
        });
    }
}
