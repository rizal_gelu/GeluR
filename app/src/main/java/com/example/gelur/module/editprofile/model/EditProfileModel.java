package com.example.gelur.module.editprofile.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.activationaccount.rest.ActivationAccountRestClient;
import com.example.gelur.module.bio.presenter.listener.OnLoadBioFinished;
import com.example.gelur.module.bio.view.Bio;
import com.example.gelur.module.editprofile.rest.EditProfileRestClient;
import com.example.gelur.pojo.ActivateAccountPojo;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.FormaterResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class EditProfileModel implements EditProfileModelInterface{

    @Override
    public void onLoadBioSubmit(final Context context, String email, final OnLoadBioFinished listener) {
        Call<BioPojo> active = EditProfileRestClient.get().getData_bio(email);
        active.enqueue(new Callback<BioPojo>() {
            @Override
            public void onResponse(Call<BioPojo> call, Response<BioPojo> response) {
                if (response.isSuccessful()) {
                    BioPojo bioPojo = response.body();
                    listener.onLoadBioDataSuccess(bioPojo);
                } else {
                    listener.onLoadBioeDataFailed(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<BioPojo> call, Throwable t) {
                listener.onLoadBioeDataFailed(t.getMessage());
            }
        });
    }
}
