package com.example.gelur.module.akunpulsa.presenter;

import android.content.Context;

public interface AkunPulsaPresenterInterface {
    void onGetInformationAkun(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
