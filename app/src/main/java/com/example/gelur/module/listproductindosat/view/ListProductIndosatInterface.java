package com.example.gelur.module.listproductindosat.view;

public interface ListProductIndosatInterface {
    void onClickPulsaRegullerIndosat();
    void onClickPaketDataIndosat();
    void onClickPaketTelponIndosat();
    void onclckPaketSmsIndosat();
}
