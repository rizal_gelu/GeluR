package com.example.gelur.module.phone.presenter;

import android.content.Context;

public interface PhonePresenterInterface {
    void onSaveClicked(Context context, String email, String phone);
    void onCancelClicked(String message);
}
