package com.example.gelur.module.bio.rest;

import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.FormaterResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface BioRestInterface {
    @FormUrlEncoded
    @POST(BioAPIURLConstant.POST_BIO)
    Call<BioPojo> postBio(@Field("email") String email, @Field("bio") String bio);

    @GET(BioAPIURLConstant.GET_BIO)
    Call<BioPojo> getBioUser(@Query("email") String email);
}
