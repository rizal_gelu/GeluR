package com.example.gelur.module.pulsadataXLXD.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataXLPXD.model.PulsaDataXLPXDModel;
import com.example.gelur.module.pulsadataXLPXD.view.PulsaDataXLPXDInterface;
import com.example.gelur.module.pulsadataXLXD.model.PulsaDataXLXDModel;
import com.example.gelur.module.pulsadataXLXD.presenter.listener.OnLoadPulsaDataXLXD;
import com.example.gelur.module.pulsadataXLXD.view.PulsaDataXLXDInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataXLXDPresenter implements PulsaDataXLPXDPresenterInterface, OnLoadPulsaDataXLXD {

    private WeakReference<PulsaDataXLXDInterface> view;
    private PulsaDataXLXDModel model;

    public PulsaDataXLXDPresenter(PulsaDataXLXDInterface view) {
        this.view = new WeakReference<PulsaDataXLXDInterface>(view);
        this.model = new PulsaDataXLXDModel();
    }

    @Override
    public void onRequesttransferpulsaDataXLPXD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataXLXD(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataXLPXD(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataXLXD(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataXLXDSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXDSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataXLXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXDGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataXLXD(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataXLXDSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaXLXDSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataXLXDGagal(String m) {

    }
}
