package com.example.gelur.module.taskboardcompany.rest;

import com.example.gelur.module.account.rest.AccountAPIURLConstant;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TaskBoardRestClient {
    private static TaskBoardRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    public static TaskBoardRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(TaskBoardAPIURLConstant.GET_TASK_BOARD)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(TaskBoardRestInterface.class);
    }
}
