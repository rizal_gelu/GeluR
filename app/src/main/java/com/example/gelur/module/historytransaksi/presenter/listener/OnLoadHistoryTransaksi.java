package com.example.gelur.module.historytransaksi.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface OnLoadHistoryTransaksi {
    void OnClickHistoryTransaksi(String data);
    void ResponseListHistoryTransaksiSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListHistoryTransaksiGagal(String m);
}
