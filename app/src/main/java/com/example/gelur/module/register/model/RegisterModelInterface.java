package com.example.gelur.module.register.model;

import com.example.gelur.module.register.presenter.listener.OnRegisteringUserFinishedListener;
import com.example.gelur.pojo.UsersPojo;

public interface RegisterModelInterface {
    void registeringUserToServer(UsersPojo usersPojo, OnRegisteringUserFinishedListener listener);
}
