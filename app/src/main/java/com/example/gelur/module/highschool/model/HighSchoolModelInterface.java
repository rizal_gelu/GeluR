package com.example.gelur.module.highschool.model;

import android.content.Context;

import com.example.gelur.module.college.presenter.listener.OnSaveCollegeFinishedListener;
import com.example.gelur.module.highschool.presenter.listener.OnSaveHighSchoolFinishedListener;


public interface HighSchoolModelInterface {
    void clickSaveHighSchool(Context context, String email, String high_school_name, String year_graduate, String graduated, String description, OnSaveHighSchoolFinishedListener listener);
}
