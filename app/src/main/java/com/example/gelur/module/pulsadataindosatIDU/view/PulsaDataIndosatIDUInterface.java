package com.example.gelur.module.pulsadataindosatIDU.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataIndosatIDUInterface {
    void TransferPulsaDataIndosatIDUSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataIndosatIDUGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataIndosatIDUSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataIndosatIDUGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
