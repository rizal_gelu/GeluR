package com.example.gelur.module.phone.rest;

import com.example.gelur.module.bio.rest.BioAPIURLConstant;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.PhonePojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PhoneRestInterface {
    @FormUrlEncoded
    @POST(PhoneAPIURLConstant.POST_PHONE)
    Call<PhonePojo> postPhone(@Field("email") String email, @Field("phone") String phone);

    @GET(BioAPIURLConstant.GET_BIO)
    Call<FormaterResponse> getBioUser();
}
