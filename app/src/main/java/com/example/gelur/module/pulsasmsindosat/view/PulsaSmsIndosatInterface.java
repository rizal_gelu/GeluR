package com.example.gelur.module.pulsasmsindosat.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaSmsIndosatInterface {
    void TransferPulsaSmsIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaSmsIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaSmsIndosat(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
