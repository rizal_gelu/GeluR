package com.example.gelur.module.listbpjsppob.presenter;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.model.EcommerceOvoModel;
import com.example.gelur.module.ecommerceovo.view.EcommerceOvoInterface;
import com.example.gelur.module.listbpjsppob.model.BPJSModel;
import com.example.gelur.module.listbpjsppob.presenter.listener.OnLoadBPJS;
import com.example.gelur.module.listbpjsppob.view.BPJSInterface;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class BPJSPresenter implements BPJSPresenterInterface, OnLoadBPJS {

    private WeakReference<BPJSInterface> view;
    private BPJSModel model;

    public BPJSPresenter(BPJSInterface view) {
        this.view = new WeakReference<BPJSInterface>(view);
        this.model = new BPJSModel();
    }

    @Override
    public void onRequestCheckBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestCheckBPJS(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequestBayarBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestBayarBPJS(context, req, kodereseller, produk, msisdn,counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onLoadCheckBPJSSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().responCheckTagihanSukses(responsTransaksiPulsa);
    }

    @Override
    public void onLoadCheckBPJSGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().responCheckTagihanGagal(responsTransaksiPulsa);
    }

    @Override
    public void onLoadBayarBPJSSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().responBayarBpjsSukses(responsTransaksiPulsa);
    }

    @Override
    public void onLoadBayarBPJSGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().responBayarBpjsGagal(responsTransaksiPulsa);
    }
}
