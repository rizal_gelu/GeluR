package com.example.gelur.module.ecommercedana.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadDanaEcommerce {
    void RequestTransferSaldoDanaSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferSaldoDanaGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickDana(String data);
    void ResponseListPriceDanaSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceDanaGagal(String m);
}
