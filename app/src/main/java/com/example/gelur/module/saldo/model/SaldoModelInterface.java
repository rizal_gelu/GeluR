package com.example.gelur.module.saldo.model;

import android.content.Context;

import com.example.gelur.module.saldo.presenter.listener.OnCekSaldoFinished;

public interface SaldoModelInterface {
    void InformationSaldo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnCekSaldoFinished listener);
}
