package com.example.gelur.module.plnprabayar.presenter;

import android.content.Context;

public interface PlnPrabayarPresenterInterface {
    void onRequestPlnPrabayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPlnPrabayar(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
