package com.example.gelur.module.listecommercegojek.view;

public interface ListEcommerceGojekInterface {
    void onClickGojekDriver();
    void onClickGojekPenumpang();
}
