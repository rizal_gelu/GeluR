package com.example.gelur.module.pulsadataXLXD.presenter;

import android.content.Context;

public interface PulsaDataXLPXDPresenterInterface {
    void onRequesttransferpulsaDataXLPXD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataXLPXD(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
