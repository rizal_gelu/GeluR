package com.example.gelur.module.ecommerceshopee.presenter;

import android.content.Context;

public interface EcommerceShopeePresenterInterface {
    void onRequesttransfersaldoShopee(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPriceShopee(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
