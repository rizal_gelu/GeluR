package com.example.gelur.module.pulsadatatelkomselTDBCN.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTelkomselTDBCNInterface {
    void TransferPulsaDataTelkomselTDBCNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTelkomselTDBCNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaDataTelkomselTDBCN(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
