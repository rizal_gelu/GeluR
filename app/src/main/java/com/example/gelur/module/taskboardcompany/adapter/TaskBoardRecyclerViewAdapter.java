package com.example.gelur.module.taskboardcompany.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.account.adapter.AccountFeedRecyclerViewAdapter;
import com.example.gelur.module.taskboardcompany.presenter.listener.OnLoadTaskFinished;
import com.example.gelur.pojo.TaskBoardPojo;

import org.w3c.dom.Text;

import java.util.List;

public class TaskBoardRecyclerViewAdapter extends RecyclerView.Adapter<TaskBoardRecyclerViewAdapter.ViewHolderContent> {

    private List<TaskBoardPojo> taskBoardPojoList;
    private OnLoadTaskFinished onLoadTaskFinished;
    private Activity activity;

    public TaskBoardRecyclerViewAdapter(Activity activity, List<TaskBoardPojo> taskBoardPojoList, OnLoadTaskFinished onLoadTaskFinished) {
        this.taskBoardPojoList = taskBoardPojoList;
        this.activity = activity;
        this.onLoadTaskFinished = onLoadTaskFinished;
    }

    @NonNull
    @Override
    public TaskBoardRecyclerViewAdapter.ViewHolderContent onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_task_board, parent, false);
        return new TaskBoardRecyclerViewAdapter.ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskBoardRecyclerViewAdapter.ViewHolderContent holder, int position) {
        holder.mItem = taskBoardPojoList.get(position);

        holder.tv_title_job.setText(holder.mItem.getTitle_task());
        holder.tv_deadline_job.setText(holder.mItem.getDeadline());
        holder.tv_description_job.setText(holder.mItem.getDescription_task());
    }

    @Override
    public int getItemCount() {
        return taskBoardPojoList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {

        public final View mView;
        public final ImageView img_job;
        public final TextView tv_title_job;
        public final TextView tv_deadline_job;
        public final TextView tv_description_job;
        public TaskBoardPojo mItem;

        public ViewHolderContent(@NonNull View view) {
            super(view);
            mView = view;
            img_job = (ImageView) view.findViewById(R.id.img_job);
            tv_title_job = (TextView) view.findViewById(R.id.tv_title_job);
            tv_deadline_job = (TextView) view.findViewById(R.id.tv_deadline_job);
            tv_description_job = (TextView) view.findViewById(R.id.tv_description_job);
        }
    }
}
