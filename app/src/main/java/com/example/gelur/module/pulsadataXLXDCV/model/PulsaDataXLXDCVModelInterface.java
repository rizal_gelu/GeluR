package com.example.gelur.module.pulsadataXLXDCV.model;

import android.content.Context;

import com.example.gelur.module.pulsadataXLXDC.presenter.listener.OnLoadPulsaDataXLXDC;
import com.example.gelur.module.pulsadataXLXDCV.presenter.listener.OnLoadPulsaDataXLXDCV;

public interface PulsaDataXLXDCVModelInterface {
    void RequestTransferPulsaDataXLXDCV(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataXLXDCV listener);
    void RequestListPricePulsaDataXLXDCV(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataXLXDCV listener);

}
