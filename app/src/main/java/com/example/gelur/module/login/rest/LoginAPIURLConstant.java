package com.example.gelur.module.login.rest;

import com.example.gelur.util.AppsConstant;

public class LoginAPIURLConstant {
    public static final String BASE_API_LOGIN_URL = AppsConstant.BASE_API_URL + "v1/users/";
    public static final String LOGIN_URL = BASE_API_LOGIN_URL + "login/";
}
