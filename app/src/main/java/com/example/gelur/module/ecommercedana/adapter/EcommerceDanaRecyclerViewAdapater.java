package com.example.gelur.module.ecommercedana.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.ecommercedana.presenter.listener.OnLoadDanaEcommerce;

import java.util.List;

public class EcommerceDanaRecyclerViewAdapater extends RecyclerView.Adapter<EcommerceDanaRecyclerViewAdapater.ViewHolderContent>{

    private Activity activity;
    private OnLoadDanaEcommerce listener;
    List<String> stringList;

    public EcommerceDanaRecyclerViewAdapater(Activity activity, List<String> datum, OnLoadDanaEcommerce listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ecommerce_dana, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.dana.setText(data);

        holder.ll_kumpulan_option_dana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickDana(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_dana;
        public final TextView dana;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_dana = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_dana);
            dana = (TextView) view.findViewById(R.id.dana);
        }
    }
}
