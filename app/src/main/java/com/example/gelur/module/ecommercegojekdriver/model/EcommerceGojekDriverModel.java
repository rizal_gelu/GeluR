package com.example.gelur.module.ecommercegojekdriver.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.ecommercedana.rest.EcommerceDanaRestClient;
import com.example.gelur.module.ecommercegojekdriver.presenter.listener.OnLoadGojekDriver;
import com.example.gelur.module.ecommercegojekdriver.rest.EcommerceGojekDriverRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EcommerceGojekDriverModel implements EcommerceGojekDriverModelInterface{
    @Override
    public void RequestTransferSaldoGojekDriver(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGojekDriver listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = EcommerceGojekDriverRestClient.get().req_transfer_gojek_driver_ecommerce(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferSaldoGojekDriverSukses(response.body());
                } else {
                    listener.RequestTransferSaldoGojekDriverGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPriceGojekDriver(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadGojekDriver listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = EcommerceGojekDriverRestClient.get().getPriceListGojekDriver(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPriceGojekDriverSukses(response.body());
                } else {
                    listener.ResponseListPriceGojekDriverGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPriceGojekDriverGagal(t.getMessage());
            }
        });
    }
}
