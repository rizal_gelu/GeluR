package com.example.gelur.module.timeline.presenter.listener;

import android.content.Context;

import com.example.gelur.pojo.LikePojo;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.pojo.UsersPojo;

import java.util.List;

public interface OnLoadTimelineFinished {

    // Get Timeline
    void onLoadTimelineDataSuccess(List<TimelinePojo> timelineList);
    void onLoadTimelineDataFailed(String message);

    // On Click Like
    void onClickLikeProses(String id_timeline, String email);
    void onPostLikeTLSuccess(LikePojo likePojo);
    void onPostLikeTLFailed(String message);

    // On Click Unlike
    void onClickUnlikeProses(String id_timeline, String email);
    void onClickUnlikeSuccess(LikePojo likePojo);
    void onClickUnlikeFail(String message);

    // On Click User
    void onClickUserName(String email);

//    void onClickUserName();
}
