package com.example.gelur.module.pulsadataindosatIDU.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDUPresenterInterface {
    void onRequesttransferpulsadataIndosatIDU(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDU(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
