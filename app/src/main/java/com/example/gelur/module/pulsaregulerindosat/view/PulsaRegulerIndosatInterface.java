package com.example.gelur.module.pulsaregulerindosat.view;

import com.example.gelur.pojo.FormatResponseNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.util.List;

public interface PulsaRegulerIndosatInterface {

    void TransferPulsaRegulerIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaRegulerIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadDataPulsaRegulerSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadDataPulsaRegulerGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
