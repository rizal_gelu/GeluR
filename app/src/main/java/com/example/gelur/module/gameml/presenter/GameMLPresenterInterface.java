package com.example.gelur.module.gameml.presenter;

import android.content.Context;

import com.example.gelur.module.gameff.presenter.listener.OnLoadGameFF;
import com.example.gelur.module.gameml.presenter.listener.OnLoadGameML;

public interface GameMLPresenterInterface {
    void onRequesttransferDiamondML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPriceGameMl(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
