package com.example.gelur.module.pulsadatatriTRDGM.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDAON.presenter.listener.OnLoadPulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDAON.rest.PulsaDataTriTRDAONRestClient;
import com.example.gelur.module.pulsadatatriTRDGM.presenter.listener.OnLoadPulsaDataTriTRDGM;
import com.example.gelur.module.pulsadatatriTRDGM.rest.PulsaDataTriTRDGMRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataTriTRDGMModel implements PulsaDataTriTRDGMModelInterface {
    @Override
    public void RequestTransferPulsaDataTriTRDGM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDGM listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataTriTRDGMRestClient.get().req_transfer_pulsa_data_tri_trdgm(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataTriTRDGMSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataTriTRDGMGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTriTRDGM(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDGM listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTriTRDAONRestClient.get().getPriceListPulsaDataTriTRDAON(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataTriTRDGMSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataTriTRDGMGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataTriTRDGMGagal(t.getMessage());
            }
        });
    }
}
