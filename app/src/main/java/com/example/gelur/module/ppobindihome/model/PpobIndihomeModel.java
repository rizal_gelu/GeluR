package com.example.gelur.module.ppobindihome.model;

import android.content.Context;

import com.example.gelur.module.listbpjsppob.rest.BPJSRestClient;
import com.example.gelur.module.ppobindihome.presenter.listener.OnLoadPpobIndihome;
import com.example.gelur.module.ppobindihome.rest.PpobIndihomeRestClient;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PpobIndihomeModel implements PpobIndihomeModelInterface{
    @Override
    public void RequestCekTagihanIndihome(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPpobIndihome listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PpobIndihomeRestClient.get().req_check_indihome(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    String tampung = response.body().getStatus();
                    listener.OnResponsesTagihanIndihomeSukses(response.body());
                } else {
                    listener.OnResponsesTagihanIndihomeGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }
}
