package com.example.gelur.module.pulsadataindosatIDF.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDFPresenterInterface {
    void onRequesttransferpulsadataIndosatIDF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDF(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
