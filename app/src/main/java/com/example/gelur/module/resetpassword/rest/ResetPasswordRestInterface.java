package com.example.gelur.module.resetpassword.rest;

import com.example.gelur.module.forgotpasswordsubmit.rest.ForgotPasswordSubmitAPIURLConstant;
import com.example.gelur.pojo.ForgotPasswordConfimPojo;
import com.example.gelur.pojo.ResetPasswordPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ResetPasswordRestInterface {
    @FormUrlEncoded
    @POST(ResetPasswordAPIURLConstant.RESET_PASSWORD)
    Call<ResetPasswordPojo> reset_password(@Field("email") String email, @Field("forgot_password_code") String forgot_password_code, @Field("new_password") String new_password);
}
