package com.example.gelur.module.ppobindihome.model;

import android.content.Context;

import com.example.gelur.module.ppobindihome.presenter.listener.OnLoadPpobIndihome;

public interface PpobIndihomeModelInterface {
    void RequestCekTagihanIndihome(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPpobIndihome listener);
}
