package com.example.gelur.module.listbpjsppob.view;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface BPJSInterface {

    void checkTagihan();
    void responCheckTagihanSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void responCheckTagihanGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void bayarBpjs(String msisdn);
    void responBayarBpjsSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void responBayarBpjsGagal(ResponsTransaksiPulsa responsTransaksiPulsa);
}
