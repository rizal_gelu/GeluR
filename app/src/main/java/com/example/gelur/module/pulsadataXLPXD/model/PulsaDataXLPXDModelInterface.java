package com.example.gelur.module.pulsadataXLPXD.model;

import android.content.Context;

import com.example.gelur.module.pulsadataXLPXD.presenter.listener.OnLoadPulsaDataXLPXD;
import com.example.gelur.module.pulsadataxlXML.presenter.listener.OnLoadPulsaDataXLXML;

public interface PulsaDataXLPXDModelInterface {
    void RequestTransferPulsaDataXLPXD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataXLPXD listener);
    void RequestListPricePulsaDataXLPXD(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataXLPXD listener);

}
