package com.example.gelur.module.pulsatelpontri.presenter;

import android.content.Context;

import com.example.gelur.module.pulsatelpontelkomsel.model.PulsaTelponTelkomselModel;
import com.example.gelur.module.pulsatelpontelkomsel.view.PulsaTelponTelkomselInterface;
import com.example.gelur.module.pulsatelpontri.model.PulsaTelponTriModel;
import com.example.gelur.module.pulsatelpontri.presenter.listener.OnLoadPulsaTelponTri;
import com.example.gelur.module.pulsatelpontri.view.PulsaTelponTriInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaTelponTriPresenter implements PulsaTelponTriPresenterInterface, OnLoadPulsaTelponTri {

    private WeakReference<PulsaTelponTriInterface> view;
    private PulsaTelponTriModel model;

    public PulsaTelponTriPresenter(PulsaTelponTriInterface view) {
        this.view = new WeakReference<PulsaTelponTriInterface>(view);
        this.model = new PulsaTelponTriModel();
    }

    @Override
    public void onRequesttransferpulsaTelponTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaTelponTri(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaTelponTri(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaTelponTri(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsatelponTriSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaTelponTriSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsatrlponTriGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaTelponTriGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaTelponTri(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaTelponTriSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaTelponTri(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaTelponTriGagal(String m) {

    }
}
