package com.example.gelur.module.laporantransaksi.view;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface LaporanTransaksiInterface {
    void transaksiSukses(String sukses, String product, String harga, String refid, String sn, String saldo_awal, String saldo);
    void transaksiGagal(String sukses, String product, String harga, String refid, String sn, String saldo_awal, String saldo);
}
