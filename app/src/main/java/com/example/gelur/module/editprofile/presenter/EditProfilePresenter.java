package com.example.gelur.module.editprofile.presenter;

import android.content.Context;

import com.example.gelur.module.editprofile.model.EditProfileModel;
import com.example.gelur.module.editprofile.presenter.listener.OnBioFinishedListener;
import com.example.gelur.module.editprofile.view.EditProfileInterface;
import com.example.gelur.pojo.BioPojo;

import java.lang.ref.WeakReference;
import java.util.List;

public class EditProfilePresenter implements EditProfilePresenterInterface, OnBioFinishedListener {

    private WeakReference<EditProfileInterface> view;
    private EditProfileModel model;

    public EditProfilePresenter(EditProfileInterface view) {
        this.view = new WeakReference<EditProfileInterface>(view);
        this.model= new EditProfileModel();
    }

    @Override
    public void onClickEditProfile() {
        if(null != view.get()) view.get().goEditBio();
    }

    @Override
    public void onEditYourAboutInfo() {
        if(null != view.get()) view.get().goEditYourAboutInfo();
    }

    @Override
    public void onClickLinks() {
        if(null != view.get()) view.get().goEditLinks();
    }

    @Override
    public void onLoadBio(Context context, String email) {
//        model.onLoadBioSubmit(context, email, this);
    }

    @Override
    public void onPostBioSuccess(BioPojo bioPojo) {
//        if (view.get() != null) view.get().onLoginSuccess(user);
    }

    @Override
    public void onPostBioFail(String message) {

    }
}
