package com.example.gelur.module.pulsadatatelkomselTDM.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;
import com.example.gelur.module.pulsadatatelkomselTDM.presenter.listener.OnLoadPulsaDataTelkomselTDM;

public interface PulsaDataTelkomselTDMModelInterface {
    void RequestTransferPulsaDataTelkomselTDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTelkomselTDM listener);
    void RequestListPricePulsaDataTelkomselTDM(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTelkomselTDM listener);
}
