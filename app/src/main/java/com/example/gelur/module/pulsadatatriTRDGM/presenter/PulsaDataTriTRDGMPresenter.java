package com.example.gelur.module.pulsadatatriTRDGM.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDAON.model.PulsaDataTriTRDAONModel;
import com.example.gelur.module.pulsadatatriTRDAON.view.PulsaDataTriTRDAONInterface;
import com.example.gelur.module.pulsadatatriTRDGM.model.PulsaDataTriTRDGMModel;
import com.example.gelur.module.pulsadatatriTRDGM.presenter.listener.OnLoadPulsaDataTriTRDGM;
import com.example.gelur.module.pulsadatatriTRDGM.view.PulsaDataTriTRDGMInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDGMPresenter implements PulsaDataTriTRDGMPresenterInterface, OnLoadPulsaDataTriTRDGM {

    private WeakReference<PulsaDataTriTRDGMInterface> view;
    private PulsaDataTriTRDGMModel model;

    public PulsaDataTriTRDGMPresenter(PulsaDataTriTRDGMInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDGMInterface>(view);
        this.model = new PulsaDataTriTRDGMModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDGM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDGM(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDGM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDGM(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDGMSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDAONSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDGMGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDAONGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDGM(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDGMSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDAONSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDGMGagal(String m) {

    }
}
