package com.example.gelur.module.saldo.view;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface SaldoInterface {
    void SaldoSukses(ResponsNonTransaksi responsNonTransaksi);
    void SaldoGagal(String message);
}
