package com.example.gelur.module.account.model;

import android.content.Context;

import com.example.gelur.module.account.presenter.listener.OnLoadAccountFinished;

public interface AccountModelInterface {
    void getFeedAccount(Context mContext, String email, OnLoadAccountFinished listener);
    void getProfileAccount(Context mContext, String email, OnLoadAccountFinished listener);
}
