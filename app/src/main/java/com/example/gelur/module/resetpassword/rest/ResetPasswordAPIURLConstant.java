package com.example.gelur.module.resetpassword.rest;

import com.example.gelur.util.AppsConstant;

public class ResetPasswordAPIURLConstant {
    public static final String BASE_API_ACTIVATE_ACCOUNT_URL = AppsConstant.BASE_API_URL + "v1/users/";
    public static final String RESET_PASSWORD = BASE_API_ACTIVATE_ACCOUNT_URL + "reset_password/";
}
