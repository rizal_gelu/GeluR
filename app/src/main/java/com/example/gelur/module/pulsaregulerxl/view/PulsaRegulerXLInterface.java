package com.example.gelur.module.pulsaregulerxl.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaRegulerXLInterface {
    void TransferPulsaRegulerXLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaRegulerXLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaRegulerXL(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
