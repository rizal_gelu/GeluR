package com.example.gelur.module.pulsadatatriTRDUL.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDPY.presenter.listener.OnLoadPulsaDataTriTRDPY;
import com.example.gelur.module.pulsadatatriTRDUL.presenter.listener.OnLoadPulsaDataTriTRDUL;

public interface PulsaDataTriTRDULModelInterface {
    void RequestTransferPulsaDataTriTRDUL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDUL listener);
    void RequestListPricePulsaDataTriTRDUL(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDUL listener);
}
