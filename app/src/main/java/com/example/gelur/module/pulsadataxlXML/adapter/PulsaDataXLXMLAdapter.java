package com.example.gelur.module.pulsadataxlXML.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDUL.presenter.listener.OnLoadPulsaDataTriTRDUL;
import com.example.gelur.module.pulsadataxlXML.presenter.listener.OnLoadPulsaDataXLXML;

import java.util.List;

public class PulsaDataXLXMLAdapter extends RecyclerView.Adapter<PulsaDataXLXMLAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataXLXML listener;
    List<String> stringList;

    public PulsaDataXLXMLAdapter(Activity activity, List<String> datum, OnLoadPulsaDataXLXML listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_xl_xml, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_xl_xml.setText(data);

        holder.ll_kumpulan_option_pulsa_data_xl_xml.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataXLXML(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_xl_xml;
        public final TextView pulsa_data_xl_xml;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_xl_xml = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_xl_xml);
            pulsa_data_xl_xml = (TextView) view.findViewById(R.id.pulsa_data_xl_xml);
        }
    }
}
