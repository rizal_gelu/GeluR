package com.example.gelur.module.forgotpasswordsubmit.rest;

import com.example.gelur.module.activationaccount.rest.ActivationAccountAPIURLConstant;
import com.example.gelur.module.timeline.rest.TimelineRestInterface;
import com.example.gelur.util.AppsConstant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ForgotPasswordSubmitRestClient {
    private static ForgotPasswordSubmitRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    private ForgotPasswordSubmitRestClient() {

    }

    public static ForgotPasswordSubmitRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(logging);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(ForgotPasswordSubmitAPIURLConstant.GET_ACCOUNT)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(ForgotPasswordSubmitRestInterface.class);
    }
}
