package com.example.gelur.module.loginpulsa.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface OnCekLogin {
    void LoginSuksesCek(ResponsNonTransaksi responsNonTransaksi);
    void LoginGagalCek(String message);
}
