package com.example.gelur.module.ecommercegojekdriver.model;

import android.content.Context;

import com.example.gelur.module.ecommercegojekdriver.presenter.listener.OnLoadGojekDriver;

public interface EcommerceGojekDriverModelInterface {
    void RequestTransferSaldoGojekDriver(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGojekDriver listener);
    void RequestListPriceGojekDriver(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadGojekDriver listener);
}
