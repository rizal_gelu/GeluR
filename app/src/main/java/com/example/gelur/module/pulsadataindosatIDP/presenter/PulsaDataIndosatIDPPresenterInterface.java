package com.example.gelur.module.pulsadataindosatIDP.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDPPresenterInterface {
    void onRequesttransferpulsadataIndosatIDP(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDP(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
