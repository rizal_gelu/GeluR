package com.example.gelur.module.gameml.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadGameML {
    void RequestTransferDiamondMLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferDiamondMLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickGameML(String data);
    void ResponseListPriceGameMLSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceGameMLGagal(String m);
}
