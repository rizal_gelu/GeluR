package com.example.gelur.module.pulsadatatelkomselTDBCN.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDBCN.model.PulsaDataTelkomselTDBCNModel;
import com.example.gelur.module.pulsadatatelkomselTDBCN.presenter.listener.OnLoadPulsaDataTelkomselTDBCN;
import com.example.gelur.module.pulsadatatelkomselTDBCN.view.PulsaDataTelkomselTDBCNInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTelkomselTDBCNPresenter implements PulsaDataTelkomselTDBCNPresenterInterface, OnLoadPulsaDataTelkomselTDBCN {

    private WeakReference<PulsaDataTelkomselTDBCNInterface> view;
    private PulsaDataTelkomselTDBCNModel model;

    public PulsaDataTelkomselTDBCNPresenter(PulsaDataTelkomselTDBCNInterface view) {
        this.view = new WeakReference<PulsaDataTelkomselTDBCNInterface>(view);
        this.model = new PulsaDataTelkomselTDBCNModel();
    }

    @Override
    public void onRequesttransferPulsaDataTelkomselTDBCN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTelkomselTDBCN(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTelkomselTDBCN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTelkomselTDBCN(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDBCNSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTelkomselTDBCNSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDBCNGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    @Override
    public void OnClickPulsaDataTelkomselTDBCN(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDBCNSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaDataTelkomselTDBCN(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDBCNGagal(String m) {

    }
}
