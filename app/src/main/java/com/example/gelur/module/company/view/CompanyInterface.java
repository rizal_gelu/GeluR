package com.example.gelur.module.company.view;

public interface CompanyInterface {
    void onClickAttendance();
    void onClickTaskBoard();
}
