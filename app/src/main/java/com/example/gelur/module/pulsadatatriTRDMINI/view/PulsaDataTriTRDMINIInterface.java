package com.example.gelur.module.pulsadatatriTRDMINI.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDMINIInterface {

    void TransferPulsaDataTriTRDMINISukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDMINIGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDMINISukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDMINIGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
