package com.example.gelur.module.login.rest;

import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.UsersPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface LoginRestInterface {
    @FormUrlEncoded
    @POST(LoginAPIURLConstant.LOGIN_URL)
    Call<FormaterResponse> doLogin(@Field("email") String email, @Field("password") String password);
}
