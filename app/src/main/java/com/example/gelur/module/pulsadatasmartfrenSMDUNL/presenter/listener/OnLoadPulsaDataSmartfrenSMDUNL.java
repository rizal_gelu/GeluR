package com.example.gelur.module.pulsadatasmartfrenSMDUNL.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataSmartfrenSMDUNL {
    void RequestTransferPulsaDataSmartFrenSMDUNLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataSmartFrenSMDUNLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataSmartFrenSMDUNL(String data);
    void ResponseListPricePulsaDataSmartFrenSMDUNLSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataSmartFrenSMDUNLGagal(String m);
}
