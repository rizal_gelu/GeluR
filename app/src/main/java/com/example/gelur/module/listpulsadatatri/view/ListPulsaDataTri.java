package com.example.gelur.module.listpulsadatatri.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listpulsadataindosat.view.ListPulsaDataIndosat;
import com.example.gelur.module.pulsadatatriTRD.view.PulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRDAON.view.PulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDDP.view.PulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDGM.view.PulsaDataTriTRDGM;
import com.example.gelur.module.pulsadatatriTRDM.view.PulsaDataTriTRDM;
import com.example.gelur.module.pulsadatatriTRDMINI.view.PulsaDataTriTRDMINI;
import com.example.gelur.module.pulsadatatriTRDMN.view.PulsaDataTriTRDMN;
import com.example.gelur.module.pulsadatatriTRDPY.view.PulsaDataTriTRDPY;
import com.example.gelur.module.pulsadatatriTRDUL.view.PulsaDataTriTRDUL;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListPulsaDataTri extends AppCompatActivity implements ListPulsaDataTriInterface {

    @BindView(R.id.activity_list_pulsa_data_tri)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_list_pulsa_data_tri_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_data_tri)
    View viewContent;

    @BindView(R.id.layout_loading_list_pulsa_data_tri) View viewLoading;

    @BindView(R.id.kumpulan_option_list_product_data_tri)
    LinearLayout kumpulan_option_list_product_data_indosat;

    @BindView(R.id.ll_pulsa_data_TRD) LinearLayout ll_pulsa_data_TRD;

    @BindView(R.id.ll_pulsa_data_TRDAON) LinearLayout ll_pulsa_data_TRDAON;

    @BindView(R.id.ll_pulsa_data_TRDDP) LinearLayout ll_pulsa_data_TRDDP;

    @BindView(R.id.ll_pulsa_data_TRDGM) LinearLayout ll_pulsa_data_TRDGM;

    @BindView(R.id.ll_pulsa_data_TRDMINI) LinearLayout ll_pulsa_data_TRDMINI;

    @BindView(R.id.ll_pulsa_data_TRDM) LinearLayout ll_pulsa_data_TRDM;

    @BindView(R.id.ll_pulsa_data_TRDMN) LinearLayout ll_pulsa_data_TRDMN;

    @BindView(R.id.ll_pulsa_data_TRDPY) LinearLayout ll_pulsa_data_TRDPY;

    @BindView(R.id.ll_pulsa_data_TRDUL) LinearLayout ll_pulsa_data_TRDUL;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_pulsa_data_tri);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Pulsa Data Tri");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListPulsaDataTri.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListPulsaDataTri.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListPulsaDataTri.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRD)
    public void onClickTRD() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRD.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDDP)
    public void onClickTRDDP() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDDP.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDM)
    public void onClickTRDM() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDM.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDPY)
    public void onClickTRDPY() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDPY.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDMINI)
    public void onClickTRDMINI() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDMINI.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDGM)
    public void onClickTRDGM() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDGM.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDUL)
    public void onClickTRDUL() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDUL.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDMN)
    public void onClickTRDMN() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDMN.class));
    }

    @OnClick(R.id.ll_pulsa_data_TRDAON)
    public void onClickTRDAON() {
        startActivity(new Intent(ListPulsaDataTri.this, PulsaDataTriTRDAON.class));
    }
}
