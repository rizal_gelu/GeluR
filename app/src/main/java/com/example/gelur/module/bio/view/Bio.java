package com.example.gelur.module.bio.view;

import android.app.Notification;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.bio.presenter.BioPresenter;
import com.example.gelur.module.editprofile.view.EditProfile;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Bio extends AppCompatActivity implements BioInterface {
    @BindView(R.id.activity_bio_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.content_bio)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.id_text_bio)
    EditText etBio;

    BioPresenter presenter;
    BottomNavigationView bottomNavigationView;
    FloatingActionButton floatingActionButton;

    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_edit_bio);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Bio.this, PostTimeline.class));
            }
        });

        /*
        Get email from session
         */
        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserDetailsFromSession();
        String email = userDetails.get(AppsConstant.KEY_ID_USER);

        ButterKnife.bind(this);
        presenter = new BioPresenter(this);

        presenter.onLoadBio(this, email);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHome() {
        startActivity(new Intent(Bio.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(Bio.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(Bio.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(Bio.this, Account.class));
    }

    @OnClick(R.id.save_bio_btn_click)
    public void save_bio(View v) {

        String email = "Rizalgelui@yahoo.co.id";
        String bio = etBio.getText().toString();

        presenter.onSaveClicked(this, email, bio);
    }

    @Override
    public void onSaveBioSuccess() {
        loadingDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogTheme);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Post Success");
        builder.setCancelable(false);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });

        builder.show();
    }

    @Override
    public void onSaveBioFail(String message) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.cancel_bio_btn_click)
    public void cancelBio(View v) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogTheme);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Cancel Post ?");
        builder.setCancelable(true);
        builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                startActivity(new Intent(Bio.this, EditProfile.class));
                finish();
            }
        });

        builder.show();
    }
}
