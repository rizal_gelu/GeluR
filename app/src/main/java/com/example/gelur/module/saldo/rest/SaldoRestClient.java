package com.example.gelur.module.saldo.rest;

import com.example.gelur.module.transferpulsa.rest.TransferPulsaRestInterface;
import com.example.gelur.util.AppsConstant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SaldoRestClient {
    private static SaldoRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    public static SaldoRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES) // connect timeout
                .writeTimeout(5, TimeUnit.MINUTES) // write timeout
                .readTimeout(5, TimeUnit.MINUTES) // read timeout
                .build();
        client.newBuilder().addInterceptor(logging);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(AppsConstant.BASE_API_NON_TRANSAKSI)
                .client(client.newBuilder().build())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(SaldoRestInterface.class);
    }
}
