package com.example.gelur.module.daftardownline.presenter;

import android.content.Context;

import com.example.gelur.module.akunpulsa.model.AkunPulsaModel;
import com.example.gelur.module.akunpulsa.view.AkunPulsaInterface;
import com.example.gelur.module.daftardownline.model.DaftarDownlineModel;
import com.example.gelur.module.daftardownline.presenter.listener.OnLoadDaftarDownline;
import com.example.gelur.module.daftardownline.view.DaftarDownlineInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;

import java.lang.ref.WeakReference;

public class DaftarDownlinePresenter implements DaftarDownlinePresenterInterface, OnLoadDaftarDownline {

    private WeakReference<DaftarDownlineInterface> view;
    private DaftarDownlineModel model;

    public DaftarDownlinePresenter(DaftarDownlineInterface view) {
        this.view = new WeakReference<DaftarDownlineInterface>(view);
        this.model = new DaftarDownlineModel();
    }

    @Override
    public void onRegsitrasiMember(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.DaftarMember(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void DaftarMemberSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().RegistrasiDownlineSukses(responsNonTransaksi);
    }

    @Override
    public void DaftarMemberGagal(String message) {

    }
}
