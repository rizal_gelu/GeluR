package com.example.gelur.module.pulsadatatriTRDPY.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDPY.presenter.listener.OnLoadPulsaDataTriTRDPY;

public interface PulsaDataTriTRDPYModelInterface {
    void RequestTransferPulsaDataTriTRDPY(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDPY listener);
    void RequestListPricePulsaDataTriTRDPY(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDPY listener);
}
