package com.example.gelur.module.listpulsadatatelkomsel.view;

public interface ListPulsaDataTelkomselInterface {
    void onClickPulsaDataTelkomselTDF();
    void onClickPulsaDataTelkomselTDB();
    void onClickPulsaDataTelkomselTDBN();
    void onClickPulsaDataTelkomselTDBCN();
    void onClickPulsaDataTelkomselTDM();
}
