package com.example.gelur.module.listproductsmartfren.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listpulsadatasmartfren.view.ListPulsaDataSmartfren;
import com.example.gelur.module.pulsadatasmartfrenSMD.view.PulsaDataSmartfrenSMD;
import com.example.gelur.module.pulsaregulersmartfren.view.PulsaRegulerSmartfren;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListProductSmartfren extends AppCompatActivity implements ListProductSmartfrenInterface {

    @BindView(R.id.coordinator_list_product_smartfren)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_list_product_smartfren)
    Toolbar toolbar;

    @BindView(R.id.content_list_product_smartfren)
    View viewContent;

    @BindView(R.id.layout_loading_list_product_smartfren) View viewLoading;

    @BindView(R.id.ll_paket_data_smartfren) LinearLayout ll_paket_data_smartfren;

    @BindView(R.id.paket_data_smartfren) TextView paket_data_smartfren;

    @BindView(R.id.ll_paket_reguler_smartfren) LinearLayout ll_paket_reguler_smartfren;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_product_smartfren);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Produk Smartfren");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListProductSmartfren.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListProductSmartfren.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListProductSmartfren.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_paket_reguler_smartfren)
    public void onClickPulsaRegulerSmartfren() {
        startActivity(new Intent(ListProductSmartfren.this, PulsaRegulerSmartfren.class));
    }

    @OnClick(R.id.ll_paket_data_smartfren)
    public void onClickPaketDataSmartfren() {
        startActivity(new Intent(ListProductSmartfren.this, ListPulsaDataSmartfren.class));
    }
}
