package com.example.gelur.module.pulsadataindosatIDM.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataIndosatIDMInterface {
    void TransferPulsaDataIndosatIDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataIndosatIDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataIndosatIDMSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataIndosatIDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
