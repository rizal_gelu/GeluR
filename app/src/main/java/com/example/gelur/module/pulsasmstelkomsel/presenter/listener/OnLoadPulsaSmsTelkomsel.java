package com.example.gelur.module.pulsasmstelkomsel.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaSmsTelkomsel {
    void RequestTransferPulsaSmsTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaSmsTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaSmsTelkomsel(String data);
    void ResponseListPricePulsaSmsTelkomselSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaSmsTelkomselGagal(String m);
}
