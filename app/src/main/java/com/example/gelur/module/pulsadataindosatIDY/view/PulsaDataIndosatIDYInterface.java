package com.example.gelur.module.pulsadataindosatIDY.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataIndosatIDYInterface {
    void TransferPulsaDataIndosatIDYSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataIndosatIDYGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataIndosatIDYSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataIndosatIDYGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
