package com.example.gelur.module.splashscreen.presenter;

public interface SplashScreenPresenterInterface {
    void doSplash();
    void destroySplash();
}
