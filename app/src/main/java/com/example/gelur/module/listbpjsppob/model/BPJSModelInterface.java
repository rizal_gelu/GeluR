package com.example.gelur.module.listbpjsppob.model;

import android.content.Context;

import com.example.gelur.module.listbpjsppob.presenter.listener.OnLoadBPJS;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.listener.OnLoadPulsaRegulerTelkomsel;

public interface BPJSModelInterface {
    void RequestCheckBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadBPJS listener);
    void RequestBayarBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadBPJS listener);
}
