package com.example.gelur.module.listproducttri.view;

public interface ListProductTriInterface {
    void onClickPulsaRegulerTri();
    void onClickPaketDataTri();
    void onClickPaketTelponTri();
}
