package com.example.gelur.module.forgotpasswordsubmit.rest;


import com.example.gelur.pojo.ForgotPasswordConfimPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ForgotPasswordSubmitRestInterface {
    @FormUrlEncoded
    @POST(ForgotPasswordSubmitAPIURLConstant.GET_ACCOUNT)
    Call<ForgotPasswordConfimPojo> confirm_forgot_pwd(@Field("email") String email, @Field("forgot_password_code") String forgot_password_code);
}
