package com.example.gelur.module.highschool.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.bio.view.Bio;
import com.example.gelur.module.highschool.presenter.HighSchoolPresenter;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class HighSchool extends AppCompatActivity implements HighSchoolInterface {

    @BindView(R.id.activity_high_school_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_high_school_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_college_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.etHighSchoolName)
    EditText tvHighSchoolName;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    HighSchoolPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_high_school);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HighSchool.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        presenter = new HighSchoolPresenter(this);

        showContent(true);
    }

    private void goToHome() {
        startActivity(new Intent(HighSchool.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(HighSchool.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(HighSchool.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(HighSchool.this, Account.class));
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.save_high_school_btn_click)
    public void saveHighSchool() {
        String email = "Rizalgelui@yahoo.co.id";
        String highschool_name = tvHighSchoolName.getText().toString();
        String year_graduate = "2019";
        String graduated = "1";
        String description = "dahdaj";

        presenter.onSaveHighSchoolClicked(this, email, highschool_name, year_graduate, graduated, description);
    }

    @Override
    public void onSuccessPostHighSchool() {

    }

    @Override
    public void onFailedPostHighSchool(String message) {

    }
}
