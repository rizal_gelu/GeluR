package com.example.gelur.module.pulsadatatelkomselTDBCN.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatelkomselTDBCN.presenter.listener.OnLoadPulsaDataTelkomselTDBCN;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;

import java.util.List;

public class PulsaDataTelkomselTDBCNAdapter extends RecyclerView.Adapter<PulsaDataTelkomselTDBCNAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataTelkomselTDBCN listener;
    List<String> stringList;

    public PulsaDataTelkomselTDBCNAdapter(Activity activity, List<String> datum, OnLoadPulsaDataTelkomselTDBCN listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_telkomsel_tdbcn, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_telkomsel_tdbcn.setText(data);

        holder.ll_kumpulan_option_pulsa_data_telkomsel_tdbcn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataTelkomselTDBCN(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_telkomsel_tdbcn;
        public final TextView pulsa_data_telkomsel_tdbcn;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_telkomsel_tdbcn = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_telkomsel_tdbcn);
            pulsa_data_telkomsel_tdbcn = (TextView) view.findViewById(R.id.pulsa_data_telkomsel_tdbcn);
        }
    }
}
