package com.example.gelur.module.address.view;

public interface AddressInterface {
    void onSaveBioSuccess();
    void onSaveBioFail(String message);
}
