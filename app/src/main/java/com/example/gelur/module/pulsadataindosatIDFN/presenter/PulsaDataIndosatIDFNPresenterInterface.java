package com.example.gelur.module.pulsadataindosatIDFN.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDFNPresenterInterface {
    void onRequesttransferpulsadataIndosatIDFN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDFN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
