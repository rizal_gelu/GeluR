package com.example.gelur.module.plnpascabayar.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.laporantransaksi.view.LaporanTransaksi;
import com.example.gelur.module.plnpascabayar.presenter.LaporanPLNPascaBayarPresenter;
import com.example.gelur.module.plnpascabayar.presenter.PlnPascaBayarPresenter;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LaporanPLNPascaBayar extends AppCompatActivity implements LaporanPLNPascaBayarInterface {

    @BindView(R.id.coordinator_laporan_pln_pasca_bayar)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_laporan_pln_pasca_bayar)
    Toolbar toolbar;

    @BindView(R.id.content_laporan_pln_pasca_bayar) View viewContent;

    @BindView(R.id.layout_loading_laporan_transaksi) View viewLoading;

    @BindView(R.id.status_sukses) TextView status_sukses;

    @BindView(R.id.sn_transaksi_sukses) TextView sn_transaksi_sukses;
    @BindView(R.id.harga_product_sukses) TextView harga_product_sukses;
    @BindView(R.id.qty_pln)
    EditText qty_pln;

    @BindView(R.id.submit_bayar_Pln)
    AppCompatButton submit_bayar_Pln;

    AlertDialog loadingDialog;
    AlertDialog.Builder dialogBuilder;

    LaporanPLNPascaBayarPresenter presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_laporan_pln_pasca_bayar);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - PPOB PLN");

        presenter = new LaporanPLNPascaBayarPresenter(this);
        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        Intent intent = getIntent();

        String status = intent.getStringExtra("status");
        String product = intent.getStringExtra("product");
        String harga = intent.getStringExtra("harga");
        String refid = intent.getStringExtra("refid");
        String sn = intent.getStringExtra("sn");
        String saldo_awal = intent.getStringExtra("saldo_awal");
        String saldo = intent.getStringExtra("saldo");
        String msisdn = intent.getStringExtra("msisdn");

        status_sukses.setText(status);
        sn_transaksi_sukses.setText(sn);
        harga_product_sukses.setText(harga);

        bayarTagihanPLN(msisdn);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void bayarTagihanPLN(String msisdn) {

    }
}
