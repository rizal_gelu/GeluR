package com.example.gelur.module.topup.presenter;

import android.content.Context;

import com.example.gelur.module.login.model.LoginModel;
import com.example.gelur.module.login.view.LoginInterface;
import com.example.gelur.module.topup.model.TopUpModel;
import com.example.gelur.module.topup.presenter.listener.OnRequestTopfinished;
import com.example.gelur.module.topup.view.TopUp;
import com.example.gelur.module.topup.view.TopUpInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;

import java.lang.ref.WeakReference;

public class TopUpPresenter implements TopUpPresenterInterface, OnRequestTopfinished {

    private WeakReference<TopUpInterface> view;
    private TopUpModel model;

    public TopUpPresenter(TopUpInterface view) {
        this.view = new WeakReference<TopUpInterface>(view);
        this.model = new TopUpModel();
    }

    @Override
    public void onRequestTopUp(Context context, String req, String kodereseller, String cmd, String time, String pin, String password) {
        model.RequestTopUp(context, req, kodereseller, cmd, time, pin, password, this);
    }

    @Override
    public void RequestTopUpSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().SuksesRequestTopUp(responsNonTransaksi);
    }

    @Override
    public void RequestTopUpGagal(String message) {

    }
}
