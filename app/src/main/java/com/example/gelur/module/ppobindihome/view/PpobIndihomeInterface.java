package com.example.gelur.module.ppobindihome.view;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PpobIndihomeInterface {
    void OnResponseTagihanIndihomeSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void OnResponseTagihanIndihomeGagal(ResponsTransaksiPulsa responsTransaksiPulsa);
}
