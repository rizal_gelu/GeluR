package com.example.gelur.module.pulsadatatelkomselTDB.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDB.model.PulsaDataTelkomselTDBModel;
import com.example.gelur.module.pulsadatatelkomselTDB.presenter.listener.OnLoadPulsaDataTelkomselTDB;
import com.example.gelur.module.pulsadatatelkomselTDB.view.PulsaDataTelkomselTDBInterface;
import com.example.gelur.module.pulsadatatelkomselTDF.model.PulsaDataTelkomselTDFModel;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.PulsaDataTelkomselTDFPresenterInterface;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;
import com.example.gelur.module.pulsadatatelkomselTDF.view.PulsaDataTelkomselTDFInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;

public class PulsaDataTelkomselTDBPresenter implements PulsaDataTelkomselTDBPresenterInterface, OnLoadPulsaDataTelkomselTDB {

    private WeakReference<PulsaDataTelkomselTDBInterface> view;
    private PulsaDataTelkomselTDBModel model;

    public PulsaDataTelkomselTDBPresenter(PulsaDataTelkomselTDBInterface view) {
        this.view = new WeakReference<PulsaDataTelkomselTDBInterface>(view);
        this.model = new PulsaDataTelkomselTDBModel();
    }

    @Override
    public void onRequesttransferPulsaDataTelkomselTDB(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTelkomselTDB(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTelkomselTDB(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTelkomselTDB(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDBSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTelkomselTDBSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTelkomselTDBGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    @Override
    public void OnClickPulsaDataTelkomselTDB(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDBSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaDataTelkomselTDB(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTelkomselTDBGagal(String m) {

    }
}
