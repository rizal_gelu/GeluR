package com.example.gelur.module.ecommercedana.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface EcommerceDanaInterface {
    void TransferSaldoDanaSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferSaldoDanaGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPriceDana(ResponsNonTransaksi responsNonTransaksi);
    void createNewPopUpDialog(String id);
}
