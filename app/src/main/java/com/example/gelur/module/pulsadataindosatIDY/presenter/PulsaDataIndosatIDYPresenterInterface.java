package com.example.gelur.module.pulsadataindosatIDY.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDYPresenterInterface {
    void onRequesttransferpulsadataIndosatIDY(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDY(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
