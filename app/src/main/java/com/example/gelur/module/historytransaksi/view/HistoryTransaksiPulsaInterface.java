package com.example.gelur.module.historytransaksi.view;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface HistoryTransaksiPulsaInterface {
    void onLoadHistoryTransaksiSukses(ResponsNonTransaksi responsNonTransaksi);
    void onLoadHistoryTranaksiGagal(String message);

    void onClickFindHistory();
}
