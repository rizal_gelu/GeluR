package com.example.gelur.module.akunpulsa.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.presenter.AkunPulsaPresenter;
import com.example.gelur.module.daftardownline.view.DaftarDownline;
import com.example.gelur.module.transferpulsa.presenter.TransferPulsaPresenter;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.module.ubahpin.view.UbahPin;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AkunPulsa extends AppCompatActivity implements AkunPulsaInterface {
    @BindView(R.id.coordinator_akun_pulsa)
    CoordinatorLayout coordinator_akun_pulsa;

    @BindView(R.id.activity_toolbar_akun_pulsa)
    Toolbar toolbar;

    @BindView(R.id.content_akun_pulsa)
    View viewContent;

    @BindView(R.id.layout_loading_akun_pulsa) View viewLoading;

    @BindView(R.id.tvUserProfile)
    TextView tvUserProfile;

    @BindView(R.id.alamat) TextView alamat;

    @BindView(R.id.nominal_saldo) TextView nominal_saldo;

    @BindView(R.id.kumpulan_option_akun_pulsa)
    LinearLayout kumpulan_option_akun_pulsa;

    @BindView(R.id.ll_change_password) LinearLayout ll_change_password;

    @BindView(R.id.ll_change_pin) LinearLayout ll_change_pin;

    @BindView(R.id.ll_register_downline) LinearLayout ll_register_downline;

    BottomNavigationView bottomNavigationView;

    AlertDialog loadingDialog;
    AkunPulsaPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_akun_pulsa);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                    case R.id.notification_menu:
//                        goToNotification();
                        break;
                }
                return true;
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Account");

        presenter = new AkunPulsaPresenter(this);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(AkunPulsa.this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);;
        String perintah = "sal";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);;
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
//
        presenter.onGetInformationAkun(this, req, kodereseller, perintah, time, pin, password);

        showContent(true);
    }

    private void goToAkun() {
        startActivity(new Intent(AkunPulsa.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void getSaldoOnAkunPulsaSukses(ResponsNonTransaksi responsNonTransaksi) {
        String msg = responsNonTransaksi.getMsg();
        String[] kata = msg.split("\\r\\n");

        String get_name = kata[0];
        String[] get_spesifik_name = get_name.split("\\s+");
        String get_spesifik_name_index_1 = get_spesifik_name[0];
        String[] name = get_spesifik_name_index_1.split("\\.");

        String get_saldo = kata[1];
        String[] saldo = get_saldo.split("\\s+");

//        List kata = Arrays.asList(msg.split("\\r\\n"));

        nominal_saldo.setText(saldo[1]);
        tvUserProfile.setText(name[1]);
        showContent(true);
    }

    @Override
    public void onClickChangePassword() {

    }

    @OnClick(R.id.ll_change_pin)
    public void onClickChangePin() {
        startActivity(new Intent(AkunPulsa.this, UbahPin.class));
    }

    @OnClick(R.id.ll_register_downline)
    public void onClickRegisterDownline() {
        startActivity(new Intent(AkunPulsa.this, DaftarDownline.class));
    }
}
