package com.example.gelur.module.workplace.models;

import android.content.Context;

import com.example.gelur.module.bio.presenter.listener.OnSaveBioFinishedListener;
import com.example.gelur.module.workplace.presenter.listener.OnSaveWorkplaceFinishedListener;

public interface WorkplaceModelInterface {
    void clickSaveWorkplace(Context context, String email, String workplace, String job_title, String location_workplace, OnSaveWorkplaceFinishedListener listener);
}
