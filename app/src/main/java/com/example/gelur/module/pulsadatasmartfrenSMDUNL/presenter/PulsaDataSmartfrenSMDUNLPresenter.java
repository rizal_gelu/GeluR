package com.example.gelur.module.pulsadatasmartfrenSMDUNL.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataSMDEVO.model.PulsaDataSmartfrenSMDEVOModel;
import com.example.gelur.module.pulsadataSMDEVO.view.PulsaDataSmartfrenSMDEVOInterface;
import com.example.gelur.module.pulsadatasmartfrenSMDUNL.model.PulsaDataSmartfrenSMDUNLModel;
import com.example.gelur.module.pulsadatasmartfrenSMDUNL.presenter.listener.OnLoadPulsaDataSmartfrenSMDUNL;
import com.example.gelur.module.pulsadatasmartfrenSMDUNL.view.PulsaDataSmartfrenSMDUNLInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataSmartfrenSMDUNLPresenter implements PulsaDataSmartfrenSMDUNLPresenterInterface, OnLoadPulsaDataSmartfrenSMDUNL {

    private WeakReference<PulsaDataSmartfrenSMDUNLInterface> view;
    private PulsaDataSmartfrenSMDUNLModel model;

    public PulsaDataSmartfrenSMDUNLPresenter(PulsaDataSmartfrenSMDUNLInterface view) {
        this.view = new WeakReference<PulsaDataSmartfrenSMDUNLInterface>(view);
        this.model = new PulsaDataSmartfrenSMDUNLModel();
    }

    @Override
    public void onRequesttransferpulsadataSmartfrenSMDUNL(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataSmartfrenSMDUNL(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataSmartfrenSMDUNL(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataSmartfrenSMDUNL(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataSmartFrenSMDUNLSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataSmartfrenSMDUNLSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataSmartFrenSMDUNLGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataSmartfrenSMDUNLGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataSmartFrenSMDUNL(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataSmartFrenSMDUNLSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataSmartfrenSMDUNLSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataSmartFrenSMDUNLGagal(String m) {

    }
}
