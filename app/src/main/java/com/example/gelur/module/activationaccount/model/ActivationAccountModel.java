package com.example.gelur.module.activationaccount.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.activationaccount.presenter.listener.ActivationAccountFinishedListener;
import com.example.gelur.module.activationaccount.rest.ActivationAccountRestClient;
import com.example.gelur.pojo.ActivateAccountPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivationAccountModel implements ActivationModelInterface {

    public void activate_account(final Context context, String email, String activate_code, final ActivationAccountFinishedListener listener) {

        Call<ActivateAccountPojo> active = ActivationAccountRestClient.get().doActive(email, activate_code);
        active.enqueue(new Callback<ActivateAccountPojo>() {
            @Override
            public void onResponse(Call<ActivateAccountPojo> call, Response<ActivateAccountPojo> response) {
                if (response.isSuccessful()) {
                    listener.onActiveAccountSuccess(response.body());
                } else {
                    listener.onActiveAccountFail(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ActivateAccountPojo> call, Throwable t) {
                listener.onActiveAccountFail(t.getMessage());
            }
        });
    }
}
