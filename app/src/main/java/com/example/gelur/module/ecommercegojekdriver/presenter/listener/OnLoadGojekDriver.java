package com.example.gelur.module.ecommercegojekdriver.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadGojekDriver {
    void RequestTransferSaldoGojekDriverSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferSaldoGojekDriverGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickGojekDriver(String data);
    void ResponseListPriceGojekDriverSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceGojekDriverGagal(String m);
}
