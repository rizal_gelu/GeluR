package com.example.gelur.module.pulsadataSMDEVO.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataSMDEVO.presenter.listener.OnLoadPulsaDataSmartfrenSMDEVO;
import com.example.gelur.module.pulsadatasmartfrenSMD.presenter.listener.OnLoadPulsaDataSmartfrenSMD;

import java.util.List;

public class PulsaDataSmartfrenSMDEVOAdapter extends RecyclerView.Adapter<PulsaDataSmartfrenSMDEVOAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataSmartfrenSMDEVO listener;
    List<String> stringList;

    public PulsaDataSmartfrenSMDEVOAdapter(Activity activity, List<String> datum, OnLoadPulsaDataSmartfrenSMDEVO listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_smartfren_smdevo, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_smartfren_smdevo.setText(data);

        holder.ll_kumpulan_option_pulsa_data_smartfren_smdevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataSmartFrenSMDEVO(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_smartfren_smdevo;
        public final TextView pulsa_data_smartfren_smdevo;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_smartfren_smdevo = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_smartfren_smdevo);
            pulsa_data_smartfren_smdevo = (TextView) view.findViewById(R.id.pulsa_data_smartfren_smdevo);
        }
    }
}
