package com.example.gelur.module.search.rest;

import com.example.gelur.util.AppsConstant;

public class SearchAPIURLConstant {
    public static final String BASE_API_SEARCH_URL = AppsConstant.BASE_API_URL + "v1/";
}
