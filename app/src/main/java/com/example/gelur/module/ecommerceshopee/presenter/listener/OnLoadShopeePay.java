package com.example.gelur.module.ecommerceshopee.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadShopeePay {
    void RequestTransferSaldoShopeeSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferSaldoShopeeGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickShopee(String data);
    void ResponseListPriceShopeeSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceShopeeGagal(String m);
}
