package com.example.gelur.module.pulsadataindosatIDT.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;
import com.example.gelur.module.pulsadataindosatIDT.presenter.listener.OnLoadPulsaDataIndosatIDT;

public interface PulsaDataIndosatIDTModelInterface {
    void RequestTransferDataIndosatIDT(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDT listener);
    void RequestListPricePulsaDataIndosatIDT(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDT listener);
}
