package com.example.gelur.module.register.presenter;

import com.example.gelur.pojo.UsersPojo;

public interface RegisterPresenterInterface {
    void signUpUser(UsersPojo usersPojo);
}
