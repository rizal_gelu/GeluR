package com.example.gelur.module.historytransaksi.view;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.ecommerceovo.adapter.EcommerceOvoApdater;
import com.example.gelur.module.ecommerceovo.presenter.EcommerceOvoPresenter;
import com.example.gelur.module.historytransaksi.adapter.HistoryTransaksiPulsaAdapter;
import com.example.gelur.module.historytransaksi.presenter.HistoryTransaksiPresenter;
import com.example.gelur.module.listpulsadatatelkomsel.view.ListPulsaDataTelkomsel;
import com.example.gelur.module.pulsadatatelkomselTDB.presenter.PulsaDataTelkomselTDBPresenter;
import com.example.gelur.module.taskboardadd.view.TaskBoardAdd;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class HistoryTransaksiPulsa extends AppCompatActivity implements HistoryTransaksiPulsaInterface {

    @BindView(R.id.coordinator_history_transaksi)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_history_transaksi)
    Toolbar toolbar;

    @BindView(R.id.content_history_transaksi)
    View viewContent;

    @BindView(R.id.layout_loading_history_transaksi) View viewLoading;

    @BindView(R.id.tv_duedate)
    TextView tv_duedate;

    @BindView(R.id.rv_transaction_history)
    RecyclerView recyclerView;

    BottomNavigationView bottomNavigationView;

    AlertDialog loadingDialog;
    HistoryTransaksiPresenter presenter;
    DatePickerDialog.OnDateSetListener setListener;
    SimpleDateFormat dateFormatter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_history_transaksi);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
//                    case R.id.history_transaksi:
//                        goToHistoryTransaksi();
//                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("History Transaction");

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);

        Calendar newCalendar = Calendar.getInstance();
        tv_duedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        HistoryTransaksiPulsa.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        tv_duedate.setText(dateFormatter.format(newDate.getTime()));
                    }
                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
            }
        });

        recyclerView.setVisibility(View.GONE);
        presenter = new HistoryTransaksiPresenter(this);
        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(HistoryTransaksiPulsa.this, TransferPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(HistoryTransaksiPulsa.this, AkunPulsa.class));
    }

    @Override
    public void onLoadHistoryTransaksiSukses(ResponsNonTransaksi responsNonTransaksi) {
        String get_msg = responsNonTransaksi.getMsg();
        String[] stringList = get_msg.split("\\r\\n");

        ArrayList<String> ListPulsa = new ArrayList<>();
        for (int counter = 0; counter < stringList.length; counter++){
            ListPulsa.add(stringList[counter]);
        }

        HistoryTransaksiPulsaAdapter adapter = new HistoryTransaksiPulsaAdapter(this, ListPulsa, presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setVisibility(View.VISIBLE);
        showContent(true);
    }

    @Override
    public void onLoadHistoryTranaksiGagal(String message) {

    }

    @OnClick(R.id.submit_history)
    public void onClickFindHistory() {
        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String get_date = tv_duedate.getText().toString();

        String date = get_date.replaceAll("-","");

        //hardcode
        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
        String perintah = "CM."+date+"."+pin;

        presenter.findHistoryTransaksi(this, req, kodereseller, perintah, time, pin, password);
    }
}
