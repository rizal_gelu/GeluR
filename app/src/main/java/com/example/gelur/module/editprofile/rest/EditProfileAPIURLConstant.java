package com.example.gelur.module.editprofile.rest;

import com.example.gelur.util.AppsConstant;

public class EditProfileAPIURLConstant {
    // User EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/users/"; // User UriSegment 2
    public static final String GET_BIO = BASE_API_USER + "get_bio/"; // User UriSegment 3
    public static final String POST_BIO = BASE_API_USER + "post_bio/";
}
