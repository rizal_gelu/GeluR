package com.example.gelur.module.plnpascabayar.presenter;

import android.content.Context;

import com.example.gelur.module.plnpascabayar.model.LaporanPlnPascaBayarModel;
import com.example.gelur.module.plnpascabayar.model.PlnPascaBayarModel;
import com.example.gelur.module.plnpascabayar.presenter.listener.OnLoadLaporanPLNPascaBayar;
import com.example.gelur.module.plnpascabayar.view.LaporanPLNPascaBayarInterface;
import com.example.gelur.module.plnpascabayar.view.PlnPascaBayarInterface;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class LaporanPLNPascaBayarPresenter implements LaporanPLNPascaBayarInterface, OnLoadLaporanPLNPascaBayar {

    private WeakReference<LaporanPLNPascaBayarInterface> view;
    private LaporanPlnPascaBayarModel model;

    public LaporanPLNPascaBayarPresenter(LaporanPLNPascaBayarInterface view) {
        this.view = new WeakReference<LaporanPLNPascaBayarInterface>(view);
        this.model = new LaporanPlnPascaBayarModel();
    }

    @Override
    public void onLoadBayarPLNPascaBayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    @Override
    public void onLoadBayarPLNPascaBayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    @Override
    public void bayarTagihanPLN(String msisdn) {

    }
}
