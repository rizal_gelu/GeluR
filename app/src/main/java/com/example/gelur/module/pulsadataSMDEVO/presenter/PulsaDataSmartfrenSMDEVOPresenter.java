package com.example.gelur.module.pulsadataSMDEVO.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataSMDEVO.model.PulsaDataSmartfrenSMDEVOModel;
import com.example.gelur.module.pulsadataSMDEVO.presenter.listener.OnLoadPulsaDataSmartfrenSMDEVO;
import com.example.gelur.module.pulsadataSMDEVO.view.PulsaDataSmartfrenSMDEVOInterface;
import com.example.gelur.module.pulsadatasmartfrenSMD.model.PulsaDataSmartfrenSMDModel;
import com.example.gelur.module.pulsadatasmartfrenSMD.view.PulsaDataSmartfrenSMDInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataSmartfrenSMDEVOPresenter implements PulsaDataSmartfrenSMDEVOPresenterInterface, OnLoadPulsaDataSmartfrenSMDEVO {

    private WeakReference<PulsaDataSmartfrenSMDEVOInterface> view;
    private PulsaDataSmartfrenSMDEVOModel model;

    public PulsaDataSmartfrenSMDEVOPresenter(PulsaDataSmartfrenSMDEVOInterface view) {
        this.view = new WeakReference<PulsaDataSmartfrenSMDEVOInterface>(view);
        this.model = new PulsaDataSmartfrenSMDEVOModel();
    }

    @Override
    public void onRequesttransferpulsadataSmartfrenSMDEVO(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataSmartfrenSMDEVO(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataSmartfrenSMDEVO(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataSmartfrenSMDEVO(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataSmartFrenSMDEVOSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataSmartfrenSMDEVOSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataSmartFrenSMDEVOGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataSmartfrenSMDEVOGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataSmartFrenSMDEVO(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataSmartFrenSMDEVOSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataSmartfrenSMDEVOSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataSmartFrenSMDEVOGagal(String m) {

    }
}
