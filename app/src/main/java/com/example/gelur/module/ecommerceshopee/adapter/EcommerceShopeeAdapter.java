package com.example.gelur.module.ecommerceshopee.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.ecommerceovo.adapter.EcommerceOvoApdater;
import com.example.gelur.module.ecommerceshopee.presenter.listener.OnLoadShopeePay;

import java.util.List;

public class EcommerceShopeeAdapter extends RecyclerView.Adapter<EcommerceShopeeAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadShopeePay listener;
    List<String> stringList;

    public EcommerceShopeeAdapter(Activity activity, List<String> datum, OnLoadShopeePay listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ecommerce_shopee, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.shopee.setText(data);

        holder.ll_kumpulan_option_shopee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickShopee(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_shopee;
        public final TextView shopee;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_shopee = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_shopee);
            shopee = (TextView) view.findViewById(R.id.shopee);
        }
    }
}
