package com.example.gelur.module.taskboardcompany.rest;

import com.example.gelur.module.bio.rest.BioAPIURLConstant;
import com.example.gelur.module.phone.rest.PhoneAPIURLConstant;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.TaskBoardPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TaskBoardRestInterface {
    @FormUrlEncoded
    @POST(PhoneAPIURLConstant.POST_PHONE)
    Call<TaskBoardPojo> postTaskBoard(@Field("email") String email, @Field("phone") String phone);

    @GET(TaskBoardAPIURLConstant.GET_TASK_BOARD)
    Call<FormaterResponse> get_taskBoard(@Query("company_id") String company_id);
}
