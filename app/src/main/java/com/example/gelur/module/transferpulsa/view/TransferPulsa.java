package com.example.gelur.module.transferpulsa.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listecommerce.view.ListEcommerce;
import com.example.gelur.module.listgame.view.ListGame;
import com.example.gelur.module.listppob.view.ListPPOB;
import com.example.gelur.module.listproductpln.view.ListProductPLN;
import com.example.gelur.module.listprovider.view.ListProvider;
import com.example.gelur.module.login.view.Login;
import com.example.gelur.module.saldo.view.Saldo;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.transferpulsa.presenter.TransferPulsaPresenter;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransferPulsa extends AppCompatActivity implements TransferPulsaInterface {
    @BindView(R.id.coordinator_dashboard)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_dashboard_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_content_dashboard)
    View viewContent;

    //    @BindView(R.id.scrolling_dashboard)
//    ScrollView scrollView;
    @BindView(R.id.layout_loading_dashboard) View viewLoading;

    @BindView(R.id.grid_dashboard)
    GridLayout gridLayout;

    @BindView(R.id.pulsa_reguler)
    CardView pulsa_reguler;

    @BindView(R.id.game) CardView game;

    @BindView(R.id.pln) CardView pln;

    @BindView(R.id.ecommerce) CardView ecommerce;

    @BindView(R.id.nominal_saldo)
    TextView nominal_saldo;

    BottomNavigationView bottomNavigationView;

    AlertDialog loadingDialog;
    TransferPulsaPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transfer_pulsa);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new TransferPulsaPresenter(this);

        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        System.out.println("Random value in int from "+min+" to "+max+ ":");
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(TransferPulsa.this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        //hardcode
        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String perintah = "sal";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        presenter.getInformasiSaldo(this, req, kodereseller, perintah, time, pin, password);

        showContent(true);
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(TransferPulsa.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(TransferPulsa.this, AkunPulsa.class));
    }

    @Override
    public void getSaldoSukses(ResponsNonTransaksi responsNonTransaksi) {

        String msg = responsNonTransaksi.getMsg();
        String[] kata = msg.split("\\r\\n");

        String get_saldo = kata[1];
        String[] saldo = get_saldo.split("\\s+");

        nominal_saldo.setText(saldo[1]);
        showContent(true);
    }

    @OnClick(R.id.nominal_saldo)
    public void goToWallet() {
        startActivity(new Intent(TransferPulsa.this, Saldo.class));
    }

    @OnClick(R.id.pulsa_reguler)
    public void goToPulsaReguler() {
        startActivity(new Intent(TransferPulsa.this, ListProvider.class));
    }

    @Override
    public void getSaldoGagal(String message) {

    }

    @OnClick(R.id.pln)
    public void goToPln() {
        startActivity(new Intent(TransferPulsa.this, ListProductPLN.class));
    }

    @OnClick(R.id.ecommerce)
    public void goToEcommerce() {
        startActivity(new Intent(TransferPulsa.this, ListEcommerce.class));
    }

    @OnClick(R.id.game)
    public void goToGame() {
        startActivity(new Intent(TransferPulsa.this, ListGame.class));
    }

    @OnClick(R.id.ppob)
    public void goToPPOB() {
        startActivity(new Intent(TransferPulsa.this, ListPPOB.class));
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_left, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.id_logout:
                presenter.userLogout(this);
                break;
        }
        return true;
    }

    @Override
    public void onUserLogout() {

        invalidateOptionsMenu();
        supportInvalidateOptionsMenu();

        Snackbar.make(coordinatorLayout, getString(R.string.logout_success), Snackbar.LENGTH_LONG).show();

        AppsUtil removeSession = new AppsUtil(this);
        removeSession.removeSession();

        startActivity(new Intent(TransferPulsa.this, Login.class));
    }
}
