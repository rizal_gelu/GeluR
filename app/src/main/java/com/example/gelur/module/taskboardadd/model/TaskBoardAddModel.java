package com.example.gelur.module.taskboardadd.model;

import android.content.Context;

import com.example.gelur.module.taskboardadd.presenter.listener.OnSaveTaskBoardAddFinished;
import com.example.gelur.module.taskboardadd.rest.TaskBoardAddRestClient;
import com.example.gelur.pojo.TaskBoardAddPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskBoardAddModel implements TaskBoardAddModelInterface {

    @Override
    public void clickSaveTaskBoard(Context context, String title_list_task, String company_id, String description_list_task, String priority, String due_date, String create_by, String task_id, OnSaveTaskBoardAddFinished listener) {
        Call<TaskBoardAddPojo> taskBoardAddPojoCall = TaskBoardAddRestClient.get().postTaskBoard(title_list_task, company_id, description_list_task, priority, due_date, create_by, task_id);
        taskBoardAddPojoCall.enqueue(new Callback<TaskBoardAddPojo>() {
            @Override
            public void onResponse(Call<TaskBoardAddPojo> call, Response<TaskBoardAddPojo> response) {
                if (response.isSuccessful()) {
                    listener.onSaveTaskBoardSuccess(response.body());
                } else {
                    listener.onSaveTaskBoardFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<TaskBoardAddPojo> call, Throwable t) {
                listener.onSaveTaskBoardFailed(t.getMessage());
            }
        });
    }
}
