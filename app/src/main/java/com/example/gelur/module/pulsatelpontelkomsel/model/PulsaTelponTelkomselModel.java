package com.example.gelur.module.pulsatelpontelkomsel.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatelkomselTDB.rest.PulsaDataTelkomselTDBRestClient;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.listener.OnLoadPulsaRegulerTelkomsel;
import com.example.gelur.module.pulsaregulertelkomsel.rest.PulsaRegulerTelkomselRestClient;
import com.example.gelur.module.pulsatelpontelkomsel.presenter.listener.OnLoadPulsaTelponTelkomsel;
import com.example.gelur.module.pulsatelpontelkomsel.rest.PulsaTelponTelkomselRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaTelponTelkomselModel implements PulsaTelponTelkomselModelInterface{
    @Override
    public void RequestTransferPulsaTelponTelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponTelkomsel listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaTelponTelkomselRestClient.get().req_transfer_pulsa_telpon_telkomsel(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn,counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaTelponTelkomselSukses(response.body());
                } else {
                    listener.RequestTransferPulsaTelponTelkomselGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTelkomselTDB(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponTelkomsel listener) {
        Call<ResponsNonTransaksi> sal = PulsaTelponTelkomselRestClient.get().getPricePulsaTelponTelkomsel(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaTelponTelkomselSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaTelponTelkomselGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaTelponTelkomselGagal(t.getMessage());
            }
        });
    }
}
