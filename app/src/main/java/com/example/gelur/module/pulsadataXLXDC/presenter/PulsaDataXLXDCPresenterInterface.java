package com.example.gelur.module.pulsadataXLXDC.presenter;

import android.content.Context;

public interface PulsaDataXLXDCPresenterInterface {
    void onRequesttransferpulsaDataXLXDC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataXLXDC(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
