package com.example.gelur.module.daftardownline.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface DaftarDownlineInterface {
    void RegistrasiDownlineSukses(ResponsNonTransaksi responsNonTransaksi);
    void RegistrasiDownlineGagal(ResponsNonTransaksi responsNonTransaksi);
}
