package com.example.gelur.module.bio.model;


import android.content.Context;

import com.example.gelur.module.bio.presenter.listener.OnLoadBioFinished;
import com.example.gelur.module.bio.presenter.listener.OnSaveBioFinishedListener;
import com.example.gelur.module.bio.rest.BioRestClient;
import com.example.gelur.pojo.BioPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BioModel implements BioModelInterface {

    @Override
    public void loadBio(Context mContext, String email, OnLoadBioFinished listener) {
        Call<BioPojo> biodata = BioRestClient.get().getBioUser(email);
        biodata.enqueue(new Callback<BioPojo>() {
            @Override
            public void onResponse(Call<BioPojo> call, Response<BioPojo> response) {
                if (response.isSuccessful()) {
                    listener.onLoadBioDataSuccess(response.body());
                } else {
                    listener.onLoadBioeDataFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<BioPojo> call, Throwable t) {
                listener.onLoadBioeDataFailed(t.getMessage());
            }
        });
    }

    @Override
    public void clickSave(Context context, String email, String bio, OnSaveBioFinishedListener listener) {
        Call<BioPojo> biodata = BioRestClient.get().postBio(email, bio);
        biodata.enqueue(new Callback<BioPojo>() {
            @Override
            public void onResponse(Call<BioPojo> call, Response<BioPojo> response) {
                if (response.isSuccessful()) {
                    listener.onSaveBioSuccess(response.body());
                } else {
                    listener.onSaveBioFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<BioPojo> call, Throwable t) {
                listener.onSaveBioFailed(t.getMessage());
            }
        });
    }
}
