package com.example.gelur.module.editprofile.model;

import android.content.Context;

import com.example.gelur.module.bio.presenter.listener.OnLoadBioFinished;

public interface EditProfileModelInterface {
    void onLoadBioSubmit(Context context, String email, OnLoadBioFinished listener);
}
