package com.example.gelur.module.account.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.module.account.adapter.AccountFeedRecyclerViewAdapter;
import com.example.gelur.module.account.presenter.AccountPresenter;
import com.example.gelur.module.editprofile.view.EditProfile;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.splashscreen.view.SplashScreen;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.TimelinePersonalUser;
import com.example.gelur.pojo.TimelinePojo;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.HashMap;
import java.util.List;

public class Account extends AppCompatActivity implements AccountInterface {

    @BindView(R.id.activity_account_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.layout_content_account)
    View viewContent;

    @BindView(R.id.layout_loading_account) View viewLoading;

    @BindView(R.id.activity_feed_user_personal)
    RecyclerView recyclerView;

//    @BindView(R.id.iv_layout_content_account)
//    ImageView iv_layout_content_account;
//
//    @BindView(R.id.tvUserProfile)
//    TextView tvUserProfile;

    FloatingActionButton floatingActionButton;
    
    BottomNavigationView bottomNavigationView;

    AccountPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_account);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Account.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);
//        hideMainContent();

        presenter = new AccountPresenter(this);

        /*
        Get email from session
         */
        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserDetailsFromSession();
        String email = userDetails.get(AppsConstant.KEY_ID_USER);

        presenter.loadProfileDetail(this, email);

        // parsing email to presenter
        presenter.loadFeedTimeline(this, email);

        showContent(true);
    }

    private void goToSearch() {
        startActivity(new Intent(Account.this, Search.class));
    }

    private void goToNotification() {
        startActivity(new Intent(Account.this, Notification.class));
    }

    private void goToHome() {
        startActivity(new Intent(Account.this, Timeline.class));
    }

    @Override
    public void ListItemClick(TimelinePojo timelinePojo) {

    }

    @OnClick(R.id.btn_edit_profile_user)
    public void OnClickUserEditProfile() {
        Intent intent = new Intent(Account.this, EditProfile.class);
        startActivity(intent);
    }

//    @OnClick(R.id.btn_edit_profile_user)
//    public void OnClickUserEditProfile() {
//        Intent intent = new Intent(SplashScreen.this, TransferPulsa.class);
//    }

    @Override
    public void onLoadTimelineUserSuccess(List<TimelinePojo> timelinePersonalUsers) {
        AccountFeedRecyclerViewAdapter adapter = new AccountFeedRecyclerViewAdapter(this, timelinePersonalUsers, presenter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoadTimelineUserFailed(String message) {

    }

    @Override
    public void onLoadUserDetailsSuccess(List<UsersPojo> usersPojos) {

//        String aa = usersPojos.get(0).getFirst_name();



//        tvUserProfile.setText(aa);
//        showMainContent();
    }

    @Override
    public void onLoadUserDetailFailed(String email) {

    }

}
