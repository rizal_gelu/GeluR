package com.example.gelur.module.pulsasmstelkomsel.rest;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PulsaSmsTelkomselRestInterface {
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsTransaksiPulsa> req_transfer_sms_telkomsel(@Body RequestTransaksiPulsa requestTransaksiPulsa);
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")Call<ResponsNonTransaksi> getPriceListPulsaSmsTelkomsel(@Body RequestNonTransaksi requestNonTransaksi);
}
