package com.example.gelur.module.timeline.model;

import com.example.gelur.module.timeline.presenter.listener.OnLoadTimelineFinished;

public interface TimelineModelInterface {
    void loadTimelineData(OnLoadTimelineFinished listener);
}
