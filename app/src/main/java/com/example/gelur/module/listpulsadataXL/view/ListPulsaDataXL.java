package com.example.gelur.module.listpulsadataXL.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listproducttri.view.ListProductTri;
import com.example.gelur.module.listpulsadatatri.view.ListPulsaDataTriInterface;
import com.example.gelur.module.pulsadataXLPXD.view.PulsaDataXLPXD;
import com.example.gelur.module.pulsadataXLXDC.view.PulsaDataXLXDC;
import com.example.gelur.module.pulsadataXLXDCV.view.PulsaDataXLXDCV;
import com.example.gelur.module.pulsadatatriTRD.view.PulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRDAON.view.PulsaDataTriTRDAON;
import com.example.gelur.module.pulsadatatriTRDDP.view.PulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDGM.view.PulsaDataTriTRDGM;
import com.example.gelur.module.pulsadatatriTRDM.view.PulsaDataTriTRDM;
import com.example.gelur.module.pulsadatatriTRDMINI.view.PulsaDataTriTRDMINI;
import com.example.gelur.module.pulsadatatriTRDMN.view.PulsaDataTriTRDMN;
import com.example.gelur.module.pulsadatatriTRDPY.view.PulsaDataTriTRDPY;
import com.example.gelur.module.pulsadatatriTRDUL.view.PulsaDataTriTRDUL;
import com.example.gelur.module.pulsadataxlXML.view.PulsaDataXLXML;
import com.example.gelur.module.pulsaregulertri.view.PulsaRegulerTri;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListPulsaDataXL extends AppCompatActivity implements ListPulsaDataXLInterface {

    @BindView(R.id.activity_list_pulsa_data_xl)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_list_pulsa_data_xl_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_data_xl)
    View viewContent;

    @BindView(R.id.layout_loading_list_pulsa_data_xl) View viewLoading;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_pulsa_data_xl);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Pulsa Data Tri");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListPulsaDataXL.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListPulsaDataXL.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListPulsaDataXL.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_pulsa_data_xl_PXD)
    public void OnClickPXD() {
        startActivity(new Intent(ListPulsaDataXL.this, PulsaDataXLPXD.class));
    }

    @OnClick(R.id.ll_pulsa_data_xl_XLM)
    public void OnClickxml() {
        startActivity(new Intent(ListPulsaDataXL.this, PulsaDataXLXML.class));
    }

    @OnClick(R.id.ll_pulsa_data_xl_XD)
    public void OnClickxd() {

    }

    @OnClick(R.id.ll_pulsa_data_xl_XDCV)
    public void OnClickxdcv() {
        startActivity(new Intent(ListPulsaDataXL.this, PulsaDataXLXDCV.class));
    }

    @OnClick(R.id.ll_paket_data_xl_XDC)
    public void OnClickxdc() {
        startActivity(new Intent(ListPulsaDataXL.this, PulsaDataXLXDC.class));
    }
}
