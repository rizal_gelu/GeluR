package com.example.gelur.module.topup.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.saldo.view.Saldo;
import com.example.gelur.module.topup.presenter.TopUpPresenter;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class TopUp extends AppCompatActivity implements TopUpInterface {

    @BindView(R.id.coordinator_topup)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_topup_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_top_up) View viewContent;

    @BindView(R.id.layout_loading_topup) View viewLoading;

    @BindView(R.id.layout_text_input)
    TextInputLayout layout_text_input;

    @BindView(R.id.TI_ET_nominal_topup)
    TextInputEditText TI_ET_nominal_topup;

    @BindView(R.id.submit_request_top_up)
    AppCompatButton submit_request;

    @BindView(R.id.judul)
    LinearLayout judul;

    @BindView(R.id.judul_respons)
    TextView judul_respons;

    @BindView(R.id.ll_nominal)
            LinearLayout ll_nominal;

    @BindView(R.id.response_segera) TextView response_segera;

    @BindView(R.id.tv_resp_nominal) TextView tv_resp_nominal;

    @BindView(R.id.ll_transfer_ke_1) LinearLayout ll_transfer_ke_1;

    @BindView(R.id.bank_1) TextView bank_1;

    @BindView(R.id.tv_bank_1) TextView tv_bank_1;

    @BindView(R.id.ll_transfer_ke_2) LinearLayout ll_transfer_ke_2;

    @BindView(R.id.bank_2) TextView bank_2;

    @BindView(R.id.tv_bank_2) TextView tv_bank_2;

    @BindView(R.id.ll_atas_nama) LinearLayout ll_atas_nama;

    @BindView(R.id.atas_nama) TextView atas_nama;

    @BindView(R.id.tv_atas_nama) TextView tv_atas_nama;

    AlertDialog loadingDialog;
    TopUpPresenter presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_topup);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new TopUpPresenter(this);

        judul.setVisibility(View.GONE);
        judul_respons.setVisibility(View.GONE);
        ll_nominal.setVisibility(View.GONE);
        response_segera.setVisibility(View.GONE);
        tv_resp_nominal.setVisibility(View.GONE);
        ll_transfer_ke_1.setVisibility(View.GONE);
        bank_1.setVisibility(View.GONE);
        ll_transfer_ke_2.setVisibility(View.GONE);
        tv_bank_2.setVisibility(View.GONE);
        ll_atas_nama.setVisibility(View.GONE);
        atas_nama.setVisibility(View.GONE);
        tv_atas_nama.setVisibility(View.GONE);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.submit_request_top_up)
    public void submit_request() {
        String nominal = TI_ET_nominal_topup.getText().toString();

        int min = 500;
        int max = 10000;

        //Generate random int value from 50 to 100
        System.out.println("Random value in int from "+min+" to "+max+ ":");
        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(TopUp.this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String perintah = "tiket."+nominal+".1234";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        presenter.onRequestTopUp(this, req, kodereseller, perintah, time, pin, password);
    }

    @Override
    public void SuksesRequestTopUp(ResponsNonTransaksi responsNonTransaksi) {
        String msg = responsNonTransaksi.getMsg();
        String[] kata = msg.split("\r\n");
        String slipt_kata = kata[1];

        String[] split_lagi = slipt_kata.split("\\s+");
        String nominal = split_lagi[0] + " " + split_lagi[1] +" "+ split_lagi[2];

        String atas_name =  split_lagi[10] + " " + split_lagi[11] + " " + split_lagi[12];

        String bank1 = split_lagi[4] + " " + split_lagi[5] + " " + split_lagi[6];

        String bank2 = split_lagi[7] + " " + split_lagi[8] + " " + split_lagi[9];

        tv_resp_nominal.setText(nominal );
        tv_resp_nominal.setVisibility(View.VISIBLE);

        ll_nominal.setVisibility(View.VISIBLE);
        response_segera.setVisibility(View.VISIBLE);
        judul.setVisibility(View.VISIBLE);
        judul_respons.setVisibility(View.VISIBLE);

        tv_bank_1.setText(bank1);
        tv_bank_1.setVisibility(View.VISIBLE);
        ll_transfer_ke_1.setVisibility(View.VISIBLE);
        bank_1.setVisibility(View.VISIBLE);

        ll_transfer_ke_2.setVisibility(View.VISIBLE);
        bank_2.setVisibility(View.VISIBLE);
        tv_bank_2.setText(bank2);
        tv_bank_2.setVisibility(View.VISIBLE);

        ll_atas_nama.setVisibility(View.VISIBLE);
        atas_nama.setVisibility(View.VISIBLE);
        tv_atas_nama.setText(atas_name);
        tv_atas_nama.setVisibility(View.VISIBLE);
    }

    @Override
    public void GagalRequestTopUp(String message) {

    }
}
