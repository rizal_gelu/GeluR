package com.example.gelur.module.loginpulsa.presenter;

import android.content.Context;

import com.example.gelur.module.loginpulsa.model.LoginPulsaModel;
import com.example.gelur.module.loginpulsa.presenter.listener.OnCekLogin;
import com.example.gelur.module.loginpulsa.view.LoginPulsaInterface;
import com.example.gelur.module.saldo.model.SaldoModel;
import com.example.gelur.module.saldo.view.SaldoInterface;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;

import java.lang.ref.WeakReference;

public class LoginPulsaPresenter implements LoginPulsaPresenterInterface, OnCekLogin {

    private WeakReference<LoginPulsaInterface> view;
    private LoginPulsaModel model;

    public LoginPulsaPresenter(LoginPulsaInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new LoginPulsaModel();
    }

    @Override
    public void LoginSuksesCek(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().onLoginSuccess(responsNonTransaksi);
    }

    @Override
    public void LoginGagalCek(String message) {

    }

    @Override
    public void LoginPulsaSubmit(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.LoginSubmit(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
