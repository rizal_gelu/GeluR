package com.example.gelur.module.ppobindihome.presenter;

import android.content.Context;

public interface PpobIndihomePresenterInterface {
    void onRequestCheckTagihanIndihome(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
}
