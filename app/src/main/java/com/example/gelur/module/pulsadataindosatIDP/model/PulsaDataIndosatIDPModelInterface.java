package com.example.gelur.module.pulsadataindosatIDP.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDM.presenter.listener.OnLoadPulsaDataIndosatIDM;
import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;

public interface PulsaDataIndosatIDPModelInterface {
    void RequestTransferDataIndosatIDP(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDP listener);
    void RequestListPricePulsaDataIndosatIDP(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDP listener);
}
