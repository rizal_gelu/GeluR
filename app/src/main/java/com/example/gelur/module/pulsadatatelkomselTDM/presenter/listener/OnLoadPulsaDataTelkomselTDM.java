package com.example.gelur.module.pulsadatatelkomselTDM.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTelkomselTDM {
    void RequestTransferPulsaDataTelkomselTDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTelkomselTDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTelkomselTDM(String data);
    void ResponseListPricePulsaDataTelkomselTDMSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTelkomselTDMGagal(String m);
}
