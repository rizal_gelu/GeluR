package com.example.gelur.module.address.rest;

import com.example.gelur.module.account.rest.AccountAPIURLConstant;
import com.example.gelur.module.bio.rest.BioRestInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AddressRestClient {

    private static AddressRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    public static AddressRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(AddressAPIURLConstant.GET_ADDRESS_USER)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(AddressRestInterface.class);
    }

}
