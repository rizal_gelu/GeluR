package com.example.gelur.module.forgotpassword.rest;

import com.example.gelur.pojo.ForgotPasswordPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ForgotPasswordRestInterface {
    @FormUrlEncoded
    @POST(ForgotPasswordAPIRULConstant.FORGOT_PASSWORD_URL)
    Call<ForgotPasswordPojo> forgotPasswordProses(@Field("email") String email);
}
