package com.example.gelur.module.loginpulsa.model;

import android.content.Context;

import com.example.gelur.module.loginpulsa.presenter.listener.OnCekLogin;

public interface LoginPulsaModelInterface {
    void LoginSubmit(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnCekLogin listener);
}
