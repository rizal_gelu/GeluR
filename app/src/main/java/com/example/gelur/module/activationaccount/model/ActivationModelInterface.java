package com.example.gelur.module.activationaccount.model;

import android.content.Context;

import com.example.gelur.module.activationaccount.presenter.listener.ActivationAccountFinishedListener;

public interface ActivationModelInterface{
    void activate_account(Context context, String email, String Activation_code, ActivationAccountFinishedListener listener);
}
