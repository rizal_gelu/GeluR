package com.example.gelur.module.ecommercedana.presenter;

import android.content.Context;

import com.example.gelur.module.ecommercedana.model.EcommerceDanaModel;
import com.example.gelur.module.ecommercedana.presenter.listener.OnLoadDanaEcommerce;
import com.example.gelur.module.ecommercedana.view.EcommerceDanaInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class EcommerceDanaPresenter implements EcommerceDanaPresenterInterface, OnLoadDanaEcommerce {

    private WeakReference<EcommerceDanaInterface> view;
    private EcommerceDanaModel model;

    public EcommerceDanaPresenter(EcommerceDanaInterface view) {
        this.view = new WeakReference<EcommerceDanaInterface>(view);
        this.model = new EcommerceDanaModel();
    }

    @Override
    public void onRequesttransfersaldoDana(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferSaldoDana(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListListDana(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceDana(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferSaldoDanaSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoDanaSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferSaldoDanaGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoDanaGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickDana(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceDanaSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPriceDana(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceDanaGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
