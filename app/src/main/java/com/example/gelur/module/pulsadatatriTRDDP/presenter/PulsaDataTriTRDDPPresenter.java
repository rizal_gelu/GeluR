package com.example.gelur.module.pulsadatatriTRDDP.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRD.model.PulsaDataTriTRDModel;
import com.example.gelur.module.pulsadatatriTRD.view.PulsaDataTriTRDInterface;
import com.example.gelur.module.pulsadatatriTRDDP.model.PulsaDataTriTRDDPModel;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDDP.view.PulsaDataTriTRDDPInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDDPPresenter implements PulsaDataTriTRDDPPresenterInterface, OnLoadPulsaDataTriTRDDP {

    private WeakReference<PulsaDataTriTRDDPInterface> view;
    private PulsaDataTriTRDDPModel model;

    public PulsaDataTriTRDDPPresenter(PulsaDataTriTRDDPInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDDPInterface>(view);
        this.model = new PulsaDataTriTRDDPModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDDP(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDDP(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDDP(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDDP(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDDPSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDDPSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDDPGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDDPGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDDP(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDDPSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDDPSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDDPGagal(String m) {

    }
}
