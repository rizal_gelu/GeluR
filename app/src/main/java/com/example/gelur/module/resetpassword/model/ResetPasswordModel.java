package com.example.gelur.module.resetpassword.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.forgotpasswordsubmit.rest.ForgotPasswordSubmitRestClient;
import com.example.gelur.module.resetpassword.presenter.listener.OnResetPasswordFinished;
import com.example.gelur.module.resetpassword.rest.ResetPasswordRestClient;
import com.example.gelur.pojo.ForgotPasswordConfimPojo;
import com.example.gelur.pojo.ResetPasswordPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordModel implements ResetPasswordModelInterface{
    @Override
    public void OnSubmitResetPassword(final Context context, String email, String forgot_password_code, String new_password, final OnResetPasswordFinished listener) {
        Call<ResetPasswordPojo> reset_password = ResetPasswordRestClient.get().reset_password(email, forgot_password_code, new_password);
        reset_password.enqueue(new Callback<ResetPasswordPojo>() {
            @Override
            public void onResponse(Call<ResetPasswordPojo> call, Response<ResetPasswordPojo> response) {
                if (response.isSuccessful()) {
                    listener.onResetPasswordSuccess(response.body());
                } else {
                    listener.onResetPasswordFail(context.getString(R.string.wrong_code));
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordPojo> call, Throwable t) {
                listener.onResetPasswordFail(t.getMessage());
            }
        });
    }
}
