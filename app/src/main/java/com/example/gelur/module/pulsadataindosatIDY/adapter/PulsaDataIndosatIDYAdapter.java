package com.example.gelur.module.pulsadataindosatIDY.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;

import java.util.List;

public class PulsaDataIndosatIDYAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDYAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDY listener;
    List<String> stringList;

    public PulsaDataIndosatIDYAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDY listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaDataIndosatIDYAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idy, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaDataIndosatIDYAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idy.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDY(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idy;
        public final TextView pulsa_data_indosat_idy;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idy = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idy);
            pulsa_data_indosat_idy = (TextView) view.findViewById(R.id.pulsa_data_indosat_idy);
        }
    }
}
