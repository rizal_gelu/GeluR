package com.example.gelur.module.contactinfo.view;

public interface ContactInfoInterface {
    void GoToAddPhone();
    void GoToAddAddress();
    void GoToAddSocialLink();
    void GoToAddWebsite();
}
