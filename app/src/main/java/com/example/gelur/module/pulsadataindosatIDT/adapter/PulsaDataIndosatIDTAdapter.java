package com.example.gelur.module.pulsadataindosatIDT.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;
import com.example.gelur.module.pulsadataindosatIDT.presenter.listener.OnLoadPulsaDataIndosatIDT;

import java.util.List;

public class PulsaDataIndosatIDTAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDTAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDT listener;
    List<String> stringList;

    public PulsaDataIndosatIDTAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDT listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idt, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idt.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDT(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idt;
        public final TextView pulsa_data_indosat_idt;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idt = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idt);
            pulsa_data_indosat_idt = (TextView) view.findViewById(R.id.pulsa_data_indosat_idt);
        }
    }
}
