package com.example.gelur.module.account.presenter.listener;

import com.example.gelur.pojo.TimelinePersonalUser;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.pojo.UsersPojo;

import java.util.List;

public interface OnLoadAccountFinished {
    void onLoadFeedTimelineSuccess(List<TimelinePojo> timelinePojoList);
    void onLoadFeedTimelineFailed(String message);

    void onLoadProfileDetail(List<UsersPojo> usersPojo);
    void onLoadProfileFailed(String message);
}
