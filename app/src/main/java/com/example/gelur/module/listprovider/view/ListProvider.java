package com.example.gelur.module.listprovider.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.ecommercegojekdriver.view.EcommerceGojekDriver;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listproductindosat.view.ListProductIndosat;
import com.example.gelur.module.listproductsmartfren.view.ListProductSmartfren;
import com.example.gelur.module.listproducttelkomsel.view.ListProductTelkomsel;
import com.example.gelur.module.listproducttri.view.ListProductTri;
import com.example.gelur.module.listproductxl.view.ListProductXl;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListProvider extends AppCompatActivity implements ListProviderInterface {

    @BindView(R.id.activity_listpricepulsa_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.content_list_provider)
    View viewContent;

    @BindView(R.id.activity_list_provider_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_loading_list_provider) View viewLoading;

    @BindView(R.id.kumpulan_option)
    LinearLayout ll_kumpulan_option;

    @BindView(R.id.ll_telkomsel) LinearLayout ll_telkomsel;

    @BindView(R.id.telkomsel)
    TextView telkomsel;

    @BindView(R.id.ll_indosat) LinearLayout ll_indosat;

    @BindView(R.id.indosat) TextView indosat;

    @BindView(R.id.ll_xl) LinearLayout ll_xl;

    @BindView(R.id.xl) TextView xl;

    @BindView(R.id.ll_smart) LinearLayout ll_smart;

    @BindView(R.id.smart) TextView smart;

    @BindView(R.id.ll_tri) LinearLayout ll_tri;

    @BindView(R.id.tri) TextView tri;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_provider);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle("GeluReload - List Provider");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        showContent(true);

    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListProvider.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListProvider.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListProvider.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_telkomsel)
    public void onClickTelkomsel() {
        startActivity(new Intent(ListProvider.this, ListProductTelkomsel.class));
    }

    @OnClick(R.id.ll_indosat)
    public void onClickIndosat() {
        startActivity(new Intent(ListProvider.this, ListProductIndosat.class));
    }

    @OnClick(R.id.ll_xl)
    public void onClickXL() {
        startActivity(new Intent(ListProvider.this, ListProductXl.class));
    }

    @OnClick(R.id.ll_smart)
    public void onClickSmartFren() {
        startActivity(new Intent(ListProvider.this, ListProductSmartfren.class));
    }

    @OnClick(R.id.ll_tri)
    public void onClickTri() {
        startActivity(new Intent(ListProvider.this, ListProductTri.class));
    }
}
