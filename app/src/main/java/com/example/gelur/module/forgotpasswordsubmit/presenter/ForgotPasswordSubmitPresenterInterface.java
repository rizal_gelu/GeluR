package com.example.gelur.module.forgotpasswordsubmit.presenter;

import android.content.Context;

public interface ForgotPasswordSubmitPresenterInterface {
    void onForgotPasswordCofirmSubmited(Context context, String email, String forgot_password_code);
}
