package com.example.gelur.module.pulsatelpontri.presenter;

import android.content.Context;

public interface PulsaTelponTriPresenterInterface {
    void onRequesttransferpulsaTelponTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaTelponTri(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
