package com.example.gelur.module.pulsadataXLXD.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.laporantransaksi.view.LaporanTransaksi;
import com.example.gelur.module.pulsadataXLPXD.adapter.PulsaDataXLPXDAdapter;
import com.example.gelur.module.pulsadataXLPXD.presenter.PulsaDataXLPXDPresenter;
import com.example.gelur.module.pulsadataXLXD.adapter.PulsaDataXLXDAdapter;
import com.example.gelur.module.pulsadataXLXD.presenter.PulsaDataXLXDPresenter;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PulsaDataXLXD extends AppCompatActivity implements PulsaDataXLXDInterface {

    @BindView(R.id.coordinator_list_pulsa_data_xl_xd)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_pulsa_data_xl_xd)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_data_xl_xd)
    View viewContent;

    @BindView(R.id.layout_loading_list_pulsa_data_xl_xd) View viewLoading;

    @BindView(R.id.rv_pulsa_data_xl_xd)
    RecyclerView recyclerView;

    AppCompatButton submit_data;

    AlertDialog loadingDialog;

    AlertDialog.Builder dialogBuilder;

    EditText no_hp;
    EditText qty_indosat;

    PulsaDataXLXDPresenter presenter;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pulsa_data_xl_xd);

        ButterKnife.bind(this);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pulsa Data XL Hotrod Mini");

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        //hardcode
        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String perintah = "ch.xd";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        presenter = new PulsaDataXLXDPresenter(this);
        presenter.onRequetListPricePulsaDataXLPXD(this, req, kodereseller, perintah, time, pin, password);
        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(PulsaDataXLXD.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(PulsaDataXLXD.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(PulsaDataXLXD.this, AkunPulsa.class));
    }

    @Override
    public void TransferPulsaDataXLXDSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(PulsaDataXLXD.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void TransferPulsaDataXLXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(PulsaDataXLXD.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void LoadPricePulsaXLXDSukses(ResponsNonTransaksi responsNonTransaksi) {
        String get_msg = responsNonTransaksi.getMsg();
        String[] stringList = get_msg.split("\\r\\n");

        String stringList8 = stringList[6];
        String[] cut_stringList8 = stringList8.split("\\s+");
        String gabungStringList3 = cut_stringList8[0]+" "+cut_stringList8[1]+" "+cut_stringList8[2]+" "+cut_stringList8[3]+" "+cut_stringList8[4];

        ArrayList<String> ListPulsa = new ArrayList<>();
        ListPulsa.add(stringList[1]);
        ListPulsa.add(stringList[2]);
        ListPulsa.add(stringList[3]);
        ListPulsa.add(stringList[4]);
        ListPulsa.add(stringList[5]);
        ListPulsa.add(gabungStringList3);

        PulsaDataXLXDAdapter adapter = new PulsaDataXLXDAdapter(this, ListPulsa, presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        showContent(true);
    }

    @Override
    public void LoadPricePulsaXLXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {

    }

    public void createNewPopUpDialog(String nominal) {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popUpDialog = getLayoutInflater().inflate(R.layout.popup_pulsa_data_xl, null);
        no_hp = (EditText) popUpDialog.findViewById(R.id.Et_nomor_handphone_data_xl);
        submit_data = (AppCompatButton) popUpDialog.findViewById(R.id.submit_request_transfer_pulsa_data_xl);
        qty_indosat = (EditText) popUpDialog.findViewById(R.id.qty_data_xl);

        dialogBuilder.setView(popUpDialog);
        loadingDialog = dialogBuilder.create();
        loadingDialog.show();

        submit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int min = 500;
                int max = 10000;

                //Generate random int value from 50 to 100
                System.out.println("Random value in int from "+min+" to "+max+ ":");
                int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

                Format f = new SimpleDateFormat("hh:mm:ss a");
                String strResult = f.format(new Date());

                String split_time = String.valueOf(strResult);
                String[] reslt_split = split_time.split(":");

                String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
                String[] split_lagi = gabung_split.split("\\s+");
                String param_split = split_lagi[0];

                AppsUtil sessionUser = new AppsUtil(PulsaDataXLXD.this);
                HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

                String req = "topup";
                String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
                String qty = qty_indosat.getText().toString();
                String refid = String.valueOf(random_int);
                String get_no_hp = no_hp.getText().toString();
                String time = param_split;
                String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
                String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
                String counter = qty_indosat.getText().toString();

                presenter.onRequesttransferpulsaDataXLPXD(PulsaDataXLXD.this, req, kode_reseller, nominal, get_no_hp, counter, qty, refid, time, pin, password);
            }
        });
    }
}
