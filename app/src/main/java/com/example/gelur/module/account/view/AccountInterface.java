package com.example.gelur.module.account.view;

import com.example.gelur.pojo.TimelinePersonalUser;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.pojo.UsersPojo;

import java.util.List;

public interface AccountInterface {
    void ListItemClick(TimelinePojo timelinePojo);
    void OnClickUserEditProfile();

    void onLoadTimelineUserSuccess(List<TimelinePojo> timelinePersonalUsers);
    void onLoadTimelineUserFailed(String message);

    void onLoadUserDetailsSuccess(List<UsersPojo> usersPojos);
    void onLoadUserDetailFailed(String email);
}
