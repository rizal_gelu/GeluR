package com.example.gelur.module.pulsadatatelkomselTDF.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindInt;
import butterknife.BindView;
import butterknife.ButterKnife;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.ecommerceovo.view.EcommerceOvo;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.laporantransaksi.view.LaporanTransaksi;
import com.example.gelur.module.pulsadatatelkomselTDF.adapter.PulsaDataTelkomselTDFAdapter;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.PulsaDataTelkomselTDFPresenter;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class PulsaDataTelkomselTDF extends AppCompatActivity implements PulsaDataTelkomselTDFInterface {

    @BindView(R.id.coordinator_list_pulsa_data_telkomsel_tdf)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_pulsa_data_telkomsel)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_data_telkomsel_tdf)
    View viewContent;

    @BindView(R.id.layout_loading_list_pulsa_data_telkomsel_tdf) View viewLoading;

    @BindView(R.id.rv_pulsa_data_telkomsel_tdf)
    RecyclerView recyclerView;

    AlertDialog loadingDialog;

    AlertDialog.Builder dialogBuilder;

    AppCompatButton submit_data;

    EditText Et_nomor_handphone;
    EditText qty_data_telkomsel_tdf;

    PulsaDataTelkomselTDFPresenter presenter;

    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pulsa_data_telkomsel_tdf);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Data Telkomsel");
        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        //hardcode
        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String perintah = "ch.tdf";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        presenter = new PulsaDataTelkomselTDFPresenter(this);
        presenter.onRequetListPricePulsaDataTelkomselTDF(this, req, kodereseller, perintah, time, pin, password);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(PulsaDataTelkomselTDF.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(PulsaDataTelkomselTDF.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(PulsaDataTelkomselTDF.this, AkunPulsa.class));
    }

    @Override
    public void TransferPulsaDataTelkomselTDFSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(PulsaDataTelkomselTDF.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void TransferPulsaDataTelkomselTDFGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(PulsaDataTelkomselTDF.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void LoadListPricePulsaDataTelkomselTDF(ResponsNonTransaksi responsNonTransaksi) {
        String get_msg = responsNonTransaksi.getMsg();
        String[] stringList = get_msg.split("\\r\\n");

        String stringList9 = stringList[9];
        String[] cut_stringList9 = stringList9.split("\\s+");
        String gabungStringList9 = cut_stringList9[0]+" "+cut_stringList9[1]+" "+cut_stringList9[2]+" "+cut_stringList9[3]+" "+cut_stringList9[4]+" "+cut_stringList9[5];

        ArrayList<String> ListPulsa = new ArrayList<>();
        ListPulsa.add(stringList[1]);
        ListPulsa.add(stringList[2]);
        ListPulsa.add(stringList[3]);
        ListPulsa.add(stringList[4]);
        ListPulsa.add(stringList[5]);
        ListPulsa.add(stringList[6]);
        ListPulsa.add(stringList[7]);
        ListPulsa.add(stringList[8]);
        ListPulsa.add(gabungStringList9);

        PulsaDataTelkomselTDFAdapter adapter = new PulsaDataTelkomselTDFAdapter(this, ListPulsa, presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        showContent(true);
    }

    @Override
    public void createNewPopUpDialog(String id) {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popUpDialog = getLayoutInflater().inflate(R.layout.popup_pulsa_data_telkomsel_tdf, null);
        Et_nomor_handphone = (EditText) popUpDialog.findViewById(R.id.Et_nomor_handphone);
        submit_data = (AppCompatButton) popUpDialog.findViewById(R.id.submit_request_transfer_pulsa_data_telkomsel_tdf);
        qty_data_telkomsel_tdf = (EditText) popUpDialog.findViewById(R.id.qty_data_telkomsel_tdf);

        dialogBuilder.setView(popUpDialog);
        loadingDialog = dialogBuilder.create();
        loadingDialog.show();

        submit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int min = 500;
                int max = 10000;

                //Generate random int value from 50 to 100
                System.out.println("Random value in int from "+min+" to "+max+ ":");
                int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

                Format f = new SimpleDateFormat("hh:mm:ss a");
                String strResult = f.format(new Date());

                String split_time = String.valueOf(strResult);
                String[] reslt_split = split_time.split(":");

                String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
                String[] split_lagi = gabung_split.split("\\s+");
                String param_split = split_lagi[0];

                AppsUtil sessionUser = new AppsUtil(PulsaDataTelkomselTDF.this);
                HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

                String req = "topup";
                String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
                String qty = qty_data_telkomsel_tdf.getText().toString();
                String refid = String.valueOf(random_int);
                String get_no_hp = Et_nomor_handphone.getText().toString();
                String time = param_split;
                String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
                String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
                String counter = qty_data_telkomsel_tdf.getText().toString();

                presenter.onRequesttransferPulsaDataTelkomselTDF(PulsaDataTelkomselTDF.this, req, kode_reseller, id, get_no_hp, counter, qty, refid, time, pin, password);
            }
        });
    }
}
