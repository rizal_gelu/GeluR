package com.example.gelur.module.pulsadataindosatIDY.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;
import com.example.gelur.module.pulsadataindosatIDY.rest.PulsaDataIndosatIDYRestClient;
import com.example.gelur.module.pulsadatatelkomselTDF.rest.PulsaDataTelkomselTDFRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataIndosatIDYIDYModel implements PulsaDataIndosatIDYModelInterface {
    @Override
    public void RequestTransferDataIndosatIDY(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDY listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaDataIndosatIDYRestClient.get().req_transfer_pulsa_data_indosat(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataIndosatIDYSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataIndosatIDYGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataIndosatIDY(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDY listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTelkomselTDFRestClient.get().getPriceListPulsaDataTelkomselTDF(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataIndosatIDYSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataIndosatIDYGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataIndosatIDYGagal(t.getMessage());
            }
        });
    }
}
