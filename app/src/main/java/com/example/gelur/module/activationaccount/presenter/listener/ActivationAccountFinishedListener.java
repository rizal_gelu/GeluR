package com.example.gelur.module.activationaccount.presenter.listener;

import android.content.Context;

import com.example.gelur.pojo.ActivateAccountPojo;

public interface ActivationAccountFinishedListener {
    void onActiveAccountSuccess(ActivateAccountPojo activateAccountPojo);
    void onActiveAccountFail(String message);
}
