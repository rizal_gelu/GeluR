package com.example.gelur.module.pulsadatatriTRDGM.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDGMInterface {

    void TransferPulsaDataTriTRDAONSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDAONGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDAONSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDAONGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
