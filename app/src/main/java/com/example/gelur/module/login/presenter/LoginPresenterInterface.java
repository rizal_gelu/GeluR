package com.example.gelur.module.login.presenter;

import android.content.Context;

public interface LoginPresenterInterface {
    void onLoginClicked(Context context, String email, String password);
    void onSignUpClicked();
    void onForgotPasswordClicked();
}
