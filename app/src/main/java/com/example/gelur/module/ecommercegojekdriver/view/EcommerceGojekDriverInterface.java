package com.example.gelur.module.ecommercegojekdriver.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface EcommerceGojekDriverInterface {
    void TransferSaldoGojekDriverSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferSaldoGojekDriverGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceGojekDriver(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
