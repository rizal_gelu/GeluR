package com.example.gelur.module.search.view;

import android.accounts.Account;
import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.example.gelur.R;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.adapter.SearchFragmentAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Search extends AppCompatActivity implements SearchInterface {

    @BindView(R.id.activity_search_list)
    RecyclerView recyclerView;

    @BindView(R.id.search_list)
    View vieContent;

    @BindView(R.id.pager_header)
    TabLayout tabLayout;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    SearchFragmentAdapter searchFragmentAdapter;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        mContext = this;
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
//                        goToSearch();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Search.this, PostTimeline.class));
            }
        });

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        SearchFragmentAdapter searchFragmentAdapter = new SearchFragmentAdapter(getSupportFragmentManager());
        vpPager.setAdapter(searchFragmentAdapter);

        ButterKnife.bind(this);
    }

    private void goToProfile() {
        startActivity(new Intent(Search.this, Account.class));
    }

    private void goToNotification() {
        startActivity(new Intent(Search.this, Notification.class));
    }

    @Override
    public void onSuccessSearch() {

    }

    @Override
    public void onFailSearch() {

    }
}
