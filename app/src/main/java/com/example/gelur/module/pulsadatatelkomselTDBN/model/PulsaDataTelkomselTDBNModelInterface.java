package com.example.gelur.module.pulsadatatelkomselTDBN.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDBN.presenter.listener.OnLoadPulsaDataTelkomselTDBN;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;

public interface PulsaDataTelkomselTDBNModelInterface {
    void RequestTransferPulsaDataTelkomselTDBN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTelkomselTDBN listener);
    void RequestListPricePulsaDataTelkomselTDBN(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTelkomselTDBN listener);
}
