package com.example.gelur.module.pulsatelpontelkomsel.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaTelponTelkomselInterface {

    void TransferPulsaRegulerTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaRegulerTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaTelponTelkomsel(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
