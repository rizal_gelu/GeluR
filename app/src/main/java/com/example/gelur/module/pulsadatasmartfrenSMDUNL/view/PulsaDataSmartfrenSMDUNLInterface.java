package com.example.gelur.module.pulsadatasmartfrenSMDUNL.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataSmartfrenSMDUNLInterface {
    void TransferPulsaDataSmartfrenSMDUNLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataSmartfrenSMDUNLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataSmartfrenSMDUNLSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataSmartfrenSMDUNLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
