package com.example.gelur.module.pulsadatatelkomselTDBCN.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTelkomselTDBCN {
    void RequestTransferPulsaDataTelkomselTDBCNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTelkomselTDBCNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTelkomselTDBCN(String data);
    void ResponseListPricePulsaDataTelkomselTDBCNSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTelkomselTDBCNGagal(String m);
}
