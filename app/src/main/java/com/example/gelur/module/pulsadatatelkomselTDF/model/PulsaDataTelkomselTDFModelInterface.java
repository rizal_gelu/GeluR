package com.example.gelur.module.pulsadatatelkomselTDF.model;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.presenter.listener.OnLoadEcommerceOvo;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;

public interface PulsaDataTelkomselTDFModelInterface {
    void RequestTransferPulsaDataTelkomselTDF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTelkomselTDF listener);
    void RequestListPricePulsaDataTelkomselTDF(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTelkomselTDF listener);
}
