package com.example.gelur.module.account.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.gelur.module.account.presenter.listener.OnLoadAccountFinished;
import com.example.gelur.pojo.TimelinePersonalUser;
import com.example.gelur.R;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.DateUtil;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class AccountFeedRecyclerViewAdapter extends RecyclerView.Adapter<AccountFeedRecyclerViewAdapter.ViewHolderContent> {

    private List<TimelinePojo> listfeedPersonal;
    private OnLoadAccountFinished onLoadAccountFinished;
    private Activity activity;

    public AccountFeedRecyclerViewAdapter(Activity activity, List<TimelinePojo> listfeedPersonal, OnLoadAccountFinished onLoadAccountFinished) {
        this.listfeedPersonal = listfeedPersonal;
        this.activity = activity;
        this.onLoadAccountFinished = onLoadAccountFinished;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_view_personal_feed_timeline, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        holder.mItem = listfeedPersonal.get(position);

        long dateCreated = holder.mItem.getDate_created();
        Date today = new Date();
        String processConvert = DateUtil.SelisihDateTime(dateCreated, today);

        if (holder.mItem.getAvatar() != "") { // Jika ada file avatar dalam database
            Glide.with(activity).load(AppsConstant.BASE_IMAGE_PROFILE_URL + holder.mItem.getAvatar())
                    .into(holder.circleImageView);
        } else { // Jika file avatar belum di upload oleh user
            Glide.with(activity).load(R.drawable.ic_logo_g).into(holder.circleImageView);
        }

        if (!holder.mItem.getAvatar().equals("")) {
            Picasso.get().load(AppsConstant.BASE_TIMELINE_FILE_URL + holder.mItem.getAvatar()).into(holder.imgView_posPic);
        } else {
            holder.imgView_posPic.setVisibility(View.GONE);
        }

        holder.mNameView.setText(holder.mItem.getFirst_name()+" "+holder.mItem.getFirst_name());
        holder.mDateday.setText(processConvert);

        holder.btn_comment_feed_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    @Override
    public int getItemCount() {
        return listfeedPersonal.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {

        public final View mView;
        public final CircleImageView circleImageView;
        public final TextView mNameView;
        public final TextView mDateday;
        public final TextView mStatus;
        public final ImageView imgView_posPic;
        public final TextView tv_count_likes_feed_account;
        public final TextView tv_count_comments_feed_account;
        public final TextView btn_like_feed_account;
        public final TextView btn_comment_feed_account;
        public final TextView btn_share_content_feed_account;
        public TimelinePojo mItem;


        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            circleImageView = (CircleImageView) view.findViewById(R.id.circleImg_profile_feed_account);
            mNameView = (TextView) view.findViewById(R.id.tv_username_feed_account);
            mDateday = (TextView) view.findViewById(R.id.tv_date_feed_acount);
            mStatus = (TextView) view.findViewById(R.id.tv_status);
            imgView_posPic = (ImageView) view.findViewById(R.id.imgView_posPic);
            tv_count_likes_feed_account = (TextView) view.findViewById(R.id.tv_count_likes_feed_account);
            tv_count_comments_feed_account = (TextView) view.findViewById(R.id.tv_count_comments_feed_account);
            btn_like_feed_account = (TextView) view.findViewById(R.id.btn_like_feed_account);
            btn_comment_feed_account = (TextView) view.findViewById(R.id.btn_comment_feed_account);
            btn_share_content_feed_account = (TextView) view.findViewById(R.id.btn_share_content_feed_account);
        }

        @Override
        public String toString() {
            return super.toString() + "'" + mNameView.getText() +"'";
        }
    }
}
