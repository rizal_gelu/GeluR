package com.example.gelur.module.taskboardcompany.model;

import android.content.Context;

import com.example.gelur.module.taskboardcompany.presenter.TaskBoardPresenter;
import com.example.gelur.module.taskboardcompany.presenter.listener.OnLoadTaskFinished;

public interface TaskBoardModelInterface {

    void loadDataTaskBoard(Context context, String company_id, OnLoadTaskFinished onLoadTaskFinished);

//    void loadDataTaskBoard(TaskBoardPresenter taskBoardPresenter, String company_id);
}
