package com.example.gelur.module.pulsadataindosatIDM.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDM.presenter.listener.OnLoadPulsaDataIndosatIDM;

import java.util.List;

public class PulsaDataIndosatIDMAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDMAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDM listener;
    List<String> stringList;

    public PulsaDataIndosatIDMAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDM listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idm, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idm.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDM(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idm;
        public final TextView pulsa_data_indosat_idm;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idm = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idm);
            pulsa_data_indosat_idm = (TextView) view.findViewById(R.id.pulsa_data_indosat_idm);
        }
    }
}
