package com.example.gelur.module.ecommerceshopee.presenter;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.model.EcommerceOvoModel;
import com.example.gelur.module.ecommerceshopee.model.EcommerceShopeeModel;
import com.example.gelur.module.ecommerceshopee.presenter.listener.OnLoadShopeePay;
import com.example.gelur.module.ecommerceshopee.view.EcommerceShopeeInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class EcommerceShopeePresenter implements EcommerceShopeePresenterInterface, OnLoadShopeePay {

    private WeakReference<EcommerceShopeeInterface> view;
    private EcommerceShopeeModel model;

    public EcommerceShopeePresenter(EcommerceShopeeInterface view) {
        this.view = new WeakReference<EcommerceShopeeInterface>(view);
        this.model = new EcommerceShopeeModel();
    }

    @Override
    public void onRequesttransfersaldoShopee(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferSaldoShopee(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceShopee(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceShopee(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferSaldoShopeeSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoShopeeSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferSaldoShopeeGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoShopeeGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickShopee(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceShopeeSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceShopee(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceShopeeGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
