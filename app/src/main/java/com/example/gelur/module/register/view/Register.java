package com.example.gelur.module.register.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.module.activationaccount.view.ActivationAccount;
import com.example.gelur.module.register.presenter.RegisterPresenter;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.snackbar.Snackbar;
import com.example.gelur.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Register extends AppCompatActivity implements RegisterInterface {
    @BindView(R.id.activity_sign_up_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_sign_up_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_sign_up_et_first_name)
    EditText etFirstName;

    @BindView(R.id.activity_sign_up_et_last_name) EditText etLastName;

    @BindView(R.id.activity_sign_up_et_email) EditText etEmail;

    @BindView(R.id.activity_sign_up_et_password) EditText etpassword;

    @BindView(R.id.activity_sign_up_et_repeat_password) EditText etRepeatPassword;

    @BindView(R.id.activity_sign_up_btn_sign_up)
    AppCompatButton btnSignUp;

    RegisterPresenter presenter;
    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new RegisterPresenter(this);
    }

    @Override
    public void onRegisterSuccess(UsersPojo usersPojo) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, "Sign up success. Please check your email", Snackbar.LENGTH_LONG).show();
        finish();
        Intent intent = new Intent(Register.this, ActivationAccount.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onRegisterFailed(String message) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @OnClick(R.id.activity_sign_up_btn_sign_up)
    public void onSignUpClick(View v) {
        String email            = etEmail.getText().toString();
        String firstName        = etFirstName.getText().toString();
        String lastName         = etLastName.getText().toString();
        String password         = etpassword.getText().toString();
        String repeatPassword   = etRepeatPassword.getText().toString();
        int status              = 0;

        if (email.equals("") || !AppsUtil.isValidEmail(email)) {
            etEmail.setError("This field can not be empty or invalid email address");
            return;
        } else if (firstName.equals("") || firstName.length() < 3) {
            etFirstName.setError("This field is required and the minimum length is 4");
            return;
        } else if (lastName.equals("") || lastName.length() < 3) {
            etLastName.setError("This field is required and the minimum length is 4");
            return;
        } else if (password.equals("") || password.length() < 6) {
            etpassword.setError("This field is required and the minimum length is 6");
            return;
        } else if (!repeatPassword.equals(password)) {
            etRepeatPassword.setError("Password not match");
            return;
        }

        loadingDialog.show();
        UsersPojo usersPojo = new UsersPojo(email, password, firstName, lastName, "", status);
        presenter.signUpUser(usersPojo);

        // create session
        String name = AppsConstant.KEY_NAME;
        AppsUtil sessionManagement = new AppsUtil(this);
        sessionManagement.createRegisterSession(name, email);
    }
}
