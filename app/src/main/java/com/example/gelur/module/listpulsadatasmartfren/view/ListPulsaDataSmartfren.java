package com.example.gelur.module.listpulsadatasmartfren.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listpulsadataXL.view.ListPulsaDataXLInterface;
import com.example.gelur.module.pulsadataSMDEVO.view.PulsaDataSmartfrenSMDEVO;
import com.example.gelur.module.pulsadataXLPXD.view.PulsaDataXLPXD;
import com.example.gelur.module.pulsadataXLXDC.view.PulsaDataXLXDC;
import com.example.gelur.module.pulsadataXLXDCV.view.PulsaDataXLXDCV;
import com.example.gelur.module.pulsadatasmartfrenSMD.view.PulsaDataSmartfrenSMD;
import com.example.gelur.module.pulsadatasmartfrenSMDUNL.view.PulsaDataSmartfrenSMDUNL;
import com.example.gelur.module.pulsadataxlXML.view.PulsaDataXLXML;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListPulsaDataSmartfren extends AppCompatActivity implements ListPulsaDataSmartfrenInterface {

    @BindView(R.id.activity_list_pulsa_data_smartfren)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_list_pulsa_data_smartfren_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_data_smartfren)
    View viewContent;

    @BindView(R.id.layout_loading_list_pulsa_data_smartfren) View viewLoading;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_pulsa_data_smartfren);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Pulsa Data Tri");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListPulsaDataSmartfren.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListPulsaDataSmartfren.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListPulsaDataSmartfren.this, AkunPulsa.class));
    }


    @OnClick(R.id.ll_pulsa_data_SMD)
    public void onClickSMD() {
        startActivity(new Intent(ListPulsaDataSmartfren.this, PulsaDataSmartfrenSMD.class));
    }

    @OnClick(R.id.ll_pulsa_data_SMDEVO)
    public void onClickSMDEVO() {
        startActivity(new Intent(ListPulsaDataSmartfren.this, PulsaDataSmartfrenSMDEVO.class));
    }

    @OnClick(R.id.ll_pulsa_data_Smart_UNLK)
    public void onClickUNLK() {
        startActivity(new Intent(ListPulsaDataSmartfren.this, PulsaDataSmartfrenSMDUNL.class));
    }
}
