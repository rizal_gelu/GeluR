package com.example.gelur.module.college.rest;

import com.example.gelur.module.bio.rest.BioAPIURLConstant;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.CollegePojo;
import com.example.gelur.pojo.FormaterResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface CollegeRestInterface {
    @FormUrlEncoded
    @POST(CollegeAPIURLConstant.POST_COLLEGE)
    Call<CollegePojo> postCollege(@Field("email") String email, @Field("college_name") String college_name, @Field("faculty_major") String faculty_major, @Field("graduated") int graduated, @Field("years_graduate") String years_graduate, @Field("description") String description);

    @GET(CollegeAPIURLConstant.GET_COLLEGE)
    Call<FormaterResponse> getCollege();
}
