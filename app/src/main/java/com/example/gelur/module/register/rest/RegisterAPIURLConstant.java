package com.example.gelur.module.register.rest;

import com.example.gelur.util.AppsConstant;

public class RegisterAPIURLConstant {
    public static final String BASE_API_USER_URL = AppsConstant.BASE_API_URL + "v1/users/";
    public static final String REGISTER_URL = BASE_API_USER_URL+ "register";
}
