package com.example.gelur.module.pulsaregulerindosat.presenter.listener;

import com.example.gelur.pojo.FormatResponseNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.util.List;

public interface OnLoadPulsaRegulerIndosat {
    void RequestTransferPulsaRegulerIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaRegulerIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void RequestListPulsaRegulerIndosatSukses(ResponsNonTransaksi responsNonTransaksi);
    void RequestListPulsaRegulerIndosatGagal(String message);

    void onClickListPulsa(String data);
}
