package com.example.gelur.module.gameff.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.gameff.presenter.listener.OnLoadGameFF;

import java.util.List;

public class GameFFAdapter extends RecyclerView.Adapter<GameFFAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadGameFF listener;
    List<String> stringList;

    public GameFFAdapter(Activity activity, List<String> datum, OnLoadGameFF listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public GameFFAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_game_ff, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(GameFFAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.game_ff.setText(data);

        holder.ll_kumpulan_option_game_ff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickGameFF(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_game_ff;
        public final TextView game_ff;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_game_ff = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_game_ff);
            game_ff = (TextView) view.findViewById(R.id.game_ff);
        }
    }
}
