package com.example.gelur.module.resetpassword.presenter;

import android.content.Context;

public interface ResetPasswordPresenterInterface {
    void submitResetPassword(Context context, String email, String forgot_password_code, String new_password);
}
