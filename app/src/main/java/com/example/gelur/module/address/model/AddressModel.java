package com.example.gelur.module.address.model;

import android.content.Context;

import com.example.gelur.module.address.presenter.listener.OnSaveAddressUserFinishedListener;
import com.example.gelur.module.address.rest.AddressRestClient;
import com.example.gelur.pojo.AddressPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddressModel implements AddressModelInterface {
    @Override
    public void clickAddressSave(Context context, String email, String name_city, String type_city, OnSaveAddressUserFinishedListener listener) {
        Call<AddressPojo> address_user = AddressRestClient.get().postAddress(email, name_city, type_city);
        address_user.enqueue(new Callback<AddressPojo>() {
            @Override
            public void onResponse(Call<AddressPojo> call, Response<AddressPojo> response) {
                if (response.isSuccessful()) {
                    listener.onSaveAddressSuccess(response.body());
                } else {
                    listener.onSaveAddressFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<AddressPojo> call, Throwable t) {
                listener.onSaveAddressFailed(t.getMessage());
            }
        });
    }
}
