package com.example.gelur.module.account.presenter;

import android.content.Context;

import com.example.gelur.module.account.model.AccountModel;
import com.example.gelur.module.account.presenter.listener.OnLoadAccountFinished;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.account.view.AccountInterface;
import com.example.gelur.pojo.TimelinePersonalUser;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.pojo.UsersPojo;

import java.lang.ref.WeakReference;
import java.util.List;

public class AccountPresenter implements AccountPresenterInterface, OnLoadAccountFinished {

    private WeakReference<AccountInterface> view;
    private AccountModel model;

    public AccountPresenter(Account view) {
        this.view = new WeakReference<>(view);
        this.model = new AccountModel();
    }
    
    @Override
    public void loadProfileDetail(Context context, String email) {
        model.getProfileAccount(context, email, this);
    }

    @Override
    public void loadFeedTimeline(Context context, String email) {
        model.getFeedAccount(context, email, this);
    }

    @Override
    public void onLoadFeedTimelineSuccess(List<TimelinePojo> timelinePojoList) {
        if (view.get() != null) view.get().onLoadTimelineUserSuccess(timelinePojoList);
    }

    @Override
    public void onLoadFeedTimelineFailed(String message) {

    }

    @Override
    public void onLoadProfileDetail(List<UsersPojo> usersPojo) {
        if (view.get() != null) view.get().onLoadUserDetailsSuccess(usersPojo);
    }

    @Override
    public void onLoadProfileFailed(String message) {

    }
}
