package com.example.gelur.module.pulsadataindosatIDFC.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDFC.model.PulsaDataIndosatIDFCModel;
import com.example.gelur.module.pulsadataindosatIDFC.presenter.listener.OnLoadPulsaDataIndosatIDFC;
import com.example.gelur.module.pulsadataindosatIDFC.view.PulsaDataIndosatIDFCInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDFCPresenter implements PulsaDataIndosatIDFCPresenterInterface, OnLoadPulsaDataIndosatIDFC {

    private WeakReference<PulsaDataIndosatIDFCInterface> view;
    private PulsaDataIndosatIDFCModel model;

    public PulsaDataIndosatIDFCPresenter(PulsaDataIndosatIDFCInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDFCInterface>(view);
        this.model = new PulsaDataIndosatIDFCModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDFCSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDYSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDFCGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDYGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDFC(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDFCSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDYSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDFCGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDFC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDFC(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDFC(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDFC(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
