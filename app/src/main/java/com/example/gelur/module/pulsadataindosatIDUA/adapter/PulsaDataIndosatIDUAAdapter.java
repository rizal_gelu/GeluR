package com.example.gelur.module.pulsadataindosatIDUA.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDUA.presenter.listener.OnLoadPulsaDataIndosatIDUA;

import java.util.List;

public class PulsaDataIndosatIDUAAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDUAAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDUA listener;
    List<String> stringList;

    public PulsaDataIndosatIDUAAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDUA listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idua, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idua.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDUA(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idua;
        public final TextView pulsa_data_indosat_idua;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idua = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idua);
            pulsa_data_indosat_idua = (TextView) view.findViewById(R.id.pulsa_data_indosat_idua);
        }
    }
}
