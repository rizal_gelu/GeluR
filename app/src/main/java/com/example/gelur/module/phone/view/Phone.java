package com.example.gelur.module.phone.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.contactinfo.view.ContactInfo;
import com.example.gelur.module.phone.presenter.PhonePresenter;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class Phone extends AppCompatActivity implements PhoneInterface {

    @BindView(R.id.activity_phone_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.layout_phone_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.et_add_phone)
    EditText et_add_phone;

    BottomNavigationView bottomNavigationView;

    FloatingActionButton floatingActionButton;

    PhonePresenter presenter;

    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_phone);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Phone.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new PhonePresenter(this);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHome() {
        startActivity(new Intent(Phone.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(Phone.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(Phone.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(Phone.this, Account.class));
    }

    @OnClick(R.id.save_phone_btn_click)
    public void save_phone(View v) {
        String email = "Rizalgelui@yahoo.co.id";
        String phone = et_add_phone.getText().toString();

        presenter.onSaveClicked(this, email, phone);
        loadingDialog.show();
        loadingDialog.dismiss();
    }

    @Override
    public void onSavePhoneSuccess() {
        loadingDialog.dismiss();
        startActivity(new Intent(Phone.this, ContactInfo.class));
    }

    @Override
    public void onSavePhoneFail(String message) {

    }
}
