package com.example.gelur.module.pulsaregulertri.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsaregulertelkomsel.rest.PulsaRegulerTelkomselRestClient;
import com.example.gelur.module.pulsaregulertri.presenter.listener.OnLoadPulsaRegulerTri;
import com.example.gelur.module.pulsaregulertri.rest.PulsaRegulerTriRestClient;
import com.example.gelur.module.pulsasmstelkomsel.rest.PulsaSmsTelkomselRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaRegulerTriModel implements PulsaRegulerTriModelInterface {

    @Override
    public void RequestTransferPulsaRegulerTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerTri listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PulsaRegulerTriRestClient.get().req_transfer_pulsa_reguler_tri(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaRegulerTriSukses(response.body());
                } else {
                    listener.RequestTransferPulsaRegulerTriGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaRegulerTri(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerTri listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = PulsaRegulerTriRestClient.get().getPriceListPulsaRegulerTri(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaRegulerTriSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaRegulerTriGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaRegulerTriGagal(t.getMessage());
            }
        });
    }
}
