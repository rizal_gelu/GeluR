package com.example.gelur.module.timeline.rest;

import com.example.gelur.module.bio.rest.BioAPIURLConstant;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.LikePojo;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface TimelineRestInterface {
    @GET(TimelineAPIURLConstant.GET_TIMELINES_URL)
    Call<FormaterResponse> getTimelines();

    @FormUrlEncoded
    @POST(TimelineAPIURLConstant.POST_LIKE_TL)
    Call<LikePojo> postLike(@Field("id_timeline") String id_timeline, @Field("email") String email);

    @FormUrlEncoded
    @DELETE(TimelineAPIURLConstant.UNLIKE_TL)
    Call<LikePojo> unlikeTL(@Field("id_timeline") String id_timeline, @Field("email") String email);
}
