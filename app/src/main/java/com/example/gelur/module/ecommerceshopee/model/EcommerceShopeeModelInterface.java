package com.example.gelur.module.ecommerceshopee.model;

import android.content.Context;

import com.example.gelur.module.ecommerceshopee.presenter.listener.OnLoadShopeePay;

public interface EcommerceShopeeModelInterface {
    void RequestTransferSaldoShopee(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadShopeePay listener);
    void RequestListPriceShopee(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadShopeePay listener);
}
