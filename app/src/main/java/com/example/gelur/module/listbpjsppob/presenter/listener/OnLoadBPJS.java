package com.example.gelur.module.listbpjsppob.presenter.listener;

import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadBPJS {
    void onLoadCheckBPJSSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void onLoadCheckBPJSGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void onLoadBayarBPJSSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void onLoadBayarBPJSGagal(ResponsTransaksiPulsa responsTransaksiPulsa);
}
