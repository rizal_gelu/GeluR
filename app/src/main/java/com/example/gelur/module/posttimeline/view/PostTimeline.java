package com.example.gelur.module.posttimeline.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.bumptech.glide.Glide;
import com.example.gelur.module.login.view.Login;
import com.example.gelur.module.posttimeline.presenter.PostTimelinePresenter;
import com.example.gelur.pojo.PostTimelinePojo;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.example.gelur.R;

import org.parceler.Parcels;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class PostTimeline extends AppCompatActivity implements PostTimelineInterface {

    private static final int CAMERA_PIC_REQUEST = 1337;
    @BindView(R.id.activity_post_timeline_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_post_timeline_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.tv_cancel_post)
    TextView tvCancePost;

    @BindView(R.id.tv_title_layout_create_post) TextView tvTitleLayoutCreatePost;

    @BindView(R.id.tv_post_feed) TextView tvPostFeed;

    @BindView(R.id.iv_post_feed)
    CircleImageView ivPostFeed;

    @BindView(R.id.tv_name_post_feed) TextView tvNamePostFeed;

    @BindView(R.id.et_post_feed)
    EditText etPostFeed;

    @BindView(R.id.photo_video)
    RelativeLayout btn_add_photo_video;

    @BindView(R.id.camera_post) RelativeLayout btn_camera;

    @BindView(R.id.check_in_position_on_post) RelativeLayout btn_check_in_position;

    @BindView(R.id.tags_person_post) RelativeLayout btn_tags_person_post;

    @BindView(R.id.iv_Image_post) ImageView iv_Image_post;

    @BindView(R.id.image_view)
    LinearLayout ContentImageView;

    @BindView(R.id.layout_content_post_timeline)
    View viewContent;

    @BindView(R.id.layout_loading_post_timeline) View viewLoading;

    BottomNavigationView bottomNavigationView;

    PostTimelinePresenter presenter;
    PostTimelinePojo postTimelinePojo;
    UsersPojo user;

    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_post_timeline);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        ButterKnife.bind(this);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ContentImageView.setVisibility(View.GONE);

        UsersPojo usersPojo = Parcels.unwrap(getIntent().getParcelableExtra(AppsConstant.KEY_ID_USER));
        populateUI(usersPojo);


        presenter = new PostTimelinePresenter(this);

        showContent(true);
    }

    public void populateUI(UsersPojo usersPojo) {
        this.user = usersPojo;

        if (null != usersPojo) {
            Glide.with(this).load(AppsConstant.BASE_IMAGE_PROFILE_URL + usersPojo.getAvatar()).into(ivPostFeed);
            tvNamePostFeed.setText(usersPojo.getFirst_name() + " " + usersPojo.getLast_name());
        }
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onSuccesPost(PostTimelinePojo postTimelinePojo) {
//        loadingDialog.dismiss();
//        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogTheme);
//        builder.setTitle(getString(R.string.app_name));
//        builder.setMessage("Post Success");
//        builder.setCancelable(false);
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.dismiss();
//                finish();
//            }
//        });
//        builder.show();
        finish();
        startActivity(getIntent());
    }

    @Override
    public void onFailedPost(String message) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_SHORT).show();
    }

    @Override
    public void onUserNotLoggedIn(String message) {
        loadingDialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogTheme);
        builder.setTitle(getString(R.string.user_not_logged_in));
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton("Login", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
                startActivity(new Intent(PostTimeline.this, Login.class));
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @OnClick(R.id.tv_post_feed)
    public void submitPost(View view) {
        loadingDialog.show();

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserDetailsFromSession();

        String email = userDetails.get(AppsConstant.KEY_ID_USER);
        String file_name = String.valueOf(iv_Image_post.getTag());
//        String file_name = iv_Image_post.getN
        String description = etPostFeed.toString();

        presenter.submitPostTimeline(this, email, file_name, description);
    }

    private void goToHome() {
//        startActivity(new Intent(PostTimeline.this, Timeline.class));
    }

    private void goToNotification() {
    }

    private void goToProfile() {
    }

    @OnClick(R.id.camera_post)
    public void accessCamera() {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_PIC_REQUEST) {
            Bitmap image = (Bitmap) data.getExtras().get("data");
            ContentImageView.setVisibility(View.VISIBLE);
            iv_Image_post.setImageBitmap(image);
        }
    }

}
