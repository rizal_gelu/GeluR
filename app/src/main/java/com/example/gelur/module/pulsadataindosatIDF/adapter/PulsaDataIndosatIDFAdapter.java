package com.example.gelur.module.pulsadataindosatIDF.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDF.presenter.listener.OnLoadPulsaDataIndosatIDF;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;

import java.util.List;

public class PulsaDataIndosatIDFAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDFAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDF listener;
    List<String> stringList;

    public PulsaDataIndosatIDFAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDF listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idf, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idf.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDF(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idf;
        public final TextView pulsa_data_indosat_idf;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idf = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idf);
            pulsa_data_indosat_idf = (TextView) view.findViewById(R.id.pulsa_data_indosat_idf);
        }
    }
}
