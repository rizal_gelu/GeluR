package com.example.gelur.module.resetpassword.rest;

import com.example.gelur.module.activationaccount.rest.ActivationAccountAPIURLConstant;
import com.example.gelur.module.activationaccount.rest.ActivationAccountRestInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ResetPasswordRestClient {
    private static ResetPasswordRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    private ResetPasswordRestClient() {

    }

    public static ResetPasswordRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(logging);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(ResetPasswordAPIURLConstant.RESET_PASSWORD)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(ResetPasswordRestInterface.class);
    }
}
