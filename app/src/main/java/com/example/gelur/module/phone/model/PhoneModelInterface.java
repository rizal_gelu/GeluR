package com.example.gelur.module.phone.model;

import android.content.Context;

import com.example.gelur.module.phone.presenter.listener.OnSavePhoneFinishedListener;


public interface PhoneModelInterface {
    void clickSavePhone(Context context, String email, String bio, OnSavePhoneFinishedListener listener);
}
