package com.example.gelur.module.pulsadatasmartfrenSMD.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadatasmartfrenSMD.model.PulsaDataSmartfrenSMDModel;
import com.example.gelur.module.pulsadatasmartfrenSMD.presenter.listener.OnLoadPulsaDataSmartfrenSMD;
import com.example.gelur.module.pulsadatasmartfrenSMD.view.PulsaDataSmartfrenSMDInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataSmartfrenSMDPresenter implements PulsaDataSmartfrenSMDPresenterInterface, OnLoadPulsaDataSmartfrenSMD {

    private WeakReference<PulsaDataSmartfrenSMDInterface> view;
    private PulsaDataSmartfrenSMDModel model;

    public PulsaDataSmartfrenSMDPresenter(PulsaDataSmartfrenSMDInterface view) {
        this.view = new WeakReference<PulsaDataSmartfrenSMDInterface>(view);
        this.model = new PulsaDataSmartfrenSMDModel();
    }

    @Override
    public void onRequesttransferpulsadataSmartfrenSMD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataSmartfren(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataSmartfrenSMD(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataSmartfrenSMD(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataSmartFrenSMDSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataSmartfrenSMDSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataSmartFrenSMDGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataSmartfrenSMDGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataSmartFrenSMD(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataSmartFrenSMDSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataSmartfrenSMDSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataSmartFrenSMDGagal(String m) {

    }
}
