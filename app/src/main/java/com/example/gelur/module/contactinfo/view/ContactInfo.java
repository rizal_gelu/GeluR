package com.example.gelur.module.contactinfo.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.address.view.Address;
import com.example.gelur.module.contactinfo.presenter.ContactInfoPresenter;
import com.example.gelur.module.phone.view.Phone;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class ContactInfo extends AppCompatActivity implements ContactInfoInterface {

    @BindView(R.id.activity_contact_info_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_contact_info_toolbar)
    Toolbar toolbar;

    @BindView(R.id.contact_info_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    ContactInfoPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_contact_info);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ContactInfo.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter = new ContactInfoPresenter(this);

        showContent(true);
    }

    private void goToHome() {
        startActivity(new Intent(ContactInfo.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(ContactInfo.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(ContactInfo.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(ContactInfo.this, Account.class));
    }


    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }


    @OnClick(R.id.add_phone_contact_info)
    public void add_phone() {
        presenter.addPhone();
    }

    @Override
    public void GoToAddPhone() {
        startActivity(new Intent(ContactInfo.this, Phone.class));
    }

    @OnClick(R.id.add_address_contact_info)
    public void add_address() {
        presenter.addAddress();
    }

    @Override
    public void GoToAddAddress() {
        startActivity(new Intent(ContactInfo.this, Address.class));
    }

    @Override
    public void GoToAddSocialLink() {

    }

    @Override
    public void GoToAddWebsite() {

    }
}
