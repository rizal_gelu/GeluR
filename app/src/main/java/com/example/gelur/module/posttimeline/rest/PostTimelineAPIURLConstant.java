package com.example.gelur.module.posttimeline.rest;

import com.example.gelur.util.AppsConstant;

public class PostTimelineAPIURLConstant {
    public static final String BASE_API_LOGIN_URL = AppsConstant.BASE_API_URL + "v1/timelines/";
    public static final String POST_TIMELINE_URL = BASE_API_LOGIN_URL + "post_timeline/";
}
