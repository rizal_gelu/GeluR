package com.example.gelur.module.timeline.presenter;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.gelur.module.timeline.model.TimelineModel;
import com.example.gelur.module.timeline.presenter.listener.OnLoadTimelineFinished;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.timeline.view.TimelineInterface;
import com.example.gelur.pojo.LikePojo;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.R;
import com.example.gelur.util.AppsUtil;

import java.lang.ref.WeakReference;
import java.util.List;

public class TimelinePresenter implements TimelinePresenterInterface, OnLoadTimelineFinished {

    private WeakReference<TimelineInterface> view;
    private TimelineModel model;

    public TimelinePresenter(TimelineInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new TimelineModel();
    }

    public void loadTimeline(Context mContext) {
        model.loadTimelineData(this);
    }

    @Override
    public void onLoadTimelineDataSuccess(List<TimelinePojo> timelineList) {
        if (view.get() != null) view.get().onLoadTimelineSuccess(timelineList);
    }

    @Override
    public void onLoadTimelineDataFailed(String message) {
        if (view.get() != null) view.get().onLoadTimelineFailed(message);
    }

    public UsersPojo getLoggedInUser(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(context.getResources().getString(R.string.app_name), Context.MODE_PRIVATE);
        UsersPojo user = new UsersPojo();

        user.setEmail(prefs.getString(AppsConstant.KEY_ID_USER, ""));
        user.setFirst_name(prefs.getString(AppsConstant.KEY_FIRST_NAME, ""));
        user.setLast_name(prefs.getString(AppsConstant.KEY_LAST_NAME, ""));
        user.setAvatar(prefs.getString(AppsConstant.KEY_AVATAR, ""));
        user.setPassword(prefs.getString(AppsConstant.KEY_PASSWORD, ""));
        user.setStatus(prefs.getInt(AppsConstant.KEY_STATUS, 0));

        return user;
    }

    @Override
    public void onPostLikeTLSuccess(LikePojo likePojo) {

    }

    @Override
    public void onPostLikeTLFailed(String message) {

    }

    @Override
    public void onClickUnlikeProses(String id_timeline, String email) {
        model.unLikeTimeline(id_timeline, email, this);
    }

    @Override
    public void onClickUnlikeSuccess(LikePojo likePojo) {

    }

    @Override
    public void onClickUnlikeFail(String message) {

    }

    @Override
    public void onClickUserName(String email) {
        if(view.get() != null) view.get().onClickUser();
    }

    @Override
    public void onClickLikeProses(String id_timeline, String email) {
        model.postLikeTimeline(id_timeline, email, this);
    }

    public void userLogout(Context context) {
        // Logout user from the session
        AppsUtil session = new AppsUtil(context);
        session.removeSession();

        if (null != view.get()) view.get().onUserLogout();
    }
}