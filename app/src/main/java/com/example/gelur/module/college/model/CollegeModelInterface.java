package com.example.gelur.module.college.model;

import android.content.Context;

import com.example.gelur.module.college.presenter.listener.OnSaveCollegeFinishedListener;

public interface CollegeModelInterface {
    void clickSaveCollege(Context context, String email, String college_name, String faculty_major, int graduated, String years_graduate, String description, OnSaveCollegeFinishedListener listener);
}
