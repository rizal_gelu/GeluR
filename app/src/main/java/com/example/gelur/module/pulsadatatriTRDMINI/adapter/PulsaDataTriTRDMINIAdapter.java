package com.example.gelur.module.pulsadatatriTRDMINI.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDMINI.presenter.listener.OnLoadPulsaDataTriTRDMINI;

import java.util.List;

public class PulsaDataTriTRDMINIAdapter extends RecyclerView.Adapter<PulsaDataTriTRDMINIAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataTriTRDMINI listener;
    List<String> stringList;

    public PulsaDataTriTRDMINIAdapter(Activity activity, List<String> datum, OnLoadPulsaDataTriTRDMINI listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_tri_trdmini, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_tri_trdmini.setText(data);

        holder.ll_kumpulan_option_pulsa_data_tri_trdmini.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataTriTRDMINI(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_tri_trdmini;
        public final TextView pulsa_data_tri_trdmini;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_tri_trdmini = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_tri_trdmini);
            pulsa_data_tri_trdmini = (TextView) view.findViewById(R.id.pulsa_data_tri_trdmini);
        }
    }
}
