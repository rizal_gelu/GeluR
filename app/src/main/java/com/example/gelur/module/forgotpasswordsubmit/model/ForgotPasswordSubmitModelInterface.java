package com.example.gelur.module.forgotpasswordsubmit.model;

import android.content.Context;

import com.example.gelur.module.forgotpasswordsubmit.presenter.listener.OnForgotPasswordSubmitListener;

public interface ForgotPasswordSubmitModelInterface{
    void OnSubmitForgotPassword(Context context, String email, String forgot_password_code, OnForgotPasswordSubmitListener listener);
}
