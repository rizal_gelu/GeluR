package com.example.gelur.module.gamepubg.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface GamePUBGInterface {
    void TransferDiamondPUBGSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferDiamondPUBGGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceGamePUBG(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
