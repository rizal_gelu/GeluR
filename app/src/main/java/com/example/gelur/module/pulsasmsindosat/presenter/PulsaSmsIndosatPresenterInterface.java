package com.example.gelur.module.pulsasmsindosat.presenter;

import android.content.Context;

public interface PulsaSmsIndosatPresenterInterface {
    void onRequesttransferpulsasmsIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricepulsasmsIndosat(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
