package com.example.gelur.module.pulsaregulertri.presenter;

import android.content.Context;

import com.example.gelur.module.pulsaregulertelkomsel.model.PulsaRegulerTelkomselModel;
import com.example.gelur.module.pulsaregulertelkomsel.view.PulsaRegulerTelkomselInterface;
import com.example.gelur.module.pulsaregulertri.model.PulsaRegulerTriModel;
import com.example.gelur.module.pulsaregulertri.presenter.listener.OnLoadPulsaRegulerTri;
import com.example.gelur.module.pulsaregulertri.view.PulsaRegulerTriInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaRegulerTriPresenter implements PulsaRegulerTriPresenterInterface, OnLoadPulsaRegulerTri {

    private WeakReference<PulsaRegulerTriInterface> view;
    private PulsaRegulerTriModel model;

    public PulsaRegulerTriPresenter(PulsaRegulerTriInterface view) {
        this.view = new WeakReference<PulsaRegulerTriInterface>(view);
        this.model = new PulsaRegulerTriModel();
    }

    @Override
    public void onRequesttransferpulsaregulerTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaRegulerTri(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaRegulerTri(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaRegulerTri(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaRegulerTriSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerTriSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaRegulerTriGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerTriGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaRegulerTri(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaRegulerTriSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaRegulerTri(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaRegulerTriGagal(String m) {

    }
}
