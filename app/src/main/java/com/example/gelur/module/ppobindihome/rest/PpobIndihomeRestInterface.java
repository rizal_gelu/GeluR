package com.example.gelur.module.ppobindihome.rest;

import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PpobIndihomeRestInterface {
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsTransaksiPulsa> req_check_indihome(@Body RequestTransaksiPulsa requestTransaksiPulsa);

//    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
//    Call<ResponsTransaksiPulsa> req_bayar_bpjs(@Body RequestTransaksiPulsa requestTransaksiPulsa);
}
