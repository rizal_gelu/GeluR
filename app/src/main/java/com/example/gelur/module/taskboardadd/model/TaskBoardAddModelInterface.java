package com.example.gelur.module.taskboardadd.model;

import android.content.Context;

import com.example.gelur.module.bio.presenter.listener.OnSaveBioFinishedListener;
import com.example.gelur.module.taskboardadd.presenter.listener.OnSaveTaskBoardAddFinished;

import retrofit2.http.Field;

public interface TaskBoardAddModelInterface {
    void clickSaveTaskBoard(Context context, String title_list_task, String company_id, String description_list_task, String priority, String due_date,
                            String create_by, String task_id, OnSaveTaskBoardAddFinished onSaveTaskBoardAddFinished);
}
