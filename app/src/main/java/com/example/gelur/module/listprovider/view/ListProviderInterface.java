package com.example.gelur.module.listprovider.view;

public interface ListProviderInterface {
    void onClickTelkomsel();
    void onClickIndosat();
    void onClickXL();
    void onClickSmartFren();
    void onClickTri();
}
