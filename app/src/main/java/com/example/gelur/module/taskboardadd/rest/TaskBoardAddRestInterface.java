package com.example.gelur.module.taskboardadd.rest;

import com.example.gelur.module.login.rest.LoginAPIURLConstant;
import com.example.gelur.module.taskboardadd.presenter.listener.OnSaveTaskBoardAddFinished;
import com.example.gelur.pojo.TaskBoardAddPojo;
import com.example.gelur.pojo.UsersPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface TaskBoardAddRestInterface {
    @FormUrlEncoded
    @POST(TaskBoardAddAPIURLConstant.POST_TASK_BOARD_ADD)
    Call<TaskBoardAddPojo> postTaskBoard(@Field("title_list_task") String title_list_task,
                                         @Field("company_id") String company_id,
                                         @Field("description_list_task") String description_list_task,
                                         @Field("priority") String priority, @Field("due_date") String due_date,
                                         @Field("create_by") String create_by, @Field("task_id") String task_id);
}
