package com.example.gelur.module.ppobindihome.rest;

import com.example.gelur.module.listbpjsppob.rest.BPJSRestInterface;
import com.example.gelur.module.login.rest.LoginAPIURLConstant;
import com.example.gelur.module.login.rest.LoginRestInterface;
import com.example.gelur.util.AppsConstant;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PpobIndihomeRestClient {
    private static PpobIndihomeRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    public static PpobIndihomeRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS) // connect timeout
                .writeTimeout(30, TimeUnit.SECONDS) // write timeout
                .readTimeout(30, TimeUnit.SECONDS) // read timeout
                .build();
        client.newBuilder().addInterceptor(logging);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(AppsConstant.BASE_API_NON_TRANSAKSI)
                .client(client.newBuilder().build())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(PpobIndihomeRestInterface.class);
    }
}
