package com.example.gelur.module.gameff.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface GameFFInterface {
    void TransferDiamondFFSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferDiamondFFGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceGameFF(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
