package com.example.gelur.module.listpulsadatatelkomsel.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.pulsadatatelkomselTDB.view.PulsaDataTelkomselTDB;
import com.example.gelur.module.pulsadatatelkomselTDBCN.view.PulsaDataTelkomselTDBCN;
import com.example.gelur.module.pulsadatatelkomselTDBN.view.PulsaDataTelkomselTDBN;
import com.example.gelur.module.pulsadatatelkomselTDF.view.PulsaDataTelkomselTDF;
import com.example.gelur.module.pulsadatatelkomselTDM.view.PulsaDataTelkomselTDM;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ListPulsaDataTelkomsel extends AppCompatActivity implements ListPulsaDataTelkomselInterface {
    @BindView(R.id.activity_list_pulsa_data_telkomsel)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_list_pulsa_data_telkomsel_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_data_telkomsel)
    View viewContent;

    @BindView(R.id.layout_loading_list_provider)View viewLoading;

    @BindView(R.id.kumpulan_option_list_product_data_telkomsel) LinearLayout kumpulan_option_list_product_data_telkomsel;

    @BindView(R.id.ll_pulsa_data_TDF) LinearLayout ll_pulsa_data_TDF;

    @BindView(R.id.pulsa_data_TDF)
    TextView pulsa_data_TDF;

    @BindView(R.id.ll_pulsa_data_TDB) LinearLayout ll_pulsa_data_TDB;

    @BindView(R.id.pulsa_data_TDB) TextView pulsa_data_TDB;

    @BindView(R.id.ll_pulsa_data_TDBCN) LinearLayout ll_pulsa_data_TDBCN;

    @BindView(R.id.pulsa_data_TDBCN) TextView pulsa_data_TDBCN;

    @BindView(R.id.ll_pulsa_data_TDBN) LinearLayout ll_pulsa_data_TDBN;

    @BindView(R.id.pulsa_data_TDBN) TextView pulsa_data_TDBN;

    @BindView(R.id.ll_pulsa_data_TDM) LinearLayout ll_pulsa_data_TDM;

    @BindView(R.id.pulsa_data_TDM) TextView pulsa_data_TDM;

    BottomNavigationView bottomNavigationView;

    AlertDialog loadingDialog;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_pulsa_data_telkomsel);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_pulsa_data_TDF)
    public void onClickPulsaDataTelkomselTDF() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, PulsaDataTelkomselTDF.class));
    }

    @OnClick(R.id.ll_pulsa_data_TDB)
    public void onClickPulsaDataTelkomselTDB() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, PulsaDataTelkomselTDB.class));
    }

    @OnClick(R.id.ll_pulsa_data_TDBCN)
    public void onClickPulsaDataTelkomselTDBN() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, PulsaDataTelkomselTDBN.class));
    }

    @OnClick(R.id.ll_pulsa_data_TDBN)
    public void onClickPulsaDataTelkomselTDBCN() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, PulsaDataTelkomselTDBCN.class));
    }

    @OnClick(R.id.ll_pulsa_data_TDM)
    public void onClickPulsaDataTelkomselTDM() {
        startActivity(new Intent(ListPulsaDataTelkomsel.this, PulsaDataTelkomselTDM.class));
    }
}
