package com.example.gelur.module.pulsaregulertri.model;

import android.content.Context;

import com.example.gelur.module.pulsaregulersmartfren.presenter.listener.OnLoadPulsaRegulerSmartfren;
import com.example.gelur.module.pulsaregulertri.presenter.listener.OnLoadPulsaRegulerTri;
import com.example.gelur.module.pulsasmsindosat.presenter.listener.OnLoadPulsaSmsIndosat;
import com.example.gelur.module.pulsatelpontri.presenter.listener.OnLoadPulsaTelponTri;

public interface PulsaRegulerTriModelInterface {
    void RequestTransferPulsaRegulerTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerTri listener);
    void RequestListPricePulsaRegulerTri(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerTri listener);
}
