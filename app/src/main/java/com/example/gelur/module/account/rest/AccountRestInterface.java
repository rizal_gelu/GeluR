package com.example.gelur.module.account.rest;

import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.UsersPojo;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AccountRestInterface {
    @GET(AccountAPIURLConstant.GET_FEED_USER_PERSONAL)
    Call<FormaterResponse> getFeedAccount(@Query("email") String email);

    @GET(AccountAPIURLConstant.GET_ACCOUNT_DETAIL)
    Call<FormaterResponse> getAccountDetail(@Query("email") String email);
}
