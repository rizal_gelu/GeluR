package com.example.gelur.module.register.model;

import com.example.gelur.module.register.presenter.listener.OnRegisteringUserFinishedListener;
import com.example.gelur.module.register.rest.RegisterRestClient;
import com.example.gelur.pojo.UsersPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterModel implements RegisterModelInterface {
    @Override
    public void registeringUserToServer(UsersPojo usersPojo, final OnRegisteringUserFinishedListener listener) {
        Call<UsersPojo> register = RegisterRestClient.get().register(usersPojo.getEmail(), usersPojo.getPassword(),
                usersPojo.getFirst_name(), usersPojo.getLast_name());

        register.enqueue(new Callback<UsersPojo>() {
            @Override
            public void onResponse(Call<UsersPojo> call, Response<UsersPojo> response) {
                if (response.isSuccessful()) {
                    listener.onRegisterSuccess(response.body());
                } else {
                    listener.onRegisterFailed(response.message());
                }
            }

            @Override
            public void onFailure(Call<UsersPojo> call, Throwable t) {
                listener.onRegisterFailed(t.getMessage());
            }
        });
    }
}
