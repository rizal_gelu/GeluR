package com.example.gelur.module.transferpulsa.presenter;

import android.content.Context;

import com.example.gelur.module.transferpulsa.model.TransferPulsaModel;
import com.example.gelur.module.transferpulsa.presenter.listener.OnLoadCekSaldoFinished;
import com.example.gelur.module.transferpulsa.view.TransferPulsaInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.util.AppsUtil;

import java.lang.ref.WeakReference;

public class TransferPulsaPresenter implements TransferPulsaPresenterInterface, OnLoadCekSaldoFinished {

    private WeakReference<TransferPulsaInterface> view;
    private TransferPulsaModel model;

    public TransferPulsaPresenter(TransferPulsaInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new TransferPulsaModel();
    }

    public void getInformasiSaldo(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.getInformationSaldo(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void onClickPPOB() {

    }

    @Override
    public void onClickPulsa() {

    }

    @Override
    public void onClickPulsaData() {

    }

    @Override
    public void onClickGame() {

    }

    @Override
    public void onClickEcommerce() {

    }

    @Override
    public void onClickPln() {

    }

    @Override
    public void cekSaldoSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().getSaldoSukses(responsNonTransaksi);
    }

    @Override
    public void cekSaldoGagal(String message) {

    }

    public void userLogout(Context context) {
        // Logout user from the session
        AppsUtil session = new AppsUtil(context);
        session.removeSession();

        if (null != view.get()) view.get().onUserLogout();
    }
}
