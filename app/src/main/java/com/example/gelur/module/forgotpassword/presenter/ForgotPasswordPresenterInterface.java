package com.example.gelur.module.forgotpassword.presenter;

public interface ForgotPasswordPresenterInterface {
    void submitForgotPassword(String email);
}
