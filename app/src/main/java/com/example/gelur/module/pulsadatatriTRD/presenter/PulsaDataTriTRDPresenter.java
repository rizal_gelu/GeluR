package com.example.gelur.module.pulsadatatriTRD.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRD.model.PulsaDataTriTRDModel;
import com.example.gelur.module.pulsadatatriTRD.presenter.listener.OnLoadPulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRD.view.PulsaDataTriTRDInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDPresenter implements PulsaDataTriTRDPresenterInterface, OnLoadPulsaDataTriTRD {

    private WeakReference<PulsaDataTriTRDInterface> view;
    private PulsaDataTriTRDModel model;

    public PulsaDataTriTRDPresenter(PulsaDataTriTRDInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDInterface>(view);
        this.model = new PulsaDataTriTRDModel();
    }

    @Override
    public void onRequesttransferpulsaDataTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRD(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRD(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRD(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRD(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDGagal(String m) {

    }
}
