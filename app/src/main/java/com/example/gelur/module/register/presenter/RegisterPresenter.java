package com.example.gelur.module.register.presenter;

import com.example.gelur.module.register.model.RegisterModel;
import com.example.gelur.module.register.presenter.listener.OnRegisteringUserFinishedListener;
import com.example.gelur.module.register.view.RegisterInterface;
import com.example.gelur.pojo.UsersPojo;

import java.lang.ref.WeakReference;

public class RegisterPresenter implements RegisterPresenterInterface, OnRegisteringUserFinishedListener {
    private WeakReference<RegisterInterface> view;
    private RegisterModel model;

    public RegisterPresenter(RegisterInterface view) {
        this.view = new WeakReference<RegisterInterface>(view);
        this.model = new RegisterModel();
    }

    @Override
    public void signUpUser(UsersPojo usersPojo) {
        model.registeringUserToServer(usersPojo, this);
    }

    @Override
    public void onRegisterSuccess(UsersPojo usersPojo) {
        if (view.get() != null) view.get().onRegisterSuccess(usersPojo);
    }

    @Override
    public void onRegisterFailed(String message) {
        if (view.get() != null) view.get().onRegisterFailed(message);
    }
}
