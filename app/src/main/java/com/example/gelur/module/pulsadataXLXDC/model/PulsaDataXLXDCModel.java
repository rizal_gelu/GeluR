package com.example.gelur.module.pulsadataXLXDC.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataXLXDC.presenter.listener.OnLoadPulsaDataXLXDC;
import com.example.gelur.module.pulsadataXLXDC.rest.PulsaDataXLPXDCRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataXLXDCModel implements PulsaDataXLXDCModelInterface {
    @Override
    public void RequestTransferPulsaDataXLXDC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataXLXDC listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataXLPXDCRestClient.get().req_transfer_pulsa_data_xl_xdc(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataXLXDCSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataXLXDCGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataXLXDC(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataXLXDC listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataXLPXDCRestClient.get().getPriceListPulsaDataXLXDC(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataXLXDCSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataXLXDCGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataXLXDCGagal(t.getMessage());
            }
        });
    }
}
