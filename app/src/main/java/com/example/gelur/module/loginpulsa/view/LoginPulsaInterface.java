package com.example.gelur.module.loginpulsa.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.UsersPojo;

import java.util.List;

public interface LoginPulsaInterface {
    void onLoginFailed(String message);
    void onLoginSuccess(ResponsNonTransaksi responsNonTransaksi);
}
