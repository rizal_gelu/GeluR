package com.example.gelur.module.pulsadatatelkomselTDB.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTelkomselTDB {
    void RequestTransferPulsaDataTelkomselTDBSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTelkomselTDBGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTelkomselTDB(String data);
    void ResponseListPricePulsaDataTelkomselTDBSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTelkomselTDBGagal(String m);
}
