package com.example.gelur.module.pulsaregulertelkomsel.presenter;

import android.content.Context;

public interface PulsaRegulerTelkomselPresenterInterface {
    void onRequesttransferpulsareguler(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaRegulerTelkomsel(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
