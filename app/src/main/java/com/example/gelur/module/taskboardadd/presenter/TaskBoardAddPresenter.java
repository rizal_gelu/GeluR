package com.example.gelur.module.taskboardadd.presenter;

import android.content.Context;

import com.example.gelur.module.bio.model.BioModel;
import com.example.gelur.module.bio.view.BioInterface;
import com.example.gelur.module.taskboardadd.model.TaskBoardAddModel;
import com.example.gelur.module.taskboardadd.presenter.listener.OnSaveTaskBoardAddFinished;
import com.example.gelur.module.taskboardadd.view.TaskBoardAddInterface;
import com.example.gelur.pojo.TaskBoardAddPojo;

import java.lang.ref.WeakReference;

public class TaskBoardAddPresenter implements TaskBoardAddPresenterInterface, OnSaveTaskBoardAddFinished {

    private WeakReference<TaskBoardAddInterface> view;
    private TaskBoardAddModel model;

    public TaskBoardAddPresenter(TaskBoardAddInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new TaskBoardAddModel();
    }

    @Override
    public void saveTaskJobList(Context context, String title_list_task, String company_id, String description_task, String priority, String due_date, String create_by, String task_id) {
        model.clickSaveTaskBoard(context, title_list_task, company_id, description_task, priority, due_date, create_by, task_id, this);
    }

    @Override
    public void onSaveTaskBoardSuccess(TaskBoardAddPojo taskBoardAddPojo) {

    }

    @Override
    public void onSaveTaskBoardFailed(String message) {

    }
}
