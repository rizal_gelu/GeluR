package com.example.gelur.module.pulsaregulerxl.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaRegulerXL {
    void RequestTransferPulsaRegulerXLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaRegulerXLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaRegulerXl(String data);
    void ResponseListPricePulsaRegulerXLSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaRegulerXlGagal(String m);
}
