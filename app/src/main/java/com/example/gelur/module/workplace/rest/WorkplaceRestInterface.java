package com.example.gelur.module.workplace.rest;

import com.example.gelur.module.bio.rest.BioAPIURLConstant;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.WorkplacePojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface WorkplaceRestInterface {
    @FormUrlEncoded
    @POST(WorkplaceAPIRULConstant.POST_WORKPLACE)
    Call<WorkplacePojo> postWorkplace(@Field("email") String email, @Field("name_company") String name_company, @Field("job_title") String job_title, @Field("location") String location);

    @GET(WorkplaceAPIRULConstant.GET_WORKPLACE)
    Call<FormaterResponse> getWorkplace();
}
