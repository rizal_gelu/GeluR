package com.example.gelur.module.register.presenter.listener;

import com.example.gelur.pojo.UsersPojo;

public interface OnRegisteringUserFinishedListener {
    void onRegisterSuccess(UsersPojo usersPojo);
    void onRegisterFailed(String message);
}
