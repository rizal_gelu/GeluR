package com.example.gelur.module.historytransaksi.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.historytransaksi.presenter.listener.OnLoadHistoryTransaksi;

import java.util.List;

public class HistoryTransaksiPulsaAdapter extends RecyclerView.Adapter<HistoryTransaksiPulsaAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadHistoryTransaksi listener;
    List<String> stringList;

    public HistoryTransaksiPulsaAdapter(Activity activity, List<String> datum, OnLoadHistoryTransaksi listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolderContent onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_transaction_history, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.history_transaksi.setText(data);

        holder.ll_kumpulan_history_transaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickHistoryTransaksi(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_history_transaksi;
        public final TextView history_transaksi;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_history_transaksi = (LinearLayout) view.findViewById(R.id.ll_kumpulan_history_transaksi);
            history_transaksi = (TextView) view.findViewById(R.id.history_transaksi);
        }
    }
}
