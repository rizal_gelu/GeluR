package com.example.gelur.module.listproducttelkomsel.view;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface ListProductTelkomselInterface {
    void onPulsaReguller();
    void onClickPaketData();
    void onClickPaketTelpon();
    void onclckPaketSms();
}
