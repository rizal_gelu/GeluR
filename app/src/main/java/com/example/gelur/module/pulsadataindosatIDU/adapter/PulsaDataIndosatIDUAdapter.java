package com.example.gelur.module.pulsadataindosatIDU.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;
import com.example.gelur.module.pulsadataindosatIDU.presenter.listener.OnLoadPulsaDataIndosatIDU;
import com.example.gelur.module.pulsadataindosatIDUA.presenter.listener.OnLoadPulsaDataIndosatIDUA;

import java.util.List;

public class PulsaDataIndosatIDUAdapter extends RecyclerView.Adapter<PulsaDataIndosatIDUAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataIndosatIDU listener;
    List<String> stringList;

    public PulsaDataIndosatIDUAdapter(Activity activity, List<String> datum, OnLoadPulsaDataIndosatIDU listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_indosat_idu, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_indosat_idu.setText(data);

        holder.ll_kumpulan_option_pulsa_data_indosat_idu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataIndosatIDU(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_indosat_idu;
        public final TextView pulsa_data_indosat_idu;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_indosat_idu = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_indosat_idu);
            pulsa_data_indosat_idu = (TextView) view.findViewById(R.id.pulsa_data_indosat_idu);
        }
    }
}
