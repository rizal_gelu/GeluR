package com.example.gelur.module.ecommercegojekdriver.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.ecommercedana.adapter.EcommerceDanaRecyclerViewAdapater;
import com.example.gelur.module.ecommercegojekdriver.presenter.listener.OnLoadGojekDriver;

import java.util.List;

public class EcommerceGojekDriverAdapter extends RecyclerView.Adapter<EcommerceGojekDriverAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadGojekDriver listener;
    List<String> stringList;

    public EcommerceGojekDriverAdapter(Activity activity, List<String> datum, OnLoadGojekDriver listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ecommerce_gojek_driver, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.gojek_driver.setText(data);

        holder.ll_kumpulan_option_gojek_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickGojekDriver(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_gojek_driver;
        public final TextView gojek_driver;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_gojek_driver = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_gojek_driver);
            gojek_driver = (TextView) view.findViewById(R.id.gojek_driver);
        }
    }
}
