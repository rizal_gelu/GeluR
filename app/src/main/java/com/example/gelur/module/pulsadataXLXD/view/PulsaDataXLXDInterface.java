package com.example.gelur.module.pulsadataXLXD.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataXLXDInterface {
    void TransferPulsaDataXLXDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataXLXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaXLXDSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaXLXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
