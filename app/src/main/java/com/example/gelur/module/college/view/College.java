package com.example.gelur.module.college.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.college.presenter.CollegePresenter;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class College extends AppCompatActivity implements CollegeInterface {

    @BindView(R.id.activity_college_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.etCollegeName)
    EditText etCollegeName;

    @BindView(R.id.concentration)
    EditText etConcentration;

    @BindView(R.id.layout_college_content) View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.graduated_tv)
    TextView tvGraduated;

    @BindView(R.id.id_checkbox_college) CheckBox ch_college_check;

    @BindView(R.id.spinnersince)
    Spinner spinnersince;

    BottomNavigationView bottomNavigationView;

    FloatingActionButton floatingActionButton;

    CollegePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_college);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(College.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);

        presenter = new CollegePresenter(this);

        showContent(true);
    }

    private void goToHome() {
        startActivity(new Intent(College.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(College.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(College.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(College.this, Account.class));
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private int setCheckBoxCollegeGraduated() {
        int check = 0;
        ch_college_check = (CheckBox) findViewById(R.id.id_checkbox_college);
        ch_college_check.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (((CheckBox) v).isChecked()) {
                    int check = 1;
                } else {
                    int check = 0;
                }
            }
        });
        return check;
    }

    @OnClick(R.id.save_college_btn_click)
    public void save_college() {
        String email = "Rizalgelui@yahoo.co.id";
        String college_name = etCollegeName.getText().toString();
        String faculty_major = etConcentration.getText().toString();
        int graduated =  setCheckBoxCollegeGraduated();
        String year_graduate = "2019";
        String description = "dahdaj";

        presenter.onSaveClicked(this, email, college_name, faculty_major, graduated, year_graduate, description);
    }

    @Override
    public void onSuccessPostCollege() {

    }

    @Override
    public void onFailedPostCollege(String message) {

    }
}
