package com.example.gelur.module.pulsadatatriTRDM.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDM.presenter.listener.OnLoadPulsaDataTriTRDM;

public interface PulsaDataTriTRDMModelInterface {
    void RequestTransferPulsaDataTriTRDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDM listener);
    void RequestListPricePulsaDataTriTRDM(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDM listener);
}
