package com.example.gelur.module.gamepubg.presenter;

import android.content.Context;

public interface GamePUBGPresenterInterface {
    void onRequesttransferDiamondPUBG(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPriceGamePUBG(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
