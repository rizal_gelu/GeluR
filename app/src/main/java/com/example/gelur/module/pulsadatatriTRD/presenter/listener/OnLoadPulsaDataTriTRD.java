package com.example.gelur.module.pulsadatatriTRD.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTriTRD {
    void RequestTransferPulsaDataTriTRDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTriTRDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTriTRD(String data);
    void ResponseListPricePulsaDataTriTRDSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTriTRDGagal(String m);
}
