package com.example.gelur.module.pulsadatatelkomselTDBN.presenter;

import android.content.Context;

public interface PulsaDataTelkomselTDBNPresenterInterface {
    void onRequesttransferPulsaDataTelkomselTDBN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTelkomselTDBN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
