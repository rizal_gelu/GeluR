package com.example.gelur.module.pulsadatatelkomselTDBN.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTelkomselTDBNInterface {
    void TransferPulsaDataTelkomselTDBNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTelkomselTDBNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaDataTelkomselTDBN(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
