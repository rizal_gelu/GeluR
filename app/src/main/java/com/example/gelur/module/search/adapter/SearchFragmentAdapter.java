package com.example.gelur.module.search.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.gelur.module.search.view.SearchCompany;
import com.example.gelur.module.search.view.SearchGroup;
import com.example.gelur.module.search.view.SearchPerson;

import java.util.ArrayList;
import java.util.List;

public class SearchFragmentAdapter extends FragmentPagerAdapter {

    private static int numOfTabs = 3;

    public SearchFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case 0: // Fragment # 0 - This will show FirstFragment
                return SearchPerson.newInstance(0, "About");

            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "Page " + position;
    }

}
