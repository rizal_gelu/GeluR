package com.example.gelur.module.ecommercegojekpenumpang.model;

import android.content.Context;

import com.example.gelur.module.ecommercegojekdriver.presenter.listener.OnLoadGojekDriver;
import com.example.gelur.module.ecommercegojekpenumpang.presenter.listener.OnLoadGojekPenumpang;

public interface EcommerceGojekPenumpangModelInterface {
    void RequestTransferSaldoGojekPenumpang(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGojekPenumpang listener);
    void RequestListPriceGojekPenumpang(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadGojekPenumpang listener);
}
