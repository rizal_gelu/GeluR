package com.example.gelur.module.historytransaksi.presenter;

import android.content.Context;

import com.example.gelur.module.historytransaksi.model.HistoryTransaksiModel;
import com.example.gelur.module.historytransaksi.presenter.listener.OnLoadHistoryTransaksi;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsaInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;

import java.lang.ref.WeakReference;


public class HistoryTransaksiPresenter implements HistoryTransaksiPresenterInterface, OnLoadHistoryTransaksi {

    private WeakReference<HistoryTransaksiPulsaInterface> view;
    private HistoryTransaksiModel model;

    public HistoryTransaksiPresenter(HistoryTransaksiPulsaInterface view) {
        this.view = new WeakReference<HistoryTransaksiPulsaInterface>(view);
        this.model = new HistoryTransaksiModel();
    }

    @Override
    public void findHistoryTransaksi(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestHistoryTransaksi(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void OnClickHistoryTransaksi(String data) {

    }

    @Override
    public void ResponseListHistoryTransaksiSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().onLoadHistoryTransaksiSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListHistoryTransaksiGagal(String m) {

    }
}
