package com.example.gelur.module.plnprabayar.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPlnPrabayar {
    void RequestTransferPlnPrabayarSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPlnPrabayarGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPlnPrabayar(String data);
    void ResponseListPricePlnPrabayarSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePlnPrabayarGagal(String m);
}
