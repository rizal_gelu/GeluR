package com.example.gelur.module.listecommerce.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.ecommercedana.view.EcommerceDana;
import com.example.gelur.module.ecommercelinkaja.view.EcommerceLinkAja;
import com.example.gelur.module.ecommerceovo.view.EcommerceOvo;
import com.example.gelur.module.ecommerceshopee.view.EcommerceShopee;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listecommercegojek.view.ListEcommerceGojek;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ListEcommerce extends AppCompatActivity implements ListEcommerceInterface {

    @BindView(R.id.coordinator_list_ecommerce)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_list_ecommerce)
    Toolbar toolbar;

    @BindView(R.id.content_list_content_ecommerce)
    View viewContent;

    @BindView(R.id.layout_loading_list_ecommerce) View viewLoading;

    @BindView(R.id.kumpulan_option_list_ecommerce)
    LinearLayout kumpulan_option_list_ecommerce;

    @BindView(R.id.ll_dana) LinearLayout ll_dana;

    @BindView(R.id.dana)
    TextView dana;

    @BindView(R.id.ll_ovo) LinearLayout ll_ovo;
    AlertDialog loadingDialog;

    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_ecommerce);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("GELU RELOAD - Ecommerce");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListEcommerce.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListEcommerce.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListEcommerce.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_dana)
    public void onClickDanaEcommerce() {
        startActivity(new Intent(ListEcommerce.this, EcommerceDana.class));
    }

    @OnClick(R.id.ll_ovo)
    public void onClickOvoEcommerce() {
        startActivity(new Intent(ListEcommerce.this, EcommerceOvo.class));
    }

    @OnClick(R.id.link_aja)
    public void onClickLinkAjaEcommerce() {
        startActivity(new Intent(ListEcommerce.this, EcommerceLinkAja.class));
    }

    @OnClick(R.id.ll_shopee)
    public void onClickShopePayEcommerce() {
        startActivity(new Intent(ListEcommerce.this, EcommerceShopee.class));
    }

    @OnClick(R.id.ll_gojek)
    public void onClickGojekEcommerce() {
        startActivity(new Intent(ListEcommerce.this, ListEcommerceGojek.class));
    }
}
