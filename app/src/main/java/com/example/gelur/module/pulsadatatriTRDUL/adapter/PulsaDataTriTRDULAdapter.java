package com.example.gelur.module.pulsadatatriTRDUL.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDPY.presenter.listener.OnLoadPulsaDataTriTRDPY;
import com.example.gelur.module.pulsadatatriTRDUL.presenter.listener.OnLoadPulsaDataTriTRDUL;

import java.util.List;

public class PulsaDataTriTRDULAdapter extends RecyclerView.Adapter<PulsaDataTriTRDULAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataTriTRDUL listener;
    List<String> stringList;

    public PulsaDataTriTRDULAdapter(Activity activity, List<String> datum, OnLoadPulsaDataTriTRDUL listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_tri_trdul, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_tri_trdul.setText(data);

        holder.ll_kumpulan_option_pulsa_data_tri_trdul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataTriTRDUL(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_tri_trdul;
        public final TextView pulsa_data_tri_trdul;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_tri_trdul = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_tri_trdul);
            pulsa_data_tri_trdul = (TextView) view.findViewById(R.id.pulsa_data_tri_trdul);
        }
    }
}
