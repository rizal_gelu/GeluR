package com.example.gelur.module.ecommercegojekdriver.presenter;

import android.content.Context;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface EcommerceGojekDriverPresenterInterface {
    void onRequesttransfersaldoGojekDriver(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPriceGojekDriver(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
