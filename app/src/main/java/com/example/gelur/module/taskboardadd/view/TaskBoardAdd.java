package com.example.gelur.module.taskboardadd.view;

import android.app.DatePickerDialog;
import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.taskboardadd.presenter.TaskBoardAddPresenter;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;

public class TaskBoardAdd extends AppCompatActivity implements TaskBoardAddInterface {

    @BindView(R.id.activity_task_board_add_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.layout_task_board_add_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.et_description_task) EditText et_description_task;

    @BindView(R.id.spinnerPriority)
    Spinner spinnerPriority;

    @BindView(R.id.et_duedate)
    TextView et_duedate;

    @BindView(R.id.etAddJobList) EditText etAddJobList;

    BottomNavigationView bottomNavigationView;

    FloatingActionButton floatingActionButton;

    AlertDialog loadingDialog;

    DatePickerDialog.OnDateSetListener setListener;

    TaskBoardAddPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_taskboard);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TaskBoardAdd.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);

        presenter = new TaskBoardAddPresenter(this);

        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        et_duedate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        TaskBoardAdd.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month = month+1;
                        String date = day+"/"+month+"/"+year;
                        et_duedate.setText(date);
                    }
                }, year, month, day);
                datePickerDialog.show();
            }
        });

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHome() {
        startActivity(new Intent(TaskBoardAdd.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(TaskBoardAdd.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(TaskBoardAdd.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(TaskBoardAdd.this, Account.class));
    }

    @OnClick(R.id.btn_save_job_list)
    public void onSaveTask() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.priority_task, R.layout.content_task_board_add);
        adapter.setDropDownViewResource(R.layout.content_task_board_add);

        String title_list_task = etAddJobList.getText().toString();
        String description_task = et_description_task.getText().toString();
        String priority = spinnerPriority.toString();
        String due_date = "27/03/2021";
        String create_by = "rizalgelu@yahoo.co.id";
        String task_id = "601e70f86b9e42584273129d";
        String company_id = "601378dd7bc4312a8996d221";

        presenter.saveTaskJobList(this, company_id, title_list_task, description_task, priority, due_date, create_by, task_id);
    }

    @Override
    public void onCancelTask() {

    }
}
