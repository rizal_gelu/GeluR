package com.example.gelur.module.search.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class SearchPerson extends Fragment {

    public static SearchPerson newInstance(int page, String title) {
        SearchPerson about = new SearchPerson();
        Bundle args = new Bundle();
        args.putInt("SomeInt", page);
        args.putString("SomeTitle", title);
        about.setArguments(args);
        return about;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
