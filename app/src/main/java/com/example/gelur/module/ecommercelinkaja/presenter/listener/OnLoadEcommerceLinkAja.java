package com.example.gelur.module.ecommercelinkaja.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadEcommerceLinkAja {
    void RequestTransferSaldoLinkAjaSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferSaldoLinkAjaGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickLinkAja(String data);
    void ResponseListPriceLinkAjaSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceLinkAjaGagal(String m);
}
