package com.example.gelur.module.pulsasmsindosat.presenter;

import android.content.Context;

import com.example.gelur.module.pulsasmsindosat.model.PulsaSmsIndosatModel;
import com.example.gelur.module.pulsasmsindosat.presenter.listener.OnLoadPulsaSmsIndosat;
import com.example.gelur.module.pulsasmsindosat.view.PulsaSmsIndosatInterface;
import com.example.gelur.module.pulsasmstelkomsel.model.PulsaSmsTelkomselModel;
import com.example.gelur.module.pulsasmstelkomsel.view.PulsaSmsTelkomselInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaSmsIndosatPresenter implements PulsaSmsIndosatPresenterInterface, OnLoadPulsaSmsIndosat {
    private WeakReference<PulsaSmsIndosatInterface> view;
    private PulsaSmsIndosatModel model;

    public PulsaSmsIndosatPresenter(PulsaSmsIndosatInterface view) {
        this.view = new WeakReference<PulsaSmsIndosatInterface>(view);
        this.model = new PulsaSmsIndosatModel();
    }

    @Override
    public void onRequesttransferpulsasmsIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaSmsIndosat(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);

    }

    @Override
    public void onRequetListPricepulsasmsIndosat(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaSmsIndosat(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaSmsIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaSmsIndosatSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaSmsIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaSmsIndosatGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaSmsIndosat(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaSmsIndosatSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaSmsIndosat(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaSmsIndosatGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
