package com.example.gelur.module.highschool.rest;

import com.example.gelur.pojo.HighSchoolPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface HighSchoolRestInterface {
    @FormUrlEncoded
    @POST(HighSchoolAPIURLConstant.POST_HIGH_SCHOOL)
    Call<HighSchoolPojo> postHighSchool(@Field("email") String email, @Field("high_school_name") String bio,
                                        @Field("year_graduate") String year_graduate, @Field("graduated") String graduated,
                                        @Field("description") String description);

    @GET(HighSchoolAPIURLConstant.GET_HIGH_SCHOOL)
    Call<HighSchoolPojo> getHighSchool();
}
