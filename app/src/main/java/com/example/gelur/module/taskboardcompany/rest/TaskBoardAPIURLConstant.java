package com.example.gelur.module.taskboardcompany.rest;

import com.example.gelur.util.AppsConstant;

public class TaskBoardAPIURLConstant {
    // POST TaskBoard EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/company/"; // User UriSegment 2
    public static final String POST_TASK_BOARD = BASE_API_USER + "post_bio/"; // User UriSegment 3

    // GET TaskBoard EndPoint
    public static final String GET_TASK_BOARD = BASE_API_USER + "get_job_by_company_id/"; // User UriSegment 3
}
