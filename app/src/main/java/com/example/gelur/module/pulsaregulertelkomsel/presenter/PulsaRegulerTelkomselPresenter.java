package com.example.gelur.module.pulsaregulertelkomsel.presenter;

import android.content.Context;

import com.example.gelur.module.pulsaregulertelkomsel.model.PulsaRegulerTelkomselModel;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.listener.OnLoadPulsaRegulerTelkomsel;
import com.example.gelur.module.pulsaregulertelkomsel.view.PulsaRegulerTelkomselInterface;
import com.example.gelur.module.topup.model.TopUpModel;
import com.example.gelur.module.topup.view.TopUpInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaRegulerTelkomselPresenter implements PulsaRegulerTelkomselPresenterInterface, OnLoadPulsaRegulerTelkomsel {

    private WeakReference<PulsaRegulerTelkomselInterface> view;
    private PulsaRegulerTelkomselModel model;

    public PulsaRegulerTelkomselPresenter(PulsaRegulerTelkomselInterface view) {
        this.view = new WeakReference<PulsaRegulerTelkomselInterface>(view);
        this.model = new PulsaRegulerTelkomselModel();
    }

    @Override
    public void RequestTransferPulsaRegulerTelkomselSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerTelkomselSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaRegulerTelkomselGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerTelkomselGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaRegulerTelkomsel(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaRegulerTelkomselSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaRegulerTelkomsel(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaRegulerTelkomselGagal(String m) {

    }


    @Override
    public void onRequesttransferpulsareguler(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsa(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaRegulerTelkomsel(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaRegulerTelkomsel(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
