package com.example.gelur.module.pulsadatasmartfrenSMDUNL.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataSMDEVO.presenter.listener.OnLoadPulsaDataSmartfrenSMDEVO;
import com.example.gelur.module.pulsadatasmartfrenSMDUNL.presenter.listener.OnLoadPulsaDataSmartfrenSMDUNL;

import java.util.List;

public class PulsaDataSmartfrenSMDUNLAdapter extends RecyclerView.Adapter<PulsaDataSmartfrenSMDUNLAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataSmartfrenSMDUNL listener;
    List<String> stringList;

    public PulsaDataSmartfrenSMDUNLAdapter(Activity activity, List<String> datum, OnLoadPulsaDataSmartfrenSMDUNL listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_smartfren_smdunl, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_smartfren_smdunl.setText(data);

        holder.ll_kumpulan_option_pulsa_data_smartfren_smdunl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataSmartFrenSMDUNL(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_smartfren_smdunl;
        public final TextView pulsa_data_smartfren_smdunl;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_smartfren_smdunl = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_smartfren_smdunl);
            pulsa_data_smartfren_smdunl = (TextView) view.findViewById(R.id.pulsa_data_smartfren_smdunl);
        }
    }
}
