package com.example.gelur.module.pulsadatatriTRDPY.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDDP.model.PulsaDataTriTRDDPModel;
import com.example.gelur.module.pulsadatatriTRDDP.view.PulsaDataTriTRDDPInterface;
import com.example.gelur.module.pulsadatatriTRDPY.model.PulsaDataTriTRDPYModel;
import com.example.gelur.module.pulsadatatriTRDPY.presenter.listener.OnLoadPulsaDataTriTRDPY;
import com.example.gelur.module.pulsadatatriTRDPY.view.PulsaDataTriTRDPYInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDPYPresenter implements PulsaDataTriTRDPYPresenterInterface, OnLoadPulsaDataTriTRDPY {

    private WeakReference<PulsaDataTriTRDPYInterface> view;
    private PulsaDataTriTRDPYModel model;

    public PulsaDataTriTRDPYPresenter(PulsaDataTriTRDPYInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDPYInterface>(view);
        this.model = new PulsaDataTriTRDPYModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDPY(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDPY(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDPY(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDPY(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDPYSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDPYSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDPYGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDPYGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDPY(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDPYSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDPYSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDPYGagal(String m) {

    }
}
