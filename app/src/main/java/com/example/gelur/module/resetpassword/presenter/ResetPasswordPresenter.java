package com.example.gelur.module.resetpassword.presenter;

import android.content.Context;

import com.example.gelur.module.resetpassword.model.ResetPasswordModel;
import com.example.gelur.module.resetpassword.presenter.listener.OnResetPasswordFinished;
import com.example.gelur.module.resetpassword.view.ResetPasswordInterface;
import com.example.gelur.pojo.ResetPasswordPojo;

import java.lang.ref.WeakReference;

public class ResetPasswordPresenter implements ResetPasswordPresenterInterface, OnResetPasswordFinished {

    private WeakReference<ResetPasswordInterface> view;
    private ResetPasswordModel model;

    public ResetPasswordPresenter(ResetPasswordInterface view) {
        this.view = new WeakReference<ResetPasswordInterface>(view);
        this.model = new ResetPasswordModel();
    }

    @Override
    public void submitResetPassword(Context context, String email, String forgot_password_code, String new_password) {
        model.OnSubmitResetPassword(context, email, forgot_password_code, new_password,this);
    }

    @Override
    public void onResetPasswordSuccess(ResetPasswordPojo resetPasswordPojo) {
        if (view.get() != null) view.get().onResetPasswordSuccess(resetPasswordPojo);
    }

    @Override
    public void onResetPasswordFail(String message) {

    }
}
