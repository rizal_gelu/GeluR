package com.example.gelur.module.bio.model;


import android.content.Context;

import com.example.gelur.module.bio.presenter.BioPresenter;
import com.example.gelur.module.bio.presenter.listener.OnLoadBioFinished;
import com.example.gelur.module.bio.presenter.listener.OnSaveBioFinishedListener;
import com.example.gelur.pojo.BioPojo;

public interface BioModelInterface {
    void loadBio(Context mContext, String email, OnLoadBioFinished onLoadBioFinished);

    void clickSave(Context context, String email, String bio, OnSaveBioFinishedListener listener);
}
