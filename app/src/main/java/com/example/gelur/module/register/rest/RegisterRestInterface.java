package com.example.gelur.module.register.rest;

import com.example.gelur.pojo.UsersPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface RegisterRestInterface {
    @FormUrlEncoded
    @POST(RegisterAPIURLConstant.REGISTER_URL)
    Call<UsersPojo> register(@Field("email") String email, @Field("password") String password,
                             @Field("first_name") String firstName, @Field("last_name") String lastName);
}
