package com.example.gelur.module.topup.presenter.listener;

import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

public interface OnRequestTopfinished {
    void RequestTopUpSukses(ResponsNonTransaksi responsNonTransaksi);
    void RequestTopUpGagal(String message);
}
