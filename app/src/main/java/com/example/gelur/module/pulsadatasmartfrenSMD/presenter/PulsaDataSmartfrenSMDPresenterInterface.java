package com.example.gelur.module.pulsadatasmartfrenSMD.presenter;

import android.content.Context;

public interface PulsaDataSmartfrenSMDPresenterInterface {
    void onRequesttransferpulsadataSmartfrenSMD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataSmartfrenSMD(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
