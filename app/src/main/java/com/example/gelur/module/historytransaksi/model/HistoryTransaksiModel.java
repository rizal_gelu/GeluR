package com.example.gelur.module.historytransaksi.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.historytransaksi.presenter.listener.OnLoadHistoryTransaksi;
import com.example.gelur.module.historytransaksi.rest.HistoryTransaksiRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryTransaksiModel implements HistoryTransaksiModelInterface{
    @Override
    public void RequestHistoryTransaksi(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadHistoryTransaksi listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = HistoryTransaksiRestClient.get().get_history_transaksi(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListHistoryTransaksiSukses(response.body());
                } else {
                    listener.ResponseListHistoryTransaksiGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListHistoryTransaksiGagal(t.getMessage());
            }
        });
    }
}
