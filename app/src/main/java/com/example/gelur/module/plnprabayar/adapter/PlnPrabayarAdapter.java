package com.example.gelur.module.plnprabayar.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.plnprabayar.presenter.listener.OnLoadPlnPrabayar;
import com.example.gelur.module.pulsadataindosatIDF.presenter.listener.OnLoadPulsaDataIndosatIDF;

import java.util.List;

public class PlnPrabayarAdapter extends RecyclerView.Adapter<PlnPrabayarAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPlnPrabayar listener;
    List<String> stringList;

    public PlnPrabayarAdapter(Activity activity, List<String> datum, OnLoadPlnPrabayar listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pln_prabayar, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pln_prabayar.setText(data);

        holder.ll_kumpulan_option_pln_prabayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPlnPrabayar(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pln_prabayar;
        public final TextView pln_prabayar;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pln_prabayar = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pln_prabayar);
            pln_prabayar = (TextView) view.findViewById(R.id.pln_prabayar);
        }
    }
}
