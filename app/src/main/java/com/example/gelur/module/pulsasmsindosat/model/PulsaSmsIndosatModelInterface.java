package com.example.gelur.module.pulsasmsindosat.model;

import android.content.Context;

import com.example.gelur.module.pulsasmsindosat.presenter.listener.OnLoadPulsaSmsIndosat;
import com.example.gelur.module.pulsasmstelkomsel.presenter.listener.OnLoadPulsaSmsTelkomsel;

public interface PulsaSmsIndosatModelInterface {
    void RequestTransferPulsaSmsIndosat(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaSmsIndosat listener);
    void RequestListPricePulsaSmsIndosat(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaSmsIndosat listener);
}
