package com.example.gelur.module.pulsadataindosatIDU.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;
import com.example.gelur.module.pulsadataindosatIDU.presenter.listener.OnLoadPulsaDataIndosatIDU;

public interface PulsaDataIndosatIDUModelInterface {
    void RequestTransferDataIndosatIDU(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDU listener);
    void RequestListPricePulsaDataIndosatIDU(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDU listener);
}
