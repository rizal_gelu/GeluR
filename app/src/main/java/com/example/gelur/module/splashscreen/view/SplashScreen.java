package com.example.gelur.module.splashscreen.view;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.gelur.R;
import com.example.gelur.module.loginpulsa.view.LoginPulsa;
import com.example.gelur.module.splashscreen.presenter.SplashScreenPresenter;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;

public class SplashScreen extends AppCompatActivity implements SplashScreenInterface {

    SplashScreenPresenter presenter;
    AppsUtil session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppsUtil.setFullScreen(this);
        setContentView(R.layout.activity_splash_screen);

        presenter = new SplashScreenPresenter(this);
        presenter.doSplash();
        session = new AppsUtil(this);
    }

    @Override
    protected void onDestroy() {
        presenter.destroySplash();
        super.onDestroy();
    }

    public void isLoggedIn() {
        if(session.isLoggedIn()) {
            Intent intent = new Intent(SplashScreen.this, TransferPulsa.class);
//            Intent intent = new Intent(SplashScreen.this, Timeline.class);
            startActivity(intent);
        } else {
//            Intent login = new Intent(SplashScreen.this, Login.class);
            Intent login_pulsa = new Intent(SplashScreen.this, LoginPulsa.class);
            startActivity(login_pulsa);
        }
    }

    @Override
    public void goToTimeline() {
        startActivity(new Intent(SplashScreen.this, Timeline.class));
        finish();
    }

    @Override
    public void goToLogin() {
//        startActivity(new Intent(SplashScreen.this, Login.class));
        startActivity(new Intent(SplashScreen.this, LoginPulsa.class));
        finish();
    }
}