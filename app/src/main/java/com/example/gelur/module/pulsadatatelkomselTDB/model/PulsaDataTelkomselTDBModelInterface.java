package com.example.gelur.module.pulsadatatelkomselTDB.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDB.presenter.listener.OnLoadPulsaDataTelkomselTDB;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;

public interface PulsaDataTelkomselTDBModelInterface {
    void RequestTransferPulsaDataTelkomselTDB(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTelkomselTDB listener);
    void RequestListPricePulsaDataTelkomselTDB(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTelkomselTDB listener);
}
