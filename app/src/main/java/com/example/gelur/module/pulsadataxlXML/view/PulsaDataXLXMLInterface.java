package com.example.gelur.module.pulsadataxlXML.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataXLXMLInterface {
    void TransferPulsaDataXLXMLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataXLXMLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaXLXMLSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaXLXMLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
