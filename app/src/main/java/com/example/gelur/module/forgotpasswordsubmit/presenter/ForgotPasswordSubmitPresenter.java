package com.example.gelur.module.forgotpasswordsubmit.presenter;

import android.content.Context;

import com.example.gelur.module.forgotpasswordsubmit.model.ForgotPasswordSubmitModel;
import com.example.gelur.module.forgotpasswordsubmit.presenter.listener.OnForgotPasswordSubmitListener;
import com.example.gelur.module.forgotpasswordsubmit.view.ForgotPasswordSubmitInterface;
import com.example.gelur.pojo.ForgotPasswordConfimPojo;

import java.lang.ref.WeakReference;

public class ForgotPasswordSubmitPresenter implements ForgotPasswordSubmitPresenterInterface, OnForgotPasswordSubmitListener {

    private WeakReference<ForgotPasswordSubmitInterface> view;
    private ForgotPasswordSubmitModel model;

    public ForgotPasswordSubmitPresenter(ForgotPasswordSubmitInterface view) {
        this.view = new WeakReference<ForgotPasswordSubmitInterface>(view);
        this.model = new ForgotPasswordSubmitModel();
    }

    @Override
    public void onForgotPasswordCofirmSubmited(Context context,String email, String forgot_password_code) {
        model.OnSubmitForgotPassword(context, email, forgot_password_code, this);
    }

    @Override
    public void onForgotPasswordConfirmRequestSuccess(ForgotPasswordConfimPojo forgotPasswordConfimPojo) {
        if (view.get() != null) view.get().onSubmitForgotPasswordCodeSuccess(forgotPasswordConfimPojo);
    }

    @Override
    public void onForgotPasswordConfirmRequestFail(String message) {

    }
}
