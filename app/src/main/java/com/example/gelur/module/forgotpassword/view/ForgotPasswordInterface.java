package com.example.gelur.module.forgotpassword.view;

public interface ForgotPasswordInterface {
    void onForgotPasswordResult(String message);
}
