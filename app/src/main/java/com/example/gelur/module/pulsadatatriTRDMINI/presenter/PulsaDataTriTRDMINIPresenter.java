package com.example.gelur.module.pulsadatatriTRDMINI.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDDP.model.PulsaDataTriTRDDPModel;
import com.example.gelur.module.pulsadatatriTRDDP.view.PulsaDataTriTRDDPInterface;
import com.example.gelur.module.pulsadatatriTRDMINI.model.PulsaDataTriTRDMINIModel;
import com.example.gelur.module.pulsadatatriTRDMINI.presenter.listener.OnLoadPulsaDataTriTRDMINI;
import com.example.gelur.module.pulsadatatriTRDMINI.view.PulsaDataTriTRDMINIInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDMINIPresenter implements PulsaDataTriTRDMINIPresenterInterface, OnLoadPulsaDataTriTRDMINI {

    private WeakReference<PulsaDataTriTRDMINIInterface> view;
    private PulsaDataTriTRDMINIModel model;

    public PulsaDataTriTRDMINIPresenter(PulsaDataTriTRDMINIInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDMINIInterface>(view);
        this.model = new PulsaDataTriTRDMINIModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDMINI(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDMINI(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDMINI(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDMINI(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDMINISukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDMINISukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDMINIGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDMINIGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDMINI(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDMINISukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDMINISukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDMINIGagal(String m) {

    }
}
