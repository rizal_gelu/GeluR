package com.example.gelur.module.forgotpasswordsubmit.rest;

import com.example.gelur.util.AppsConstant;

public class ForgotPasswordSubmitAPIURLConstant {
    public static final String BASE_API_URL = AppsConstant.BASE_API_URL + "v1/users/";
    public static final String GET_ACCOUNT = BASE_API_URL + "forgot_password_confirm/";
}
