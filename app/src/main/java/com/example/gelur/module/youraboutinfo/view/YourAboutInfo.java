package com.example.gelur.module.youraboutinfo.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.college.view.College;
import com.example.gelur.module.contactinfo.view.ContactInfo;
import com.example.gelur.module.highschool.view.HighSchool;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.workplace.view.Workplace;
import com.example.gelur.module.youraboutinfo.presenter.YourAboutInfoPresenter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class YourAboutInfo extends AppCompatActivity implements YourAboutInfoInterface {

    @BindView(R.id.activity_your_about_info_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_your_about_info_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_youraboutinfo_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.college_edu)
    TextView etCollegeEdu;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    YourAboutInfoPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_youraboutinfo);

        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(YourAboutInfo.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);
//        setSupportActionBar(toolbar);
        presenter = new YourAboutInfoPresenter(this);
        showContent(true);
    }

    public void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToProfile() {
        startActivity(new Intent(YourAboutInfo.this, Account.class));
    }

    private void goToNotification() {
//        startActivity(new Intent(YourAboutInfo.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(YourAboutInfo.this, Search.class));
    }

    private void goToHome() {
        startActivity(new Intent(YourAboutInfo.this, Timeline.class));
    }

    @OnClick(R.id.work_experience)
    public void workplaceOnClick(View v) {
        presenter.addWorkplace();
    }

    @Override
    public void goToAddWorkplace() {
        startActivity(new Intent(YourAboutInfo.this, Workplace.class));
    }

    @OnClick(R.id.college_edu)
    public void addCollegeClicked(View v) {
        presenter.addCollege();
    }

    @Override
    public void goToAddCollege() {
        startActivity(new Intent(YourAboutInfo.this, College.class));
    }

    @OnClick(R.id.high_school)
    public void addHighSchoolClicked(View v) {
        presenter.addHighSchool();
    }

    @Override
    public void goToAddHighSchool() {
        startActivity(new Intent(YourAboutInfo.this, HighSchool.class));
    }

    @Override
    public void goToAddCity() {

    }

    @Override
    public void goToEditBasicInfo() {

    }

    @OnClick(R.id.edit_contact_info)
    public void editContactInfo() {
        presenter.editContactInfo();
    }

    @Override
    public void goToEditContactInfo() {
        startActivity(new Intent(YourAboutInfo.this, ContactInfo.class));
    }

    @Override
    public void onLoadWorkplaceSuccess() {

    }

    @Override
    public void onLoadWorkplaceFailed() {

    }

    @Override
    public void onLoadCollegeSuccess() {

    }

    @Override
    public void onLoadCollegeFailed() {

    }
}
