package com.example.gelur.module.pulsadatatelkomselTDM.presenter;

import android.content.Context;

public interface PulsaDataTelkomselTDMPresenterInterface {
    void onRequesttransferPulsaDataTelkomselTDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTelkomselTDM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
