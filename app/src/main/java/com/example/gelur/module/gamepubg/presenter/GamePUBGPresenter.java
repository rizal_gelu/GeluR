package com.example.gelur.module.gamepubg.presenter;

import android.content.Context;

import com.example.gelur.module.gameml.model.GameMLModel;
import com.example.gelur.module.gameml.view.GameMLInterface;
import com.example.gelur.module.gamepubg.model.GamePUBGModel;
import com.example.gelur.module.gamepubg.presenter.listener.OnLoadGamePUBG;
import com.example.gelur.module.gamepubg.view.GamePUBGInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class GamePUBGPresenter implements GamePUBGPresenterInterface, OnLoadGamePUBG {

    private WeakReference<GamePUBGInterface> view;
    private GamePUBGModel model;

    public GamePUBGPresenter(GamePUBGInterface view) {
        this.view = new WeakReference<GamePUBGInterface>(view);
        this.model = new GamePUBGModel();
    }

    @Override
    public void onRequesttransferDiamondPUBG(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDiamondML(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceGamePUBG(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {

    }

    @Override
    public void RequestTransferDiamondPUBGSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferDiamondPUBGSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferDiamondPUBGGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferDiamondPUBGGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickGamePUBG(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceGamePUBGSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceGamePUBG(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceGamePUBGGagal(String m) {

    }
}
