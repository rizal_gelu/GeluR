package com.example.gelur.module.plnpascabayar.presenter;

import android.content.Context;

public interface PlnPascaBayarPresenterInterface {
    void onRequestCheckPLNPascaBayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
}
