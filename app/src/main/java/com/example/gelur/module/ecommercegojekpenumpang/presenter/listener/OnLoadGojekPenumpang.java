package com.example.gelur.module.ecommercegojekpenumpang.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadGojekPenumpang {
    void RequestTransferSaldoGojekPenumpangSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferSaldoGojekPenumpangGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickGojekPenumpang(String data);
    void ResponseListPriceGojekDriverSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPriceGojekDriverGagal(String m);
}
