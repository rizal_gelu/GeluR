package com.example.gelur.module.saldo.presenter;

import android.content.Context;

import com.example.gelur.module.saldo.model.SaldoModel;
import com.example.gelur.module.saldo.presenter.listener.OnCekSaldoFinished;
import com.example.gelur.module.saldo.view.SaldoInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;

import java.lang.ref.WeakReference;

public class SaldoPresenter implements SaldoPresenterInterface, OnCekSaldoFinished {

    private WeakReference<SaldoInterface> view;
    private SaldoModel model;

    public SaldoPresenter(SaldoInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new SaldoModel();
    }

    public void getInformasiSaldo(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.InformationSaldo(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void TopUp() {

    }

    @Override
    public void TransactionHistory() {

    }

    @Override
    public void TransferSaldo() {

    }

    @Override
    public void SaldoSuksesCek(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().SaldoSukses(responsNonTransaksi);
    }

    @Override
    public void SaldoGagalCek(String message) {

    }
}
