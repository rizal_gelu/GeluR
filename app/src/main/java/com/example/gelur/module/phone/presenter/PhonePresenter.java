package com.example.gelur.module.phone.presenter;

import android.content.Context;

import com.example.gelur.module.phone.model.PhoneModel;
import com.example.gelur.module.phone.presenter.listener.OnSavePhoneFinishedListener;
import com.example.gelur.module.phone.view.PhoneInterface;
import com.example.gelur.pojo.PhonePojo;

import java.lang.ref.WeakReference;

public class PhonePresenter implements PhonePresenterInterface, OnSavePhoneFinishedListener {
    private WeakReference<PhoneInterface> view;
    private PhoneModel model;

    public PhonePresenter(PhoneInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new PhoneModel();
    }

    @Override
    public void onSaveClicked(Context context, String email, String phone) {
        model.clickSavePhone(context, email, phone, this);
    }

    @Override
    public void onCancelClicked(String message) {

    }

    @Override
    public void onSavePhoneSuccess(PhonePojo phonePojo) {

    }

    @Override
    public void onSavePhoneFailed(String message) {

    }
}
