package com.example.gelur.module.activationaccount.rest;

import com.example.gelur.module.login.rest.LoginAPIURLConstant;
import com.example.gelur.pojo.ActivateAccountPojo;
import com.example.gelur.pojo.UsersPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ActivationAccountRestInterface {
    @FormUrlEncoded
    @POST(ActivationAccountAPIURLConstant.ACTIVATE_ACCOUNT)
    Call<ActivateAccountPojo> doActive(@Field("email") String email, @Field("activation_code") String activate_code);
}
