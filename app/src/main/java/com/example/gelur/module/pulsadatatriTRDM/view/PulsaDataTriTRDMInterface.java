package com.example.gelur.module.pulsadatatriTRDM.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDMInterface {

    void TransferPulsaDataTriTRDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDMSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDMGagal(ResponsNonTransaksi responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
