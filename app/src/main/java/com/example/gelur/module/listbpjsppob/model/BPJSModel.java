package com.example.gelur.module.listbpjsppob.model;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.rest.EcommerceOvoRestClient;
import com.example.gelur.module.listbpjsppob.presenter.listener.OnLoadBPJS;
import com.example.gelur.module.listbpjsppob.rest.BPJSRestClient;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BPJSModel implements BPJSModelInterface{
    @Override
    public void RequestCheckBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadBPJS listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = BPJSRestClient.get().req_check_bpjs(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.onLoadCheckBPJSSukses(response.body());
                } else {
                    listener.onLoadCheckBPJSGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestBayarBPJS(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadBPJS listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = BPJSRestClient.get().req_bayar_bpjs(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.onLoadBayarBPJSSukses(response.body());
                } else {
                    listener.onLoadBayarBPJSGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }
}
