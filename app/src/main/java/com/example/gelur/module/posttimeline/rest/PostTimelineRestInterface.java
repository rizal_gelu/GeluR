package com.example.gelur.module.posttimeline.rest;

import com.example.gelur.pojo.PostTimelinePojo;

import retrofit2.Call;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface PostTimelineRestInterface {
    @FormUrlEncoded
    @POST(PostTimelineAPIURLConstant.POST_TIMELINE_URL)
//    Call<PostTimelinePojo> doPostFeed(PostTimelinePresenter postTimelinePresenter, @Field("email") String email, @Field("description") String description, @Field("file") String file);

    Call<PostTimelinePojo> doPostFeed(String email, String description, String file);
}
