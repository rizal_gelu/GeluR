package com.example.gelur.module.pulsadatatriTRDM.presenter;


import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDDP.model.PulsaDataTriTRDDPModel;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.PulsaDataTriTRDDPPresenterInterface;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDDP.view.PulsaDataTriTRDDPInterface;
import com.example.gelur.module.pulsadatatriTRDM.model.PulsaDataTriTRDMModel;
import com.example.gelur.module.pulsadatatriTRDM.presenter.listener.OnLoadPulsaDataTriTRDM;
import com.example.gelur.module.pulsadatatriTRDM.view.PulsaDataTriTRDMInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataTriTRDMPresenter implements PulsaDataTriTRDMPresenterInterface, OnLoadPulsaDataTriTRDM {

    private WeakReference<PulsaDataTriTRDMInterface> view;
    private PulsaDataTriTRDMModel model;

    public PulsaDataTriTRDMPresenter(PulsaDataTriTRDMInterface view) {
        this.view = new WeakReference<PulsaDataTriTRDMInterface>(view);
        this.model = new PulsaDataTriTRDMModel();
    }

    @Override
    public void onRequesttransferpulsaDataTriTRDM(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataTriTRDM(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataTriTRDM(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataTriTRDM(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDMSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataTriTRDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataTriTRDMGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataTriTRDM(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDMSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaTriTRDMSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataTriTRDMGagal(String m) {

    }
}
