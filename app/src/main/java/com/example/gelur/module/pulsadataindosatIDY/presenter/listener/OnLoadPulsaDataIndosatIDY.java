package com.example.gelur.module.pulsadataindosatIDY.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataIndosatIDY {
    void RequestTransferPulsaDataIndosatIDYSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataIndosatIDYGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataIndosatIDY(String data);
    void ResponseListPricePulsaDataIndosatIDYSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataIndosatIDYGagal(String m);
}
