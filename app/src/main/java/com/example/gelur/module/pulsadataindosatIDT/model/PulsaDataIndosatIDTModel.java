package com.example.gelur.module.pulsadataindosatIDT.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDM.rest.PulsaDataIndosatIDMRestClient;
import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;
import com.example.gelur.module.pulsadataindosatIDT.presenter.listener.OnLoadPulsaDataIndosatIDT;
import com.example.gelur.module.pulsadataindosatIDT.rest.PulsaDataIndosatIDTRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataIndosatIDTModel implements PulsaDataIndosatIDTModelInterface {
    @Override
    public void RequestTransferDataIndosatIDT(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDT listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataIndosatIDTRestClient.get().req_transfer_pulsa_data_indosat_idt(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataIndosatIDTSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataIndosatIDTGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataIndosatIDT(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDT listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataIndosatIDTRestClient.get().getPriceListPulsaDataIndosatIDT(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataIndosatIDTSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataIndosatIDTGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataIndosatIDTGagal(t.getMessage());
            }
        });
    }
}
