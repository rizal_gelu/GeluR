package com.example.gelur.module.highschool.model;

import android.content.Context;

import com.example.gelur.module.college.presenter.listener.OnSaveCollegeFinishedListener;
import com.example.gelur.module.highschool.presenter.listener.OnSaveHighSchoolFinishedListener;
import com.example.gelur.module.highschool.rest.HighSchoolRestClient;
import com.example.gelur.pojo.HighSchoolPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HighSchoolModel implements HighSchoolModelInterface {
    @Override
    public void clickSaveHighSchool(Context context, String email, String high_school_name, String year_graduate, String graduated, String description, OnSaveHighSchoolFinishedListener listener) {
        Call<HighSchoolPojo> highSchool = HighSchoolRestClient.get().postHighSchool(email, high_school_name, year_graduate, graduated, description);
        highSchool.enqueue(new Callback<HighSchoolPojo>() {
            @Override
            public void onResponse(Call<HighSchoolPojo> call, Response<HighSchoolPojo> response) {
                if (response.isSuccessful()) {
                    listener.onSaveHighSchoolSuccess(response.body());
                } else {
                    listener.onSaveHighSchoolFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<HighSchoolPojo> call, Throwable t) {
                listener.onSaveHighSchoolFailed(t.getMessage());
            }
        });
    }
}
