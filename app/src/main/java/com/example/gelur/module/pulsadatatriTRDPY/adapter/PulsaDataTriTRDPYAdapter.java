package com.example.gelur.module.pulsadatatriTRDPY.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDPY.presenter.listener.OnLoadPulsaDataTriTRDPY;

import java.util.List;

public class PulsaDataTriTRDPYAdapter extends RecyclerView.Adapter<PulsaDataTriTRDPYAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataTriTRDPY listener;
    List<String> stringList;

    public PulsaDataTriTRDPYAdapter(Activity activity, List<String> datum, OnLoadPulsaDataTriTRDPY listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_tri_trdpy, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_tri_trdpy.setText(data);

        holder.ll_kumpulan_option_pulsa_data_tri_trdpy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataTriTRDPY(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_tri_trdpy;
        public final TextView pulsa_data_tri_trdpy;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_tri_trdpy = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_tri_trdpy);
            pulsa_data_tri_trdpy = (TextView) view.findViewById(R.id.pulsa_data_tri_trdpy);
        }
    }
}
