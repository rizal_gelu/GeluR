package com.example.gelur.module.highschool.presenter;

import android.content.Context;

import com.example.gelur.module.highschool.model.HighSchoolModel;
import com.example.gelur.module.highschool.presenter.listener.OnSaveHighSchoolFinishedListener;
import com.example.gelur.module.highschool.view.HighSchool;
import com.example.gelur.module.highschool.view.HighSchoolInterface;
import com.example.gelur.pojo.HighSchoolPojo;

import java.lang.ref.WeakReference;

public class HighSchoolPresenter implements HighSchoolPresenterInterface, OnSaveHighSchoolFinishedListener {

    private WeakReference<HighSchoolInterface> view;
    private HighSchoolModel model;

    public HighSchoolPresenter(HighSchoolInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new HighSchoolModel();
    }

    @Override
    public void onSaveHighSchoolClicked(Context context, String email, String high_school_name, String year_graduate, String graduated, String description) {
        model.clickSaveHighSchool(context, email, high_school_name, year_graduate, graduated, description,  this);
    }

    @Override
    public void onCancelHighSchoolClicked() {

    }

    @Override
    public void onSaveHighSchoolSuccess(HighSchoolPojo highSchoolPojo) {

    }

    @Override
    public void onSaveHighSchoolFailed(String message) {

    }
}
