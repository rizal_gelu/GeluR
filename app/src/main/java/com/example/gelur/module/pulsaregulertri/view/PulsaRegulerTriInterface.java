package com.example.gelur.module.pulsaregulertri.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaRegulerTriInterface {
    void TransferPulsaRegulerTriSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaRegulerTriGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaRegulerTri(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
