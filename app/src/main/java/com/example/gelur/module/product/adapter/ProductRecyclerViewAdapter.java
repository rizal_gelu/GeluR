package com.example.gelur.module.product.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.product.presenter.listener.OnLoadProductFinished;
import com.example.gelur.pojo.ProductPojo;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProductRecyclerViewAdapter extends RecyclerView.Adapter<ProductRecyclerViewAdapter.ViewHolderContent> {

    private List<ProductPojo> productList;
    private OnLoadProductFinished onLoadProductFinished;
    private Activity activity;

    public ProductRecyclerViewAdapter(Activity activity, List<ProductPojo> productList, OnLoadProductFinished onLoadProductFinished) {
        this.productList = productList;
        this.activity = activity;
        this.onLoadProductFinished = onLoadProductFinished;
    }

    @NonNull
    @Override
    public ProductRecyclerViewAdapter.ViewHolderContent onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product, parent, false);
        return new ProductRecyclerViewAdapter.ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductRecyclerViewAdapter.ViewHolderContent holder, int position) {

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {

        public final View mView;
        public final TextView tv_title_product;
        public final TextView tv_description_product;
        public ProductPojo productListPojo;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            tv_title_product = (TextView) view.findViewById(R.id.tv_title_product);
            tv_description_product = (TextView) view.findViewById(R.id.tv_description_product);
        }
    }
}
