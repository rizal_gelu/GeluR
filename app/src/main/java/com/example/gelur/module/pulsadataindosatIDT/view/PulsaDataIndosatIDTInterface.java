package com.example.gelur.module.pulsadataindosatIDT.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataIndosatIDTInterface {
    void TransferPulsaDataIndosatIDTSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataIndosatIDTGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataIndosatIDTSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataIndosatIDTGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
