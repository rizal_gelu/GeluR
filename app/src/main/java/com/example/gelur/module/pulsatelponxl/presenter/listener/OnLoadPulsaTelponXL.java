package com.example.gelur.module.pulsatelponxl.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaTelponXL {
    void RequestTransferPulsaTelponXLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaTelponXLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaTelponXL(String data);
    void ResponseListPricePulsaTelponXLSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaTelponXLGagal(String m);
}
