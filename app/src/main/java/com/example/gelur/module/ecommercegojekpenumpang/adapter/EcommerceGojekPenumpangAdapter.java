package com.example.gelur.module.ecommercegojekpenumpang.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.ecommercegojekpenumpang.presenter.listener.OnLoadGojekPenumpang;

import java.util.List;

public class EcommerceGojekPenumpangAdapter extends RecyclerView.Adapter<EcommerceGojekPenumpangAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadGojekPenumpang listener;
    List<String> stringList;

    public EcommerceGojekPenumpangAdapter(Activity activity, List<String> datum, OnLoadGojekPenumpang listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ecommerce_gojek_penumpang, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.gojek_penumpang.setText(data);

        holder.ll_kumpulan_option_gojek_penumpang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickGojekPenumpang(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_gojek_penumpang;
        public final TextView gojek_penumpang;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_gojek_penumpang = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_gojek_penumpang);
            gojek_penumpang = (TextView) view.findViewById(R.id.gojek_penumpang);
        }
    }
}
