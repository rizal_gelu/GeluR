package com.example.gelur.module.pulsadatatelkomselTDBN.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTelkomselTDBN {
    void RequestTransferPulsaDataTelkomselTDBNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTelkomselTDBNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTelkomselTDBN(String data);
    void ResponseListPricePulsaDataTelkomselTDBNSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTelkomselTDBNGagal(String m);
}
