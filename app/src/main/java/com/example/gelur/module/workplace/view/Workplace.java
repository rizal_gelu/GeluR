package com.example.gelur.module.workplace.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.workplace.presenter.WorkplacePresenter;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class Workplace extends AppCompatActivity implements WorkplaceInterface {

    @BindView(R.id.activity_workplace_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_workplace_toolbar)
    Toolbar toolbar;

    @BindView(R.id.content_workplace)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.etWorkPlace)
    EditText etWorkplace;

    @BindView(R.id.save_workplace_btn_click)
    TextView tvSaveWorkplace;

    @BindView(R.id.job_title) EditText etJobTitle;

    @BindView(R.id.location_workplace) EditText etLocationWorkPlace;

    BottomNavigationView bottomNavigationView;

    FloatingActionButton floatingActionButton;

    WorkplacePresenter presenter;

    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_workplace);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Workplace.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new WorkplacePresenter(this);

        showContent(true);
    }

    private void goToNotification() {
//        startActivity(new Intent(Workplace.this, Notif.class));
    }

    private void goToSearch() {
        startActivity(new Intent(Workplace.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(Workplace.this, Account.class));
    }

    private void goToHome() {
        startActivity(new Intent(Workplace.this, Timeline.class));
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.save_workplace_btn_click)
    public void onSavePostWorkPlaceClicked() {
//        loadingDialog.show();

        String email = "Rizalgelui@yahoo.co.id";
        String name_company = etWorkplace.getText().toString();
        String job_title = etJobTitle.getText().toString();
        String location = etLocationWorkPlace.getText().toString();

        presenter.onSaveWorkplaceClicked(this, email, name_company, job_title, location);
    }

    @Override
    public void onSuccesPostWorkplace() {

    }

    @Override
    public void onFailedPostWorkplace() {

    }
}
