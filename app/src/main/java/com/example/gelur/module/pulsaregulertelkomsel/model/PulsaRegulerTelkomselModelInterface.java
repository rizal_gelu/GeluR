package com.example.gelur.module.pulsaregulertelkomsel.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDB.presenter.listener.OnLoadPulsaDataTelkomselTDB;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.listener.OnLoadPulsaRegulerTelkomsel;
import com.example.gelur.module.topup.presenter.listener.OnRequestTopfinished;

public interface PulsaRegulerTelkomselModelInterface {
    void RequestTransferPulsa(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaRegulerTelkomsel listener);
    void RequestListPricePulsaRegulerTelkomsel(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaRegulerTelkomsel listener);
}
