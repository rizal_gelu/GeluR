package com.example.gelur.module.bio.view;

public interface BioInterface {
    void onSaveBioSuccess();
    void onSaveBioFail(String message);
}
