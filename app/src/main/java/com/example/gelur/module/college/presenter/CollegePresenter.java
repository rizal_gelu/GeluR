package com.example.gelur.module.college.presenter;

import android.content.Context;

import com.example.gelur.module.college.model.CollegeModel;
import com.example.gelur.module.college.presenter.listener.OnLoadCollegeFinishedListener;
import com.example.gelur.module.college.presenter.listener.OnSaveCollegeFinishedListener;
import com.example.gelur.module.college.view.CollegeInterface;
import com.example.gelur.pojo.CollegePojo;

import java.lang.ref.WeakReference;

public class CollegePresenter implements CollegePresenterInterface, OnSaveCollegeFinishedListener, OnLoadCollegeFinishedListener {

    private WeakReference<CollegeInterface> view;
    private CollegeModel model;

    public CollegePresenter(CollegeInterface view) {
        this.view = new WeakReference<>(view);
        this.model = new CollegeModel();
    }

    @Override
    public void onSaveClicked(Context context, String email, String college_name, String faculty_major, int graduated, String year_graduate, String description) {
        model.clickSaveCollege(context, email, college_name, faculty_major, graduated, description, year_graduate, this);
    }

    @Override
    public void onCancelClicked() {

    }

    @Override
    public void onSaveCollegeSuccess(CollegePojo collegePojo) {

    }

    @Override
    public void onSaveCollegeFailed(String message) {

    }

    @Override
    public void onLoadCollegeSucces() {

    }

    @Override
    public void onLoadCollegeFailed() {

    }
}
