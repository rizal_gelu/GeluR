package com.example.gelur.module.akunpulsa.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.presenter.listener.OnLoadAkunPulsa;
import com.example.gelur.module.akunpulsa.rest.AkunPulsaRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AkunPulsaModel implements AkunPulsaModelInterface{
    @Override
    public void getInformationSaldo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadAkunPulsa listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = AkunPulsaRestClient.get().getSaldoAkun(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.cekSaldoSukses(response.body());
                } else {
                    listener.cekSaldoGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.cekSaldoGagal(t.getMessage());
            }
        });
    }
}
