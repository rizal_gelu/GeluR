package com.example.gelur.module.pulsadataSMDEVO.presenter;

import android.content.Context;

public interface PulsaDataSmartfrenSMDEVOPresenterInterface {
    void onRequesttransferpulsadataSmartfrenSMDEVO(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataSmartfrenSMDEVO(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
