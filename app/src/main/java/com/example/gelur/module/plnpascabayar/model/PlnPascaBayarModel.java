package com.example.gelur.module.plnpascabayar.model;

import android.content.Context;

import com.example.gelur.module.listbpjsppob.rest.BPJSRestClient;
import com.example.gelur.module.plnpascabayar.presenter.listener.OnLoadPlnPascaBayar;
import com.example.gelur.module.plnpascabayar.rest.PlnPascaBayarRestClient;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlnPascaBayarModel implements PlnPascaBayarModelInterface{
    @Override
    public void RequestCheckPLNPascaBayar(Context context, String req, String kodereseller, String produk, String msisdn, String counter,String qty, String reffid, String time, String pin, String password, OnLoadPlnPascaBayar listener) {
        retrofit2.Call<ResponsTransaksiPulsa> tiket = PlnPascaBayarRestClient.get().req_check_pln_pasca_bayar(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equalsIgnoreCase("sukses")) {
                        listener.onLoadCheckPLNPascaBayarSukses(response.body());
                    } else {
                        listener.onLoadCheckPLNPascaBayarGagal(response.body());
                    }
                } else {
                    listener.onLoadCheckPLNPascaBayarGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }
}
