package com.example.gelur.module.transferpulsa.presenter;

public interface TransferPulsaPresenterInterface {
    void onClickPPOB();
    void onClickPulsa();
    void onClickPulsaData();
    void onClickGame();
    void onClickEcommerce();
    void onClickPln();
}
