package com.example.gelur.module.plnpascabayar.rest;

import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.util.AppsConstant;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface PlnPascaBayarRestInterface {
    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsTransaksiPulsa> req_check_pln_pasca_bayar(@Body RequestTransaksiPulsa requestTransaksiPulsa);

    @POST(AppsConstant.BASE_API_NON_TRANSAKSI +"api")
    Call<ResponsTransaksiPulsa> req_bayar_pln_pasca_bayar(@Body RequestTransaksiPulsa requestTransaksiPulsa);
}
