package com.example.gelur.module.topup.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.topup.presenter.listener.OnRequestTopfinished;
import com.example.gelur.module.topup.rest.TopUpRestClient;
import com.example.gelur.module.transferpulsa.rest.TransferPulsaRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopUpModel implements TopUpModelInterface{
    @Override
    public void RequestTopUp(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnRequestTopfinished listener) {
        retrofit2.Call<ResponsNonTransaksi> tiket = TopUpRestClient.get().req_tiket(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        tiket.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.RequestTopUpSukses(response.body());
                } else {
                    listener.RequestTopUpGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.RequestTopUpGagal(t.getMessage());
            }
        });
    }
}
