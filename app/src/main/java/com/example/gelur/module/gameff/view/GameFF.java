package com.example.gelur.module.gameff.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.ecommerceshopee.adapter.EcommerceShopeeAdapter;
import com.example.gelur.module.ecommerceshopee.presenter.EcommerceShopeePresenter;
import com.example.gelur.module.ecommerceshopee.view.EcommerceShopee;
import com.example.gelur.module.gameff.adapter.GameFFAdapter;
import com.example.gelur.module.gameff.presenter.GameFFPresenter;
import com.example.gelur.module.gameml.presenter.GameMLPresenter;
import com.example.gelur.module.gameml.view.GameML;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.laporantransaksi.view.LaporanTransaksi;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;
import com.example.gelur.R;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GameFF extends AppCompatActivity implements GameFFInterface {

    @BindView(R.id.coordinator_list_game_ff)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_game_ff)
    Toolbar toolbar;

    @BindView(R.id.content_game_ff)
    View viewContent;

    @BindView(R.id.layout_loading_game_ff) View viewLoading;

    @BindView(R.id.rv_game_ff)
    RecyclerView recyclerView;

    AppCompatButton submit_data;

    AlertDialog loadingDialog;

    AlertDialog.Builder dialogBuilder;

    EditText Et_id_game_ff;
    EditText qty_game_ff;

    BottomNavigationView bottomNavigationView;
    GameFFPresenter presenter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game_ff);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Diamond Game FF");

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        //hardcode
        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String perintah = "ch.dFF";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        presenter = new GameFFPresenter(this);
        presenter.onRequetListPriceGameFF(this, req, kodereseller, perintah, time, pin, password);

        presenter = new GameFFPresenter(this);
        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(GameFF.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(GameFF.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(GameFF.this, AkunPulsa.class));
    }

    @Override
    public void TransferDiamondFFSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(GameFF.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void TransferDiamondFFGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(GameFF.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void LoadListPriceGameFF(ResponsNonTransaksi responsNonTransaksi) {
        String get_msg = responsNonTransaksi.getMsg();
        String[] stringList = get_msg.split("\\r\\n");

        String stringList16 = stringList[11];
        String[] cut_stringList16 = stringList16.split("\\s+");
        String gabungStringList16 = cut_stringList16[0]+" "+cut_stringList16[1]+" "+cut_stringList16[2]+" "+cut_stringList16[3]+" "+cut_stringList16[4];


        ArrayList<String> ListPulsa = new ArrayList<>();
        ListPulsa.add(stringList[1]);
        ListPulsa.add(stringList[2]);
        ListPulsa.add(stringList[3]);
        ListPulsa.add(stringList[4]);
        ListPulsa.add(stringList[5]);
        ListPulsa.add(stringList[6]);
        ListPulsa.add(stringList[7]);
        ListPulsa.add(stringList[8]);
        ListPulsa.add(stringList[9]);
        ListPulsa.add(stringList[10]);
        ListPulsa.add(gabungStringList16);

        GameFFAdapter adapter = new GameFFAdapter(this, ListPulsa, presenter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        showContent(true);
    }

    public void createNewPopUpDialog(String nominal) {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popUpDialog = getLayoutInflater().inflate(R.layout.popup_game_ff, null);
        Et_id_game_ff = (EditText) popUpDialog.findViewById(R.id.Et_id_game_ff);
        submit_data = (AppCompatButton) popUpDialog.findViewById(R.id.submit_request_transfer_diamond_ff);
        qty_game_ff = (EditText) popUpDialog.findViewById(R.id.qty_game_ff);

        dialogBuilder.setView(popUpDialog);
        loadingDialog = dialogBuilder.create();
        loadingDialog.show();

        submit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int min = 500;
                int max = 10000;

                //Generate random int value from 50 to 100
                System.out.println("Random value in int from "+min+" to "+max+ ":");
                int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

                Format f = new SimpleDateFormat("hh:mm:ss a");
                String strResult = f.format(new Date());

                String split_time = String.valueOf(strResult);
                String[] reslt_split = split_time.split(":");

                String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
                String[] split_lagi = gabung_split.split("\\s+");
                String param_split = split_lagi[0];

                AppsUtil sessionUser = new AppsUtil(GameFF.this);
                HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

                String req = "topup";
                String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
                String qty = qty_game_ff.getText().toString();
                String refid = String.valueOf(random_int);
                String get_no_hp = Et_id_game_ff.getText().toString();
                String time = param_split;
                String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
                String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
                String counter = qty_game_ff.getText().toString();

                presenter.onRequesttransferDiamondFF(GameFF.this, req, kode_reseller, nominal, get_no_hp, counter, qty, refid, time, pin, password);
            }
        });
    }
}
