package com.example.gelur.module.timeline.model;

import android.content.Context;

import com.example.gelur.module.timeline.presenter.listener.OnLoadTimelineFinished;
import com.example.gelur.module.timeline.rest.TimelineRestClient;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.LikePojo;
import com.example.gelur.pojo.TimelinePojo;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimelineModel implements TimelineModelInterface {
    @Override
    public void loadTimelineData(final OnLoadTimelineFinished listener) {

        Call<FormaterResponse> timelineResponse = TimelineRestClient.get().getTimelines();

        timelineResponse.enqueue(new Callback<FormaterResponse>() {
            @Override
            public void onResponse(Call<FormaterResponse> call, Response<FormaterResponse> response) {
                FormaterResponse resource = response.body();
                List<TimelinePojo> timelineList = resource.getTimeline();
                listener.onLoadTimelineDataSuccess(timelineList);
            }

            @Override
            public void onFailure(Call<FormaterResponse> call, Throwable t) {
                listener.onLoadTimelineDataFailed("Error Response");
            }
        });

    }

    public void postLikeTimeline(String id_timeline, String email, OnLoadTimelineFinished listener ) {
        Call<LikePojo> like = TimelineRestClient.get().postLike(id_timeline, email);
        like.enqueue(new Callback<LikePojo>() {
            @Override
            public void onResponse(Call<LikePojo> call, Response<LikePojo> response) {
                if (response.isSuccessful()) {
                    listener.onPostLikeTLSuccess(response.body());
                } else {
                    listener.onPostLikeTLFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<LikePojo> call, Throwable t) {
                listener.onPostLikeTLFailed(t.getMessage());
            }
        });
    }

    public void unLikeTimeline(String id_timeline, String email, OnLoadTimelineFinished listener) {
        Call<LikePojo> unlike = TimelineRestClient.get().unlikeTL(id_timeline, email);
        unlike.enqueue(new Callback<LikePojo>() {
            @Override
            public void onResponse(Call<LikePojo> call, Response<LikePojo> response) {
                if (response.isSuccessful()) {
                    listener.onClickUnlikeSuccess(response.body());
                } else {
                    listener.onClickUnlikeFail("unlike fail");
                }
            }

            @Override
            public void onFailure(Call<LikePojo> call, Throwable t) {
                listener.onClickUnlikeFail(t.getMessage());
            }
        });
    }
}
