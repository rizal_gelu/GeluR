package com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTelkomselTDF {
    void RequestTransferPulsaDataTelkomselTDFSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTelkomselTDFGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTelkomselTDF(String data);
    void ResponseListPricePulsaDataTelkomselTDFSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTelkomselTDFGagal(String m);
}
