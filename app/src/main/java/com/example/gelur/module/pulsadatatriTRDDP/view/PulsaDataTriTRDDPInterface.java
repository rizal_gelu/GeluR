package com.example.gelur.module.pulsadatatriTRDDP.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDDPInterface {

    void TransferPulsaDataTriTRDDPSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDDPGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDDPSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDDPGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
