package com.example.gelur.module.ecommercedana.presenter;

import android.content.Context;

public interface EcommerceDanaPresenterInterface {
    void onRequesttransfersaldoDana(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListListDana(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
