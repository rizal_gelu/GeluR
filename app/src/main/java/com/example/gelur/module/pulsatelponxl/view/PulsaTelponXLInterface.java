package com.example.gelur.module.pulsatelponxl.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaTelponXLInterface {
    void TransferPulsaTelponXLSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaTelponXLGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaTelponXL(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
