package com.example.gelur.module.daftardownline.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface OnLoadDaftarDownline {
    void DaftarMemberSukses(ResponsNonTransaksi responsNonTransaksi);
    void DaftarMemberGagal(String message);
}
