package com.example.gelur.module.college.presenter.listener;

public interface OnLoadCollegeFinishedListener {
    void onLoadCollegeSucces();
    void onLoadCollegeFailed();
}
