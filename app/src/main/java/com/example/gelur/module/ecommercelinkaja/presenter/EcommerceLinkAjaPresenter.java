package com.example.gelur.module.ecommercelinkaja.presenter;

import android.content.Context;

import com.example.gelur.module.ecommercelinkaja.model.EcommerceLinkAjaModel;
import com.example.gelur.module.ecommercelinkaja.presenter.listener.OnLoadEcommerceLinkAja;
import com.example.gelur.module.ecommercelinkaja.view.EcommerceLinkAja;
import com.example.gelur.module.ecommercelinkaja.view.EcommerceLinkAjaInterface;
import com.example.gelur.module.ecommerceovo.model.EcommerceOvoModel;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class EcommerceLinkAjaPresenter implements EcommerceLinkAjaPresenterInterface, OnLoadEcommerceLinkAja {

    private WeakReference<EcommerceLinkAjaInterface> view;
    private EcommerceLinkAjaModel model;

    public EcommerceLinkAjaPresenter(EcommerceLinkAjaInterface view) {
        this.view = new WeakReference<EcommerceLinkAjaInterface>(view);
        this.model = new EcommerceLinkAjaModel();
    }

    @Override
    public void onRequesttransfersaldoLinkAja(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferSaldoLinkAja(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceLinkAja(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceLinkAja(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferSaldoLinkAjaSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoLinkAjaSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferSaldoLinkAjaGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoLinkAjaGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickLinkAja(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceLinkAjaSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceLinkAja(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceLinkAjaGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
