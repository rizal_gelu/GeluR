package com.example.gelur.module.akunpulsa.view;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface AkunPulsaInterface {
    void getSaldoOnAkunPulsaSukses(ResponsNonTransaksi responsNonTransaksi);

    void onClickChangePassword();
    void onClickChangePin();
    void onClickRegisterDownline();
}
