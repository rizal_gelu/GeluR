package com.example.gelur.module.taskboardadd.rest;

import com.example.gelur.module.login.rest.LoginAPIURLConstant;
import com.example.gelur.module.login.rest.LoginRestInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TaskBoardAddRestClient {
    private static TaskBoardAddRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    private TaskBoardAddRestClient() {

    }

    public static TaskBoardAddRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(TaskBoardAddAPIURLConstant.POST_TASK_BOARD_ADD)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(TaskBoardAddRestInterface.class);
    }
}
