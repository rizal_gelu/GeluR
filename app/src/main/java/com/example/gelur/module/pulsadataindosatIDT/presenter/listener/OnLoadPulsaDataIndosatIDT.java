package com.example.gelur.module.pulsadataindosatIDT.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataIndosatIDT {
    void RequestTransferPulsaDataIndosatIDTSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataIndosatIDTGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataIndosatIDT(String data);
    void ResponseListPricePulsaDataIndosatIDTSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataIndosatIDTGagal(String m);
}
