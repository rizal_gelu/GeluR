package com.example.gelur.module.forgotpassword.presenter;

import com.example.gelur.module.forgotpassword.model.ForgotPasswordModel;
import com.example.gelur.module.forgotpassword.presenter.listener.OnForgotPasswordFinishedListener;
import com.example.gelur.module.forgotpassword.view.ForgotPasswordInterface;
import com.example.gelur.pojo.ForgotPasswordConfimPojo;

import java.lang.ref.WeakReference;

public class ForgotPasswordPresenter implements ForgotPasswordPresenterInterface, OnForgotPasswordFinishedListener {

    private WeakReference<ForgotPasswordInterface> view;
    private ForgotPasswordModel model;

    public ForgotPasswordPresenter(ForgotPasswordInterface view) {
        this.view = new WeakReference<ForgotPasswordInterface>(view);
        this.model = new ForgotPasswordModel();
    }

    @Override
    public void submitForgotPassword(String email) {
        model.forgotPasswordRequest(email, this);
    }

    @Override
    public void onForgotPasswordRequestFinish(String message) {
        if (view.get() != null) view.get().onForgotPasswordResult("success");
    }

    @Override
    public void onForgotPasswordRequestFinishFail(String message) {

    }
}