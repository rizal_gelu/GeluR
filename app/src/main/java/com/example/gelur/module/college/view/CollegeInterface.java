package com.example.gelur.module.college.view;

public interface CollegeInterface {
    void onSuccessPostCollege();
    void onFailedPostCollege(String message);
}
