package com.example.gelur.module.pulsadataXLXDCV.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataXLXDC.presenter.listener.OnLoadPulsaDataXLXDC;
import com.example.gelur.module.pulsadataXLXDCV.presenter.listener.OnLoadPulsaDataXLXDCV;

import java.util.List;

public class PulsaDataXLXDCVAdapter extends RecyclerView.Adapter<PulsaDataXLXDCVAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaDataXLXDCV listener;
    List<String> stringList;

    public PulsaDataXLXDCVAdapter(Activity activity, List<String> datum, OnLoadPulsaDataXLXDCV listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_data_xl_xdcv, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_data_xl_xdcv.setText(data);

        holder.ll_kumpulan_option_pulsa_data_xl_xdcv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaDataXLXDCV(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_data_xl_xdcv;
        public final TextView pulsa_data_xl_xdcv;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_data_xl_xdcv = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_data_xl_xdcv);
            pulsa_data_xl_xdcv = (TextView) view.findViewById(R.id.pulsa_data_xl_xdcv);
        }
    }
}
