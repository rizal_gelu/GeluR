package com.example.gelur.module.pulsadatatelkomselTDBCN.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDBCN.presenter.listener.OnLoadPulsaDataTelkomselTDBCN;
import com.example.gelur.module.pulsadatatelkomselTDF.presenter.listener.OnLoadPulsaDataTelkomselTDF;

public interface PulsaDataTelkomselTDBCNModelInterface {
    void RequestTransferPulsaDataTelkomselTDBCN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTelkomselTDBCN listener);
    void RequestListPricePulsaDataTelkomselTDBCN(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTelkomselTDBCN listener);
}
