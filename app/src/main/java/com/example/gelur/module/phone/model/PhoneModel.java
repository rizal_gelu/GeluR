package com.example.gelur.module.phone.model;

import android.content.Context;

import com.example.gelur.module.phone.presenter.listener.OnSavePhoneFinishedListener;
import com.example.gelur.module.phone.rest.PhoneRestClient;
import com.example.gelur.pojo.PhonePojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhoneModel implements PhoneModelInterface {
    @Override
    public void clickSavePhone(Context context, String email, String phone, OnSavePhoneFinishedListener listener) {
        Call<PhonePojo> phone_number = PhoneRestClient.get().postPhone(email, phone);
        phone_number.enqueue(new Callback<PhonePojo>() {
            @Override
            public void onResponse(Call<PhonePojo> call, Response<PhonePojo> response) {
                if (response.isSuccessful()) {
                    listener.onSavePhoneSuccess(response.body());
                } else {
                    listener.onSavePhoneFailed("Post fail");
                }
            }

            @Override
            public void onFailure(Call<PhonePojo> call, Throwable t) {
                listener.onSavePhoneFailed(t.getMessage());
            }
        });
    }
}
