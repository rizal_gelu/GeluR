package com.example.gelur.module.activationaccount.rest;


import com.example.gelur.module.login.rest.LoginAPIURLConstant;
import com.example.gelur.module.login.rest.LoginRestInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ActivationAccountRestClient {
    private static ActivationAccountRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    private ActivationAccountRestClient() {

    }

    public static ActivationAccountRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(logging);

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(ActivationAccountAPIURLConstant.BASE_API_ACTIVATE_ACCOUNT_URL)
                .client(client.build())
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(ActivationAccountRestInterface.class);
    }
}
