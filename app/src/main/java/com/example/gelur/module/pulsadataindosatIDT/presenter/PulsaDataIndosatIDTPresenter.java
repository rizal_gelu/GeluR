package com.example.gelur.module.pulsadataindosatIDT.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDP.model.PulsaDataIndosatIDPModel;
import com.example.gelur.module.pulsadataindosatIDP.view.PulsaDataIndosatIDPInterface;
import com.example.gelur.module.pulsadataindosatIDT.model.PulsaDataIndosatIDTModel;
import com.example.gelur.module.pulsadataindosatIDT.presenter.listener.OnLoadPulsaDataIndosatIDT;
import com.example.gelur.module.pulsadataindosatIDT.view.PulsaDataIndosatIDTInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDTPresenter implements PulsaDataIndosatIDTPresenterInterface, OnLoadPulsaDataIndosatIDT {

    private WeakReference<PulsaDataIndosatIDTInterface> view;
    private PulsaDataIndosatIDTModel model;

    public PulsaDataIndosatIDTPresenter(PulsaDataIndosatIDTInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDTInterface>(view);
        this.model = new PulsaDataIndosatIDTModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDTSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDTSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDTGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDTGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDT(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDTSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDTSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDTGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDT(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDT(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDT(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDT(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
