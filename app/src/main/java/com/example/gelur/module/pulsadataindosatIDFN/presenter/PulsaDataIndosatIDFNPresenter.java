package com.example.gelur.module.pulsadataindosatIDFN.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDFN.model.PulsaDataIndosatIDFNModel;
import com.example.gelur.module.pulsadataindosatIDFN.presenter.listener.OnLoadPulsaDataIndosatIDFN;
import com.example.gelur.module.pulsadataindosatIDFN.view.PulsaDataIndosatIDFNInterface;
import com.example.gelur.module.pulsadataindosatIDY.model.PulsaDataIndosatIDYIDYModel;
import com.example.gelur.module.pulsadataindosatIDY.presenter.PulsaDataIndosatIDYPresenterInterface;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;
import com.example.gelur.module.pulsadataindosatIDY.view.PulsaDataIndosatIDYInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDFNPresenter implements PulsaDataIndosatIDFNPresenterInterface, OnLoadPulsaDataIndosatIDFN {

    private WeakReference<PulsaDataIndosatIDFNInterface> view;
    private PulsaDataIndosatIDFNModel model;

    public PulsaDataIndosatIDFNPresenter(PulsaDataIndosatIDFNInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDFNInterface>(view);
        this.model = new PulsaDataIndosatIDFNModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDFNSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDFNSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDFNGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDFNGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDFN(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDFNSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDFNSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDFNGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDFN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDFN(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDFN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDFN(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
