package com.example.gelur.module.pulsadataindosatIDP.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDM.model.PulsaDataIndosatIDMModel;
import com.example.gelur.module.pulsadataindosatIDM.view.PulsaDataIndosatIDMInterface;
import com.example.gelur.module.pulsadataindosatIDP.model.PulsaDataIndosatIDPModel;
import com.example.gelur.module.pulsadataindosatIDP.presenter.listener.OnLoadPulsaDataIndosatIDP;
import com.example.gelur.module.pulsadataindosatIDP.view.PulsaDataIndosatIDPInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDPPresenter implements PulsaDataIndosatIDPPresenterInterface, OnLoadPulsaDataIndosatIDP {

    private WeakReference<PulsaDataIndosatIDPInterface> view;
    private PulsaDataIndosatIDPModel model;

    public PulsaDataIndosatIDPPresenter(PulsaDataIndosatIDPInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDPInterface>(view);
        this.model = new PulsaDataIndosatIDPModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDPSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDPSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDPGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDPGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDP(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDPSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDPSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDPGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDP(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDP(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDP(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDP(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
