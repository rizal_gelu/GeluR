package com.example.gelur.module.college.presenter;

import android.content.Context;

import com.example.gelur.module.college.presenter.listener.OnSaveCollegeFinishedListener;

public interface CollegePresenterInterface {
    void onSaveClicked(Context context, String email, String college_name, String faculty_major, int graduated, String year_graduate, String description);
    void onCancelClicked();
}
