package com.example.gelur.module.listgame.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.gameff.view.GameFF;
import com.example.gelur.module.gameml.view.GameML;
import com.example.gelur.module.gamepubg.view.GamePUBG;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listproducttri.view.ListProductTri;
import com.example.gelur.module.pulsaregulertri.view.PulsaRegulerTri;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ListGame extends AppCompatActivity implements ListGameInterface{

    @BindView(R.id.coordinator_list_game)
    CoordinatorLayout coordinator_list_game;

    @BindView(R.id.activity_toolbar_game)
    Toolbar toolbar;

    @BindView(R.id.content_list_game)
    View viewContent;

    @BindView(R.id.layout_loading_list_game) View viewLoading;

    @BindView(R.id.kumpulan_option_list_game)
    LinearLayout kumpulan_option_list_game;

    @BindView(R.id.ll_mobile_legend) LinearLayout ll_mobile_legend;
    @BindView(R.id.ll_pubg) LinearLayout ll_pubg;
    @BindView(R.id.ll_free_fire) LinearLayout ll_free_fire;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_game);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List Game");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListGame.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListGame.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListGame.this, AkunPulsa.class));
    }

    @OnClick(R.id.ll_mobile_legend)
    public void onClickMobileLegend() {
        startActivity(new Intent(ListGame.this, GameML.class));
    }

    @OnClick(R.id.ll_pubg)
    public void onClickPubgMobile() {
        startActivity(new Intent(ListGame.this, GamePUBG.class));
    }

    @OnClick(R.id.ll_free_fire)
    public void onClickFreeFire() {
        startActivity(new Intent(ListGame.this, GameFF.class));
    }
}
