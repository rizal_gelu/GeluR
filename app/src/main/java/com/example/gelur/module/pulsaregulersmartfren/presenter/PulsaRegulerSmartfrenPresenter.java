package com.example.gelur.module.pulsaregulersmartfren.presenter;

import android.content.Context;

import com.example.gelur.module.pulsaregulerindosat.model.PulsaRegulerIndosatModel;
import com.example.gelur.module.pulsaregulerindosat.view.PulsaRegulerIndosatInterface;
import com.example.gelur.module.pulsaregulersmartfren.model.PulsaRegulerSmartfrenModel;
import com.example.gelur.module.pulsaregulersmartfren.presenter.listener.OnLoadPulsaRegulerSmartfren;
import com.example.gelur.module.pulsaregulersmartfren.view.PulsaRegulerSmartfrenInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaRegulerSmartfrenPresenter implements PulsaRegulerSmartfrenPresenterInterface, OnLoadPulsaRegulerSmartfren {

    private WeakReference<PulsaRegulerSmartfrenInterface> view;
    private PulsaRegulerSmartfrenModel model;

    public PulsaRegulerSmartfrenPresenter(PulsaRegulerSmartfrenInterface view) {
        this.view = new WeakReference<PulsaRegulerSmartfrenInterface>(view);
        this.model = new PulsaRegulerSmartfrenModel();
    }

    @Override
    public void onRequesttransferpulsaregulerSmartfren(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaRegulerSmartFren(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaRegulerSmartfren(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaRegulerSmartfren(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaRegulerSmartfrenSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerSmartfrenSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaRegulerSmartfrenGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerSmartfrenGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaRegulerSmartfren(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaRegulerSmartfrenSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPricePulsaRegulerSmartfren(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaRegulerSmartfrenGagal(String m) {

    }
}
