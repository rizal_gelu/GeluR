package com.example.gelur.module.pulsadatatriTRDMN.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTriTRDMN {
    void RequestTransferPulsaDataTriTRDMNSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTriTRDMNGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTriTRDMN(String data);
    void ResponseListPricePulsaDataTriTRDMNSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTriTRDMNGagal(String m);
}
