package com.example.gelur.module.pulsadatatriTRDPY.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTriTRDPY {
    void RequestTransferPulsaDataTriTRDPYSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTriTRDPYGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTriTRDPY(String data);
    void ResponseListPricePulsaDataTriTRDPYSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTriTRDPYGagal(String m);
}
