package com.example.gelur.module.login.view;

import com.example.gelur.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.example.gelur.module.forgotpassword.view.ForgotPassword;
import com.example.gelur.module.register.view.Register;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.example.gelur.module.login.presenter.LoginPresenter;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Login extends AppCompatActivity implements LoginInterface {

    @BindView(R.id.activity_login_toolbar) Toolbar toolbar;

    @BindView(R.id.activity_login_coordinator_layout) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_login_etEmail) EditText etEmail;

    @BindView(R.id.activity_login_et_password) EditText etPassword;

    @BindView(R.id.activity_login_tv_sign_up) TextView tvSignup;

    @BindView(R.id.activity_login_tv_forgot_password) TextView tvForgotPassword;

    @BindView(R.id.activity_login_layout_content) View layoutContent;

    @BindView(R.id.activity_login_layout_loading) View layoutLoading;

    LoginPresenter presenter;
    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.title_activity_login);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new LoginPresenter(this);

        showContent(true);
    }

    @OnClick(R.id.activity_login_btn_login)
    public void login(View v) {
        loadingDialog.show();

        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        if(email.isEmpty() || !AppsUtil.isValidEmail(email)) {
            Snackbar.make(coordinatorLayout, "Email is not valid", Snackbar.LENGTH_SHORT).show();
        } else if (password.isEmpty() || password.toString().length() < 6) {
            Snackbar.make(coordinatorLayout, "Password length must be least six (6) characters", Snackbar.LENGTH_SHORT).show();
        } else {
            presenter.onLoginClicked(this, email, password);
            String name = AppsConstant.KEY_NAME;
            AppsUtil sessionManagement = new AppsUtil(this);
            sessionManagement.createLoginSession(name, email, password);
        }
    }

    @OnClick(R.id.activity_login_tv_sign_up)
    public void signUp(View v) {
        presenter.onSignUpClicked();
    }

    @OnClick(R.id.activity_login_tv_forgot_password)
    public void forgotPassword() {
        presenter.onForgotPasswordClicked();
    }

    @Override
    public void onLoginFailed(String message) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onLoginSuccess(List<UsersPojo> user) {
        loadingDialog.dismiss();



        Snackbar.make(coordinatorLayout, "Login Success", Snackbar.LENGTH_LONG).show();
        finish();
        
        Intent intent = new Intent(Login.this, Timeline.class);
//        Intent intent = new Intent(Login.this, TransferPulsa.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void goToSignUpScreen() {
        startActivity(new Intent(Login.this, Register.class));
    }

    @Override
    public void goToForgotPasswordScreen() {
        startActivity(new Intent(Login.this, ForgotPassword.class));
    }

    private void showContent(boolean yes) {
        if (yes) {
            layoutContent.setVisibility(View.VISIBLE);
            layoutLoading.setVisibility(View.GONE);
        } else {
            layoutContent.setVisibility(View.GONE);
            layoutLoading.setVisibility(View.VISIBLE);
        }
    }
}
