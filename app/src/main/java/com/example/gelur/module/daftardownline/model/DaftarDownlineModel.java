package com.example.gelur.module.daftardownline.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.daftardownline.presenter.listener.OnLoadDaftarDownline;
import com.example.gelur.module.daftardownline.rest.DaftarDownlineRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarDownlineModel implements DaftarDownlineModelInterface{
    @Override
    public void DaftarMember(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadDaftarDownline listener) {
        retrofit2.Call<ResponsNonTransaksi> sal = DaftarDownlineRestClient.get().daftar_downline(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.DaftarMemberSukses(response.body());
                } else {
                    listener.DaftarMemberGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
            }
        });
    }
}
