package com.example.gelur.module.topup.presenter;

import android.content.Context;

public interface TopUpPresenterInterface {
    void onRequestTopUp(Context context, String req, String kodereseller, String cmd, String time, String pin, String password);
}
