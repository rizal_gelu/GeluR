package com.example.gelur.module.pulsadatatelkomselTDB.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTelkomselTDBInterface {
    void TransferPulsaDataTelkomselTDBSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTelkomselTDBGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaDataTelkomselTDB(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
