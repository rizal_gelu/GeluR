package com.example.gelur.module.daftardownline.model;

import android.content.Context;

import com.example.gelur.module.akunpulsa.presenter.listener.OnLoadAkunPulsa;
import com.example.gelur.module.daftardownline.presenter.listener.OnLoadDaftarDownline;

public interface DaftarDownlineModelInterface {
    void DaftarMember(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadDaftarDownline listener);
}
