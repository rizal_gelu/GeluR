package com.example.gelur.module.transferpulsa.model;

import android.content.Context;

import com.example.gelur.module.transferpulsa.presenter.listener.OnLoadCekSaldoFinished;

public interface TransferPulsaModelInterface {
    void getInformationSaldo(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadCekSaldoFinished listener);
}
