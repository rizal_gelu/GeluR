package com.example.gelur.module.pulsadataXLXDC.model;

import android.content.Context;

import com.example.gelur.module.pulsadataXLXDC.presenter.listener.OnLoadPulsaDataXLXDC;

public interface PulsaDataXLXDCModelInterface {
    void RequestTransferPulsaDataXLXDC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataXLXDC listener);
    void RequestListPricePulsaDataXLXDC(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataXLXDC listener);

}
