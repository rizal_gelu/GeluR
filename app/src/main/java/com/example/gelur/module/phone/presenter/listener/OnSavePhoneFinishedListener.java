package com.example.gelur.module.phone.presenter.listener;

import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.PhonePojo;

public interface OnSavePhoneFinishedListener {
    void onSavePhoneSuccess(PhonePojo phonePojo);
    void onSavePhoneFailed(String message);
}
