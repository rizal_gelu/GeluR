package com.example.gelur.module.pulsadatatriTRDAON.presenter;

import android.content.Context;

public interface PulsaDataTriTRDAONPresenterInterface {
    void onRequesttransferpulsaDataTriTRDAON(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRDAON(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
