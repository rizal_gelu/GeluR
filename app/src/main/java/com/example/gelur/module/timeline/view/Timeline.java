package com.example.gelur.module.timeline.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.company.view.Company;
import com.example.gelur.module.login.view.Login;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.timeline.adapter.TimelineRecyclerViewAdapter;
import com.example.gelur.module.timeline.presenter.TimelinePresenter;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;
import com.example.gelur.R;
import com.google.android.material.snackbar.Snackbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Timeline extends AppCompatActivity implements TimelineInterface {
    @BindView(R.id.activity_timeline_toolbar)
    Toolbar toolbar;

    @BindView(R.id.activity_timeline_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_timeline_rv_user)
    RecyclerView recyclerView;

    @BindView(R.id.layout_content_timeline)
    View viewContent;

    @BindView(R.id.layout_loading_timeline)
    View viewLoading;

    FloatingActionButton floatingActionButton;

    BottomNavigationView bottomNavigationView;

    TimelinePresenter presenter;
    UsersPojo user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_timeline);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                        break;
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Timeline.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.timeline);

        hideMainContent();

        presenter = new TimelinePresenter(this);

        presenter.loadTimeline(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_left, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
//            case R.id.id_company_area_left_menu:
//                goToCompanyActivity();
//                break;
            case R.id.id_logout:
                presenter.userLogout(this);
                break;
        }
        return true;
    }

    private void goToCompanyActivity() {
        startActivity(new Intent(Timeline.this, Company.class));
    }

    private void goToSearch() {
        startActivity(new Intent(Timeline.this, Search.class));
    }

    private void goToNotification() {

    }

    @Override
    public void onUserLogout() {
        this.user = presenter.getLoggedInUser(this);

        invalidateOptionsMenu();
        supportInvalidateOptionsMenu();

        Snackbar.make(coordinatorLayout, getString(R.string.logout_success), Snackbar.LENGTH_LONG).show();

        AppsUtil removeSession = new AppsUtil(this);
        removeSession.removeSession();

        startActivity(new Intent(Timeline.this, Login.class));
    }

    @Override
    public void onLoadTimelineSuccess(List<TimelinePojo> timelineList) {
        TimelineRecyclerViewAdapter adapter = new TimelineRecyclerViewAdapter(this, timelineList, presenter);
        recyclerView.setAdapter(adapter);
        showMainContent();
    }

    @Override
    public void onLoadTimelineFailed(String message) {
        showMainContent();
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogTheme);
        builder.setMessage(message);
        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onClickUser() {
        startActivity(new Intent(Timeline.this, Account.class));
    }

    private void showMainContent(){
        viewLoading.setVisibility(View.GONE);
        viewContent.setVisibility(View.VISIBLE);
    }

    private void hideMainContent(){
        viewContent.setVisibility(View.GONE);
        viewLoading.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickComment(TimelinePojo timeline) {

    }

//    @Override
//    public void onClickLike() {
//        String id_timeline = "5e0322cbd3ac50ba84005308"; // hardcode id_timeline
////        AppsUtil session_email = AppsUtil.
//        String email = "irawatikasim@yahoo.com"; // hardcode email
//        presenter.onClickLikeProses(this, id_timeline, email);
//    }

    @Override
    public void onClickShare(TimelinePojo timelinePojo) {

    }

    @OnClick(R.id.account_menu)
    public void goToProfile() {
        startActivity(new Intent(Timeline.this, Account.class));
    }
}
