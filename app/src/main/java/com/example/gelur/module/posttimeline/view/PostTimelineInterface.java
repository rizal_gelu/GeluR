package com.example.gelur.module.posttimeline.view;

import com.example.gelur.pojo.PostTimelinePojo;
import com.example.gelur.pojo.UsersPojo;

public interface PostTimelineInterface {
    void onSuccesPost(PostTimelinePojo postTimelinePojo);
    void onFailedPost(String Message);
    void onUserNotLoggedIn(String string);
//    void populateUI(UsersPojo usersPojo);
}
