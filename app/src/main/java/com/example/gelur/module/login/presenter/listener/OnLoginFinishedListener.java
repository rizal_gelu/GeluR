package com.example.gelur.module.login.presenter.listener;

import com.example.gelur.pojo.UsersPojo;

import java.util.List;

public interface OnLoginFinishedListener {
    void onLoginSuccess(List<UsersPojo> user);
    void onLoginFailed(String message);
}
