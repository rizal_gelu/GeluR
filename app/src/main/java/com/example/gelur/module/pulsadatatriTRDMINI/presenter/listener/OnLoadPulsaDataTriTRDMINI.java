package com.example.gelur.module.pulsadatatriTRDMINI.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTriTRDMINI {
    void RequestTransferPulsaDataTriTRDMINISukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTriTRDMINIGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTriTRDMINI(String data);
    void ResponseListPricePulsaDataTriTRDMINISukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTriTRDMINIGagal(String m);
}
