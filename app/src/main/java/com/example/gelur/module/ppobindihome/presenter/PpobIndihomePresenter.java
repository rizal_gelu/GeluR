package com.example.gelur.module.ppobindihome.presenter;

import android.content.Context;

import com.example.gelur.module.ppobindihome.model.PpobIndihomeModel;
import com.example.gelur.module.ppobindihome.presenter.listener.OnLoadPpobIndihome;
import com.example.gelur.module.ppobindihome.view.PpobIndihomeInterface;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PpobIndihomePresenter implements PpobIndihomePresenterInterface, OnLoadPpobIndihome {

    private WeakReference<PpobIndihomeInterface> view;
    private PpobIndihomeModel model;

    public PpobIndihomePresenter(PpobIndihomeInterface view) {
        this.view = new WeakReference<PpobIndihomeInterface>(view);
        this.model = new PpobIndihomeModel();
    }

    @Override
    public void onRequestCheckTagihanIndihome(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestCekTagihanIndihome(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void OnResponsesTagihanIndihomeSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().OnResponseTagihanIndihomeSukses(responsTransaksiPulsa);
    }

    @Override
    public void OnResponsesTagihanIndihomeGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().OnResponseTagihanIndihomeGagal(responsTransaksiPulsa);
    }
}
