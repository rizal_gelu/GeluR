package com.example.gelur.module.pulsadataXLPXD.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataXLPXD {
    void RequestTransferPulsaDataXLPXDSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataXLPXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataXLPXD(String data);
    void ResponseListPricePulsaDataXLPXDSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataXLPXDGagal(String m);
}
