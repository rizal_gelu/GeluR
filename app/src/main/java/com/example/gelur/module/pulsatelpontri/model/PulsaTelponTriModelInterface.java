package com.example.gelur.module.pulsatelpontri.model;

import android.content.Context;

import com.example.gelur.module.pulsatelpontelkomsel.presenter.listener.OnLoadPulsaTelponTelkomsel;
import com.example.gelur.module.pulsatelpontri.presenter.listener.OnLoadPulsaTelponTri;

public interface PulsaTelponTriModelInterface {
    void RequestTransferPulsaTelponTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponTri listener);
    void RequestListPricePulsaTelponTri(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponTri listener);
}
