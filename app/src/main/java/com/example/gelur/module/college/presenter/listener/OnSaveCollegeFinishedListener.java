package com.example.gelur.module.college.presenter.listener;

import com.example.gelur.pojo.CollegePojo;

public interface OnSaveCollegeFinishedListener {
    void onSaveCollegeSuccess(CollegePojo collegePojo);
    void onSaveCollegeFailed(String message);
}
