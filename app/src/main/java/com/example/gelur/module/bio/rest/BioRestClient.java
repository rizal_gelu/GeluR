package com.example.gelur.module.bio.rest;

import com.example.gelur.module.account.rest.AccountAPIURLConstant;
import com.example.gelur.module.account.rest.AccountRestInterface;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BioRestClient {
    private static BioRestInterface REST_CLIENT;

    static {
        setupRestClient();
    }

    public static BioRestInterface get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(logging).build();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(BioAPIURLConstant.GET_BIO)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create());

        Retrofit restAdapter = builder.build();
        REST_CLIENT = restAdapter.create(BioRestInterface.class);
    }
}
