package com.example.gelur.module.ecommercegojekdriver.presenter;

import android.content.Context;

import com.example.gelur.module.ecommercedana.model.EcommerceDanaModel;
import com.example.gelur.module.ecommercegojekdriver.model.EcommerceGojekDriverModel;
import com.example.gelur.module.ecommercegojekdriver.presenter.listener.OnLoadGojekDriver;
import com.example.gelur.module.ecommercegojekdriver.view.EcommerceGojekDriverInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class EcommerceGojekDriverPresenter implements EcommerceGojekDriverPresenterInterface, OnLoadGojekDriver {

    private WeakReference<EcommerceGojekDriverInterface> view;
    private EcommerceGojekDriverModel model;

    public EcommerceGojekDriverPresenter(EcommerceGojekDriverInterface view) {
        this.view = new WeakReference<EcommerceGojekDriverInterface>(view);
        this.model = new EcommerceGojekDriverModel();
    }

    @Override
    public void onRequesttransfersaldoGojekDriver(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferSaldoGojekDriver(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceGojekDriver(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceGojekDriver(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferSaldoGojekDriverSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoGojekDriverSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferSaldoGojekDriverGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoGojekDriverGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickGojekDriver(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceGojekDriverSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceGojekDriver(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceGojekDriverGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
