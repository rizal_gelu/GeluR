package com.example.gelur.module.ecommerceovo.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface EcommerceOvoInterface {
    void TransferSaldoDanaSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferSaldoDanaGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceOvo(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
