package com.example.gelur.module.pulsatelpontelkomsel.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsatelpontelkomsel.presenter.listener.OnLoadPulsaTelponTelkomsel;

import java.util.List;

public class PulsaTelponTelkomselAdapter extends RecyclerView.Adapter<PulsaTelponTelkomselAdapter.ViewHolderContent> {

    private Activity activity;
    private OnLoadPulsaTelponTelkomsel listener;
    List<String> stringList;

    public PulsaTelponTelkomselAdapter(Activity activity, List<String> datum, OnLoadPulsaTelponTelkomsel listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public PulsaTelponTelkomselAdapter.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_telpon_telkomsel, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(PulsaTelponTelkomselAdapter.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_telpon_telkomsel.setText(data);

        holder.ll_kumpulan_option_pulsa_telpon_telkomsel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaTelponTelkomsel(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_telpon_telkomsel;
        public final TextView pulsa_telpon_telkomsel;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_telpon_telkomsel = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_telpon_telkomsel);
            pulsa_telpon_telkomsel = (TextView) view.findViewById(R.id.pulsa_telpon_telkomsel);
        }
    }
}
