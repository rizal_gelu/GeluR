package com.example.gelur.module.gamepubg.model;

import android.content.Context;

import com.example.gelur.module.gamepubg.presenter.listener.OnLoadGamePUBG;

public interface GamePUBGModelInterface {
    void RequestTransferDiamondML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGamePUBG listener);
}
