package com.example.gelur.module.timeline.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.gelur.module.timeline.presenter.listener.OnLoadTimelineFinished;
import com.example.gelur.pojo.TimelinePojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.DateUtil;

import java.util.Date;
import java.util.List;
import com.example.gelur.R;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class TimelineRecyclerViewAdapter extends RecyclerView.Adapter<TimelineRecyclerViewAdapter.ViewHolderContent> {

    private List<TimelinePojo> listTimeline;
    private OnLoadTimelineFinished onLoadTimelineFinished;
    private Activity activity;

    public TimelineRecyclerViewAdapter(Activity activity, List<TimelinePojo> listTimeline, OnLoadTimelineFinished onLoadTimelineFinished) {
        this.listTimeline = listTimeline;
        this.activity = activity;
        this.onLoadTimelineFinished = onLoadTimelineFinished;
    }

    @Override
    public ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_timeline_list, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(ViewHolderContent holder, int position) {
        holder.mItem = listTimeline.get(position);

        long dateCreated = holder.mItem.getDate_created();
        Date today = new Date();
        String processConvert = DateUtil.SelisihDateTime(dateCreated, today);

        if (holder.mItem.getAvatar() != "") { // Jika ada file avatar dalam database
            Glide.with(activity).load(AppsConstant.BASE_IMAGE_PROFILE_URL + holder.mItem.getAvatar())
                    .into(holder.circleImageView);
        } else { // Jika file avatar belum di upload oleh user
            Glide.with(activity).load(R.drawable.ic_logo_g).into(holder.circleImageView);
        }

        if (!holder.mItem.getFile().equals("")) {
            Picasso.get().load(AppsConstant.BASE_TIMELINE_FILE_URL + holder.mItem.getFile()).into(holder.imageViewPostFeed);
        } else {
            holder.imageViewPostFeed.setVisibility(View.GONE);
        }

        holder.mNameView.setText(holder.mItem.getFirst_name()+" "+holder.mItem.getLast_name());
        holder.mDateday.setText(processConvert);
        holder.mDescription.setText(holder.mItem.getDescription());

        holder.btn_comment_feed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
        });

        holder.btn_like_content.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoadTimelineFinished.onClickLikeProses(holder.mItem.getId(), holder.mItem.getEmail());
                holder.like_post_tl.setText(R.string.unlike);
                holder.like_post_tl.setVisibility(View.VISIBLE);

                holder.btn_like_content.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onLoadTimelineFinished.onClickUnlikeProses(holder.mItem.getId(), holder.mItem.getEmail());
                        holder.like_post_tl.setVisibility(View.INVISIBLE);
                    }
                });
            }
        });

        holder.mNameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLoadTimelineFinished.onClickUserName(holder.mItem.getEmail());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listTimeline.size();
    }

    public void remove(String item) {
        int position = listTimeline.indexOf(item);
        listTimeline.remove(position);
        notifyItemRemoved(position);
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final CircleImageView circleImageView;
        public final TextView mNameView;
        public final TextView mDateday;
        public final TextView mDescription;
        public final ImageView imageViewPostFeed;
        public final TextView btn_comment_feed;
        public final LinearLayout btn_like_content;
        public final TextView tv_count_likes;
        public final TextView tv_count_comments;
        public final TextView like_post_tl;
        public TimelinePojo mItem;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            circleImageView = (CircleImageView) view.findViewById(R.id.circleImg_profile_feed);
            mNameView = (TextView) view.findViewById(R.id.tv_username);
            mDateday = (TextView) view.findViewById(R.id.tv_date_feed);
            mDescription = (TextView) view.findViewById(R.id.tv_status);
            imageViewPostFeed = (ImageView) view.findViewById(R.id.imgView_posPic);
            btn_comment_feed = (TextView) view.findViewById(R.id.btn_comment_feed);
            btn_like_content = (LinearLayout) view.findViewById(R.id.btn_like_content);
            like_post_tl = (TextView) view.findViewById(R.id.like_post_tl);
            tv_count_likes = (TextView) view.findViewById(R.id.tv_count_likes);
            tv_count_comments = (TextView) view.findViewById(R.id.tv_count_comments);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
