package com.example.gelur.module.taskboardcompany.presenter.listener;

import com.example.gelur.module.taskboardcompany.view.TaskBoard;
import com.example.gelur.pojo.TaskBoardPojo;

import java.util.List;

public interface OnLoadTaskFinished {
    void onLoadTaskBoardSuccess(List<TaskBoardPojo> taskBoardPojo);
    void onLoadTaskBoardFailed(String message);
}
