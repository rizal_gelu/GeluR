package com.example.gelur.module.pulsadataXLXDCV.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataXLXDCV {
    void RequestTransferPulsaDataXLXDCVSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataXLXDCVGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataXLXDCV(String data);
    void ResponseListPricePulsaDataXLXDCVSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataXLXDCVGagal(String m);
}
