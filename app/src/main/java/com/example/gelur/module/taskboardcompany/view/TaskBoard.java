package com.example.gelur.module.taskboardcompany.view;

import android.app.Notification;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.account.view.Account;
import com.example.gelur.module.posttimeline.view.PostTimeline;
import com.example.gelur.module.search.view.Search;
import com.example.gelur.module.taskboardadd.view.TaskBoardAdd;
import com.example.gelur.module.taskboardcompany.adapter.TaskBoardRecyclerViewAdapter;
import com.example.gelur.module.taskboardcompany.presenter.TaskBoardPresenter;
import com.example.gelur.module.timeline.view.Timeline;
import com.example.gelur.pojo.TaskBoardPojo;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class TaskBoard extends AppCompatActivity implements TaskBoardInterface {

    @BindView(R.id.activity_task_board_coordinator_layout)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_task_board_toolbar)
    Toolbar toolbar;

    @BindView(R.id.layout_task_board_content)
    View viewContent;

    @BindView(R.id.layout_loading) View viewLoading;

    @BindView(R.id.activity_task_board_rv)
    RecyclerView recyclerView;

    @BindView(R.id.btn_add_job_list)
    Button btn_add_job_list;

    BottomNavigationView bottomNavigationView;

    FloatingActionButton floatingActionButton;

    AlertDialog loadingDialog;

    TaskBoardPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_task_board);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home_menu:
                        goToHome();
                        break;
                    case R.id.account_menu:
                        goToProfile();
                        break;
                    case R.id.search_menu:
                        goToSearch();
                    case R.id.notification_menu:
                        goToNotification();
                        break;
                }
                return true;
            }
        });

        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab_post_timeline);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TaskBoard.this, PostTimeline.class));
            }
        });

        ButterKnife.bind(this);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new TaskBoardPresenter(this);
        String company_id = "601378dd7bc4312a8996d221";
        presenter.onLoadTaskBoard(this, company_id);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.btn_add_job_list)
    public void setBtn_add_job_list() {
        presenter.onClickBtnAddJob();
    }

    @Override
    public void goToAddJob() {
        startActivity(new Intent(TaskBoard.this, TaskBoardAdd.class));
    }

    private void goToHome() {
        startActivity(new Intent(TaskBoard.this, Timeline.class));
    }

    private void goToNotification() {
        startActivity(new Intent(TaskBoard.this, Notification.class));
    }

    private void goToSearch() {
        startActivity(new Intent(TaskBoard.this, Search.class));
    }

    private void goToProfile() {
        startActivity(new Intent(TaskBoard.this, Account.class));
    }

    @Override
    public void onLoadTaskBoardSuccess(List<TaskBoardPojo> taskBoardPojo) {
        TaskBoardRecyclerViewAdapter adapter = new TaskBoardRecyclerViewAdapter(this, taskBoardPojo, presenter);
        recyclerView.setAdapter(adapter);
        showContent(true);
    }

    @Override
    public void onLoadTaskBoardFailed(String Message) {

    }

    @Override
    public void onClickTaskItem() {

    }
}
