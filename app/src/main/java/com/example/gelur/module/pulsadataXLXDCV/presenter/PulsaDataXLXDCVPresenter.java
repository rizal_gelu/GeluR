package com.example.gelur.module.pulsadataXLXDCV.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataXLXDC.model.PulsaDataXLXDCModel;
import com.example.gelur.module.pulsadataXLXDC.view.PulsaDataXLXDCInterface;
import com.example.gelur.module.pulsadataXLXDCV.model.PulsaDataXLXDCVModel;
import com.example.gelur.module.pulsadataXLXDCV.presenter.listener.OnLoadPulsaDataXLXDCV;
import com.example.gelur.module.pulsadataXLXDCV.view.PulsaDataXLXDCVInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataXLXDCVPresenter implements PulsaDataXLXDCVPresenterInterface, OnLoadPulsaDataXLXDCV {

    private WeakReference<PulsaDataXLXDCVInterface> view;
    private PulsaDataXLXDCVModel model;

    public PulsaDataXLXDCVPresenter(PulsaDataXLXDCVInterface view) {
        this.view = new WeakReference<PulsaDataXLXDCVInterface>(view);
        this.model = new PulsaDataXLXDCVModel();
    }

    @Override
    public void onRequesttransferpulsaDataXLXDCV(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataXLXDCV(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataXLXDCV(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataXLXDCV(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataXLXDCVSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXDCVSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataXLXDCVGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLXDCVGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataXLXDCV(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataXLXDCVSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaXLXDCVSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataXLXDCVGagal(String m) {

    }
}
