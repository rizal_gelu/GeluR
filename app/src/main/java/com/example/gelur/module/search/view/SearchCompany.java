package com.example.gelur.module.search.view;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

public class SearchCompany extends Fragment {

    public static SearchCompany newInstance(int page, String title) {
        SearchCompany searchCompany = new SearchCompany();
        Bundle args = new Bundle();
        args.putInt("1", page);
        args.putString("Search Person", title);
        searchCompany.setArguments(args);
        return searchCompany;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
