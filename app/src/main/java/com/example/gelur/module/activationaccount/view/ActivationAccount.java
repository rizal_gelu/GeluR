package com.example.gelur.module.activationaccount.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.activationaccount.presenter.ActivationAccountPresenter;
import com.example.gelur.module.login.view.Login;
import com.example.gelur.pojo.ActivateAccountPojo;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.snackbar.Snackbar;

import java.util.HashMap;

public class ActivationAccount extends AppCompatActivity implements ActivationAccountInterface{

    @BindView(R.id.activity_activation_account_coordinator_layout) CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_activation_account_toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_activation_code)
    TextView tv_activation_code;

    @BindView(R.id.et_activate_code)
    EditText et_activation_code;

    @BindView(R.id.btn_activate)
    Button btn_activate;

    @BindView(R.id.activity_login_layout_content)
    View layoutContent;

    @BindView(R.id.activity_login_layout_loading) View layoutLoading;

    ActivationAccountPresenter presenter;
    AlertDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_activation_account);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(R.string.title_activity_account);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);

        presenter = new ActivationAccountPresenter(this);

        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            layoutContent.setVisibility(View.VISIBLE);
            layoutLoading.setVisibility(View.GONE);
        } else {
            layoutContent.setVisibility(View.GONE);
            layoutLoading.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onActivateSuccess(ActivateAccountPojo activateAccountPojo) {
        loadingDialog.dismiss();
        Snackbar.make(coordinatorLayout, "Your account active now. Please log in first", Snackbar.LENGTH_LONG).show();
        finish();
        Intent intent = new Intent(ActivationAccount.this, Login.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onActivateFail(String message) {

    }

    @OnClick(R.id.btn_activate)
    public void onClickActivate(View v) {
        loadingDialog.dismiss();

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserDetailsFromSession();

        String email = userDetails.get(AppsConstant.KEY_ID_USER);
        String code = et_activation_code.getText().toString();

        presenter.submitActivateCode(this, email, code);

        sessionUser.logoutUserSession();
    }
}
