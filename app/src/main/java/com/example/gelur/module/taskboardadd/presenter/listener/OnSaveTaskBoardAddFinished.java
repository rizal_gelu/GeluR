package com.example.gelur.module.taskboardadd.presenter.listener;

import com.example.gelur.pojo.TaskBoardAddPojo;

public interface OnSaveTaskBoardAddFinished {
    void onSaveTaskBoardSuccess(TaskBoardAddPojo taskBoardAddPojo);
    void onSaveTaskBoardFailed(String message);
}
