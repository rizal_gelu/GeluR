package com.example.gelur.module.listproductpln.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.listproductindosat.view.ListProductIndosat;
import com.example.gelur.module.plnprabayar.view.PlnPrabayar;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ListProductPLN extends AppCompatActivity implements ListProductPLNInterface {

    @BindView(R.id.coordinator_list_product_pln)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_list_product_pln)
    Toolbar toolbar;

    @BindView(R.id.content_product_pln)
    View viewContent;

    @BindView(R.id.layout_loading_list_product_pln) View viewLoading;

    @BindView(R.id.kumpulan_option_list_product_pln)
    LinearLayout kumpulan_option_list_product_pln;

    @BindView(R.id.ll_pln_pasca_bayar) LinearLayout ll_pln_pasca_bayar;

    @BindView(R.id.pln_pasca_bayar)
    TextView pln_pasca_bayar;

    @BindView(R.id.ll_paket_pra_bayar) LinearLayout ll_paket_pra_bayar;

    @BindView(R.id.pln_pra_bayar) TextView pln_pra_bayar;

    AlertDialog loadingDialog;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_list_product_pln);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("List PLN");

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(ListProductPLN.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(ListProductPLN.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(ListProductPLN.this, AkunPulsa.class));
    }

    @Override
    public void onClickPLNPascabayar() {

    }

    @OnClick(R.id.ll_paket_pra_bayar)
    public void onClickPLNPrabayar() {
        startActivity(new Intent(ListProductPLN.this, PlnPrabayar.class));
    }
}
