package com.example.gelur.module.historytransaksi.model;

import android.content.Context;

import com.example.gelur.module.historytransaksi.presenter.listener.OnLoadHistoryTransaksi;

public interface HistoryTransaksiModelInterface {
    void RequestHistoryTransaksi(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadHistoryTransaksi listener);
}
