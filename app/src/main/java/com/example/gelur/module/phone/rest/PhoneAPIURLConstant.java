package com.example.gelur.module.phone.rest;

import com.example.gelur.util.AppsConstant;

public class PhoneAPIURLConstant {
    // POST BIO EndPoint
    public static final String BASE_API_USER = AppsConstant.BASE_API_URL + "v1/users/"; // User UriSegment 2
    public static final String POST_PHONE = BASE_API_USER + "post_phone/"; // User UriSegment 3

    // POST BIO EndPoint
    public static final String GET_PHONE = BASE_API_USER + "get_phone/"; // User UriSegment 3
}
