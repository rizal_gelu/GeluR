package com.example.gelur.module.pulsaregulerindosat.presenter;

import android.content.Context;

import com.example.gelur.module.pulsaregulerindosat.model.PulsaRegulerIndosatModel;
import com.example.gelur.module.pulsaregulerindosat.presenter.listener.OnLoadPulsaRegulerIndosat;
import com.example.gelur.module.pulsaregulerindosat.view.PulsaRegulerIndosatInterface;
import com.example.gelur.module.pulsaregulertelkomsel.model.PulsaRegulerTelkomselModel;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.PulsaRegulerTelkomselPresenterInterface;
import com.example.gelur.module.pulsaregulertelkomsel.view.PulsaRegulerTelkomselInterface;
import com.example.gelur.pojo.FormatResponseNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;
import java.util.List;

public class PulsaRegulerIndosatPresenter implements PulsaRegulerIndosatPresenterInterface, OnLoadPulsaRegulerIndosat {

    private WeakReference<PulsaRegulerIndosatInterface> view;
    private PulsaRegulerIndosatModel model;

    public PulsaRegulerIndosatPresenter(PulsaRegulerIndosatInterface view) {
        this.view = new WeakReference<PulsaRegulerIndosatInterface>(view);
        this.model = new PulsaRegulerIndosatModel();
    }

    @Override
    public void RequestTransferPulsaRegulerIndosatSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerIndosatSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaRegulerIndosatGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaRegulerIndosatSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestListPulsaRegulerIndosatSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadDataPulsaRegulerSukses(responsNonTransaksi);
    }

    @Override
    public void RequestListPulsaRegulerIndosatGagal(String message) {

    }

    @Override
    public void onClickListPulsa(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void onRequesttransferpulsareguler(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaIndosat(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPulsaIndosat(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.CheckListPulsa(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void createNewPopUpDialog(String nominal) {
        if (view.get() != null) view.get().createNewPopUpDialog(nominal);
    }
}
