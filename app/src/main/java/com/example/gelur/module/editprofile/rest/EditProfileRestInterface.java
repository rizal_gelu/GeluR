package com.example.gelur.module.editprofile.rest;

import com.example.gelur.module.account.rest.AccountAPIURLConstant;
import com.example.gelur.pojo.BioPojo;
import com.example.gelur.pojo.FormaterResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EditProfileRestInterface {

    @GET(EditProfileAPIURLConstant.GET_BIO)
    Call<BioPojo> getData_bio(@Query("email") String email);

//    @GET(AccountAPIURLConstant.GET_ACCOUNT_DETAIL)
//    Call<FormaterResponse> getAccountDetail(@Query("email") String email);
}
