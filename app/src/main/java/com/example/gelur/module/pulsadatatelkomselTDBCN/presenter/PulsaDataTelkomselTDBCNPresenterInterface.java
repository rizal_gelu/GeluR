package com.example.gelur.module.pulsadatatelkomselTDBCN.presenter;

import android.content.Context;

public interface PulsaDataTelkomselTDBCNPresenterInterface {
    void onRequesttransferPulsaDataTelkomselTDBCN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTelkomselTDBCN(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
