package com.example.gelur.module.pulsadatatriTRDPY.presenter;

import android.content.Context;

public interface PulsaDataTriTRDPYPresenterInterface {
    void onRequesttransferpulsaDataTriTRDPY(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataTriTRDPY(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
