package com.example.gelur.module.youraboutinfo.view;

public interface YourAboutInfoInterface {
//    void addWorkplace();
//    void addCollege();
//    void addHighSchool();
//    void basicInfoClick();
//    void contactInfoClick();
    void goToAddWorkplace();
    void goToAddCollege();
    void goToAddCity();
    void goToAddHighSchool();
    void goToEditBasicInfo();
    void goToEditContactInfo();
    void onLoadWorkplaceSuccess();
    void onLoadWorkplaceFailed();
    void onLoadCollegeSuccess();
    void onLoadCollegeFailed();
}
