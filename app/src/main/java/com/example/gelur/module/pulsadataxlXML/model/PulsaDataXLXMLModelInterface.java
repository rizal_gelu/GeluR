package com.example.gelur.module.pulsadataxlXML.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatriTRDUL.presenter.listener.OnLoadPulsaDataTriTRDUL;
import com.example.gelur.module.pulsadataxlXML.presenter.listener.OnLoadPulsaDataXLXML;

public interface PulsaDataXLXMLModelInterface {
    void RequestTransferPulsaDataXLXML(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataXLXML listener);
    void RequestListPricePulsaDataXLXML(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataXLXML listener);

}
