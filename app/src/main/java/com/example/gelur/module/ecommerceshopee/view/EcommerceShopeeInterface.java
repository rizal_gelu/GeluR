package com.example.gelur.module.ecommerceshopee.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface EcommerceShopeeInterface {
    void TransferSaldoShopeeSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferSaldoShopeeGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPriceShopee(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
