package com.example.gelur.module.pulsatelponxl.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.module.akunpulsa.view.AkunPulsa;
import com.example.gelur.module.historytransaksi.view.HistoryTransaksiPulsa;
import com.example.gelur.module.laporantransaksi.view.LaporanTransaksi;
import com.example.gelur.module.pulsaregulerxl.presenter.PulsaRegulerXLPresenter;
import com.example.gelur.module.pulsaregulerxl.view.PulsaRegulerXL;
import com.example.gelur.module.pulsatelpontri.presenter.PulsaTelponTriPresenter;
import com.example.gelur.module.pulsatelpontri.view.PulsaTelponTri;
import com.example.gelur.module.pulsatelponxl.presenter.PulsaTelponXLPresenter;
import com.example.gelur.module.transferpulsa.view.TransferPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.example.gelur.R;
import com.example.gelur.util.AppsConstant;
import com.example.gelur.util.AppsUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

public class PulsaTelponXL extends AppCompatActivity implements PulsaTelponXLInterface {

    @BindView(R.id.coordinator_list_pulsa_telpon_xl)
    CoordinatorLayout coordinatorLayout;

    @BindView(R.id.activity_toolbar_pulsa_telpon_xl)
    Toolbar toolbar;

    @BindView(R.id.content_list_pulsa_telpon_xl)
    View viewContent;

    @BindView(R.id.layout_loading_list_pulsa_telpon_xl) View viewLoading;

    @BindView(R.id.rv_pulsa_telpon_XL)
    RecyclerView recyclerView;

    AppCompatButton submit_data;

    AlertDialog loadingDialog;

    AlertDialog.Builder dialogBuilder;

    EditText no_hp;
    EditText qty_telkomsel;

    PulsaTelponXLPresenter presenter;
    BottomNavigationView bottomNavigationView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pulsa_telpon_xl);
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_nav_position_transaksi);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.home_menu_transaksi:
                        goToHomeTransaksi();
                        break;
                    case R.id.account_menu_pulsa:
                        goToAkun();
                        break;
                    case R.id.history_transaksi:
                        goToHistoryTransaksi();
                        break;
                }
                return true;
            }
        });
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Pulsa Telpon XL");

        Format f = new SimpleDateFormat("hh:mm:ss a");
        String strResult = f.format(new Date());

        String split_time = String.valueOf(strResult);
        String[] reslt_split = split_time.split(":");

        String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
        String[] split_lagi = gabung_split.split("\\s+");
        String param_split = split_lagi[0];

        AppsUtil sessionUser = new AppsUtil(this);
        HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

        //hardcode
        String req = "cmd";
        String kodereseller = userDetails.get(AppsConstant.KODERESELLER);
        String perintah = "ch.tth";
        String time = param_split;
        String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
        String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);

        presenter = new PulsaTelponXLPresenter(this);
        presenter.onRequetListPricePulsaTelponXL(this, req, kodereseller, perintah, time, pin, password);

        loadingDialog = AppsUtil.showLoadingDialog(this, R.style.DialogTheme, R.layout.layout_loading);
        showContent(true);
    }

    private void showContent(boolean yes) {
        if (yes) {
            viewContent.setVisibility(View.VISIBLE);
            viewLoading.setVisibility(View.GONE);
        } else {
            viewContent.setVisibility(View.GONE);
            viewLoading.setVisibility(View.VISIBLE);
        }
    }

    private void goToHomeTransaksi() {
        startActivity(new Intent(PulsaTelponXL.this, TransferPulsa.class));
    }

    private void goToHistoryTransaksi() {
        startActivity(new Intent(PulsaTelponXL.this, HistoryTransaksiPulsa.class));
    }

    private void goToAkun() {
        startActivity(new Intent(PulsaTelponXL.this, AkunPulsa.class));
    }

    @Override
    public void TransferPulsaTelponXLSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(PulsaTelponXL.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void TransferPulsaTelponXLGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        String get_status = responsTransaksiPulsa.getStatus();
        String get_product = responsTransaksiPulsa.getProduk();
        String get_harga = responsTransaksiPulsa.getHarga();
        String get_refid = responsTransaksiPulsa.getReffid();
        String get_sn = responsTransaksiPulsa.getSn();
        String get_saldo_awal = responsTransaksiPulsa.getSaldo_awal();
        String get_saldo = responsTransaksiPulsa.getSaldo();

        Intent intent = new Intent(PulsaTelponXL.this, LaporanTransaksi.class);
        intent.putExtra("status", get_status);
        intent.putExtra("product", get_product);
        intent.putExtra("harga", get_harga);
        intent.putExtra("refid", get_refid);
        intent.putExtra("sn", get_sn);
        intent.putExtra("saldo_awal", get_saldo_awal);
        intent.putExtra("saldo", get_saldo);

        startActivity(intent);
    }

    @Override
    public void LoadListPricePulsaTelponXL(ResponsNonTransaksi responsNonTransaksi) {

    }


    public void createNewPopUpDialog(String nominal) {
        dialogBuilder = new AlertDialog.Builder(this);
        final View popUpDialog = getLayoutInflater().inflate(R.layout.popup_pulsa_reguler_telkomsel, null);
        no_hp = (EditText) popUpDialog.findViewById(R.id.Et_nomor_handphone);
        qty_telkomsel = (EditText) popUpDialog.findViewById(R.id.qty_telkomsel);
        submit_data = (AppCompatButton) popUpDialog.findViewById(R.id.submit_request_transfer_pulsa);

        dialogBuilder.setView(popUpDialog);
        loadingDialog = dialogBuilder.create();
        loadingDialog.show();

        submit_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int min = 500;
                int max = 10000;

                //Generate random int value from 50 to 100
                System.out.println("Random value in int from "+min+" to "+max+ ":");
                int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);

                Format f = new SimpleDateFormat("hh:mm:ss a");
                String strResult = f.format(new Date());

                String split_time = String.valueOf(strResult);
                String[] reslt_split = split_time.split(":");

                String gabung_split = reslt_split[0] + reslt_split[1] + reslt_split[2];
                String[] split_lagi = gabung_split.split("\\s+");
                String param_split = split_lagi[0];

                AppsUtil sessionUser = new AppsUtil(PulsaTelponXL.this);
                HashMap<String, String> userDetails = sessionUser.getUserPulsaDetailsFromSession();

                String req = "topup";
                String kode_reseller = userDetails.get(AppsConstant.KODERESELLER);
                String refid = String.valueOf(random_int);
                String get_no_hp = no_hp.getText().toString();
                String qty = qty_telkomsel.getText().toString();
                String time = param_split;
                String pin = userDetails.get(AppsConstant.KEY_PIN_PULSA);
                String password = userDetails.get(AppsConstant.KEY_PASSWORD_PULSA);
                String counter = qty_telkomsel.getText().toString();

                presenter.onRequesttransferpulsaTelponXL(PulsaTelponXL.this, req,kode_reseller, nominal, get_no_hp, counter, qty, refid, time, pin, password);
            }
        });
    }
}
