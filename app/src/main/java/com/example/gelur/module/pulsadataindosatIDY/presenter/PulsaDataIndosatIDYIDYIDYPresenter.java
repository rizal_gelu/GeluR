package com.example.gelur.module.pulsadataindosatIDY.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDY.model.PulsaDataIndosatIDYIDYModel;
import com.example.gelur.module.pulsadataindosatIDY.presenter.listener.OnLoadPulsaDataIndosatIDY;
import com.example.gelur.module.pulsadataindosatIDY.view.PulsaDataIndosatIDYInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataIndosatIDYIDYIDYPresenter implements PulsaDataIndosatIDYPresenterInterface, OnLoadPulsaDataIndosatIDY {

    private WeakReference<PulsaDataIndosatIDYInterface> view;
    private PulsaDataIndosatIDYIDYModel model;

    public PulsaDataIndosatIDYIDYIDYPresenter(PulsaDataIndosatIDYInterface view) {
        this.view = new WeakReference<PulsaDataIndosatIDYInterface>(view);
        this.model = new PulsaDataIndosatIDYIDYModel();
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDYSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDYSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataIndosatIDYGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataIndosatIDYGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataIndosatIDY(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDYSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaDataIndosatIDYSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataIndosatIDYGagal(String m) {

    }

    @Override
    public void onRequesttransferpulsadataIndosatIDY(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferDataIndosatIDY(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataIndosatIDY(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataIndosatIDY(context, req, kodereseller, perintah, time, pin, password, this);
    }
}
