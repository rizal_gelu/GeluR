package com.example.gelur.module.timeline.view;

import com.example.gelur.pojo.TimelinePojo;

import java.util.List;

public interface TimelineInterface {
    void onClickComment(TimelinePojo timeline);
//    void onClickLike();
    void onClickShare(TimelinePojo timelinePojo);
    void onUserLogout();
//    void onClickMenuCompany();
    void onLoadTimelineSuccess(List<TimelinePojo> timelineList);
    void onLoadTimelineFailed(String message);

    void onClickUser();
}
