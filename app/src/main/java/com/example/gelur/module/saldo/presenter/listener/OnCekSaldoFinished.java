package com.example.gelur.module.saldo.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;

public interface OnCekSaldoFinished {
    void SaldoSuksesCek(ResponsNonTransaksi responsNonTransaksi);
    void SaldoGagalCek(String message);
}
