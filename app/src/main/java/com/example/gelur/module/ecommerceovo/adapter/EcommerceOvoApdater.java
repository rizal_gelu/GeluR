package com.example.gelur.module.ecommerceovo.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.ecommerceovo.presenter.listener.OnLoadEcommerceOvo;

import java.util.List;

public class EcommerceOvoApdater extends RecyclerView.Adapter<EcommerceOvoApdater.ViewHolderContent> {

    private Activity activity;
    private OnLoadEcommerceOvo listener;
    List<String> stringList;

    public EcommerceOvoApdater(Activity activity, List<String> datum, OnLoadEcommerceOvo listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @Override
    public EcommerceOvoApdater.ViewHolderContent onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ecommerce_ovo, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(EcommerceOvoApdater.ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.ovo.setText(data);

        holder.ll_kumpulan_option_ovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickOvo(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_ovo;
        public final TextView ovo;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_ovo = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_ovo);
            ovo = (TextView) view.findViewById(R.id.ovo);
        }
    }
}
