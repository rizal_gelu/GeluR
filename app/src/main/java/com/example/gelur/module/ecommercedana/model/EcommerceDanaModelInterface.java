package com.example.gelur.module.ecommercedana.model;

import android.content.Context;

import com.example.gelur.module.ecommercedana.presenter.listener.OnLoadDanaEcommerce;

public interface EcommerceDanaModelInterface {
    void RequestTransferSaldoDana(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadDanaEcommerce listener);
    void RequestListPriceDana(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadDanaEcommerce listener);
}
