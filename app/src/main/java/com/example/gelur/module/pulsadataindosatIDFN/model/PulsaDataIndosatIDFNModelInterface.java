package com.example.gelur.module.pulsadataindosatIDFN.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDFN.presenter.listener.OnLoadPulsaDataIndosatIDFN;

public interface PulsaDataIndosatIDFNModelInterface {
    void RequestTransferDataIndosatIDFN(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDFN listener);
    void RequestListPricePulsaDataIndosatIDFN(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDFN listener);
}
