package com.example.gelur.module.pulsadataindosatIDUA.presenter;

import android.content.Context;

public interface PulsaDataIndosatIDUAPresenterInterface {
    void onRequesttransferpulsadataIndosatIDUA(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaDataIndosatIDUA(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
