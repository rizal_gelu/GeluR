package com.example.gelur.module.pulsadatatelkomselTDM.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTelkomselTDMInterface {
    void TransferPulsaDataTelkomselTDMSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTelkomselTDMGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadListPricePulsaDataTelkomselTDM(ResponsNonTransaksi responsNonTransaksi);

    void createNewPopUpDialog(String id);
}
