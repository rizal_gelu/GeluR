package com.example.gelur.module.pulsadataindosatIDUA.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadataindosatIDU.presenter.listener.OnLoadPulsaDataIndosatIDU;
import com.example.gelur.module.pulsadataindosatIDU.rest.PulsaDataIndosatIDURestClient;
import com.example.gelur.module.pulsadataindosatIDUA.presenter.listener.OnLoadPulsaDataIndosatIDUA;
import com.example.gelur.module.pulsadataindosatIDUA.rest.PulsaDataIndosatIDUARestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataIndosatIDUAModel implements PulsaDataIndosatIDUAModelInterface {
    @Override
    public void RequestTransferDataIndosatIDUA(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDUA listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataIndosatIDUARestClient.get().req_transfer_pulsa_data_indosat_idua(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataIndosatIDUASukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataIndosatIDUAGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataIndosatIDUA(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDUA listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataIndosatIDUARestClient.get().getPriceListPulsaDataIndosatIDUA(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataIndosatIDUASukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataIndosatIDUAGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataIndosatIDUAGagal(t.getMessage());
            }
        });
    }
}
