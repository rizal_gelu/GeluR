package com.example.gelur.module.pulsaregulertri.presenter;

import android.content.Context;

public interface PulsaRegulerTriPresenterInterface {
    void onRequesttransferpulsaregulerTri(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password);
    void onRequetListPricePulsaRegulerTri(Context context, String req, String kodereseller, String perintah, String time, String pin, String password);
}
