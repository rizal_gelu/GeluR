package com.example.gelur.module.contactinfo.presenter;

public interface ContactInfoPresenterInterface {
    void addPhone();
    void addAddress();
    void addSocialLink();
    void addWebsite();
}
