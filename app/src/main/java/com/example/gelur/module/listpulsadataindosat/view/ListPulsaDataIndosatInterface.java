package com.example.gelur.module.listpulsadataindosat.view;

public interface ListPulsaDataIndosatInterface {
    void onClickIDY();
    void onClickIDF();
    void onClickIDFN();
    void onClickIDFC();
    void onClickIDM();
    void onClickIDP();
    void onClickIDT();
    void onClickIDU();
    void onClickIDUA();
}
