package com.example.gelur.module.pulsadatatriTRDDP.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.pulsadatatriTRD.presenter.listener.OnLoadPulsaDataTriTRD;
import com.example.gelur.module.pulsadatatriTRD.rest.PulsaDataTriTRDRestClient;
import com.example.gelur.module.pulsadatatriTRDDP.presenter.listener.OnLoadPulsaDataTriTRDDP;
import com.example.gelur.module.pulsadatatriTRDDP.rest.PulsaDataTriTRDDPRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.RequestTransaksiPulsa;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PulsaDataTriTRDDPModel implements PulsaDataTriTRDDPModelInterface {
    @Override
    public void RequestTransferPulsaDataTriTRDDP(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataTriTRDDP listener) {
        Call<ResponsTransaksiPulsa> tiket = PulsaDataTriTRDDPRestClient.get().req_transfer_pulsa_data_tri_trd(new RequestTransaksiPulsa(req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password));
        tiket.enqueue(new Callback<ResponsTransaksiPulsa>() {
            @Override
            public void onResponse(Call<ResponsTransaksiPulsa> call, Response<ResponsTransaksiPulsa> response) {
                if (response.isSuccessful()) {
                    listener.RequestTransferPulsaDataTriTRDDPSukses(response.body());
                } else {
                    listener.RequestTransferPulsaDataTriTRDDPGagal(response.body());
                }
            }

            @Override
            public void onFailure(Call<ResponsTransaksiPulsa> call, Throwable t) {
            }
        });
    }

    @Override
    public void RequestListPricePulsaDataTriTRDDP(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataTriTRDDP listener) {
        Call<ResponsNonTransaksi> sal = PulsaDataTriTRDRestClient.get().getPriceListPulsaDataTriTRD(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        sal.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.ResponseListPricePulsaDataTriTRDDPSukses(response.body());
                } else {
                    listener.ResponseListPricePulsaDataTriTRDDPGagal(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
                listener.ResponseListPricePulsaDataTriTRDDPGagal(t.getMessage());
            }
        });
    }
}
