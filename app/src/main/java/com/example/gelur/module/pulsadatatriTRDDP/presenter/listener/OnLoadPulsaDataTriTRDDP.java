package com.example.gelur.module.pulsadatatriTRDDP.presenter.listener;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface OnLoadPulsaDataTriTRDDP {
    void RequestTransferPulsaDataTriTRDDPSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void RequestTransferPulsaDataTriTRDDPGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void OnClickPulsaDataTriTRDDP(String data);
    void ResponseListPricePulsaDataTriTRDDPSukses(ResponsNonTransaksi responsNonTransaksi);
    void ResponseListPricePulsaDataTriTRDDPGagal(String m);
}
