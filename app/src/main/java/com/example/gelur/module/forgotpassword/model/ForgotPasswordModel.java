package com.example.gelur.module.forgotpassword.model;

import com.example.gelur.module.forgotpassword.presenter.listener.OnForgotPasswordFinishedListener;
import com.example.gelur.module.forgotpassword.rest.ForgotPasswordRestClient;
import com.example.gelur.pojo.ForgotPasswordPojo;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordModel implements ForgotPasswordModelInterface {
    @Override
    public void forgotPasswordRequest(String email, final OnForgotPasswordFinishedListener listener) {

        Call<ForgotPasswordPojo> forgotpasswordProses = ForgotPasswordRestClient.get().forgotPasswordProses(email);

        forgotpasswordProses.enqueue(new Callback<ForgotPasswordPojo>() {
            @Override
            public void onResponse(Call<ForgotPasswordPojo> call, Response<ForgotPasswordPojo> response) {
                listener.onForgotPasswordRequestFinish("Please check your email for reset your password");
            }

            @Override
            public void onFailure(Call<ForgotPasswordPojo> call, Throwable t) {
                listener.onForgotPasswordRequestFinishFail("Sorry, your email is not registered");
            }
        });
    }
}