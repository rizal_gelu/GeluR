package com.example.gelur.module.gameff.model;

import android.content.Context;

import com.example.gelur.module.ecommerceshopee.presenter.listener.OnLoadShopeePay;
import com.example.gelur.module.gameff.presenter.listener.OnLoadGameFF;
import com.example.gelur.module.gameml.presenter.listener.OnLoadGameML;

public interface GameFFModelInterface {
    void RequestTransferDiamondFF(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadGameFF listener);
    void RequestListPriceGameFF(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadGameFF listener);
}
