package com.example.gelur.module.pulsadatatriTRDUL.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDULInterface {

    void TransferPulsaDataTriTRDULSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDULGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDULSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDULGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
