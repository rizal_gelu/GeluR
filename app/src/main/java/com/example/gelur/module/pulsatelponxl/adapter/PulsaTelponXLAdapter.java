package com.example.gelur.module.pulsatelponxl.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gelur.R;
import com.example.gelur.module.pulsatelpontri.presenter.listener.OnLoadPulsaTelponTri;
import com.example.gelur.module.pulsatelponxl.presenter.listener.OnLoadPulsaTelponXL;

import java.util.List;

public class PulsaTelponXLAdapter extends RecyclerView.Adapter<PulsaTelponXLAdapter.ViewHolderContent> {
    private Activity activity;
    private OnLoadPulsaTelponXL listener;
    List<String> stringList;

    public PulsaTelponXLAdapter(Activity activity, List<String> datum, OnLoadPulsaTelponXL listener) {
        this.activity = activity;
        this.stringList = datum;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolderContent onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pulsa_telpon_xl, parent, false);
        return new ViewHolderContent(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderContent holder, int position) {
        String data = stringList.get(position);

        holder.pulsa_telpon_xl.setText(data);

        holder.ll_kumpulan_option_pulsa_telpon_xl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.OnClickPulsaTelponXL(data);
            }
        });
    }

    @Override
    public int getItemCount() {
        return stringList.size();
    }

    public class ViewHolderContent extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout ll_kumpulan_option_pulsa_telpon_xl;
        public final TextView pulsa_telpon_xl;

        public ViewHolderContent(View view) {
            super(view);
            mView = view;
            ll_kumpulan_option_pulsa_telpon_xl = (LinearLayout) view.findViewById(R.id.ll_kumpulan_option_pulsa_telpon_xl);
            pulsa_telpon_xl = (TextView) view.findViewById(R.id.pulsa_telpon_xl);
        }
    }
}
