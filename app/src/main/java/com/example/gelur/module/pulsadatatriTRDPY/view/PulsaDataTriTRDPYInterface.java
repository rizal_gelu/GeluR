package com.example.gelur.module.pulsadatatriTRDPY.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataTriTRDPYInterface {

    void TransferPulsaDataTriTRDPYSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataTriTRDPYGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaTriTRDPYSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaTriTRDPYGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
