package com.example.gelur.module.loginpulsa.model;

import android.content.Context;

import com.example.gelur.R;
import com.example.gelur.module.loginpulsa.presenter.listener.OnCekLogin;
import com.example.gelur.module.transferpulsa.rest.TransferPulsaRestClient;
import com.example.gelur.pojo.RequestNonTransaksi;
import com.example.gelur.pojo.ResponsNonTransaksi;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPulsaModel implements LoginPulsaModelInterface{
    @Override
    public void LoginSubmit(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnCekLogin listener) {
        retrofit2.Call<ResponsNonTransaksi> cek = TransferPulsaRestClient.get().getSaldo(new RequestNonTransaksi(req, kodereseller, cmd, time, pin, password));
        cek.enqueue(new Callback<ResponsNonTransaksi>() {
            @Override
            public void onResponse(retrofit2.Call<ResponsNonTransaksi> call, Response<ResponsNonTransaksi> response) {
                if (response.isSuccessful()) {
                    listener.LoginSuksesCek(response.body());
                } else {
                    listener.LoginGagalCek(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<ResponsNonTransaksi> call, Throwable t) {
            }
        });
    }
}
