package com.example.gelur.module.pulsadataXLPXD.presenter;

import android.content.Context;

import com.example.gelur.module.pulsadataXLPXD.model.PulsaDataXLPXDModel;
import com.example.gelur.module.pulsadataXLPXD.presenter.listener.OnLoadPulsaDataXLPXD;
import com.example.gelur.module.pulsadataXLPXD.view.PulsaDataXLPXDInterface;
import com.example.gelur.module.pulsadataxlXML.model.PulsaDataXLXMLModel;
import com.example.gelur.module.pulsadataxlXML.view.PulsaDataXLXMLInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class PulsaDataXLPXDPresenter implements PulsaDataXLPXDPresenterInterface, OnLoadPulsaDataXLPXD {

    private WeakReference<PulsaDataXLPXDInterface> view;
    private PulsaDataXLPXDModel model;

    public PulsaDataXLPXDPresenter(PulsaDataXLPXDInterface view) {
        this.view = new WeakReference<PulsaDataXLPXDInterface>(view);
        this.model = new PulsaDataXLPXDModel();
    }

    @Override
    public void onRequesttransferpulsaDataXLPXD(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferPulsaDataXLPXD(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPricePulsaDataXLPXD(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPricePulsaDataXLPXD(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferPulsaDataXLPXDSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLPXDSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferPulsaDataXLPXDGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferPulsaDataXLPXDGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickPulsaDataXLPXD(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPricePulsaDataXLPXDSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadPricePulsaXLPXDSukses(responsNonTransaksi);
    }

    @Override
    public void ResponseListPricePulsaDataXLPXDGagal(String m) {

    }
}
