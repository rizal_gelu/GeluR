package com.example.gelur.module.pulsatelpontelkomsel.model;

import android.content.Context;

import com.example.gelur.module.pulsadatatelkomselTDB.presenter.listener.OnLoadPulsaDataTelkomselTDB;
import com.example.gelur.module.pulsaregulertelkomsel.presenter.listener.OnLoadPulsaRegulerTelkomsel;
import com.example.gelur.module.pulsatelpontelkomsel.presenter.listener.OnLoadPulsaTelponTelkomsel;

public interface PulsaTelponTelkomselModelInterface {
    void RequestTransferPulsaTelponTelkomsel(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaTelponTelkomsel listener);
    void RequestListPricePulsaDataTelkomselTDB(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaTelponTelkomsel listener);
}
