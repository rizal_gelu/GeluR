package com.example.gelur.module.login.model;

import android.content.Context;

import com.example.gelur.module.forgotpassword.presenter.listener.OnForgotPasswordFinishedListener;
import com.example.gelur.module.login.presenter.listener.OnLoginFinishedListener;
import com.example.gelur.module.login.rest.LoginRestClient;
import com.example.gelur.pojo.FormaterResponse;
import com.example.gelur.pojo.UsersPojo;
import com.example.gelur.R;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginModel implements LoginModelInterface{
    @Override
    public void checkUser(final Context context, String email, String password, final OnLoginFinishedListener listener) {

        Call<FormaterResponse> doLogin = LoginRestClient.get().doLogin(email, password);
        doLogin.enqueue(new Callback<FormaterResponse>() {
            @Override
            public void onResponse(Call<FormaterResponse> call, Response<FormaterResponse> response) {
                if (response.isSuccessful()) {
                    FormaterResponse resource = response.body();
                    List<UsersPojo> user = resource.getData_profile();
                    listener.onLoginSuccess(user);
                } else {
                    listener.onLoginFailed(context.getString(R.string.login_failed_message));
                }
            }

            @Override
            public void onFailure(Call<FormaterResponse> call, Throwable t) {
                listener.onLoginFailed(t.getMessage());
            }
        });
    }
}
