package com.example.gelur.module.pulsadataindosatIDFC.model;

import android.content.Context;

import com.example.gelur.module.pulsadataindosatIDFC.presenter.listener.OnLoadPulsaDataIndosatIDFC;

public interface PulsaDataIndosatIDFCModelInterface {
    void RequestTransferDataIndosatIDFC(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password, OnLoadPulsaDataIndosatIDFC listener);
    void RequestListPricePulsaDataIndosatIDFC(Context context, String req, String kodereseller, String cmd, String time, String pin, String password, OnLoadPulsaDataIndosatIDFC listener);
}
