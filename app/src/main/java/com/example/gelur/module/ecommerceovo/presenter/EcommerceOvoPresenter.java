package com.example.gelur.module.ecommerceovo.presenter;

import android.content.Context;

import com.example.gelur.module.ecommerceovo.model.EcommerceOvoModel;
import com.example.gelur.module.ecommerceovo.presenter.listener.OnLoadEcommerceOvo;
import com.example.gelur.module.ecommerceovo.view.EcommerceOvoInterface;
import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

import java.lang.ref.WeakReference;

public class EcommerceOvoPresenter implements EcommerceOvoPresenterInterface, OnLoadEcommerceOvo {

    private WeakReference<EcommerceOvoInterface> view;
    private EcommerceOvoModel model;

    public EcommerceOvoPresenter(EcommerceOvoInterface view) {
        this.view = new WeakReference<EcommerceOvoInterface>(view);
        this.model = new EcommerceOvoModel();
    }

    @Override
    public void onRequesttransfersaldoOvo(Context context, String req, String kodereseller, String produk, String msisdn, String counter, String qty, String reffid, String time, String pin, String password) {
        model.RequestTransferSaldoOvo(context, req, kodereseller, produk, msisdn, counter, qty, reffid, time, pin, password, this);
    }

    @Override
    public void onRequetListPriceOvo(Context context, String req, String kodereseller, String perintah, String time, String pin, String password) {
        model.RequestListPriceOvo(context, req, kodereseller, perintah, time, pin, password, this);
    }

    @Override
    public void RequestTransferSaldoOvoSukses(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoDanaSukses(responsTransaksiPulsa);
    }

    @Override
    public void RequestTransferSaldoOvoGagal(ResponsTransaksiPulsa responsTransaksiPulsa) {
        if (view.get() != null) view.get().TransferSaldoDanaGagal(responsTransaksiPulsa);
    }

    @Override
    public void OnClickOvo(String data) {
        String[] split_data = data.split("\\s+");
        String last_char = split_data[split_data.length-1];

        String[] get_id = last_char.split("=");
        String id = get_id[0];
        createNewPopUpDialog(id);
    }

    @Override
    public void ResponseListPriceOvoSukses(ResponsNonTransaksi responsNonTransaksi) {
        if (view.get() != null) view.get().LoadListPriceOvo(responsNonTransaksi);
    }

    @Override
    public void ResponseListPriceOvoGagal(String m) {

    }

    private void createNewPopUpDialog(String id) {
        if (view.get() != null) view.get().createNewPopUpDialog(id);
    }
}
