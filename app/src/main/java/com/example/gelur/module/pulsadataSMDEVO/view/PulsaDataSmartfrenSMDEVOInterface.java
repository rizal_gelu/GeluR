package com.example.gelur.module.pulsadataSMDEVO.view;

import com.example.gelur.pojo.ResponsNonTransaksi;
import com.example.gelur.pojo.ResponsTransaksiPulsa;

public interface PulsaDataSmartfrenSMDEVOInterface {
    void TransferPulsaDataSmartfrenSMDEVOSukses(ResponsTransaksiPulsa responsTransaksiPulsa);
    void TransferPulsaDataSmartfrenSMDEVOGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void LoadPricePulsaDataSmartfrenSMDEVOSukses(ResponsNonTransaksi responsNonTransaksi);
    void LoadPricePulsaDataSmartfrenSMDEVOGagal(ResponsTransaksiPulsa responsTransaksiPulsa);

    void createNewPopUpDialog(String data);
}
