package com.example.gelur.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.text.TextUtils;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;

import com.example.gelur.module.login.view.Login;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.HashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class AppsUtil {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    public AppsUtil(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(AppsConstant.PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static AlertDialog showLoadingDialog(Context context, int theme, int layoutLoading) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context, theme);
        builder.setView(layoutLoading);
        builder.setCancelable(false);
        return builder.create();
    }

    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isFirstTime(Context context, String key, boolean value) {
        SharedPreferences mSharedPref = null;
        mSharedPref.getBoolean(key, value);
        return true;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(inputMethodManager != null) {
            if(activity.getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    public static void setFullScreen(Activity activity) {
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static String md5(final String password) {
        try {
            MessageDigest digest = MessageDigest
                    .getInstance("MD5");
            digest.update(password.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String sha1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        sha1hash = md.digest();
        return toHexString(sha1hash);
    }

    private static String calculateRFC2104HMAC(String data, String key) throws SignatureException,
            NoSuchAlgorithmException, InvalidKeyException {

        SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(signingKey);
        return toHexString(mac.doFinal(data.getBytes()));
    }

    private static String toHexString(byte[] bytes) {
        Formatter formatter = new Formatter();

        for (byte b : bytes) {
            formatter.format("%02x", b);
        }

        String ret = formatter.toString();
        formatter.close();

        return ret;
    }

    /**
     * Create login session
     * */
    public void createLoginSession(String name, String email, String password){
        // Storing login value as TRUE
        editor.putBoolean(AppsConstant.IS_LOGIN, true);

        // Storing name in pref
        editor.putString(AppsConstant.KEY_NAME, name);

        // Storing email in pref
        editor.putString(AppsConstant.KEY_ID_USER, email);
//
//        // Storing first name
//        editor.putString(AppsConstant.KEY_FIRST_NAME, first_name);
//
//        // Storing last name
//        editor.putString(AppsConstant.KEY_LAST_NAME, last_name);
//
//        // Storing status
//        editor.putInt(AppsConstant.KEY_STATUS, status);
//
//        // Storing avatar
//        editor.putString(AppsConstant.KEY_AVATAR, avatar);

        editor.putString(AppsConstant.KEY_PASSWORD, password);

        // commit changes
        editor.commit();
    }

    public void createLoginPulsaSession(String kodereseller, String pin, String password) {
        // Storing login value as TRUE
        editor.putBoolean(AppsConstant.IS_LOGIN, true);

        // Storing name in pref
        editor.putString(AppsConstant.KODERESELLER, kodereseller);

        // Storing email in pref
        editor.putString(AppsConstant.KEY_PIN_PULSA, pin);

        editor.putString(AppsConstant.KEY_PASSWORD_PULSA, password);
        editor.commit();
    }

    public void createRegisterSession(String name, String email) {
        editor.putString(AppsConstant.KEY_ID_USER, name);
        editor.putString(AppsConstant.KEY_ID_USER, email);
        editor.commit();
    }

    /**
     * Create Forgot Password Session
     **/
    public void createForgotPasswordSession(String prefName, String email, String forgot_password_code) {
        // Storing name Apps
        editor.putString(AppsConstant.PREF_NAME, prefName);

        // String email
        editor.putString(AppsConstant.KEY_ID_USER, email);

        // Storing forgot password code
        editor.putString(AppsConstant.FORGOT_PASSWORD_CODE, forgot_password_code);

        // comit changes
        editor.commit();
    }

    /**
     * Get stored user forgoted password
     **/
    public  HashMap<String, String> getForgotPasswordCodeFormSession() {
        HashMap<String, String> forgot_password_code = new HashMap<>();

        forgot_password_code.put(AppsConstant.PREF_NAME, pref.getString(AppsConstant.PREF_NAME, null));

        forgot_password_code.put(AppsConstant.KEY_ID_USER, pref.getString(AppsConstant.KEY_ID_USER, null));

        forgot_password_code.put(AppsConstant.FORGOT_PASSWORD_CODE, pref.getString(AppsConstant.FORGOT_PASSWORD_CODE, null));

        return forgot_password_code;
    }

    public boolean checkUserLoginSession(){
        if(pref.getBoolean(AppsConstant.IS_LOGIN, true)) {
            return true;
        } else {
            return false;
        }
    }

    public void logoutUserSession() {
        editor.putString(AppsConstant.KODERESELLER, "").commit();
        editor.putString(AppsConstant.KEY_ID_USER, "").commit();
        editor.putString(AppsConstant.KEY_PIN_PULSA, "").commit();
        editor.putString(AppsConstant.KEY_PASSWORD_PULSA, "").commit();
        editor.clear();
        editor.commit();
    }

    /**
     * Get stored user login session data
     * */
    public HashMap<String, String> getUserDetailsFromSession(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(AppsConstant.KEY_NAME, pref.getString(AppsConstant.KEY_NAME, null));

        // user email id
        user.put(AppsConstant.KEY_ID_USER, pref.getString(AppsConstant.KEY_ID_USER, null));

        user.put(AppsConstant.KEY_TOKEN, pref.getString(AppsConstant.KEY_TOKEN, null));

        user.put(AppsConstant.KEY_EMPLOYEE_ID, pref.getString(AppsConstant.KEY_EMPLOYEE_ID, null));

        user.put(AppsConstant.KEY_COMPANY_ID, pref.getString(AppsConstant.KEY_COMPANY_ID, null));

        // return user
        return user;
    }

    public HashMap<String, String> getUserPulsaDetailsFromSession(){
        HashMap<String, String> user = new HashMap<String, String>();
        // user name
        user.put(AppsConstant.KEY_NAME, pref.getString(AppsConstant.KEY_NAME, null));

        // user email id
        user.put(AppsConstant.KODERESELLER, pref.getString(AppsConstant.KODERESELLER, null));

        user.put(AppsConstant.KEY_PIN_PULSA, pref.getString(AppsConstant.KEY_PIN_PULSA, null));

        user.put(AppsConstant.KEY_PASSWORD_PULSA, pref.getString(AppsConstant.KEY_PASSWORD_PULSA, null));

        // return user
        return user;
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(AppsConstant.IS_LOGIN, false);
    }

    public void removeSession() {
        editor.putString(AppsConstant.KODERESELLER, "").commit();
        editor.putString(AppsConstant.KEY_ID_USER, "").commit();
        editor.clear();
        editor.commit();
    }

    /*
    File
     */

    public void writeFileOnInternalStorage(Context mcoContext, String sFileName, String sBody){
        File dir = new File(mcoContext.getFilesDir(), "static/timeline_file");
        if(!dir.exists()){
            dir.mkdir();
        }

        try {
            File gpxfile = new File(dir, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
