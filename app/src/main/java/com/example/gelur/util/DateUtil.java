package com.example.gelur.util;

import java.util.Date;

public class DateUtil {
    public static String SelisihDateTime(long waktuSatu, Date waktuDua){

        Date convertMilsecToRealTime = new Date(waktuSatu);

        long selisihMS      = Math.abs(convertMilsecToRealTime.getTime() - waktuDua.getTime());
        long selisihDetik   = selisihMS / 1000 % 60;
        long selisihMenit   = selisihMS / (60 * 1000) % 60;
        long selisihJam     = selisihMS / (60 * 60 * 1000) % 24;
        long selisihHari    = selisihMS / (24 * 60 * 60 * 1000);

        if (selisihHari != 0) {
            String HariSelisih =  selisihHari + " Hari " + selisihJam + " Jam ";
            return HariSelisih;
        } else if(selisihJam != 0) {
            String JamSelisih =  selisihJam + " Jam " + selisihMenit + " Menit ";
            return JamSelisih;
        } else if(selisihMenit != 0) {
            String MenitSelisih = selisihMenit + " Menit ";
            return MenitSelisih;
        } else if(selisihDetik != 0) {
            String DetikSelisih = selisihDetik + " Detik ";
            return DetikSelisih;
        }
        return null;
    }
}
