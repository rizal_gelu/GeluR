package com.example.gelur.util;

import java.io.File;

public class AppsConstant {
//    public static final String BASE_API_URL = "http://192.168.1.2:8080/";
//    public static final String BASE_IMAGE_PROFILE_URL = "http://192.168.1.2:8080/static/photo_profile/";
//    public static final String BASE_TIMELINE_FILE_URL = "http://192.168.1.2:8080/static/timeline_file/";

    //Go Online
    public static final String BASE_API_URL = "http://gelurpulsa.ddns.net:8080/";
    public static final String BASE_IMAGE_PROFILE_URL = "http://gelurpulsa.ddns.net:8080/static/photo_profile/";
    public static final String BASE_TIMELINE_FILE_URL = "http://gelurpulsa.ddns.net:8080/static/timeline_file/";

    // Server Pulsa
    public static final String BASE_API_TRANSAKSI = "http://gelurpulsa.ddns.net:8000/transaksi";
    public static final String BASE_API_NON_TRANSAKSI = "http://gelurpulsa.ddns.net:8000/";

    // Directory File Timeline
    public static final String DIRECTORY_TIMELINE_FILE = "static/timeline_file";

    public static final String KEY_ID_USER = "EMAIL";
    public static final String KEY_PASSWORD = "PASSWORD";
    public static final String KEY_FIRST_NAME = "FIRST_NAME";
    public static final String KEY_LAST_NAME = "LAST_NAME";
    public static final String KEY_AVATAR = "AVATAR";
    public static final String KEY_STATUS = "STATUS";

    // SESSION PULSA
    public static final String KEY_PASSWORD_PULSA = "PASSWORD_PULSA";
    public static final String KEY_PIN_PULSA = "PIN_PULSA";
    public static final String KODERESELLER = "KODERESELLER";

    public static final String FORGOT_PASSWORD_CODE = "FORGOT_PASSWORD_CODE";

    // Company Employee
    public static final String KEY_EMPLOYEE_ID = "EMPLOYEE_ID";
    public static final String KEY_COMPANY_ID ="COMPANY_ID";

    // Shared pref mode
    public static int PRIVATE_MODE = 0;


    // Sharedpref file name
    public static final String PREF_NAME = "GeluR";
    public static final String KEY_TOKEN = "token";
    public static final String SESSION_KEY = "session_key";

    // All Shared Preferences Keys
    public static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";
}
