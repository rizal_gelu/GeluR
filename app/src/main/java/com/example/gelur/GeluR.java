package com.example.gelur;

import android.app.Application;

import com.github.silvestrpredko.dotprogressbar.BuildConfig;

import timber.log.Timber;

public class GeluR extends Application {
    private GeluR myApp;

    public GeluR getInstance() {
        return myApp;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myApp = this;

        if(BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}